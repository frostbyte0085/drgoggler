#version 120

uniform sampler2D diffuseTexture;


varying vec4 lightDiffuse, lightAmbient;
varying vec3 normal, lightDir, halfVector;

varying vec3 vertexPosition;

varying vec4 varBlobShadowColor;
varying vec3 varBlobShadowCenter;
varying float varBlobShadowRadius;
varying float varBlobShadowDistance;
varying float varBlobShadowMaxDistance;

vec4 doLight() {
    vec3 n, halfV;
    float NdotL, NdotHV;
    
    vec4 color = lightAmbient;
    
    n = normalize(normal);
    
    NdotL = max(dot(n, lightDir), 0.0);
    
    if (NdotL > 0.0f) {
        halfV = normalize(halfVector);
        
        NdotHV = max(dot(n, halfV), 0.0);
        
        color += gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV, gl_FrontMaterial.shininess);
        color += lightDiffuse * NdotL;
    }
    
    return color;
}

vec4 doShadow() {
    vec4 color = vec4(1,1,1,1);
    
    float maxDistance2 = pow(varBlobShadowMaxDistance, 2);
    
    float distance2 = pow (varBlobShadowCenter.x - vertexPosition.x, 2) + pow(varBlobShadowCenter.z - vertexPosition.z, 2);
    float distanceNormalized = distance2 / maxDistance2;
    
    float radius2 = pow(varBlobShadowRadius, 2);
    
    if (distance2 <= radius2) {
        
        color = varBlobShadowColor;
        
        if (distance2 > radius2/20) {
            color.rgb += pow(distanceNormalized,2);
            color.rgb = clamp(color.rgb, 0.0, 1.0);
        }
    }
    
    return color;
}

void main(void)
{    
    vec4 litColor = doLight();
    vec4 shadowedColor = doShadow();
    
    vec4 color = texture2D (diffuseTexture, gl_TexCoord[0].xy);
	vec4 lightColor = color * litColor;
	vec4 shadowColor = color * litColor * shadowedColor;
    
    vec4 finalColor = (1.0 - shadowedColor.a) * lightColor + shadowedColor.a * shadowColor;
    
	gl_FragColor = finalColor;
}