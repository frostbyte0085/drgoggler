#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;


uniform vec4 blobShadowColor;
uniform vec3 blobShadowCenter;
uniform float blobShadowRadius;
uniform float blobShadowDistance;
uniform float blobShadowMaxDistance;

varying vec4 lightDiffuse, lightAmbient;
varying vec3 lightDir, normal, halfVector;

varying vec3 vertexPosition;


varying vec4 varBlobShadowColor;
varying vec3 varBlobShadowCenter;
varying float varBlobShadowRadius;
varying float varBlobShadowDistance;
varying float varBlobShadowMaxDistance;

void doLight(mat4 pmv) {
    mat3 normalMatrix = mat3(modelMatrix * viewMatrix);
    normalMatrix = -transpose(normalMatrix);
    normal = normalMatrix * gl_Normal.xyz;
        
    lightDir = normalize (gl_LightSource[0].position.xyz);
    
    halfVector = normalize(gl_LightSource[0].halfVector.xyz);
    
    lightDiffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
    lightAmbient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
    lightAmbient += gl_LightModel.ambient * gl_FrontMaterial.ambient;
}

void main()
{
    mat4 pmv = projectionMatrix * viewMatrix * modelMatrix;
    gl_TexCoord[0] = gl_MultiTexCoord0;
	
    gl_Position = pmv * gl_Vertex;
    vec4 vertexWorldSpace = modelMatrix * gl_Vertex;
    vertexPosition = vec3(vertexWorldSpace.xyz);
    
    varBlobShadowColor = blobShadowColor;
    varBlobShadowCenter = blobShadowCenter;
    varBlobShadowRadius = blobShadowRadius;
    varBlobShadowDistance = blobShadowDistance;
    varBlobShadowMaxDistance = blobShadowMaxDistance;
    
    doLight(pmv);
}
