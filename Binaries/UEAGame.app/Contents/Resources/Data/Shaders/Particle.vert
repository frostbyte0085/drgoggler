#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 invViewMatrix;

void main()
{
    gl_TexCoord[0] = gl_MultiTexCoord0;
    
    gl_FrontColor = gl_Color;
    
    vec2 extend = gl_TexCoord[0].xy * 2 - vec2(1,1);
    
    float size = gl_MultiTexCoord1.x;
    
    vec4 pos = modelMatrix * gl_Vertex;
    
    pos.xyz += extend.x * invViewMatrix[0].xyz * size;
    pos.xyz += extend.y * invViewMatrix[1].xyz * size;
    
    gl_Position = projectionMatrix * viewMatrix * pos;
}
