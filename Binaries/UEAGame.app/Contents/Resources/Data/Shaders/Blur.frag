#version 120

uniform sampler2D framebufferTexture;
uniform float rtWidth; // render target width
uniform float rtHeight; // render target height

vec4 fetchTextureBorder (vec2 uv) {
    if (uv.x < 0) uv.x = 0;
	if (uv.y < 0) uv.y = 0;
	if (uv.x > 1) uv.x = 1;
	if (uv.y > 1) uv.y = 1;
        
    return texture2D(framebufferTexture, uv);
}

vec4 boxBlur (vec2 texelSize, vec2 uv) {

    vec2 texCoord = uv - 2.0 * texelSize;
    
    vec4 result = vec4(0,0,0,0);
    
    for (int i=0; i<5; i++) {
        texCoord.x = uv.x - 2.0 * texelSize.x;
        
        for (int j=0; j<5; j++) {
            result += texture2D(framebufferTexture, texCoord);
            texCoord.x += texelSize.x;
        }
        texCoord.y += texelSize.y;
    }
    
    return result / 25;
	
}

void main(void) {
    vec2 uv = gl_TexCoord[0].xy;
        
    //vec4 color = texture2D(framebufferTexture, uv);
    
    vec2 texelSize = vec2(1.0 / rtWidth, 1.0 / rtHeight);
    vec4 color = boxBlur (texelSize, uv * 4);
    
    gl_FragColor = color;
}