#version 120

uniform sampler2D framebufferTexture;
uniform float rtWidth; // render target width
uniform float rtHeight; // render target height

vec4 boxBlur (vec2 texelSize, vec2 uv) {

    vec2 texCoord = uv;
    
    vec4 result = vec4(0,0,0,0);
        
    for (int i=0; i<4; i++) {
        texCoord.x = uv.x;
        for (int j=0; j<4; j++) {
            result += texture2D(framebufferTexture, texCoord);
            texCoord.x += texelSize.x;
        }
        texCoord.y += texelSize.y;
    }
    
    return result / 16;
}

void main(void) {
    // ugly hack for ugly bug in PostProcessRenderer. :)
    vec2 uv = gl_TexCoord[0].xy * 4;
    vec4 color = texture2D(framebufferTexture, uv);
    
    vec2 texelSize = vec2(1.0 / rtWidth, 1.0 / rtHeight);
    color = boxBlur (texelSize, uv);
    
    gl_FragColor = color * (1-color.a);
}