uniform samplerCube skyTexture;

void main(void)
{
	vec4 cube = textureCube(skyTexture, normalize(gl_TexCoord[0].xyz));
    cube.a = 1.0;
    
	gl_FragColor = cube;
}