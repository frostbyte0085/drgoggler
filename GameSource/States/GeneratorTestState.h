#ifndef GENERATOR_TEST_STATE_H_DEF
#define GENERATOR_TEST_STATE_H_DEF

#include <GameApplication.h>

class SceneNode;
class Hud;
class PauseMenu;
class PlayerController;

class GeneratorTestState : public State {
public:
    GeneratorTestState();
    ~GeneratorTestState();
    
    void Enter();
    void Exit();
    bool Update();
    
private:
    UILayer *ui;
	Texture *face0;
    Hud *hud;
    
    PauseMenu *pauseMenu;
    bool paused;
    bool gameOver;
    void TogglePause();
    
    PlayerController *player;
};

#endif
