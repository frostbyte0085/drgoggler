#include "AnimationTestState.h"
#include "Behaviours/PlatformBehaviour.h"
#include "Behaviours/PlayerController.h"
#include <GameApplication.h>

#include <Objects/SharedResources.h>

AnimationTestState::AnimationTestState() {
    
}

AnimationTestState::~AnimationTestState() {
    
}

void AnimationTestState::Enter() {
    // setup the scene camera
    SceneNode *cameraNode = Scene::GetSceneNode( Scene::AddSceneNode("camera") );
    cameraNode->camera = new Camera();
    cameraNode->camera->SetupCamera(45.0f, 
                                    (float)GraphicsContext::GetCurrentDisplayMode().Width / GraphicsContext::GetCurrentDisplayMode().Height,
                                    0.05f, 10000.0f);
    

    cameraNode->SetTranslation(0,300,1200);
    cameraNode->SetRotation(-30,0,0);
    
    Scene::SetActiveCamera(cameraNode->GetID());
    
    /*
    smallExplosionNode = Scene::GetSceneNode (Scene::AddSceneNode ("smallExplosion"));
    smallExplosionNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/SmallExplosion.xml", false))->ParticleObject);

    smokeExplosionNode = Scene::GetSceneNode (Scene::AddSceneNode ("smokeExplosion"));
    smokeExplosionNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/SmokeExplosion.xml", false))->ParticleObject);
    
    bigExplosionNode = Scene::GetSceneNode (Scene::AddSceneNode ("bigExplosion"));
    bigExplosionNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/BigExplosion.xml", false))->ParticleObject);


    shockwaveNode = Scene::GetSceneNode(Scene::AddSceneNode("shockwave"));
    
    Mesh *shockwaveMesh = ResourceManager::Create<MESH>(new MeshResourceDescriptor("shockwave", true))->MeshObject;
    
    shockwaveMesh->MakePlane(100,100);
    shockwaveMesh->GetSubmesh(0)->material = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor("Data/Materials/Shockwave.mat", false))->MaterialObject;
    shockwaveMesh->GetSubmesh(0)->material->SetDiffuseColor(Vector4(1,0.4f,0,1));
    shockwaveNode->SetMesh(shockwaveMesh);
    shockwaveNode->SetScale(0.1f,0.1f,0.1f);
     */

    
    bloodNode = Scene::GetSceneNode (Scene::AddSceneNode ("pickableSparks"));
    bloodNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/SmokeExplosion.xml", false))->ParticleObject);
    
    
    /*
     // USING RESOURCE CACHE
     
    ResourceManager::CacheResources = true;
    
    unsigned int resCount = ResourceManager::GetResourceCount();
    

    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/uglyBastard.jpg"));
    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/ueaLogo.png"));
        ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/Logo.png"));
    
    ResourceManager::Remove (ResourceManager::Get<TextureResource>("Data/UI/ueaLogo.png"));
    resCount = ResourceManager::GetResourceCount();    

    
    ResourceManager::ClearCache();
    
    resCount = ResourceManager::GetResourceCount();

     */
    
    character = Scene::GetSceneNode(Scene::AddSceneNode("char"));
    character->SetMesh(ResourceManager::Create<MESH>(new MeshResourceDescriptor("Data/Models/goggledude/DrGoggler.md5mesh"))->MeshObject);
    character->SetTranslation(0,0,-400);
    character->GetMesh()->Play("run", ANIM_LOOP);
    
        Scene::SetSkyBox(SharedResources::skyboxMaterial);
}

void AnimationTestState::Exit() {
    
    
}

bool emit = false;
float alpha = 1.0f;

bool AnimationTestState::Update() {
    
    if (Input::IsClicked ("Up")) {
        GraphicsContext::SetFastWindowedMode();
    }
    if (Input::IsClicked ("Fire")) {
        /*
        emit = true;
        bigExplosionNode->GetParticleSystem()->Emit();
        smallExplosionNode->GetParticleSystem()->Emit();
        smokeExplosionNode->GetParticleSystem()->Emit();
        
        float randomX = Math::RandomF(-90.0f, 90.0f);
        float randomY = Math::RandomF(-90.0f, 90.0f);
        float randomZ = Math::RandomF(-90.0f, 90.0f);
        shockwaveNode->Rotate(randomX, randomY, randomZ, SPACE_LOCAL);
        */
        bloodNode->GetParticleSystem()->Emit();
    }
    /*
    if (emit) {
        if (alpha <= 0) {
            emit = false;
            shockwaveNode->SetScale(0.1f, 0.1f, 0.1f);
            alpha = 1.0f;
        }
        shockwaveNode->Scale(1+Timing::LastFrameTime*6, 1+Timing::LastFrameTime*6, 1+Timing::LastFrameTime*6);
        Material* mat = shockwaveNode->GetMesh()->GetSubmesh(0)->material;
        alpha -= Timing::LastFrameTime/2;
        mat->SetDiffuseColor(Vector4(1,0.4f,0,alpha));
    }
    //shockwaveNode->Rotate(Timing::LastFrameTime*50, 0, 0, SPACE_LOCAL);
    //shockwaveNode->Scale(Timing::LastFrameTime, Timing::LastFrameTime, 1);
     */
    
    return true;
}