#ifndef INTROSTATE_H_DEF
#define INTROSTATE_H_DEF

#include <GameApplication.h>

class SceneNode;

class IntroState : public State {
public:
    IntroState();
    ~IntroState();
    
    void Enter();
    void Exit();
    bool Update();
    
private:
    float fade;
    float startFade;
    TextureResource *ueaLogo;
    Texture *ueaLogoTexture;
    TextureResource *grey;
    Texture *greyTexture;
};

#endif
