#include "GeneratorTestState.h"
#include "Generator/LevelGenerator.h"
#include "Behaviours/PlayerController.h"

#include <UI/Hud.h>
#include <UI/PauseMenu.h>
#include <GameApplication.h>
#include <Objects/ObjectManager.h>

#include <Objects/SharedResources.h>

GeneratorTestState::GeneratorTestState() {
    pauseMenu = new PauseMenu();
}

GeneratorTestState::~GeneratorTestState() {
    delete pauseMenu;
}

void GeneratorTestState::Enter() {
    
    ResourceManager::CacheResources = true;
    
    GameResources::Initialize();
    
	// setup the skybox
    Scene::SetSkyBox(SharedResources::skyboxMaterial);

	// setup the generator
	LevelGeneratorParam params= ObjectManager::GetDefaultGenerator();
    
	LevelGenerator * generator = new LevelGenerator(params);

	generator->Generate();

	// setup the scene camera
    SceneNode *cameraNode = Scene::GetSceneNode( Scene::AddSceneNode("camera") );
    cameraNode->camera = new Camera();
    cameraNode->camera->SetupCamera(45.0f, 
                                    (float)GraphicsContext::GetCurrentDisplayMode().Width / GraphicsContext::GetCurrentDisplayMode().Height,
                                    0.7f, 10000000.0f);
    
    
    Scene::SetActiveCamera(cameraNode->GetID());
    
    // Get the 1st platform
    Object *platform0 = generator->GetPlatforms()[0];
    PlatformType platType = ObjectManager::GetPlatformTypeByID(platform0->GetObjectTypeID());
    
    // Set up the Animated Character in the Virtual Environment
    
    SceneNode *chr = Scene::GetSceneNode(Scene::AddSceneNode("character",0));
    chr->SetMesh ( ResourceManager::Create<MESH>(new MeshResourceDescriptor("Data/Models/goggledude/DrGoggler.md5mesh"))->MeshObject, CT_BOX);
    chr->SetTranslation(platType.playerSpawn);
	chr->SetRotation(0,0,0);
    chr->SetCollisionGroup(OCG_CHARACTER);
    
    player = new PlayerController(cameraNode);
    chr->AddBehaviour("controller", player);
    MeshAnimation *anim = chr->GetMesh()->GetAnimation("run");
    assert(anim);
    anim->Speed = 3;
    
    anim = chr->GetMesh()->GetAnimation("flytwitch0");
    assert (anim);
    anim->Speed = 2;
    
    // Set up the UI
    this->hud = new Hud(player);
    this->hud->Start();
    
    cameraNode->SetParent(chr->GetID());
    cameraNode->SetTranslation(0,300,320);
    cameraNode->SetRotation(0,0,0);
    
    SceneNode *conkerNode = Scene::GetSceneNodeWithName("conker");
    if (conkerNode) {
        conkerNode->audioSources["chunk"]->Play(true);
    }

    // IMPORTANT BIT
    // SET COLLISION GROUPS
    
    // The character can hit stuff:
    Scene::EnableCollisions(OCG_CHARACTER, OCG_PLATFORM);
    Scene::EnableCollisions(OCG_CHARACTER, OCG_UFO);
    Scene::EnableCollisions(OCG_CHARACTER, OCG_HEALTH);
    Scene::EnableCollisions(OCG_CHARACTER, OCG_FUEL);
    Scene::EnableCollisions(OCG_CHARACTER, OCG_ORB_REFILL);
    Scene::EnableCollisions(OCG_CHARACTER, OCG_MONEY);
    
    // The UFO lasers can hit players and platforms (and other ufos?)
    Scene::EnableCollisions(OCG_UFOLASER, OCG_CHARACTER);
    Scene::EnableCollisions(OCG_UFOLASER, OCG_PLATFORM);
    
    // The player lasers can hit UFOs (duh).
    Scene::EnableCollisions(OCG_PLAYERLASER, OCG_UFO); // TODO
    Scene::EnableCollisions(OCG_PLAYERLASER, OCG_PLATFORM);
    
    Application::GameApp->SetPause(false);
    paused = false;
    gameOver = false;
    Input::ShowMouse(false);
    
    SharedResources::ingameMusic->Play(true);
	delete generator;
    
    player->startedAt = Timing::CurrentTime;
    player->enterFlying();
}

void GeneratorTestState::Exit() {
    
    SharedResources::ingameMusic->Stop();
    Application::GameApp->SetPause(false);
    
    delete this->hud;
    this->hud = nullptr;
    
    Scene::Clear();
    
    ResourceManager::ClearCache();
}

void GeneratorTestState::TogglePause() {
    paused = !paused;
    
    Application::GameApp->SetPause(paused);
    Input::ShowMouse(paused);
    
    AudioSystem::SetPause(paused, SharedResources::ingameMusic);
}

bool GeneratorTestState::Update() {
    
    // Is it Game Over?
    if(!gameOver && (player->IsDead() || player->GotStar())) {
        gameOver = true;
        Application::GameApp->SetPause(true);
        Input::ShowMouse(true);
    }
    
    // Get user input that's not related the character:
    if (Input::IsClicked("Quit") && !gameOver) {
        TogglePause();
    }
    
    this->hud->Update();
    
    // Show the UI Layer
    if(paused) {
        bool stayInPause = this->pauseMenu->Update();
        if(!stayInPause) TogglePause();
    }
    else if(player->GotStar() || player->IsDead()){
        AudioSystem::SetPause(false, SharedResources::ingameMusic);
    }
    
	return true;
}