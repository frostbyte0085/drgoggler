//
//  MainMenuState.cpp
//  UEAGame
//
//  Created by Olivier Legat on 24/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "MainMenuState.h"
#include "UI/MainMenu.h"

#include "States/Behaviours/PlayerController.h"
#include "Objects/ObjectManager.h"
#include "Behaviours/PlatformBehaviour.h"

#include "Objects/SharedResources.h"

MainMenuState::MainMenuState() {
    menu = new MainMenu();
    
    gogglerMesh = ResourceManager::Create<MESH>
    (new MeshResourceDescriptor("Data/Models/goggledude/DrGoggler.md5mesh"))->MeshObject;
    
    ufoMesh = ResourceManager::Create<MESH>
    (new MeshResourceDescriptor("Data/Models/ufo/ufo.md5mesh"))->MeshObject;
    
    platformMesh = ResourceManager::Create<MESH>
    (new MeshResourceDescriptor("Data/Models/platform1/platform1.md5mesh"))->MeshObject;
}
MainMenuState::~MainMenuState() {
    delete menu;
}

void MainMenuState::Enter() {
    ResourceManager::CacheResources = true;
    
    menu->Start();
    
	// setup the skybox
    Scene::SetSkyBox(SharedResources::skyboxMaterial);
    
    
	// setup the scene camera
    cameraNode = Scene::GetSceneNode( Scene::AddSceneNode("camera") );
    cameraNode->camera = new Camera();
    cameraNode->camera->SetupCamera(45.0f, 
                                    (float)GraphicsContext::GetCurrentDisplayMode().Width / GraphicsContext::GetCurrentDisplayMode().Height,
                                    0.1f, 2000.0f);
    cameraNode->SetTranslation(0,100,290);
    cameraNode->SetRotation(0,0,0);
    Scene::SetActiveCamera(cameraNode->GetID());
    
    // Setup the platform
    platform = Scene::GetSceneNode(Scene::AddSceneNode("platform",0));
    platform->SetMesh (platformMesh);
    platform->AddBehaviour("flunct", new PlatformBehaviour(0.05f, false));
    platform->SetTranslation(-140,0,0);
	platform->SetRotation(0,0,0);
    
    // Set up the Animated Character in the Virtual Environment
    goggler = Scene::GetSceneNode(Scene::AddSceneNode("character",platform->GetID()));
    goggler->SetMesh (gogglerMesh);
    goggler->SetTranslation(0,25,0);
	goggler->SetRotation(0,-150,0);
    goggler->GetMesh()->Play("wave", ANIM_LOOP);
    
    Scene::SetBlobShadow(goggler->GetGlobalTranslation() + Vector3(10,0,20), 40.0f, Vector4(0.2f,0.2f,0.2f,1), 20.0f, 40.0f);
    Scene::EnableBlobShadow();
    
    // Setup the UFO
    ufo = Scene::GetSceneNode(Scene::AddSceneNode("ufo",0));
    ufo->SetMesh (ufoMesh);
    ufo->SetTranslation(170,80,0);
	ufo->SetRotation(0,0,0);
    ufo->GetMesh()->Play("ufo", ANIM_LOOP);
    
    Input::ShowMouse(true);
    glfwEnable(GLFW_MOUSE_CURSOR);
    
    SharedResources::menuMusic->Play(true);
    

    fadeFactor = 1.0f;
}

void MainMenuState::Exit() {
    
    SharedResources::menuMusic->Stop();
    
    Scene::Clear();
    
    ResourceManager::ClearCache();
}

void MainMenuState::bringGogglerToFocus() {
    Vector3 position = cameraNode->GetGlobalTranslation();
    
    if (position.x < -100) {
//    Vector2 screenPosition = Scene::WorldToScreenPoint(position);
//    if (screenPosition.x >= Scene::GetUILayer()->GetVirtualWidth()/2-200) {
        fadeFactor = fadeFactor -= Timing::LastFrameTime*2;
    }
    
    cameraNode->Translate(-Timing::LastFrameTime * 200 * fadeFactor, 0, 0, SPACE_GLOBAL);
}

bool MainMenuState::Update() {

    bool changeNow;
    bool updateValue = menu->Update (changeNow);

    
    if (changeNow) {
        gogglerMesh->Play("flystill", ANIM_LOOP);
        
        bringGogglerToFocus();
        
        if (fadeFactor < 0){
            fadeFactor = 0;
            Application::GameApp->SetState("LoadingState");
        }
    }
    
    return updateValue;
}