//
//  MainMenuState.h
//  UEAGame
//
//  Created by Olivier Legat on 24/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef MAIN_MENU_STATE_H_
#define MAIN_MENU_STATE_H_

#include <GameApplication.h>

class MainMenu;

class MainMenuState : public State {
public:
    MainMenuState();
    ~MainMenuState();
    
    void Enter();
    void Exit();
    bool Update();

    
private:
    void bringGogglerToFocus();
    MainMenu *menu;
    
    float fadeFactor;
    
    Mesh *gogglerMesh;
    Mesh *ufoMesh;
    Mesh *platformMesh;
    
    SceneNode *goggler;
    SceneNode *ufo;
    SceneNode *platform;
    
    SceneNode *cameraNode;
};

#endif
