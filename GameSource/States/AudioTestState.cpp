#include "AudioTestState.h"
#include <Objects/SharedResources.h>

AudioTestState::AudioTestState() {
   ac = nullptr; 
}

AudioTestState::~AudioTestState() {
    
}

SceneNode *node;

void AudioTestState::Enter() {
    
    SceneNode *cameraNode = Scene::GetSceneNode( Scene::AddSceneNode("camera") );
    cameraNode->camera = new Camera();
    cameraNode->camera->SetupCamera(45.0f, 
                                    (float)GraphicsContext::GetCurrentDisplayMode().Width / GraphicsContext::GetCurrentDisplayMode().Height,
                                    0.05f, 10000.0f);
    
    
    cameraNode->SetTranslation(0,0,250);
    
    Scene::SetActiveCamera(cameraNode->GetID());

    node = Scene::GetSceneNode(Scene::AddSceneNode("sound"));
    //AudioSource *source = SharedResources::ingameMusic;//new AudioSource(SharedResources::explosionSound);

    AudioSource *source = new AudioSource(GameResources::explosionSound);

    
    node->audioSources["test"] = source;
    
    node->SetTranslation(0,0,1000);
}

void AudioTestState::Exit() {

}

bool AudioTestState::Update() {
    
    AudioSystem::UpdateListener(Vector3(0,0,0), Vector3(0,0,0), Vector3(0,0,0), Vector3(0,1,0));
    
	if(Input::IsClicked("Fire")){
        node->audioSources["test"]->Play(true);
	} 
    return true;
}