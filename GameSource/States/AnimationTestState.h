#ifndef ANIMATION_TEST_STATE_H_DEF
#define ANIMATION_TEST_STATE_H_DEF

#include <GameApplication.h>

class SceneNode;

class AnimationTestState : public State {
public:
    AnimationTestState();
    ~AnimationTestState();
    
    void Enter();
    void Exit();
    bool Update();

private:
    SceneNode *bigExplosionNode, *smallExplosionNode, *smokeExplosionNode, *shockwaveNode;
    
    SceneNode *bloodNode;
    
    
    SceneNode *character;
    SceneNode *orb;
};

#endif
