#ifndef LOADINGSTATE_H_DEF
#define LOADINGSTATE_H_DEF

#include <GameApplication.h>

class SceneNode;

class LoadingState : public State {
public:
    LoadingState();
    ~LoadingState();
    
    void Enter();
    void Exit();
    bool Update();
    
private:
    float timePassed;
    TextureResource *loadingResource;
    Texture *loadingTexture;
    
};

#endif