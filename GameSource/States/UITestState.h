#ifndef UI_TEST_STATE_H_DEF
#define UI_TEST_STATE_H_DEF

#include <GameApplication.h>

class SceneNode;

class UITestState : public State {
public:
    UITestState();
    ~UITestState();
    
    void Enter();
    void Exit();
    bool Update();
    
private:
    UILayer *ui;
    Texture *uglyBastard;
};

#endif