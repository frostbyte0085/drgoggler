#include "LoadingState.h"

#define MAX_SCREENS 10

LoadingState::LoadingState() {
}

LoadingState::~LoadingState() {
    
}

void LoadingState::Enter() {
    timePassed = 0.0f;
    int randomScreen = Math::Random (1, MAX_SCREENS);
    loadingResource = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/Loading/screen" + StringOperations::ToString(randomScreen) + ".png"));
    loadingTexture = loadingResource->TextureObject;
}

void LoadingState::Exit() {
    ResourceManager::Remove(loadingResource);
}

bool LoadingState::Update() {
    timePassed += Timing::LastFrameTime;
    
    UILayer *ui = Scene::GetUILayer();
    assert (ui);
    
    float scaleX = (float)ui->GetVirtualWidth() / (float)loadingTexture->GetWidth();
    float scaleY = (float)ui->GetVirtualHeight() / (float)loadingTexture->GetHeight();
    
    ui->TexturedQuad(loadingTexture, Vector2(0,0), Vector2(scaleX, scaleY));
    
    // wait 3.5 seconds, let the user read the loading text :)
    if (timePassed >= 3.5f) {
        //ui->Clear();
        Application::GameApp->SetState("GeneratorTestState");
    }
    return true;
}