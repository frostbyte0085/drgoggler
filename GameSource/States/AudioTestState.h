#ifndef AUDIO_TEST_STATE_H_DEF
#define AUDIO_TEST_STATE_H_DEF

#include <GameApplication.h>

class SceneNode;

class AudioTestState : public State {
public:
    AudioTestState();
    ~AudioTestState();
    
    void Enter();
    void Exit();
    bool Update();
    
private:
	AudioClip *ac;

	/*
    UILayer *ui;
    Texture *uglyBastard;
	*/
};

#endif