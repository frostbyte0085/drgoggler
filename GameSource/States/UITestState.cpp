#include "UITestState.h"

UITestState::UITestState() {
    
}

UITestState::~UITestState() {
    
}

Font *font;
void UITestState::Enter() {    
    
    font = ResourceManager::Create<FONT>(new FontResourceDescriptor("Data/UI/Fonts/arial_small.fnt"))->FontObject;
    
    
    
    SceneNode *cameraNode = Scene::GetSceneNode( Scene::AddSceneNode("camera") );
    cameraNode->camera = new Camera();
    cameraNode->camera->SetupCamera(45.0f, 
                                    (float)GraphicsContext::GetCurrentDisplayMode().Width / GraphicsContext::GetCurrentDisplayMode().Height,
                                    0.05f, 10000.0f);
    
    
    cameraNode->SetTranslation(0,0,250);
    
    Scene::SetActiveCamera(cameraNode->GetID());
    
    glfwEnable( GLFW_MOUSE_CURSOR );
    uglyBastard = ResourceManager::Create<TEXTURE>( new TextureResourceDescriptor("Data/UI/uglyBastard.jpg"))->TextureObject;
}

void UITestState::Exit() {

}

bool UITestState::Update() {
    if (Input::IsClicked("Conker")) {
        GraphicsContext::SetFastWindowedMode();
    }
    
    ui->TextLabel(font, Vector2(10,10), "this is a test " + StringOperations::ToString(IndependentTiming::FramesPerSecond));
    
    if (ui->Button(font, Vector2(0,100), Vector2(500,180), "Click Me!")) {
        Log ("clicked me :( ");
    }
    /*
    ui->TexturedQuad(uglyBastard, Vector2(450,450), Vector2(1,1));
    */
    return true;
}