#include "IntroState.h"
#include <Objects/SharedResources.h>

IntroState::IntroState() {
    GraphicsContext::BackgroundColor = Vector4(0,0,0,1);
    fade = 1.0f;
    startFade = 4.0f;    
}

IntroState::~IntroState() {

}

void IntroState::Enter() {

    ueaLogo = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/ueaLogo.png", false, false, TAM_CLAMP));
    ueaLogoTexture = ueaLogo->TextureObject;
    
    grey = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/GreyMark.png", false, false, TAM_CLAMP));
    greyTexture = grey->TextureObject;
}

void IntroState::Exit() {
    ResourceManager::Remove(ueaLogo);
}

bool IntroState::Update() {
    
    startFade -= Timing::LastFrameTime;
    
    if (startFade <= 0) {
        startFade = 0;
        fade -= Timing::LastFrameTime * 0.5f;
    }
    
    UILayer *ui = Scene::GetUILayer();
    
    float greyPosX = (float)ui->GetVirtualWidth()/2-(float)greyTexture->GetWidth()/2;
    float greyPosY = (float)ui->GetVirtualHeight()/2-(float)greyTexture->GetHeight()/2;
    
    ui->TexturedQuad(greyTexture, Vector2(greyPosX, greyPosY),
                     Vector2(1,1),
                     Vector4(1,1,1,fade));
    
    float logoPosX = (float)ui->GetVirtualWidth()/2-(float)ueaLogoTexture->GetWidth()/2;
    float logoPosY = (float)ui->GetVirtualHeight()/2-(float)ueaLogoTexture->GetHeight()/2;
    
    ui->TexturedQuad(ueaLogoTexture, Vector2(logoPosX, logoPosY),
                     Vector2(1,1),
                     Vector4(1,1,1,fade));
    
    String text = "MC27 Module Project";
    float textWidth = SharedResources::arial_small->GetTextWidth(text);
    float textPosX = ui->GetVirtualWidth()/2 - textWidth/2;
    float textPosY = logoPosY - SharedResources::arial_small->GetLineHeight() * 2;
    
    ui->TextLabel(SharedResources::arial_small, Vector2(textPosX, textPosY), text, Vector4(1,1,1,fade));
    
    if (fade <= 0) {
        ui->Clear();
        Application::GameApp->SetState("MainMenuState");
    }
        
    return true;
}