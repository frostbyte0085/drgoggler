#include "PlayerController.h"
#include <GameApplication.h>
#include <Objects/ObjectManager.h>

#include "LaserBehaviour.h"
#include "BloodSplatterBehaviour.h"
#include "UfoAIBehaviour.h"
#include "PickableTakenEffect.h"
#include "OrbBehaviour.h"
#include "PlatformBehaviour.h"

#include <Objects/SharedResources.h>

#define PLAYER_CONTROLLER_HEIGHT_MOD 1.18

// This macros plays an animation if it is not already playing.
//   pa - PlayerAnimation. New animation to play
//   s  - String. Name of the md5anim
#define playNewAnim(pa, s) \
if(playAnim!=(pa) && !playingSingleAnimation) {\
playAnim=(pa); \
node->GetMesh()->Play( (s), ANIM_LOOP);\
}

const float PlayerController::PLAYER_LOOK_SENSITIVITY_LOW = 0.5f;
const float PlayerController::PLAYER_LOOK_SENSITIVITY_NORMAL = 1.0f;
const float PlayerController::PLAYER_LOOK_SENSITIVITY_HIGH = 2.0f;
float PlayerController::lookSensitivity = PlayerController::PLAYER_LOOK_SENSITIVITY_NORMAL;

int PlayerController::previousScore = 0;
int PlayerController::previousKills = 0;
int PlayerController::previousLevel = 0;
int PlayerController::previousRoundsFired = 0;

PlayerController* PlayerController::currentPlayerController;
Vector3 PlayerController::GetCurrentPlayerPosition() {
    return currentPlayerController->node->GetGlobalTranslation();
}

void PlayerController::AddAKillToCurrentPlayer() {
    currentPlayerController->kills++;
    currentPlayerController->score += 100;
}


PlayerController::PlayerController(SceneNode* camera) {
    currentPlayerController = this;
    
    state = PS_FLYING;
    platform = nullptr;
	this->camera = camera;
    
	forwardThrust = 5000.0f;
	backwardThrust = -3000.0f; // +z is forward.
	sidewardThrust = 1000.0f;
    upwardThrust = 2000.0f;
	downwardThrust = 2000.0f;
    stunDuration = 0.0f;
    stunnedStart = 0.0f;
	timeFlying = 0.0f;
	timeToNextManaLoss = 5.0f;
}

PlayerController::~PlayerController() {
	delete physics;
    
    if (currentPlayerController == this) 
        currentPlayerController = nullptr;
}

bool PlayerController::Start() {
	float myMass = 1.0f;
    
    renderCrossHair = true;
    lockedUfo = nullptr;
    
	this->physics = new PhysicalState(node, myMass);
	//this->physics->SetSubjectToGravity(true);
    this->physics->GravityExaggeration = 2000;
    this->physics->SetDragCoof(3);
    
    ResetInput();
	lookX  = 0.0f;
	dlookX = 0.0f;
    jumping = false;
	playingSingleAnimation = false;

	// Init fire cooldown and lasers
	fireCD = 0.1f;
	fireCDts = 0.0f;
    
    orbCD = 5.0f;
    orbCDts = Timing::CurrentTime;
    orbsLeft = 5;
    
    
    BoxCollider *sc = (BoxCollider*)node->GetCollider();
    characterBoundingBox = sc->GetBoundingBox();
    
    CharacterType& goggler = ObjectManager::GetCharacterTypeByName("Dr. Goggler");
    hp = goggler.Hitpoints;
    mana = goggler.Manapoints;
    gotStar = false;

    score = previousScore;
    kills = previousKills;
	level = previousLevel+1;
    roundsFired = previousRoundsFired;
	ResetPreviousScore();

    turnOffEngines = false;
    
    jet1 = Scene::GetSceneNode (Scene::AddSceneNode("jet1", node->GetID()));
    jet1->SetParticleSystem(ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/JetEngine.xml", false))->ParticleObject);
    
    jet1->SetTranslation(-10,50,35);
    jet1->GetParticleSystem()->Emit();

    jet2 = Scene::GetSceneNode (Scene::AddSceneNode("jet2", node->GetID()));
    jet2->SetParticleSystem(ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/JetEngine.xml", false))->ParticleObject);
    
    jet2->SetTranslation(10,50,35);
    jet2->GetParticleSystem()->Emit();  
    
    destinedToDieAt = -1; // we don't know... yet. MUAHAHAHAHA :P
    
    node->audioSources["laser"] = new AudioSource(GameResources::playerLaserSound);
    node->audioSources["jetpack"] = new AudioSource (GameResources::jetpackSound);
    node->audioSources["orbLock"] = new AudioSource (GameResources::orbLockSound);
    node->audioSources["orbLockNA"] = new AudioSource (GameResources::orbLockNASound);
	node->audioSources["healthCollect"] = new AudioSource (GameResources::healthCollectSound);
	node->audioSources["orbCollect"] = new AudioSource (GameResources::orbCollectSound);
	node->audioSources["fuelCollect"] = new AudioSource (GameResources::fuelCollectSound);
	node->audioSources["coinCollect"] = new AudioSource (GameResources::coinCollectSound);
	node->audioSources["splash"] = new AudioSource (GameResources::splashSound);
    
    return true;
}

void PlayerController::ResetPreviousScore() {
	previousScore = previousKills = previousLevel= previousRoundsFired = 0;
}

void PlayerController::CheckPlaySingleAnimation() {
	if(!isStunned()) playingSingleAnimation = false;
}
void PlayerController::PlaySingleAnimation(String animName, float duration) {
	Stun(duration);
	this->node->GetMesh()->Play(animName, ANIM_ONCE);
    this->playAnim = PA_OTHER;
	playingSingleAnimation=true;
}

bool PlayerController::isStunned() {
    return !(Timing::CurrentTime > stunDuration + stunnedStart);
}
void PlayerController::Stun(float duration) {
    stunDuration = duration;
    stunnedStart = Timing::CurrentTime;
    ResetInput();
}

void PlayerController::ResetInput() {
    moveX = moveZ = moveY = moveStrafe = dlookX = 0.0f;
}

void PlayerController::fireOrb() {
    if (!lockedUfo || (orbsLeft == 0) )
        return;
    
    if(Timing::CurrentTime - orbCDts < orbCD) return;
    
    orbCDts = Timing::CurrentTime;
    
    SceneNode *orb = Scene::GetSceneNode(Scene::AddSceneNode("orb"));
    orb->SetTag("orb", 0);
    orb->SetMesh(GameResources::orbMesh, CT_SPHERE);
    orb->SetCollisionGroup(OCG_PLAYERLASER);
    
    // Get the position of the head + position of the character:
    MeshAnimation *anim = node->GetMesh()->GetCurrentAnimation();
    MeshAnimation::SkeletonJointList joints = anim->GetSkeleton().joints;
    Vector3 pos = joints[0].position + joints[14].position;
    pos = glm::rotate(node->GetGlobalRotation(), pos);
    pos += node->GetGlobalTranslation();
    
	orb->SetGlobalTranslation(pos);
    
	orb->AddBehaviour("orb", new OrbBehaviour(lockedUfo));
    
    orbsLeft--;
    
    roundsFired++;
}

void PlayerController::fireLasers() {
    // Wait for cooldown
	if(Timing::CurrentTime - fireCDts < fireCD) return;
    
	fireCDts = Timing::CurrentTime;
    
    node->audioSources["laser"]->Play();
    
    // Create a new scene node parented to the root:
	SceneNode* laser = Scene::GetSceneNode(Scene::AddSceneNode("laser"));
	laser->SetTag("laser", 2);
    laser->SetMesh(GameResources::dualRedLaserMesh, CT_BOX);
    laser->SetCollisionGroup(OCG_PLAYERLASER);
    
    // Get the position of the head + position of the character:
    MeshAnimation *anim = node->GetMesh()->GetCurrentAnimation();
    MeshAnimation::SkeletonJointList joints = anim->GetSkeleton().joints;
    Vector3 pos = joints[0].position + joints[14].position;
    pos = glm::rotate(node->GetGlobalRotation(), pos);
    pos += node->GetGlobalTranslation();
    
	laser->SetGlobalTranslation(pos);
    
    // Get the rotation:
	laser->SetGlobalRotation(this->camera->GetGlobalRotation());
    
    // Get the velocity (i.e. direction): 
    Vector3 vel = glm::rotate(this->camera->GetGlobalRotation(), Vector3(0,0,-8000));
    //GLfloat viewport[4];
    //glGetFloatv(GL_VIEWPORT, viewport);
    //const Vector<Plane> frustum = Scene::GetCurrentFrustum();
    
    
    //Vector2 crosshairPoint (viewport[2]/2,viewport[3]/2);
    //Vector3 targetPoint = Scene::ScreenToWorldRay(crosshairPoint).GetOrigin();
    //targetPoint = glm::normalize(targetPoint);
    //Vector3 frustumPoint = frustum[FP_FAR].GetPosition();// * camera->camera->GetFarClipPlane();
    
    
    //Vector3 vel = (targetPoint - frustum[FP_NEAR].GetPosition()) * -5000.0f;
    //Vector3 vel = (frustumPoint- targetPoint) * 10000.0f;

	laser->AddBehaviour("blaster", new LaserBehaviour(vel));
    
    roundsFired++;
}

void PlayerController::fire() {
    if (renderCrossHair)
        fireLasers();
    else
        fireOrb();
}

void PlayerController::processFlyInput()
{
    
    float oldLookX = lookX;
	lookX += Input::GetValue("LookUp") * Timing::LastFrameTime * -150 * lookSensitivity;
	lookX += Input::GetValue("LookDown") * Timing::LastFrameTime * -150 * lookSensitivity;
	lookX = lookX >  90 ?  90 : lookX;
	lookX = lookX < -90 ? -90 : lookX;
	dlookX = lookX - oldLookX;
    
    if(isStunned()) return;
    
	moveX += Input::GetValue("Left") * Timing::LastFrameTime * 250 * lookSensitivity;
	moveX += Input::GetValue("Right") * Timing::LastFrameTime * 250 * lookSensitivity;
    
    // No timing * needed. These passed the physics engine which is Timing dependant 
	moveY += Input::GetValue("Up") * upwardThrust;
    if (mana <= 0)
        moveY = 0;
    
	moveY += Input::GetValue("Down") * downwardThrust;
	moveZ += Input::GetValue("Forward") * -forwardThrust;  
	moveZ += Input::GetValue("Backward") * backwardThrust;
	moveStrafe += Input::GetValue("StrafeL") * sidewardThrust;
	moveStrafe += Input::GetValue("StrafeR") * sidewardThrust;
    
	if(Input::IsPressed("Fire")) fire();
}

void PlayerController::processFlyAnimation()
{
	CheckPlaySingleAnimation();
	PlayerAnimation newAnim = PA_FLYSTILL;

	if(moveZ < 0.0f) newAnim = PA_FLYFW;
	else if(moveZ > 0.0f) newAnim = PA_FLYBW;

	if(fabs(moveStrafe) > fabs(moveZ) ) {
		if(moveStrafe < 0.0f) newAnim = PA_FLYLEFT;
		else if(moveStrafe > 0.0f) newAnim = PA_FLYRIGHT;
	}

	switch(newAnim) {
	case PA_FLYFW:    
		playNewAnim(PA_FLYFW, "flyfw"); break;
	case PA_FLYBW:   
		playNewAnim(PA_FLYBW, "flybw"); break;
	case PA_FLYLEFT:  
		playNewAnim(PA_FLYLEFT, "flyleft"); break;
	case PA_FLYRIGHT: 
		playNewAnim(PA_FLYRIGHT, "flyright"); break;
	case PA_FLYSTILL: 
		playNewAnim(PA_FLYSTILL, "flystill"); break;
	default: break;
	}
}

void PlayerController::processWalkingInput() {	
    float oldLookX = lookX;
	lookX += Input::GetValue("LookUp") * Timing::LastFrameTime * -150 * lookSensitivity;
	lookX += Input::GetValue("LookDown") * Timing::LastFrameTime * -150 * lookSensitivity;
	lookX = lookX >  90 ?  90 : lookX;
	lookX = lookX < -90 ? -90 : lookX;
	dlookX = lookX - oldLookX;
    
    if(isStunned()) return;
    
    moveX += Input::GetValue("Left") * Timing::LastFrameTime * 250 * lookSensitivity;
	moveX += Input::GetValue("Right") * Timing::LastFrameTime * 250 * lookSensitivity;
	moveY += Input::GetValue("Up");
    if (mana <= 0)
        moveY = 0;
    
	moveY += Input::GetValue("Down");
	moveZ += Input::GetValue("Forward") * -Timing::LastFrameTime * 250;
	moveZ += Input::GetValue("Backward") * -Timing::LastFrameTime * 250;
	moveStrafe += Input::GetValue("StrafeL") * Timing::LastFrameTime * 250;
	moveStrafe += Input::GetValue("StrafeR") * Timing::LastFrameTime * 250;
    
	if(Input::IsPressed("Fire")) fire();
}

void PlayerController::processWalkingAnimation()
{
	CheckPlaySingleAnimation();
	PlayerAnimation newAnim = PA_STAND;
    
	if(moveZ < 0.0f) newAnim = PA_RUN;
	else if(moveZ > 0.0f) newAnim = PA_RUN;
    
	if(fabs(moveStrafe) > fabs(moveZ) ) {
		if(moveStrafe < 0.0f) newAnim = PA_RUN;
		else if(moveStrafe > 0.0f) newAnim = PA_RUN;
	}

	MeshAnimation *anim ;
	float valF, valR, valL, valB;
	switch(newAnim) {
        case PA_RUN:
			// Start the animation? Or not.
            playNewAnim(PA_RUN, "run"); 

			// Get the speed factor (Effect only noticeable on Gamepad)
			valF = (float)fabs(Input::GetValue("Forward"));
			valB = (float)fabs(Input::GetValue("Backward"));
			valR = (float)fabs(Input::GetValue("StrafeR"));
			valL = (float)fabs(Input::GetValue("StrafeL"));

			// = max(valF, valB, valR, valL)
			valF = valF > valB ? valF : valB;
			valF = valF > valR ? valF : valR;
			valF = valF > valL ? valF : valL;

			// Set speed
			anim = node->GetMesh()->GetAnimation("run");
			anim->Speed = 3 * valF;
			break;
		case PA_STAND:
            playNewAnim(PA_STAND, "stand"); break;

        default: break;
	}
}

void PlayerController::enterFlying() {
    turnOffEngines = false;
    
    Vector3 k = Scene::GetSceneNode(node->GetParent())->GetScale();
    float y = PLAYER_CONTROLLER_HEIGHT_MOD * k.y;
    
    node->Translate(Vector3(0,y,0), SPACE_LOCAL);
    node->SetParent(0);
    platform = nullptr;
    
    node->audioSources["jetpack"]->Play(true);
    
    node->GetMesh()->Play("flystill", ANIM_LOOP);
    processFlyAnimation();

    ResetInput();
}

void PlayerController::processLock() {
    renderCrossHair = true;
    bool canFire = (Timing::CurrentTime - orbCDts >= orbCD) && (orbsLeft > 0);
    String lockAudio = "orbLock";
    if (!canFire)
        lockAudio = "orbLockNA";
    
    const QueryResult &ufos = Scene::GetSceneNodesWithTag("ufo");
    
    if (Input::IsPressed("Lock")) {
        renderCrossHair = false;
        
        UILayer *ui = Scene::GetUILayer();
        assert (ui);
        
        Texture *lockTexture = GameResources::lockWindowTexture;
        
        float lockWindowPosX = ui->GetVirtualWidth()/2 - lockTexture->GetWidth()/2;
        float lockWindowPosY = ui->GetVirtualHeight()/2 - lockTexture->GetHeight()/2;
        
        Vector3 headPosition = node->GetGlobalTranslation();        
        
        for (int i=0; i<ufos.nodeIds.size(); i++) {
            SceneNode *ufoNode = Scene::GetSceneNode(ufos.nodeIds[i]);     
            Log (ufoNode->GetName());
            
            if (!ufoNode || ufoNode->IsDestroying() || ufoNode->IsFrustumCulled()) {
                continue;
            }
            
            Vector3 worldPosition = ufoNode->GetGlobalTranslation();
            Vector2 screenPosition = Scene::WorldToScreenPoint(worldPosition);
            
            float distance = glm::distance(worldPosition, node->GetGlobalTranslation());
            if (distance < 1000) {
                continue;
            }
            
            
            Vector2 actualRes = Vector2(GraphicsContext::GetCurrentDisplayMode().Width, GraphicsContext::GetCurrentDisplayMode().Height);
            Vector2 virtualRes = Vector2(ui->GetVirtualWidth(), ui->GetVirtualHeight());
            
            float widthDiff = virtualRes.x / actualRes.x;
            float heightDiff = virtualRes.y / actualRes.y;
            
            screenPosition.x *= widthDiff;
            screenPosition.y *= heightDiff;
            
            if ( (screenPosition.x > lockWindowPosX && screenPosition.x < lockWindowPosX + lockTexture->GetWidth())
                && (screenPosition.y > lockWindowPosY && screenPosition.y < lockWindowPosY + lockTexture->GetHeight()) ) {
                
                lockedPositionX = screenPosition.x;
                lockedPositionY = screenPosition.y;
                lockedUfo = ufoNode;
                
                // raycast to see if we hit the alien
                RayHitInfo hitInfo;
                if (Scene::Raycast(node->GetGlobalTranslation(), glm::normalize(ufoNode->GetGlobalTranslation() - headPosition), 150000.0f, hitInfo, node->GetID())) {
                
                    if (hitInfo.node && hitInfo.node->HasTag()) {
                        if (hitInfo.node->GetTag()->first != "ufo") {
                            lockedUfo = nullptr;
                        }
                    }
                }
                break;
            }
            else {
                lockedUfo = nullptr;
            }
        }
        
        String ammoText = "Orbs:  ";
        float ammoTextWidth = SharedResources::arial_small->GetTextWidth(ammoText);
        
        ui->TexturedQuad(lockTexture, Vector2(lockWindowPosX, lockWindowPosY));
        Vector4 ammoColor = Vector4(1,1,1,1);
        if (orbsLeft == 2 || orbsLeft == 1)
            ammoColor = Vector4(1,0.6f, 0.1f, 1);
        else if (orbsLeft == 0)
            ammoColor = Vector4(1,0,0,1);
        
        ui->TextLabel(SharedResources::arial_small, Vector2(ui->GetVirtualWidth()/2-ammoTextWidth/2, lockWindowPosY), "Orbs: " + StringOperations::ToString(orbsLeft), ammoColor);
        
        if (lockedUfo) {
            if (!node->audioSources[lockAudio]->IsPlaying())
                node->audioSources[lockAudio]->Play(true);
            
            Texture *lockTarget = GameResources::lockTargetTexture;
            if(!canFire)
                lockTarget = GameResources::lockTargetNATexture;
            
            ui->TexturedQuad(lockTarget, Vector2(lockedPositionX - lockTarget->GetWidth()/2, lockedPositionY - lockTarget->GetHeight()/2));
        }
        else {
            node->audioSources[lockAudio]->Stop();
        }
    }else {
        node->audioSources[lockAudio]->Stop();
        
        lockedUfo = nullptr;
    }
    if (lockAudio == "orbLockNA")
        node->audioSources["orbLock"]->Stop();
    else if (lockAudio == "orbLock")
        node->audioSources["orbLockNA"]->Stop();
    
    Scene::FreeQueryResult(ufos);
}

void PlayerController::enterWalking(const RayHitInfo &hit) {
    turnOffEngines = true;
    
    node->SetGlobalTranslation(platformPoint.x, platformPoint.y+25, platformPoint.z);
    
    node->SetParent(hit.node->GetID());
    platform = hit.node;
    platformPoint = hit.point;
    
    Vector3 k = Scene::GetSceneNode(node->GetParent())->GetScale();
    float y = -PLAYER_CONTROLLER_HEIGHT_MOD * k.y;
    node->Translate(Vector3(0,-PLAYER_CONTROLLER_HEIGHT_MOD,0), SPACE_LOCAL);
    
    node->audioSources["jetpack"]->Stop();
    
	float landingSpeed = physics->GetSpeed();
    physics->SetAtRest();
    
	if(landingSpeed < 700.0f) {
		PlaySingleAnimation("land", 0.4f);
	}
	else {
		PlaySingleAnimation("hard-land", 2);
		TakeDamage(1);
	}
#ifdef LOG_LANDINGSPEED
	Log("landingSpeed:" + StringOperations::ToString<float>(landingSpeed));
#endif

    ResetInput();
}

bool PlayerController::Update() {
    if (Application::GameApp->IsPaused())
        return true;
    
    // check for raycast hits
    RayHitInfo hitInfo;
    Vector3 rayOrigin = node->GetGlobalTranslation();
    
    bool upPressed = Input::IsPressed("Up");
                                      
    if (!IsDoomed()
        && Scene::Raycast(rayOrigin, Vector3(0,-1,0), 1000.0f, hitInfo, node->GetID())
        && !upPressed)
    {
        
        if (hitInfo.node->HasTag()) {
            
            float boxVertexDistance = 30.0f;
            if (hitInfo.node->GetTag()->first == "platformTypeID") {
                Scene::SetBlobShadow(hitInfo.point, 40.0f, Vector4(0.2f,0.2f,0.2f,1), hitInfo.distance, 40.0f);
                
                
                if (hitInfo.distance < boxVertexDistance * 2)
                    Scene::EnableBlobShadow();
                else
                    Scene::DisableBlobShadow();
                
                if (hitInfo.distance < boxVertexDistance) {
                    
					platformPoint = hitInfo.point;

                    if (state != PS_WALKING) {
                        state = PS_WALKING;
                        
                        enterWalking(hitInfo);
                    }           
                }
                else {
                    if (state == PS_WALKING) {
                        state = PS_FLYING;
                        
                        moveStrafe *= 3;
                        moveZ *= 3;
                        
                        
                        SceneNode *parentPlatform = Scene::GetSceneNode(node->GetParent());
                        assert (parentPlatform);
                        PlatformBehaviour *platformB = (PlatformBehaviour*)parentPlatform->GetBehaviour("hover");
                        assert (platformB);
                        float flunct = platformB->GetFlunct();                        
                        
                        
                        std::cout << flunct << std::endl;
                        float upspeed = 5;
                        if (flunct < 0)
                            upspeed = 5;
                        else
                            upspeed = flunct + 5;
                        
                        
                        node->Translate (moveStrafe, upspeed, moveZ, SPACE_LOCAL);
                        
                        enterFlying();
                    }
                }
            }
        }
        
    }
    else
    {
        if (IsDoomed()) {
            turnOffEngines = true;
            jet1->GetParticleSystem()->Stop();
            jet2->GetParticleSystem()->Stop();
        }
        
        if (upPressed) {
            if (mana <= 0) {
                // do nothing here
            }
            else {
                if (state == PS_WALKING) {
                    state = PS_FLYING;
                    
                    enterFlying();
                }
            }
        }
        else {
            if (state == PS_WALKING) {
                state = PS_FLYING;
                
                enterFlying();
            }
        }
    }
    
    // In the water?
	if(state==PS_FLYING && node->GetGlobalTranslation().y <= -400)
		Die();

    switch (state) {
        case PS_FLYING:
            Scene::EnableCollisions(OCG_CHARACTER, OCG_PLATFORM);
            Scene::DisableBlobShadow();
            updateFlying();
            break;
            
        case PS_WALKING:
            Scene::DisableCollisions(OCG_CHARACTER, OCG_PLATFORM);
            updateWalking();
            break;
            
        case PS_DEAD_AIR:
            physics->Update(Timing::LastFrameTime);
            break;
            
        default: break;
    }
    
    processLock();
    
    if(jumping && !isStunned()) jump();

    float jetScale = jet1->GetGlobalScale().x;
    
    if (turnOffEngines) {
        jetScale -= Timing::LastFrameTime * 3;
        jetScale = Math::Clamp(jetScale, 0.0f, 1.0f);
        
        jet1->SetScale(jetScale, jetScale, jetScale);
        jet2->SetScale(jetScale, jetScale, jetScale);
        if (node->audioSources["jetpack"]->IsPlaying())
            node->audioSources["jetpack"]->Stop();
    }
    else {
        if (!node->audioSources["jetpack"]->IsPlaying())
            node->audioSources["jetpack"]->Play(true);
        
        jetScale += Timing::LastFrameTime * 3;
        jetScale = Math::Clamp(jetScale, 0.0f, 1.0f);
        
        jet1->SetScale(jetScale, jetScale, jetScale);
        jet2->SetScale(jetScale, jetScale, jetScale);        
    }
    MeshAnimation *currentAnimation = node->GetMesh()->GetCurrentAnimation();
    if (currentAnimation) {
        MeshAnimation::FrameSkeleton skeleton = currentAnimation->GetSkeleton();
        MeshAnimation::SkeletonJoint jetJoint = skeleton.joints[1];
        jet1->SetRotation(jetJoint.orientation);
        jet2->SetRotation(jetJoint.orientation);
    }
    
    AudioSystem::UpdateListener(node->GetGlobalTranslation(), Vector3(0,0,0), Vector3(0,0,0), Vector3(0,1,0));
    
    return true;
}

void PlayerController::updateFlying() {

    // Get input:
	ResetInput();
    processFlyInput();

	// Play animation:
    processFlyAnimation();
    
	// Refresh physics
	if(mana==0) {
		physics->SetSubjectToGravity(true);
        turnOffEngines = true;
	}
	else {
		Vector3 acceleration = Vector3(moveStrafe, moveY, moveZ) * 1.0f;
		physics->SetThrust(acceleration);

		// Refresh mana (fuel):
		timeFlying += Timing::LastFrameTime;
		if(timeFlying > timeToNextManaLoss) {
			timeFlying -= timeToNextManaLoss;
			mana--;
		}
	}
	physics->Update(Timing::LastFrameTime);
	physics->RotateSceneNode(0, -moveX, 0, SPACE_LOCAL);

	// Refresh camera:
	updateCamera();
}

void PlayerController::updateWalking() {
    ResetInput();
    
    processWalkingInput();
    processWalkingAnimation();
    
    Vector3 currentTranslation = node->GetGlobalTranslation();
//    node->SetGlobalTranslation(currentTranslation.x + moveStrafe, platformPoint.y+25, currentTranslation.z + moveZ);
    
    SceneNode* platform = Scene::GetSceneNode(node->GetParent());
    float k = platform->GetScale().x;
    
	node->Translate (moveStrafe/k, 0, moveZ/k, SPACE_LOCAL);
    
    node->Rotate(0, -moveX, 0, SPACE_LOCAL);
   
    // Refresh camera:
    updateCamera();
    
    if(moveY > 0.0f && !jumping) {
        startJumping();
    }
}

void PlayerController::updateCamera() {
    
    camera->Rotate(Vector3(dlookX,0,0), SPACE_LOCAL);
    Vector3 position = Vector3(0,120,350);
    position = glm::rotate(position, lookX, Vector3(1,0,0));
    camera->SetTranslation(position);
}

void PlayerController::startJumping() {
    jumping = true;
	PlaySingleAnimation("jump", 0.3f);
}
void PlayerController::jump() {
    jumping = false;
    Log("TAKE OFF");
    Vector3 pos = physics->GetPosition();
    pos += Vector3(0,10,0);
    physics->SetPosition(pos);
    physics->SetVelocity(Vector3(0,1000,0));
    state = PS_FLYING;
    enterFlying();
}

void PlayerController::PositionKillCam() {
    camera->SetTranslation(Vector3(0,100,290));
    camera->SetRotation(Vector3(-10,0,0));
}

void PlayerController::Die() {
    if (IsDoomed()) return; // we only die once.
    
    Scene::DisableCollisions(OCG_PLATFORM, OCG_CHARACTER);
    Scene::DisableCollisions(OCG_UFO, OCG_CHARACTER);

    // How long shall this soul's torment last?
    float deathDuration = 0.0f;
    
    // How shall we torment this poor soul?
    Vector3 pos = node->GetGlobalTranslation();
	if(pos.y <= -400) {

        node->audioSources["splash"]->Play();
        // Generic stuff:
        PlaySingleAnimation("drown", 10);
        state = PS_DEAD_DROWN;
        PositionKillCam();
        deathDuration = 2;
        
        // Additional stuff for this death:
        hp = 0;       // INSTA-KILL!
        mana = 0;     // There's no escaping death.. muaha.. muahahahaha!!
        pos.y = -400; // Lift the character up a bit for a controlled cinematic event:
        node->SetGlobalTranslation(pos);  // (we don't him to already be underwater... no no)
	}
	if(state == PS_FLYING) {
        // Generic stuff:
        PlaySingleAnimation("flydeath", 10);
		state = PS_DEAD_AIR;
        PositionKillCam();
        deathDuration = 2;
        
        // Additional stuff for this death:
        ResetInput();
        physics->SetAtRest();
        camera->SetParent(0); // We dont want the camera to follow the player anymore.
        physics->SetSubjectToGravity(true); // I won't kill you. The earth will.
        physics->GravityExaggeration = 200; // Don't fall too fast. We wanna see you suffer.
	}
	else if (state == PS_WALKING) {
        // Generic stuff:
        PlaySingleAnimation("death", 10);
		state = PS_DEAD_GROUND;
        PositionKillCam();
        deathDuration = 2;
	}
    
    ResetInput(); // surpress Input for this iteration
    
    // When does faith decide to claim this soul for the afterlife?
    destinedToDieAt = Timing::CurrentTime + deathDuration;
    stoppedAt = Timing::CurrentTime;
}

bool PlayerController::IsDoomed() {
    return state == PS_DEAD_AIR || 
           state == PS_DEAD_DROWN ||
           state == PS_DEAD_GROUND;
}

bool PlayerController::IsDying() {
    return IsDoomed() && Timing::CurrentTime < destinedToDieAt;
}

bool PlayerController::IsDead() {
    return IsDoomed() && Timing::CurrentTime >= destinedToDieAt;
}

bool PlayerController::TakeDamage(int amount) {
	hp -= amount;
    
    // Show the user some BLOOD!!!
    SceneNode *bloodNode = Scene::GetSceneNode (Scene::AddSceneNode("Blood"));
    bloodNode->AddBehaviour("BloodSplatter", 
                            new BloodSplatterBehaviour(node->GetGlobalTranslation()));
    
	if(hp <= 0 && !IsDoomed()) {
		Die();
		return true;
	}
    
	return false;
}

void PlayerController::OnCollideEnter(SceneNode *otherNode) {
    
    bool lasered = false;
    if (otherNode->HasTag() ) {
        Pair<String, int> *nodeTag = otherNode->GetTag();
        
        // Did a UFO hit me?
        if (nodeTag->first == "enemylaser") {
            // Yes... OUCH! It burns.
            lasered = true;
            TakeDamage(1);
        }
    }
    
    // Ok no a UFO didn't hit me, I just smacked my head against something...
    //   (I'm not convinced that's less painful)
    if(!lasered) {
        Vector3 vel = physics->GetVelocity();
        physics->SetVelocity(-vel);
        switch(state) {
            case PS_FLYING: PlaySingleAnimation("flytwitch0", 1); break;
            case PS_WALKING: PlaySingleAnimation("twitch0", 1); break;
            default: /* Do nothing */ break;
        }
    }
}

void PlayerController::OnTriggerEnter(SceneNode *otherNode) {
   
    if (otherNode->HasTag() ) {
        Pair<String, int> *nodeTag = otherNode->GetTag();
        
        String tagName = nodeTag->first;
        String effect = "PickableValuable";
        if (tagName == "fuel")
            effect = "PickableFuel";
        else if (tagName == "health")
            effect = "PickableHealth";
        
        SceneNode *sparksNode = Scene::GetSceneNode(Scene::AddSceneNode("pickableSparks"));
        sparksNode->SetGlobalTranslation(otherNode->GetGlobalTranslation());
        sparksNode->AddBehaviour("sparksBehaviour", new PickableTakenEffect(effect));
        
        
        if (nodeTag->first == "money") {
            int worth = nodeTag->second;
            score += worth;

			node->audioSources["coinCollect"]->Play();
            Scene::DestroySceneNode(otherNode->GetID());
        }
        
        else if (nodeTag->first == "health") {
            int hpRestore = nodeTag->second;
            hp = hpRestore;
            
			node->audioSources["healthCollect"]->Play();
            Scene::DestroySceneNode(otherNode->GetID());
        }
        
        else if (nodeTag->first == "fuel") {
            int refuel = nodeTag->second;
			timeFlying = 0.0f; // reset any fuel point fraction
            mana = refuel;     // refill :)
			physics->SetSubjectToGravity(false);
            
			node->audioSources["fuelCollect"]->Play();
            Scene::DestroySceneNode(otherNode->GetID());
        }
        
        else if (nodeTag->first == "orbrefill") {
            int moreOrbs = nodeTag->second;
            
            this->orbsLeft += (unsigned int) moreOrbs;
            
			node->audioSources["orbCollect"]->Play();
            Scene::DestroySceneNode(otherNode->GetID());
        }
        
        else if (nodeTag->first == "star") {
            gotStar = true;
            stoppedAt = Timing::CurrentTime;
            Scene::DestroySceneNode(otherNode->GetID());
        }
    }
}

void PlayerController::OnCollideExit(SceneNode *otherNode) {
    //Log ("collide exited " + otherNode->GetName());
}

void PlayerController::OnTriggerExit(SceneNode *otherNode) {
    //Log ("trigger exited " + otherNode->GetName());
}

void PlayerController::OnCollideStay(SceneNode *otherNode) {
    
    //Log ("collide stay with " + otherNode->GetName());
    if (otherNode->HasTag() && otherNode->GetTag()->first == "platformTypeID") {
        physics->SetVelocity(Vector3(0,-1000,0));
    }
}

void PlayerController::OnTriggerStay(SceneNode *otherNode) {
    
}