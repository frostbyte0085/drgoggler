#ifndef ANIM_TEST_H_DEF
#define ANIM_TEST_H_DEF

#include <GameApplication.h>

class SceneNode;

class AnimTest : public SceneNodeBehaviour {
public:
    AnimTest();
	~AnimTest();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
private:
};

#endif