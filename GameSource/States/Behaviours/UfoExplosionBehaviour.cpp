#include "UfoExplosionBehaviour.h"

#include <Objects/SharedResources.h>

UfoExplosionBehaviour::UfoExplosionBehaviour(const Vector3 &ufoPosition) {

    this->ufoPosition = ufoPosition;
}
UfoExplosionBehaviour::~UfoExplosionBehaviour() {
}

bool UfoExplosionBehaviour::Start() {
    
    smallExplosionNode = Scene::GetSceneNode (Scene::AddSceneNode ("smallExplosion"));
    smallExplosionNode->SetGlobalTranslation(ufoPosition);
    smallExplosionNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/SmallExplosion.xml", false))->ParticleObject);
    
    smokeExplosionNode = Scene::GetSceneNode (Scene::AddSceneNode ("smokeExplosion"));
    smokeExplosionNode->SetGlobalTranslation(ufoPosition);
    smokeExplosionNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/SmokeExplosion.xml", false))->ParticleObject);
    
    bigExplosionNode = Scene::GetSceneNode (Scene::AddSceneNode ("bigExplosion"));
    bigExplosionNode->SetGlobalTranslation(ufoPosition);
    bigExplosionNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/BigExplosion.xml", false))->ParticleObject);
    
    shockwaveNode = Scene::GetSceneNode(Scene::AddSceneNode("shockwave"));
    
    Mesh *shockwaveMesh = ResourceManager::Create<MESH>(new MeshResourceDescriptor("shockwave", true))->MeshObject;
    
    shockwaveMesh->MakePlane(100,100);
    shockwaveMesh->GetSubmesh(0)->material = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor("Data/Materials/Shockwave.mat", false))->MaterialObject;
    shockwaveMesh->GetSubmesh(0)->material->SetDiffuseColor(Vector4(1,0.4f,0,1));
    shockwaveNode->SetMesh(shockwaveMesh);
    shockwaveNode->SetScale(0.1f,0.1f,0.1f);
    
    shockwaveNode->SetGlobalTranslation(ufoPosition);
    
    float randomX = Math::RandomF(-30.0f, 30.0f);
    float randomY = Math::RandomF(-30.0f, 30.0f);
    float randomZ = Math::RandomF(-20.0f, 20.0f);
    shockwaveNode->Rotate(randomX, randomY, randomZ, SPACE_LOCAL);
    
    shockwaveAlpha = 1.0f;
    
    bigExplosionNode->GetParticleSystem()->Emit();
    smallExplosionNode->GetParticleSystem()->Emit();
    smokeExplosionNode->GetParticleSystem()->Emit();
    
    node->audioSources["explosion"] = new AudioSource (GameResources::explosionSound);
    node->audioSources["explosion"]->Play();
    
    return true;
}

void UfoExplosionBehaviour::destroyNodes() {
    Scene::DestroySceneNode(smallExplosionNode->GetID());
    Scene::DestroySceneNode(bigExplosionNode->GetID());
    Scene::DestroySceneNode(smokeExplosionNode->GetID());
    Scene::DestroySceneNode(shockwaveNode->GetID());
    
    Scene::DestroySceneNode(node->GetID());
}

bool UfoExplosionBehaviour::Update() {
    shockwaveAlpha -= Timing::LastFrameTime/2;
    
    bool smallStopped = smallExplosionNode->GetParticleSystem()->IsEmitting();
    bool bigStopped = bigExplosionNode->GetParticleSystem()->IsEmitting();
    bool smokeStopped = smokeExplosionNode->GetParticleSystem()->IsEmitting();
    bool shockwaveStopped = shockwaveAlpha <= 0;
    
    shockwaveNode->Scale(1+Timing::LastFrameTime*5, 1+Timing::LastFrameTime*5, 1+Timing::LastFrameTime*5);
    Material* mat = shockwaveNode->GetMesh()->GetSubmesh(0)->material;
    mat->SetDiffuseColor(Vector4(1,0.4f,0,shockwaveAlpha));
    
    if (smallStopped && bigStopped && smokeStopped && shockwaveStopped) {
        destroyNodes();
    }
    return true;
}

void UfoExplosionBehaviour::OnTriggerEnter(SceneNode *otherNode) {}
void UfoExplosionBehaviour::OnTriggerExit(SceneNode *otherNode) {}
void UfoExplosionBehaviour::OnTriggerStay(SceneNode *otherNode) {}

void UfoExplosionBehaviour::OnCollideEnter(SceneNode *otherNode) {}
void UfoExplosionBehaviour::OnCollideExit(SceneNode *otherNode) {}
void UfoExplosionBehaviour::OnCollideStay(SceneNode *otherNode) {}