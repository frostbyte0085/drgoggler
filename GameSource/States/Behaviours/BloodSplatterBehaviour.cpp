//
//  BloodSplatterBehaviour.cpp
//  UEAGame
//
//  Created by Olivier Legat on 04/12/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "BloodSplatterBehaviour.h"


BloodSplatterBehaviour::BloodSplatterBehaviour(const Vector3 &impactPosition) {
    
    this->impactPosition = impactPosition;
}
BloodSplatterBehaviour::~BloodSplatterBehaviour() {
}

bool BloodSplatterBehaviour::Start() {
    
    bloodSplatterNode = Scene::GetSceneNode (Scene::AddSceneNode ("blood"));
    bloodSplatterNode->SetGlobalTranslation(impactPosition);
    bloodSplatterNode->SetParticleSystem (ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/BloodSplatter.xml", false))->ParticleObject);
    
    bloodSplatterNode->GetParticleSystem()->Emit();
    
    return true;
}

void BloodSplatterBehaviour::destroyNodes() {
    Scene::DestroySceneNode(bloodSplatterNode->GetID());
    Scene::DestroySceneNode(node->GetID());
}

bool BloodSplatterBehaviour::Update() {
    
    if (!bloodSplatterNode->GetParticleSystem()->IsEmitting()) {
        destroyNodes();
    }
    return true;
}

void BloodSplatterBehaviour::OnTriggerEnter(SceneNode *otherNode) {}
void BloodSplatterBehaviour::OnTriggerExit(SceneNode *otherNode) {}
void BloodSplatterBehaviour::OnTriggerStay(SceneNode *otherNode) {}

void BloodSplatterBehaviour::OnCollideEnter(SceneNode *otherNode) {}
void BloodSplatterBehaviour::OnCollideExit(SceneNode *otherNode) {}
void BloodSplatterBehaviour::OnCollideStay(SceneNode *otherNode) {}