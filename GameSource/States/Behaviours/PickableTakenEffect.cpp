//
//  PickupAnimation.cpp
//  UEAGame
//
//  Created by Olivier Legat on 16/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "PickableTakenEffect.h"

PickableTakenEffect::PickableTakenEffect(const String &effect){
    this->effect = effect;
    sparks = nullptr;
}
PickableTakenEffect::~PickableTakenEffect() {
}

bool PickableTakenEffect::Start() {
    String filename = "Data/ParticleSystems/" + effect + ".xml";
    
    sparksResource = ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor(filename, false));
    sparks = sparksResource->ParticleObject;
    node->SetParticleSystem(sparks);
    sparks->Emit();
    
    return true;
}

bool PickableTakenEffect::Update() {

    if (!sparks->IsEmitting()) {
        ResourceManager::Remove(sparksResource);
        Scene::DestroySceneNode(node->GetID());
    }
    
    return true;
}

void PickableTakenEffect::OnTriggerEnter(SceneNode *otherNode) {

}

void PickableTakenEffect::OnTriggerExit(SceneNode *otherNode) {
    
}

void PickableTakenEffect::OnTriggerStay(SceneNode *otherNode) {}

void PickableTakenEffect::OnCollideEnter(SceneNode *otherNode) {}
void PickableTakenEffect::OnCollideExit(SceneNode *otherNode) {}
void PickableTakenEffect::OnCollideStay(SceneNode *otherNode) {}