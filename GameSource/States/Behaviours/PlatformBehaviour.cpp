#include "PlatformBehaviour.h"
#include <GameApplication.h>

PlatformBehaviour::PlatformBehaviour(float modifier, bool rotate) {
    this->modifier = modifier;
    this->rotate = rotate;
}

PlatformBehaviour::~PlatformBehaviour() {
    
}

bool PlatformBehaviour::Start() {
    
    return true;
}

bool PlatformBehaviour::Update() {

    if(Timing::LastFrameTime != 0) { 
        flunct = sinf(Timing::CurrentTime) * 2.2f * modifier;

        // hack to allow the platform to move up and down when in-menu.
        if (Application::GameApp->GetCurrentState() == "MainMenuState")
            node->Translate(0,flunct,0,SPACE_LOCAL);
        
        if (rotate)
            node->Rotate (0,Timing::LastFrameTime * 10,0, SPACE_LOCAL);
    }
    return true;
}

void PlatformBehaviour::OnCollideEnter(SceneNode *otherNode) {
    if(otherNode->HasTag() 
       && (otherNode->GetTag()->first == "laser" || 
           otherNode->GetTag()->first == "enemylaser" || 
           otherNode->GetTag()->first == "orb")) 
    {
        Scene::DestroySceneNode(otherNode->GetID());
    }
}

void PlatformBehaviour::OnTriggerEnter(SceneNode *otherNode) {
    
}

void PlatformBehaviour::OnCollideExit(SceneNode *otherNode) {
    
}

void PlatformBehaviour::OnTriggerExit(SceneNode *otherNode) {
    
}

void PlatformBehaviour::OnCollideStay(SceneNode *otherNode) {
    
}

void PlatformBehaviour::OnTriggerStay(SceneNode *otherNode) {
    
}