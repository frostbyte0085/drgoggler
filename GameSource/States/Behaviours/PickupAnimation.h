//
//  PickupAnimation.h
//  UEAGame
//
//  Created by Olivier Legat on 16/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef PICKUP_ANIMATION_H
#define PICKUP_ANIMATION_H

#include <GameApplication.h>

class SceneNode;

class PickupAnimation : public SceneNodeBehaviour {
public:
    PickupAnimation();
    ~PickupAnimation();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
};


#endif
