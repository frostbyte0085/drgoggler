//
//  UfoAIBehaviour.h
//  UEAGame
//
//  Created by Olivier Legat on 16/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef UFO_AI_BEHAVIOUR_h
#define UFO_AI_BEHAVIOUR_h

#include <GameApplication.h>

class SceneNode;

class UfoAIBehaviour : public SceneNodeBehaviour {
public:
    UfoAIBehaviour(float radius, float angVel, int dir, float atk_radius, float atk_rate);
    ~UfoAIBehaviour();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
    void Lock() { isLocked = true; }
    
private:
    float radius;
    float angVel; // angluar velocity
    int dir;
    float atk_radius;
    
    Vector4 originalDiffuse;
    Vector4 originalAmbient;
    
    float fireCD;   // Fire CD in seconds
    float fireCDts; // Timestamp of last fire.

    bool isLocked;
};


#endif
