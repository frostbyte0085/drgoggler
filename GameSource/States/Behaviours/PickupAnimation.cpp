//
//  PickupAnimation.cpp
//  UEAGame
//
//  Created by Olivier Legat on 16/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "PickupAnimation.h"
#include "PickableTakenEffect.h"

PickupAnimation::PickupAnimation(){
}
PickupAnimation::~PickupAnimation() {
}

bool PickupAnimation::Start() {
    
    return true;
}

bool PickupAnimation::Update() {
    Quaternion angle = glm::rotate(Quaternion(), Timing::CurrentTime*100, Vector3(0,1,0));
    this->node->SetGlobalRotation(angle);
    
    return true;
}

void PickupAnimation::OnTriggerEnter(SceneNode *otherNode) {
}

void PickupAnimation::OnTriggerExit(SceneNode *otherNode) {
    
}

void PickupAnimation::OnTriggerStay(SceneNode *otherNode) {}

void PickupAnimation::OnCollideEnter(SceneNode *otherNode) {}
void PickupAnimation::OnCollideExit(SceneNode *otherNode) {}
void PickupAnimation::OnCollideStay(SceneNode *otherNode) {}