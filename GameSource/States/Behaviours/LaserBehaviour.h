#ifndef LASER_BEHAVIOUR_H_
#define LASER_BEHAVIOUR_H_

#include <GameApplication.h>

class SceneNode;

class LaserBehaviour : public SceneNodeBehaviour {
public:
    LaserBehaviour(Vector3 velocity);
	~LaserBehaviour();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
private:
	// we're gonna implement a mini physics engine to save memory
	// and time (time in money friend)
	Vector3 velocity;
	float timeFlying;
};

#endif