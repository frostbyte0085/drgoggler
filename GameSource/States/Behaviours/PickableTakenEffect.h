//
//  PickupAnimation.h
//  UEAGame
//
//  Created by Olivier Legat on 16/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef PICKUPABLE_TAKEN_EFFECT_H
#define PICKUPABLE_TAKEN_EFFECT_H

#include <GameApplication.h>

class SceneNode;

class PickableTakenEffect : public SceneNodeBehaviour {
public:
    PickableTakenEffect(const String &effect);
    ~PickableTakenEffect();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
private:
    ParticleSystemResource *sparksResource;
    ParticleSystem *sparks;
    String effect;
};


#endif
