//
//  UfoExplosionBehaviour.h
//  UEAGame
//
//  Created by Me on 12/3/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#ifndef UFO_EXPLOSION_BEHAVIOUR_H_DEF
#define UFO_EXPLOSION_BEHAVIOUR_H_DEF

#include <GameApplication.h>

class SceneNode;

class UfoExplosionBehaviour : public SceneNodeBehaviour {
public:
    UfoExplosionBehaviour(const Vector3 &ufoPosition);
    ~UfoExplosionBehaviour();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
private:
    void destroyNodes();
    Vector3 ufoPosition;
    float shockwaveAlpha;
    SceneNode *bigExplosionNode, *smallExplosionNode, *smokeExplosionNode, *shockwaveNode;
};

#endif
