#include "LaserBehaviour.h"
#include "Platform.h"
#include <GameApplication.h>

LaserBehaviour::LaserBehaviour(Vector3 velocity) {
	this->velocity = velocity;
	this->timeFlying = 0.0f;
}
LaserBehaviour::~LaserBehaviour() {
}

bool LaserBehaviour::Start() {
    
    return true;
}

bool LaserBehaviour::Update() {
    
    node->Translate(velocity * Timing::LastFrameTime, SPACE_GLOBAL);
    timeFlying += Timing::LastFrameTime*5;

    if(timeFlying > 6.0f) {
        Scene::DestroySceneNode(node->GetID());
    }
    
	return true;
}

void LaserBehaviour::OnCollideEnter(SceneNode *otherNode) {

}
void LaserBehaviour::OnTriggerEnter(SceneNode *otherNode) {}

void LaserBehaviour::OnCollideExit(SceneNode *otherNode) {}
void LaserBehaviour::OnTriggerExit(SceneNode *otherNode) {}
void LaserBehaviour::OnCollideStay(SceneNode *otherNode) {}
void LaserBehaviour::OnTriggerStay(SceneNode *otherNode) {}