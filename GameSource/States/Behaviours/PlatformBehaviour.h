#ifndef PLATFORM_BEHAVIOUR_H_DEF
#define PLATFORM_BEHAVIOUR_H_DEF

#include <GameApplication.h>

class SceneNode;

class PlatformBehaviour : public SceneNodeBehaviour {
public:
    PlatformBehaviour(float modifier=1.0f, bool rotate=true);
    ~PlatformBehaviour();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
    float GetFlunct() const { return flunct; }
    
private:
    float flunct;
    float modifier;
    bool rotate;
};

#endif
