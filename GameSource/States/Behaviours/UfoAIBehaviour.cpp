//
//  UfoAIBehaviour.cpp
//  UEAGame
//
//  Created by Olivier Legat on 16/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//
#include "UfoAIBehaviour.h"
#include "PlayerController.h"
#include "LaserBehaviour.h"
#include <Objects/ObjectManager.h>
#include <Objects/SharedResources.h>

#include "UfoExplosionBehaviour.h"


UfoAIBehaviour::UfoAIBehaviour(float radius, float angVel, int dir, float atk_radius, float atk_rate) {
    this->radius = radius;
    this->angVel = angVel;
    this->dir = dir;
    this->atk_radius = atk_radius;
    
    this->fireCD = 1.0f / atk_rate;
    this->fireCDts = 0.0f;
    
    isLocked = false;
}
UfoAIBehaviour::~UfoAIBehaviour() {
}

bool UfoAIBehaviour::Start() {
    node->GetMesh()->Play("ufo", ANIM_LOOP);

    originalAmbient = node->GetMesh()->GetSubmesh(0)->material->GetAmbientColor();
    originalDiffuse = node->GetMesh()->GetSubmesh(0)->material->GetAmbientColor();
    
    return true;
}

bool UfoAIBehaviour::Update() {
    
    // Move the UFO:
    float w = Timing::LastFrameTime * angVel;
    Quaternion angle = glm::rotate(Quaternion(), w, Vector3(0,1*dir,0));
    this->node->RotateAroundPoint(node->GetTranslation(), Vector3(0,0,0), angle, SPACE_LOCAL);
    
    // Find out where the player is and where this UFO is:
    Vector3 player = PlayerController::GetCurrentPlayerPosition();
    player.y += 50;
    Vector3 myUFO  = node->GetGlobalTranslation();
    
    // What the distance between these 2 nodes?
    Vector3 between = player - myUFO;
    float distance = glm::length(between);
    
    // Should I shoot the player?
    bool coolingDown = Timing::CurrentTime - fireCDts < fireCD;
    if(distance <= atk_radius && !coolingDown) {
        // YES! FIRE!!!!
        fireCDts = Timing::CurrentTime;
        
        // Create a new scene node parented to the root:
        SceneNode* laser = Scene::GetSceneNode(Scene::AddSceneNode("enemylaser",0));
        laser->SetTag("enemylaser", 2);
        laser->SetMesh(GameResources::singleGreenLaserMesh, CT_BOX);
        laser->SetCollisionGroup(OCG_UFOLASER);
        node->audioSources["laser"]->Play();
        
        // Set the position of the Laser
        laser->SetGlobalTranslation(myUFO);
        
        // Get the rotation:
        float compassAngle   = (float)atan2(between.z, between.x);
        float w = (float)sqrt(between.x*between.x + between.z*between.z);
        float elevationAngle = (float)atan2(between.y, w);
        
        elevationAngle = (float)RAD2DEG(elevationAngle);
        compassAngle   = -(float)RAD2DEG(compassAngle)-90;

#ifdef LOG_UFO_FIRE
        printf("@Player:  (%5.2f, %5.2f, %5.2f)\n",player.x, player.y, player.z);
        printf("@MyUFO:   (%5.2f, %5.2f, %5.2f)\n",myUFO.x, myUFO.y, myUFO.z);
        printf("@Between: (%5.2f, %5.2f, %5.2f)\n",between.x, between.y, between.z);
        printf("@elevation: %3.2f   @compass:%3.2f\n\n", elevationAngle, compassAngle);
#endif
        
        // Get the velocity (i.e. direction): 
        Vector3 vel = Vector3(0,0,-2000);
        vel = glm::rotate(vel, elevationAngle, Vector3(1,0,0)); // rotate v on x axis
		vel = glm::rotate(vel, compassAngle,   Vector3(0,1,0)); // rotate v on y axis
        laser->Rotate(elevationAngle, compassAngle, 0.0f, SPACE_GLOBAL);
        laser->AddBehaviour("blaster", new LaserBehaviour(vel));
    }
    else {
        // Arrrr... darn cooldown. We cannot fire captain.
    }
    
    return true;
}

void UfoAIBehaviour::OnTriggerEnter(SceneNode *otherNode) {}
void UfoAIBehaviour::OnTriggerExit(SceneNode *otherNode) {}
void UfoAIBehaviour::OnTriggerStay(SceneNode *otherNode) {}

void UfoAIBehaviour::OnCollideEnter(SceneNode *otherNode) {
    if(otherNode->HasTag() && (otherNode->GetTag()->first == "laser" || otherNode->GetTag()->first == "orb")) {
        // MAY DAY!!! MAY DAAAAAY!!!
//        UfoExplosion *explosion = new UfoExplosion(node->GetGlobalTranslation());
//        explosion->Explode();
        SceneNode *explosionNode = Scene::GetSceneNode (Scene::AddSceneNode("UfoExplosion"));
        explosionNode->AddBehaviour("UfoExplosionNodeBehaviour", new UfoExplosionBehaviour(node->GetGlobalTranslation()));
        
        Scene::DestroySceneNode(this->node->GetID());
        Scene::DestroySceneNode(otherNode->GetID());
        
        PlayerController::AddAKillToCurrentPlayer();
    }
}
void UfoAIBehaviour::OnCollideExit(SceneNode *otherNode) {}
void UfoAIBehaviour::OnCollideStay(SceneNode *otherNode) {}