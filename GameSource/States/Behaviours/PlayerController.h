#ifndef PLAYER_CONTROLLER_H_DEF
#define PLAYER_CONTROLLER_H_DEF

#include <GameApplication.h>

class SceneNode;

enum PlayerAnimation {
    // fly
    PA_FLYSTILL,
    PA_FLYFW,
    PA_FLYBW,
	PA_FLYLEFT,
	PA_FLYRIGHT,

    // grounded
    PA_RUN,
	PA_STAND,
    
    // misc
    PA_OTHER
};

enum PlayerState {
    PS_FLYING,
    PS_ENTER_FLYING,
    PS_WALKING,
    PS_ENTER_WALKING,
	PS_DEAD_DROWN,
	PS_DEAD_GROUND,
	PS_DEAD_AIR
};

class Hud;
class GeneratorTestState;

class PlayerController : public SceneNodeBehaviour {
    friend class Hud;
    friend class GeneratorTestState;
    
public:
    PlayerController(SceneNode* camera);
    ~PlayerController();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
    static Vector3 GetCurrentPlayerPosition();
    static void AddAKillToCurrentPlayer();
    
    bool IsDoomed(); // Is the character dead or dying?
    bool IsDying();  // Is the controlled cinematic scene being played?
    bool IsDead();   // Is the character dead? (is the cinematic scene over?)
    bool GotStar() {return gotStar;}
    
	static float lookSensitivity;
	static const float PLAYER_LOOK_SENSITIVITY_LOW;
	static const float PLAYER_LOOK_SENSITIVITY_NORMAL;
	static const float PLAYER_LOOK_SENSITIVITY_HIGH;

private:
    static PlayerController* currentPlayerController;
    
    // Movement:
    float moveX; //"lock left/right"
    float moveZ;
    float moveY;
	float moveStrafe;
    float stunnedStart;
    float stunDuration;
	float lookX, dlookX;
    
    // Jump / land:
    void enterWalking(const RayHitInfo &hit);
    void enterFlying();
    void startJumping();
    void jump(); // transition from walk to fly
    bool jumping;

	// Firing
	float fireCD;    // cd duration
	float fireCDts;  // cd timestamp
    
    float orbCD;
    float orbCDts;
    
	void fire();
    void fireLasers();
    void fireOrb();
    SceneNode *lockedUfo;
    unsigned int orbsLeft;
    
    float lockedPositionX;
    float lockedPositionY;

    void updateFlying();
    void updateWalking();
    void updateCamera();
    
    // Health, "Mana" and Score
    int hp;
    int mana;
	float timeFlying, timeToNextManaLoss;
    int score;
    int kills;
	int level;
    int roundsFired;
    float startedAt;
    float stoppedAt;
    bool gotStar;

	// persistent score (example of usage: Hud.cpp DrawYouAreDONE())
	static int previousScore;
	static int previousKills;
    static int previousRoundsFired;
	static int previousLevel;
	void ResetPreviousScore();
    
    // Death is a horrible and painful thing:
    float destinedToDieAt;
	void Die();
    void PositionKillCam();
	bool TakeDamage(int amount);

    // "Constant" thrust variable. Initialised once in the constructor
	PhysicalState* physics;
    float forwardThrust;
    float backwardThrust;
    float sidewardThrust;
    float downwardThrust;
    float upwardThrust;

    // Stunning stuff
    bool isStunned();
    void Stun(float duration); // block user input.
    
    void processLock();
    
    void ResetInput();
	void processFlyInput();
	void processFlyAnimation();
    
    void processWalkingInput();
    void processWalkingAnimation();
    
	void CheckPlaySingleAnimation();
	void PlaySingleAnimation(String animName, float duration);
	PlayerAnimation playAnim;
	bool playingSingleAnimation;
    
    PlayerState state;
    SceneNode *platform;
    Vector3 platformPoint;
    Box characterBoundingBox;

    bool renderCrossHair;
    
    SceneNode *jet1, *jet2;
    bool turnOffEngines;
    
	SceneNode *camera;
    
    ParticleSystem *splash;
};

#endif
