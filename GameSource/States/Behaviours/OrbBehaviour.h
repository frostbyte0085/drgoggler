#ifndef ORB_BEHAVIOUR_H_
#define ORB_BEHAVIOUR_H_

#include <GameApplication.h>

class SceneNode;

class OrbBehaviour : public SceneNodeBehaviour {
public:
    OrbBehaviour(SceneNode *target);
	~OrbBehaviour();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
private:
    SceneNode *target;
	Vector3 velocity;
	float timeFlying;
};

#endif