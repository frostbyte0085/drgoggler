//
//  BloodSplatterBehaviour.h
//  UEAGame
//
//  Created by Olivier Legat on 04/12/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef BLOOD_SPLATTER_BEHAVIOUR_H_
#define BLOOD_SPLATTER_BEHAVIOUR_H_

#include <GameApplication.h>

class SceneNode;

class BloodSplatterBehaviour : public SceneNodeBehaviour {
public:
    BloodSplatterBehaviour(const Vector3 &impactPosition);
    ~BloodSplatterBehaviour();
    
    bool Start();
    bool Update();
    
    void OnTriggerEnter(SceneNode *otherNode);
    void OnTriggerExit(SceneNode *otherNode);
    void OnTriggerStay(SceneNode *otherNode);
    
    void OnCollideEnter(SceneNode *otherNode);
    void OnCollideExit(SceneNode *otherNode);
    void OnCollideStay(SceneNode *otherNode);
    
private:
    void destroyNodes();
    Vector3 impactPosition;
    
    SceneNode *bloodSplatterNode;
};

#endif
