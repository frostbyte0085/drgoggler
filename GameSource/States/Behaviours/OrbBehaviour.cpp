#include "OrbBehaviour.h"
#include "Platform.h"
#include <GameApplication.h>

#include <Objects/SharedResources.h>

OrbBehaviour::OrbBehaviour(SceneNode *target) {
    velocity = Vector3(0,0,0);
    this->target = target;
	this->timeFlying = 0.0f;
}

OrbBehaviour::~OrbBehaviour() {
}

bool OrbBehaviour::Start() {
    
    node->SetParticleSystem(GameResources::orbParticles);
    node->GetParticleSystem()->Emit();
    return true;
}

bool OrbBehaviour::Update() {
    if (!target)
        return true;
    
    node->LookAt(target->GetGlobalTranslation() + Vector3(0,20,0));
    
    node->Translate(0,0,-Timing::LastFrameTime * 5000, SPACE_LOCAL);
    
    timeFlying += Timing::LastFrameTime;
    
    if(timeFlying > 6.0f) {
        Scene::DestroySceneNode(node->GetID());
    }
    
	return true;
}

void OrbBehaviour::OnCollideEnter(SceneNode *otherNode) {

}
void OrbBehaviour::OnTriggerEnter(SceneNode *otherNode) {}

void OrbBehaviour::OnCollideExit(SceneNode *otherNode) {}
void OrbBehaviour::OnTriggerExit(SceneNode *otherNode) {}
void OrbBehaviour::OnCollideStay(SceneNode *otherNode) {}
void OrbBehaviour::OnTriggerStay(SceneNode *otherNode) {}