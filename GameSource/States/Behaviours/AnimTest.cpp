#include "AnimTest.h"
#include "Platform.h"
#include <GameApplication.h>

AnimTest::AnimTest() {
}
AnimTest::~AnimTest() {
}

bool AnimTest::Start() {
    
    return true;
}

bool AnimTest::Update() {
    
    if (Input::IsPressed("Left")) 
		this->node->GetMesh()->Play("wave", ANIM_LOOP);

	if (Input::IsPressed("Backward"))
		this->node->GetMesh()->Play("jump", ANIM_ONCE);

	if (Input::IsPressed("Right"))
		this->node->GetMesh()->Play("land", ANIM_ONCE);

	if(Input::IsPressed("Forward"))
		this->node->GetMesh()->Play("run", ANIM_LOOP);

    return true;
}

void AnimTest::OnCollideEnter(SceneNode *otherNode) {}
void AnimTest::OnTriggerEnter(SceneNode *otherNode) {}
void AnimTest::OnCollideExit(SceneNode *otherNode) {}
void AnimTest::OnTriggerExit(SceneNode *otherNode) {}
void AnimTest::OnCollideStay(SceneNode *otherNode) {}
void AnimTest::OnTriggerStay(SceneNode *otherNode) {}