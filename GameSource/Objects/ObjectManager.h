#ifndef OBJECT_MANAGER_H_
#define OBJECT_MANAGER_H_

#include <GameApplication.h>
#include <Generator/LevelGenerator.h>

enum ObjectCollisionGroup {
    OCG_DEFAULT = 0,
    OCG_CHARACTER = 1,
    OCG_HEALTH = 2,
    OCG_FUEL = 3,
    OCG_ORB_REFILL = 4,
    OCG_PLATFORM = 5,
    OCG_UFO = 6,
    OCG_MONEY = 7,
    OCG_PLAYERLASER = 8,
    OCG_UFOLASER = 9
};

enum ObjectType {
	OTYPE_CHAR = 0,
	OTYPE_PLATFORM = 1,
	OTYPE_CASH_PICKUP = 2,
	OTYPE_HEALTH_PICKUP = 3,
	OTYPE_FUEL_PICKUP = 4
};

struct CharacterType {
	// Basic properties :
	String name;
	String suffix;
	unsigned int id;

	// External assets properties :
	String modelFilepath;
	float scale;

	// Game Logic properties :
	String characterType;
	int Hitpoints;
	int Manapoints;  // ("Mana" is the generic term, it can be fuel if need be)
	String Mananame; // different characters can have different mana types
};

struct PlatformType {
	// Basic properties :
	String name;
	String suffix;
	unsigned int id;

	// External assets properties :
	String modelFilepath;
	float scale;

	// Spawn pts :
	Vector3 fuelPackSpawn;
	Vector3 healthPackSpawn;
    Vector3 orbsPackSpawn;
	Vector3 cashSpawn;
	Vector3 playerSpawn;
	Vector3 critterSpawn;
};

struct PickupType {
	// Basic properties :
	String name;
	String suffix;
	unsigned int id;

	// External assets properties :
	String modelFilepath;
	float scale;

	// Game Logic properties :
	int value;
	String type;
};

struct DoodadType {
	// Basic properties:
	String name;
	String suffix;
	unsigned int id;

	// External assets properties :
	String modelFilepath;
	float scale;
};

class SceneNode;
class Object {
public:
	Object(SceneNode* node, ObjectType type, int typeID);
	~Object();

	SceneNode* GetSceneNode();
	ObjectType GetObjectType();
	int GetObjectTypeID();
private:
	SceneNode* node;
	ObjectType type;
	int typeID;
};

class ObjectManager {
public:
	static void LoadContent();

	// Number of objects imported
	static int N_PLATFORMS;
	static int N_HEALTH;
	static int N_FUELPACH;
	static int N_CASH;
	static int N_GENERATORS;

	// Object Directories
	static String DIR_OBJECT;
	static String DIR_PLATFORM;
	static String DIR_PICKUP;
	static String DIR_CASH;
	static String DIR_POWERUP;
	static String DIR_CHARACTER;
	static String DIR_GENERATOR;
	static String DIR_DOODADS;

	static Vector<CharacterType>            GetCharacterTypes();
	static Vector<PlatformType>             GetPlatformTypes();
	static Vector<PickupType>               GetCash();
	static Vector<PickupType>               GetHealthpacks();
	static Vector<PickupType>               GetFuelpacks();
	static Vector<LevelGeneratorParam>      GetGenerators();
	static Vector<DoodadType>				GetDoodads();

	static CharacterType& GetCharacterTypeByName(const String &name);
	static int GetCharacterTypeIDByName(const String &name);
    
    static PlatformType& GetPlatformTypeByID(int id);

	static int GetDoodadTypeIDByName(const String &name);
    
    static int GetItemTypeIDByName(const String &name);
    static PickupType& GetPickupTypeByName(const String &name);

	static LevelGeneratorParam& GetDefaultGenerator();
	static void SetDefaultGeneratorByID(int id);

private:
	static Vector<CharacterType>            characters; 
	static Vector<PlatformType>             platforms;
	static Vector<PickupType>               cash;
	static Vector<PickupType>               health;
	static Vector<PickupType>               fuelpack;
    static Vector<PickupType>               otherItems;
	static Vector<LevelGeneratorParam>      generators;
	static Vector<DoodadType>               doodads;

	static LevelGeneratorParam defaultGenerator;

	static void LoadPlatformTypes();
	static void ParsePlatformType(const String &filename, unsigned int id);

	static void LoadPickupTypes();
	static void ParsePickupType(const String &filename, unsigned int id);

	static void LoadCharacterTypes();
	static void ParseCharacterType(const String &filename, unsigned int id);
	
	static void LoadGenerators();
	static void ParseGenerator(const String &filename, unsigned int id);

	static void LoadDoodads();
	static void ParseDoodad(const String &filename, unsigned int id);
};

#endif