#include "SharedResources.h"

AudioClip *GameResources::explosionSound;
AudioClip *GameResources::playerLaserSound;
AudioClip *GameResources::ufoLaserSound;
AudioClip *GameResources::squirrelSound;
AudioClip *GameResources::jetpackSound;
AudioClip *GameResources::orbLockSound;
AudioClip *GameResources::orbLockNASound;
AudioClip *GameResources::coinCollectSound;
AudioClip *GameResources::fuelCollectSound;
AudioClip *GameResources::healthCollectSound;
AudioClip *GameResources::orbCollectSound;
AudioClip *GameResources::splashSound;

Mesh* GameResources::singleGreenLaserMesh;
Mesh* GameResources::dualRedLaserMesh;
Mesh* GameResources::orbMesh;
Texture* GameResources::lockWindowTexture;
Texture* GameResources::lockTargetTexture;
Texture* GameResources::lockTargetNATexture;

ParticleSystem* GameResources::orbParticles;

void GameResources::precache() {
    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/ParticleSystems/Fire.png"));
    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/ParticleSystems/FlameD.png"));
    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/ParticleSystems/FlameE.png"));
    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/ParticleSystems/Smoke.png"));
}

void GameResources::Initialize() {
    precache();
    
    // sounds
    explosionSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Explosion/explosionufo.wav", true))->AudioObject;
    playerLaserSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Lasers/playerLaser.wav", false))->AudioObject;
    ufoLaserSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Lasers/ufoLaser.wav", true))->AudioObject;
    squirrelSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Squirrel/squirrel.wav", true))->AudioObject;
    jetpackSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Jetpack/Jetpack.wav", false))->AudioObject;
    orbLockSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Lasers/orbLock.wav", false))->AudioObject;
    orbLockNASound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/Lasers/orbLockNA.wav", false))->AudioObject;
	coinCollectSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/coinCollect.wav", false))->AudioObject;
    fuelCollectSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/fuelCollect.wav", false))->AudioObject;
	healthCollectSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/healthCollect.wav", false))->AudioObject;
	orbCollectSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/orbCollect.wav", false))->AudioObject;
	splashSound = ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/splash.wav", false))->AudioObject;
    
    // meshes
    singleGreenLaserMesh = ResourceManager::Create<MESH>(new MeshResourceDescriptor("Data/Models/laser/single_laser.md5mesh"))->MeshObject;
    dualRedLaserMesh = ResourceManager::Create<MESH>(new MeshResourceDescriptor("Data/Models/laser/dual_laser.md5mesh"))->MeshObject;
    orbMesh = ResourceManager::Create<MESH>(new MeshResourceDescriptor("Data/Models/magicmissile/magicmissile.md5mesh"))->MeshObject;
    
    // misc ui textures
    lockWindowTexture = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/LockWindow.png"))->TextureObject;
    lockTargetTexture = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/LockTarget.png"))->TextureObject;
    lockTargetNATexture = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/LockTargetNA.png"))->TextureObject;

    // particles
    orbParticles = ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/Orb.xml", false))->ParticleObject;
}

void GameResources::Destroy() {
    
}