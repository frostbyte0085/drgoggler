//
//  SharedResources.cpp
//  UEAGame
//
//  Created by Olivier Legat on 04/12/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "SharedResources.h"

Font* SharedResources::arial_small;

AudioSource* SharedResources::ingameMusic;
AudioSource* SharedResources::menuMusic;

AudioSource* SharedResources::menuClickSource;

Material* SharedResources::skyboxMaterial;


void SharedResources::Initialize() {
    // fonts
    arial_small = ResourceManager::Create<FONT>(
        new FontResourceDescriptor("Data/UI/Fonts/arial_small.fnt"))->FontObject;
    
    // sounds    
    ingameMusic = new AudioSource(ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/ingame-kirby.ogg", false, true))->AudioObject);
    menuMusic = new AudioSource(ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/menu-theme.ogg", false, true))->AudioObject);
    
    menuClickSource = new AudioSource(ResourceManager::Create<AUDIO>(new AudioResourceDescriptor("Data/Sounds/click.wav", false, false))->AudioObject);

    // materials
    skyboxMaterial = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor("Data/Materials/SunnySkybox.mat", false))->MaterialObject;
}

void SharedResources::Destroy() {

}