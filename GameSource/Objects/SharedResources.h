//
//  SharedResources.h
//  UEAGame
//
//  Created by Olivier Legat on 04/12/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef SHARED_RESOURES_H_
#define SHARED_RESOURES_H_

#include <GameApplication.h>

class GameResources {
public:
    static void Initialize();
    static void Destroy();
    
    // sound resources
    static AudioClip *explosionSound;
    static AudioClip *playerLaserSound;
    static AudioClip *ufoLaserSound;
    static AudioClip *squirrelSound;
    static AudioClip *jetpackSound;
    static AudioClip *orbLockSound;
    static AudioClip *orbLockNASound;
	static AudioClip *coinCollectSound;
	static AudioClip *fuelCollectSound;
	static AudioClip *healthCollectSound;
	static AudioClip *orbCollectSound;
    static AudioClip *splashSound;
    
    // meshes
    static Mesh *singleGreenLaserMesh;
    static Mesh *dualRedLaserMesh;
    static Mesh *orbMesh;
    
    // particles
    static ParticleSystem *orbParticles;
    
    // misc ui textures
    static Texture *lockWindowTexture;
    static Texture *lockTargetTexture;
    static Texture *lockTargetNATexture;
    
private:
    static void precache();
    
};

class SharedResources {
public:
    static void Initialize();
    static void Destroy();
    
    static Font *arial_small;
    
    // game music
    static AudioSource *ingameMusic;
    static AudioSource *menuMusic;
    
    // game menu sound sources
    static AudioSource *menuClickSource;
    
    // materials
    static Material *skyboxMaterial;
    
private:
};

#endif
