#include "ObjectManager.h"

#define suffix(str) (str)=="" ? "" : (str)

Object::Object(SceneNode* node, ObjectType type, int typeID) {
	this->node = node;
	this->type = type;
	this->typeID = typeID;
}
Object::~Object() {
}
SceneNode* Object::GetSceneNode() {
	return this->node;
}
ObjectType Object::GetObjectType() {
	return this->type;
}
int Object::GetObjectTypeID() {
	return this->typeID;
}

Vector<CharacterType> ObjectManager::characters;
Vector<PlatformType>  ObjectManager::platforms;
Vector<PickupType>    ObjectManager::cash;
Vector<PickupType>    ObjectManager::health;
Vector<PickupType>    ObjectManager::fuelpack;
Vector<PickupType>    ObjectManager::otherItems;
Vector<DoodadType>    ObjectManager::doodads;

Vector<LevelGeneratorParam> ObjectManager::generators;
LevelGeneratorParam ObjectManager::defaultGenerator;

String ObjectManager::DIR_OBJECT    = "Data/Objects/";
String ObjectManager::DIR_PLATFORM  = DIR_OBJECT+"Platforms/";
String ObjectManager::DIR_PICKUP    = DIR_OBJECT+"Pickups/";
String ObjectManager::DIR_CASH      = DIR_PICKUP+"Cash/";
String ObjectManager::DIR_POWERUP   = DIR_PICKUP+"Powerups/";
String ObjectManager::DIR_CHARACTER = DIR_OBJECT+"Characters/";
String ObjectManager::DIR_GENERATOR = DIR_OBJECT+"Generators/";
String ObjectManager::DIR_DOODADS   = DIR_OBJECT+"Doodads/";

CharacterType& ObjectManager::GetCharacterTypeByName(const String &name) {
	for(int i = 0; i<(int)characters.size(); i++) {
		if(characters[i].name == name) return characters[i];
	}
}
int ObjectManager::GetCharacterTypeIDByName(const String &name) {
	for(int i = 0; i<(int)characters.size(); i++) {
		if(characters[i].name == name) return i;
	}
    return -1;
}
PlatformType& ObjectManager::GetPlatformTypeByID(int id) {
    return platforms[id];
}
LevelGeneratorParam& ObjectManager::GetDefaultGenerator() {
	return defaultGenerator;
}
void ObjectManager::SetDefaultGeneratorByID(int id) {
	defaultGenerator = generators[id];
}

Vector<CharacterType> ObjectManager::GetCharacterTypes(){
	return characters;
}
Vector<PlatformType> ObjectManager::GetPlatformTypes() {
	return platforms;
}
Vector<PickupType> ObjectManager::GetCash() {
	return cash;
}
Vector<PickupType> ObjectManager::GetHealthpacks() {
	return health;
}
Vector<PickupType> ObjectManager::GetFuelpacks() {
	return fuelpack;
}
int ObjectManager::GetItemTypeIDByName(const String &name) {
    int j = 0;
    for(int i = 0; i<(int)cash.size(); i++) {
		if(cash[i].name == name) return j;
        j++;
	}
    for(int i = 0; i<(int)health.size(); i++) {
		if(health[i].name == name) return j;
        j++;
	}
    for(int i = 0; i<(int)fuelpack.size(); i++) {
		if(fuelpack[i].name == name) return j;
        j++;
	}
    for(int i = 0; i<(int)otherItems.size(); i++) {
		if(otherItems[i].name == name) return j;
        j++;
	}
    return -1;
}
PickupType& ObjectManager::GetPickupTypeByName(const String &name) {
    for(int i = 0; i<(int)cash.size(); i++) {
		if(cash[i].name == name) return cash[i];
	}
    for(int i = 0; i<(int)health.size(); i++) {
		if(health[i].name == name) return health[i];
	}
    for(int i = 0; i<(int)fuelpack.size(); i++) {
		if(fuelpack[i].name == name) return fuelpack[i];
	}
    for(int i = 0; i<(int)otherItems.size(); i++) {
		if(otherItems[i].name == name) return otherItems[i];
	}
    return cash[0];
}

Vector<DoodadType> ObjectManager::GetDoodads() {
	return doodads;
}

void ObjectManager::LoadContent() {
	LoadPlatformTypes();
	LoadPickupTypes();
	LoadCharacterTypes();
	LoadGenerators();
	LoadDoodads();
}

void ObjectManager::LoadPlatformTypes() {
	// Load platform (we currently have 2 platforms, Windows and OSX)
	Vector<String> platformFilenames;
    Filesystem::EnumerateFiles(DIR_PLATFORM, "xml", platformFilenames);

	for (int i=0; i<(int)platformFilenames.size(); i++) {
		ParsePlatformType(
			Filesystem::GetFullPathForFile(DIR_PLATFORM+platformFilenames[i]) ,
			(unsigned int)i);
	}
}

void ObjectManager::ParsePlatformType(const String &name, unsigned int id) {
	// Get XML Nodes
	XMLNode mainNode     = XMLNode::openFileHelper(name.c_str(), "object");
    XMLNode nameNode     = mainNode.getChildNode("name");
	XMLNode suffixNode   = mainNode.getChildNode("suffix");
	XMLNode modelNode    = mainNode.getChildNode("model");
	XMLNode scaleNode    = mainNode.getChildNode("scale");
	XMLNode spawnNode    = mainNode.getChildNode("spawnpoints");
	XMLNode healthSpawn  = spawnNode.getChildNodeWithAttribute("spawnpoint", "type", "health");
	XMLNode fuelSpawn    = spawnNode.getChildNodeWithAttribute("spawnpoint", "type", "fuel");
    XMLNode orbsSpawn    = spawnNode.getChildNodeWithAttribute("spawnpoint", "type", "orbs");
	XMLNode cashSpawn    = spawnNode.getChildNodeWithAttribute("spawnpoint", "type", "cash");
	XMLNode playerSpawn  = spawnNode.getChildNodeWithAttribute("spawnpoint", "type", "player");
	XMLNode critterSpawn = spawnNode.getChildNodeWithAttribute("spawnpoint", "type", "critter");

	// Get values
	PlatformType plat;
	plat.name = String(nameNode.getText());
	plat.suffix = suffix(String(suffixNode.getText()));
	plat.id = id;
	plat.modelFilepath = String(modelNode.getText());
	plat.scale  = (float) atof(scaleNode.getText());

	// Set spawn points
	float x,y,z;
	x = (float) atof(healthSpawn.getAttribute("x"));
	y = (float) atof(healthSpawn.getAttribute("y"));
	z = (float) atof(healthSpawn.getAttribute("z"));
	plat.healthPackSpawn = Vector3(x,y,z);

	x = (float) atof(fuelSpawn.getAttribute("x"));
	y = (float) atof(fuelSpawn.getAttribute("y"));
	z = (float) atof(fuelSpawn.getAttribute("z"));
	plat.fuelPackSpawn = Vector3(x,y,z);
    
	x = (float) atof(orbsSpawn.getAttribute("x"));
	y = (float) atof(orbsSpawn.getAttribute("y"));
	z = (float) atof(orbsSpawn.getAttribute("z"));
	plat.orbsPackSpawn = Vector3(x,y,z);

	x = (float) atof(cashSpawn.getAttribute("x"));
	y = (float) atof(cashSpawn.getAttribute("y"));
	z = (float) atof(cashSpawn.getAttribute("z"));
	plat.cashSpawn = Vector3(x,y,z);

	x = (float) atof(playerSpawn.getAttribute("x"));
	y = (float) atof(playerSpawn.getAttribute("y"));
	z = (float) atof(playerSpawn.getAttribute("z"));
	plat.playerSpawn = Vector3(x,y,z);

	x = (float) atof(critterSpawn.getAttribute("x"));
	y = (float) atof(critterSpawn.getAttribute("y"));
	z = (float) atof(critterSpawn.getAttribute("z"));
	plat.critterSpawn = Vector3(x,y,z);

	platforms.push_back(plat);
}

void ObjectManager::LoadPickupTypes() {
	unsigned int ui = 0;

	// Load Cash (MONEY!!!!)
	Vector<String> pickupFilenames;
    Filesystem::EnumerateFiles(DIR_CASH, "xml", pickupFilenames);
	for (int i=0; i<(int)pickupFilenames.size(); i++) {
		ParsePickupType(
			Filesystem::GetFullPathForFile(DIR_CASH+pickupFilenames[i]),
			ui++);
	}

	// Load powerups (Go go power up.. ers?)
	pickupFilenames.clear();
	Filesystem::EnumerateFiles(DIR_POWERUP, "xml", pickupFilenames);
	for (int i=0; i<(int)pickupFilenames.size(); i++) {
		ParsePickupType(
			Filesystem::GetFullPathForFile(DIR_POWERUP+pickupFilenames[i]),
			ui++);
	}
}

void ObjectManager::ParsePickupType(const String &filename, unsigned int id) {
	// Get XML Nodes
	int r=0;
	XMLNode mainNode    = XMLNode::openFileHelper(filename.c_str(), "object");
    XMLNode nameNode    = mainNode.getChildNode("name");
	XMLNode modelNode   = mainNode.getChildNode("model");
    XMLNode suffixNode  = mainNode.getChildNode("suffix");
	XMLNode scaleNode   = mainNode.getChildNode("scale");
	XMLNode valueNode   = mainNode.getChildNode("value");

	// Set values:
	PickupType pick;
	pick.name = String(nameNode.getText());
	pick.suffix = suffix(String(suffixNode.getText()));
	pick.id = id;
	pick.modelFilepath = String(modelNode.getText());
	pick.scale  = (float) atof(scaleNode.getText());
	pick.value  = atoi(valueNode.getText());
	String className = String(mainNode.getAttribute("class"));
	pick.type   = className;

	if(className == "Cash")
		cash.push_back(pick);
	else if(className == "Health")
		health.push_back(pick);
	else if(className == "Fuel")
		fuelpack.push_back(pick);
    else
        otherItems.push_back(pick);
}

void ObjectManager::LoadCharacterTypes() {
	// Load characters (by reading characters... in a file)
	Vector<String> charFilename;
	Filesystem::EnumerateFiles(DIR_CHARACTER, "xml", charFilename);
	for (int i=0; i<(int)charFilename.size(); i++) {
		ParseCharacterType(
			Filesystem::GetFullPathForFile(DIR_CHARACTER+charFilename[i]),
			(unsigned int)i);
	}
}

void ObjectManager::ParseCharacterType(const String &filename, unsigned int id) {
	// Get XML Nodes
	XMLNode mainNode    = XMLNode::openFileHelper(filename.c_str(), "object");
    XMLNode nameNode    = mainNode.getChildNode("name");
	XMLNode modelNode   = mainNode.getChildNode("model");
    XMLNode suffixNode  = mainNode.getChildNode("suffix");
	XMLNode scaleNode   = mainNode.getChildNode("scale");
	XMLNode hitptNode   = mainNode.getChildNode("hitpoints");
	XMLNode manaNode    = mainNode.getChildNode("mana");

	// Set values:
	CharacterType chara;
	chara.name = String(nameNode.getText());
	chara.suffix = suffix(String(suffixNode.getText()));
	chara.id = id;
	chara.modelFilepath = String(modelNode.getText());
	chara.scale  = (float) atof(scaleNode.getText());
	chara.characterType   = String(mainNode.getAttribute("class"));
	chara.Hitpoints = atoi(hitptNode.getAttribute("amount"));
	chara.Manapoints = atoi(manaNode.getAttribute("amount"));
	chara.Mananame = manaNode.getAttribute("name");

	characters.push_back(chara);
}

void ObjectManager::LoadGenerators() {
	// Load generator (generate the generators... boo yeh.)
	Vector<String> genFilename;
	Filesystem::EnumerateFiles(DIR_GENERATOR, "xml", genFilename);
	for (int i=0; i<(int)genFilename.size(); i++) {
		ParseGenerator(
			Filesystem::GetFullPathForFile(DIR_GENERATOR+genFilename[i]),
			(unsigned int)i);
	}
	defaultGenerator = generators[0]; // TODO implement "default" attribute
}

void ObjectManager::ParseGenerator(const String &filename, unsigned int id) {
	LevelGeneratorParam param;
	XMLNode mainNode    = XMLNode::openFileHelper(filename.c_str(), "object");
	XMLNode paramNode;

	int i=0, n=mainNode.nChildNode();
	for(i; i<n; i++) {
		paramNode = mainNode.getChildNode("param", i);
		String name = String(paramNode.getAttribute("name"));
		const char* value = paramNode.getAttribute("value");

		if(name == "MIN_PLATFORM_DIST") {
			param.MIN_PLATFORM_DIST = (float) atof(value);
		}
		else if(name == "MAX_PLATFORM_DIST") {
			param.MAX_PLATFORM_DIST = (float) atof(value);
		}
        else if(name == "DIST_SCALE_RATIO") {
            // Parse platform ratio
            if(String(paramNode.getAttribute("plats"))=="off") {
                param.DIST_SCALE_RATIO_PLATS_ON = false;
            }
            else {
                value = paramNode.getAttribute("plats");
                param.DIST_SCALE_RATIO_PLATS = (float)atof(value);
                param.DIST_SCALE_RATIO_PLATS_ON = true;
            }
            // Parse ufo ratio
            if(String(paramNode.getAttribute("boogies"))=="off") {
                param.DIST_SCALE_RATIO_UFO_ON = false;
            }
            else {
                value = paramNode.getAttribute("boogies");
                param.DIST_SCALE_RATIO_UFO = (float)atof(value);
                param.DIST_SCALE_RATIO_UFO_ON = true;
            }
		}
		else if(name == "MIN_ELEV_ANGLE") {
			param.MIN_ELEV_ANGLE = (float) atof(value);
		}
		else if(name == "MAX_ELEV_ANGLE") {
			param.MAX_ELEV_ANGLE = (float) atof(value);
		}
		else if(name == "MIN_X_POINT") {
			param.MIN_X_POINT = (float) atof(value);
		}
		else if(name == "MAX_X_POINT") {
			param.MAX_X_POINT = (float) atof(value);
		}
		else if(name == "MIN_Z_POINT") {
			param.MIN_Z_POINT = (float) atof(value);
		}
		else if(name == "MAX_Z_POINT") {
			param.MAX_Z_POINT = (float) atof(value);
		}
		else if(name == "N_PLATFORMS") {
			param.N_PLATFORMS = atoi(value);
		}
		else if(name == "MIN_ENEMIES") {
			param.MIN_ENEMIES = atoi(value);
		}
		else if(name == "MAX_ENEMIES") {
			param.MAX_ENEMIES = atoi(value);
		}
		else if(name == "MIN_ORBIT_RADIUS") {
			param.MIN_ORBIT_RADIUS = (float) atof(value);
		}
		else if(name == "MAX_ORBIT_RADIUS") {
			param.MAX_ORBIT_RADIUS = (float) atof(value);
		}
        else if(name == "MIN_SPEED") {
			param.MIN_SPEED = (float) atof(value);
		}
		else if(name == "MAX_SPEED") {
			param.MAX_SPEED = (float) atof(value);
		}
		else if(name == "UFO_RADIUS") {
			param.UFO_RADIUS = (float) atof(value);
		}
        else if(name == "UFO_ATK_RADIUS") {
			param.UFO_ATK_RADIUS = (float) atof(value);
		}
        else if(name == "UFO_ATK_RATE") {
			param.UFO_ATK_RATE = (float) atof(value);
		}
		else if(name == "MIN_HP_GAP") {
			param.MIN_HP_GAP = atoi(value);
		}
		else if(name == "MAX_HP_GAP") {
			param.MAX_HP_GAP = atoi(value);
		}
		else if(name == "MIN_FP_GAP") {
			param.MIN_FP_GAP = atoi(value);
		}
		else if(name == "MAX_FP_GAP") {
			param.MAX_FP_GAP = atoi(value);
		}
        else if(name == "MIN_ORB_GAP") {
			param.MIN_ORB_GAP = atoi(value);
		}
		else if(name == "MAX_ORB_GAP") {
			param.MAX_ORB_GAP = atoi(value);
		}
		else if(name == "PROB_1EURO") {
			param.PROB_1EURO = (float) atof(value);
		}
		else if(name == "PROB_2EURO") {
			param.PROB_2EURO = (float) atof(value);
		}
		else if(name == "PROB_5EURO") {
			param.PROB_5EURO = (float) atof(value);
		}
		else if(name == "PROB_GEM") {
			param.PROB_GEM = (float) atof(value);
		}
	}
	generators.push_back(param);
}

void ObjectManager::LoadDoodads() {
	// Load doodads (this is totally a Warcraft 3 World Editor reference :P)
	Vector<String> dooFilename;
	Filesystem::EnumerateFiles(DIR_DOODADS, "xml", dooFilename);
	for (int i=0; i<(int)dooFilename.size(); i++) {
		ParseDoodad(
			Filesystem::GetFullPathForFile(DIR_DOODADS+dooFilename[i]),
			(unsigned int)i);
	}
}

void ObjectManager::ParseDoodad(const String &filename, unsigned int id) {
	// Get XML Nodes
	XMLNode mainNode    = XMLNode::openFileHelper(filename.c_str(), "object");
    XMLNode nameNode    = mainNode.getChildNode("name");
	XMLNode modelNode   = mainNode.getChildNode("model");
    XMLNode suffixNode  = mainNode.getChildNode("suffix");
	XMLNode scaleNode   = mainNode.getChildNode("scale");

	// Set values:
	DoodadType doo;
	doo.name = String(nameNode.getText());
	doo.suffix = suffix(String(suffixNode.getText()));
	doo.id = id;
	doo.modelFilepath = String(modelNode.getText());
	doo.scale  = (float) atof(scaleNode.getText());

	doodads.push_back(doo);
}

int ObjectManager::GetDoodadTypeIDByName(const String &name) {
	for(int i = 0; i<(int)doodads.size(); i++) {
		if(doodads[i].name == name) return i;
	}
    return -1;
}