#include "PlatformGenerator.h"

#include <GameApplication.h>
#include <Objects/ObjectManager.h>

#include <States/Behaviours/PlatformBehaviour.h>

#define MAX_ATTEMPTS 1000

PlatformGenerator::PlatformGenerator(PlatformGeneratorParam args) {
	this->args = args;
}

PlatformGenerator::~PlatformGenerator() {
    // delete platforms[i] ?
	platforms.clear();
}

int PlatformGenerator::Generate() {
	Object* obj = GetPlatformByType(Vector3(0), 0, 3);
	Vector<PlatformType> plats = ObjectManager::GetPlatformTypes(); 
    PlatformType& platType0 = plats[obj->GetObjectTypeID()];
	platforms.push_back(obj);

	SceneNode* plat = obj->GetSceneNode();
    nextScaleFactor = platType0.scale;

	for(int i=1; i<args.N_PLATFORMS; i++) {
		// Get a position of the next platform :
		Vector3 pos0 = plat->GetGlobalTranslation();
		Vector3 vect = GetRandomVector();
		Vector3 pos1  = vect + pos0;

		// Place a random platform type and record it :
		obj = GetRandomPlatform(pos1, i);
		plat = obj->GetSceneNode();
		platforms.push_back(obj);
	}

	Log("PlatformGenerator::Generate() successful \n");

	return args.N_PLATFORMS;
}

Vector3 PlatformGenerator::GetRandomVector() {
	float distance;
	for(int i=0; i<MAX_ATTEMPTS; i++) {
		// Create random arguments for a vector:
		distance = Math::RandomF(args.MIN_PLATFORM_DIST,  args.MAX_PLATFORM_DIST);
        if(args.DIST_SCALE_RATIO_ON) distance *= nextScaleFactor * args.DIST_SCALE_RATIO;
		float compassAngle = Math::RandomF(0, 360);
		float elevationAngle = Math::RandomF(args.MIN_ELEV_ANGLE, args.MAX_ELEV_ANGLE);

		// Calculate that vector:
		Vector3 v = Vector3(0, 0, -distance);
		v = glm::rotate(v, elevationAngle, Vector3(1,0,0)); // rotate v on x axis
		v = glm::rotate(v, compassAngle,   Vector3(0,1,0)); // rotate v on y axis
		
		// Check if that vector is in range:
		if( v.x >= args.MIN_X_POINT && v.x <= args.MAX_X_POINT &&
			v.z >= args.MIN_Z_POINT && v.z <= args.MAX_Z_POINT) {
			Log("PlatformGenerator::GetRandomVector: generated vector ("+
				StringOperations::ToString<float>(v.x) + "," +
				StringOperations::ToString<float>(v.y) + "," +
				StringOperations::ToString<float>(v.z) +")");
			Log("  with angle1= "+ StringOperations::ToString<float>(compassAngle)+
				"  angle2="+ StringOperations::ToString<float>(elevationAngle)+
				"  distance="+ StringOperations::ToString<float>(distance));

			return v;
		}

		// TODO check if it collides with other platforms.
	}
	Log("PlatformGenerator::GetRandomVector: no vector found.\n");
	// error: no suitable random vector could be found.
	return Vector3(0,distance,0);
}

Object* PlatformGenerator::GetRandomPlatform(Vector3 position, int id) {
	// Get object:
	Vector<PlatformType> platforms = ObjectManager::GetPlatformTypes();
	int numberOfPlatformVariants = (int)platforms.size();

	// Get strings:
	int whichPlatform = rand() % numberOfPlatformVariants;
    return GetPlatformByType(position, id, whichPlatform);
}

Object* PlatformGenerator::GetPlatformByType(Vector3 position, int id, int platType) {
    // Get object:
	Vector<PlatformType> platforms = ObjectManager::GetPlatformTypes();
    
	// Get strings:
	String platformFilename = platforms[platType].modelFilepath;
	String sceneNodeName = "platform" + StringOperations::ToString<int>(id);
    
	// Create the scene node (platform) and return it:
	SceneNode *platformNode = Scene::GetSceneNode(Scene::AddSceneNode(sceneNodeName,0));
    platformNode->SetMesh ( ResourceManager::Create<MESH> (new MeshResourceDescriptor(platformFilename))->MeshObject, CT_BOX);
	platformNode->SetGlobalTranslation(position);
	platformNode->SetScale(Vector3(platforms[platType].scale));
    nextScaleFactor = platforms[platType].scale;
	platformNode->SetTag("platformTypeID", platType);
    platformNode->SetCollisionGroup (OCG_PLATFORM);
    platformNode->AddBehaviour("hover", new PlatformBehaviour());
    
	Log("PlatformGenerator::GetRandomPlatform() positioned platform " + StringOperations::ToString<int>(id));
	return new Object(platformNode, OTYPE_PLATFORM, platType);
}