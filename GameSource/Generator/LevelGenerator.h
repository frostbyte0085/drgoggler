#ifndef LEVEL_GENERATOR_H_DEF
#define LEVEL_GENERATOR_H_DEF

#include <GameApplication.h>

class EnemyGenerator;
class PlatformGenerator;
class PickupGenerator;

/**
 * RECOMMENDED CONSTRAINTS:
 *
 * #  MIN_PLATFORM_DIST > MAX_ORBIT_RADIUS + (1/2) * radius of ufo
 * #  MAX_ORBIT_RADIUS  > 2 * radius of ufo * (MAX_ENEMIES + 1)
 * #  MAX_X_POINT - MIN_X_POINT > MAX_PLATFORM_DIST
 * #  MAX_Y_POINT - MIN_Y_POINT > MAX_PLATFORM_DIST
 * #  MIN_ENEMIES = 0
 * #  sum(PROB_*) < 1
 *
 *
 * MUST RESPECT:
 *
 * #  sum(PROB_*) <= 1
 */

struct LevelGeneratorParam {
	// Defines the rectangle in which platforms can be placed.
	float MIN_X_POINT;
	float MAX_X_POINT;
	float MIN_Z_POINT;
	float MAX_Z_POINT;

	// Defines the minimum / maximum distance between adjacent platforms.
	float MIN_PLATFORM_DIST;
	float MAX_PLATFORM_DIST;

	// Define the minimum / maximum elevation angles for new platforms.
	float MIN_ELEV_ANGLE;
	float MAX_ELEV_ANGLE;

	// Defines the minimum / maximum radius of orbit of the UFOs.
	float MIN_ORBIT_RADIUS;
	float MAX_ORBIT_RADIUS;
    
    // Defines the minimum / maximum rotational speed of the UFOs:
    float MIN_SPEED;
    float MAX_SPEED;
    
    // Defines distance : scale ratio
    float DIST_SCALE_RATIO_PLATS;
    bool  DIST_SCALE_RATIO_PLATS_ON;
    float DIST_SCALE_RATIO_UFO;
    bool  DIST_SCALE_RATIO_UFO_ON;

	// Defines the minimum / maximum number of enemies per platform.
	int MIN_ENEMIES;
	int MAX_ENEMIES;
    
    // Define the minimum distance between the centers of enemies.
    float UFO_RADIUS;
    
    // Define the distance from the player when UFOs start shooting.
    float UFO_ATK_RADIUS;
    float UFO_ATK_RATE;   // Attacks / second

	// Defines the minimum / maximum # of platforms before encountering a HealthPack.
	int MIN_HP_GAP;
	int MAX_HP_GAP;

	// Defines the minimum / maximum # of platforms before encountering a FuelPack.
	int MIN_FP_GAP;
	int MAX_FP_GAP;
    
    // Defines the minimum / maximum # of platforms before encountering a Orb refill.
	int MIN_ORB_GAP;
	int MAX_ORB_GAP;

	// Defines the number of platforms to place.
	int N_PLATFORMS;

	// Define the probabilities [0,1] of the money appearances.
	float PROB_1EURO;
	float PROB_2EURO;
	float PROB_5EURO;
	float PROB_GEM;
};

class Object;
class LevelGenerator {
public:
	LevelGenerator(LevelGeneratorParam args);
	~LevelGenerator();

	void Generate();
	void Clear();
    
    Vector<Object*> GetPlatforms() {return platforms;}
    Vector<Object*> GetUfos() {return ufos;}
    Vector<Object*> GetHealthpacks() {return hpacks;}
    Vector<Object*> GetFuelpacks() {return fpacks;}
    Vector<Object*> GetMoney() {return money;}

private:
	LevelGeneratorParam args;

	PlatformGenerator *genplat;
	EnemyGenerator *genufos;
	PickupGenerator *genpick;

	// All the Scene nodes:
	Vector<Object*> platforms, ufos, hpacks, fpacks, money;

	void GeneratePlatforms();
	void GenerateEnemies();
	void GeneratePickups();
	void GenerateSquirrel();
};

#endif