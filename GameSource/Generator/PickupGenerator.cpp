#include "PickupGenerator.h"
#include <States/Behaviours/PickupAnimation.h>

#define RandomHP() Math::Random(args.MIN_HP_GAP, args.MAX_HP_GAP)
#define RandomFP() Math::Random(args.MIN_FP_GAP, args.MAX_FP_GAP)
#define RandomORB() Math::Random(args.MIN_ORB_GAP, args.MAX_ORB_GAP)

PickupGenerator::PickupGenerator(PickupGeneratorParam args) {
	this->args = args;

	this->spwn1euro = args.PROB_1EURO * 100;
	this->spwn2euro = spwn1euro + args.PROB_2EURO * 100;
	this->spwn5euro = spwn2euro + args.PROB_5EURO * 100;
	this->spwngem   = spwn5euro + args.PROB_GEM * 100;
}
PickupGenerator::~PickupGenerator() {
	healthpacks.clear();
	fuelpacks.clear();
}

int PickupGenerator::Generate(const Vector<Object*> &platforms, int nPlats) {
	int nPickups = 0;

	int index = RandomHP();
	while(index < nPlats) {
		addHealth(platforms[index]);
		index += RandomHP();
		nPickups++;
	}

	index = RandomFP();
	while(index < nPlats) {
		addFuel(platforms[index]);
		index += RandomFP();
		nPickups++;
	}
    
    index = RandomORB();
	while(index < nPlats) {
		addOrb(platforms[index]);
		index += RandomORB();
		nPickups++;
	}

    // Note: no money on the first or last platform
	for(index=1; index<nPlats-1; index++)
		addMoney(platforms[index]);

    addStar(platforms[nPlats-1]);
    
	return nPickups;
}

void PickupGenerator::addHealth(Object * plat) {
    PlatformType platType = ObjectManager::GetPlatformTypeByID(plat->GetObjectTypeID());
	PickupType pick = ObjectManager::GetHealthpacks()[0];
	String filename = pick.modelFilepath;
	SceneNode * hp = Scene::GetSceneNode(Scene::AddSceneNode(pick.name, plat->GetSceneNode()->GetID()));
	hp->SetMesh(ResourceManager::Create<MESH> (new MeshResourceDescriptor(filename))->MeshObject, CT_SPHERE);
    hp->SetTranslation(platType.healthPackSpawn); // TODO get spwn pt of this platform.
    hp->SetScale(Vector3(pick.scale));
    hp->GetCollider()->IsTrigger = true;
	hp->SetTag ("health", pick.value);
    hp->SetCollisionGroup (OCG_HEALTH);
    hp->AddBehaviour("SpinAnimation", new PickupAnimation());
    
	healthpacks.push_back(new Object(hp, OTYPE_HEALTH_PICKUP, 0));
}

void PickupGenerator::addFuel(Object * plat) {
    PlatformType platType = ObjectManager::GetPlatformTypeByID(plat->GetObjectTypeID());
	PickupType pick = ObjectManager::GetFuelpacks()[0];
	String filename = pick.modelFilepath;
	SceneNode * fp = Scene::GetSceneNode(Scene::AddSceneNode(pick.name, plat->GetSceneNode()->GetID()));
	fp->SetMesh(ResourceManager::Create<MESH> (new MeshResourceDescriptor(filename))->MeshObject, CT_SPHERE);
	fp->SetTranslation(platType.fuelPackSpawn); 
    fp->SetScale(Vector3(pick.scale));
    fp->GetCollider()->IsTrigger = true;
	fp->SetTag ("fuel", pick.value);
    fp->SetCollisionGroup (OCG_FUEL);
    fp->AddBehaviour("SpinAnimation", new PickupAnimation());
    
    fuelpacks.push_back(new Object(fp, OTYPE_FUEL_PICKUP, 0));
}

void PickupGenerator::addOrb(Object * plat) {
    PlatformType platType = ObjectManager::GetPlatformTypeByID(plat->GetObjectTypeID());
	PickupType pick = ObjectManager::GetPickupTypeByName("Orbs");
	String filename = pick.modelFilepath;
	SceneNode * orb = Scene::GetSceneNode(Scene::AddSceneNode(pick.name, plat->GetSceneNode()->GetID()));
	orb->SetMesh(ResourceManager::Create<MESH> (new MeshResourceDescriptor(filename))->MeshObject, CT_SPHERE);
	orb->SetTranslation(platType.orbsPackSpawn); 
    orb->SetScale(Vector3(pick.scale));
    orb->GetCollider()->IsTrigger = true;
	orb->SetTag ("orbrefill", pick.value);
    orb->SetCollisionGroup (OCG_ORB_REFILL);
    orb->AddBehaviour("SpinAnimation", new PickupAnimation());
    orb->SetParticleSystem(ResourceManager::Create<PARTICLE>(new ParticleSystemResourceDescriptor("Data/ParticleSystems/Orb.xml", false))->ParticleObject);
    orb->GetParticleSystem()->Emit();
    
    //fuelpacks.push_back(new Object(orb, OTYPE_FUEL_PICKUP, 0));
}

void PickupGenerator::addMoney(Object * plat) {
	int i = getRandomMoney();
	if(i==-1) return;
    PlatformType platType = ObjectManager::GetPlatformTypeByID(plat->GetObjectTypeID());
	PickupType pick = ObjectManager::GetCash()[i];

	SceneNode * coin = Scene::GetSceneNode(Scene::AddSceneNode("coin", plat->GetSceneNode()->GetID()));
	Log("PickupGenerator::addMoney "+pick.name+" "+pick.suffix);

	coin->SetMesh(ResourceManager::Create<MESH> (new MeshResourceDescriptor(pick.modelFilepath))->MeshObject, CT_SPHERE);
	coin->SetTranslation(platType.cashSpawn);
    coin->SetScale(Vector3(pick.scale));
    coin->GetCollider()->IsTrigger = true;
	coin->SetTag("money", pick.value);
    coin->SetCollisionGroup (OCG_MONEY);
    coin->AddBehaviour("SpinAnimation", new PickupAnimation());
    
	money.push_back(new Object(coin, OTYPE_CASH_PICKUP, i));
}

int PickupGenerator::getRandomMoney() {
	float dice = Math::RandomF(0,100);

	if(dice <= spwn1euro)
		return 0;

	else if(dice <= spwn2euro)
		return 1;

	else if(dice <= spwn5euro)
		return 2;

	else if(dice <= spwngem)
		return 3;

	else
		return -1;
}

void PickupGenerator::addStar(Object * plat) {
    PlatformType platType = ObjectManager::GetPlatformTypeByID(plat->GetObjectTypeID());
	PickupType pick = ObjectManager::GetPickupTypeByName("Star");
    
	SceneNode * coin = Scene::GetSceneNode(Scene::AddSceneNode("star", plat->GetSceneNode()->GetID()));
	Log("PickupGenerator::addStar "+pick.name+" "+pick.suffix);
    
	coin->SetMesh(ResourceManager::Create<MESH> (new MeshResourceDescriptor(pick.modelFilepath))->MeshObject, CT_SPHERE);
	coin->SetTranslation(platType.cashSpawn);
    coin->SetScale(Vector3(pick.scale));
    coin->GetCollider()->IsTrigger = true;
	coin->SetTag("star", pick.value);
    coin->SetCollisionGroup (OCG_MONEY);
    coin->AddBehaviour("SpinAnimation", new PickupAnimation());
    
	money.push_back(new Object(coin, OTYPE_CASH_PICKUP, pick.id));
}