#include "LevelGenerator.h"
#include "PlatformGenerator.h"
#include "EnemyGenerator.h"
#include "PickupGenerator.h"

#include "../States/Behaviours/PlatformBehaviour.h"

#include <GameApplication.h>
#include <Objects/ObjectManager.h>
#include <Objects/SharedResources.h>

#define MAX_ATTEMPTS 1000

LevelGenerator::LevelGenerator(LevelGeneratorParam args) {
	this->args = args;

	this->genplat = nullptr;
	this->genpick = nullptr;
	this->genufos = nullptr;
}

LevelGenerator::~LevelGenerator() {
	if(genplat!=nullptr) delete genplat;
	if(genufos!=nullptr) delete genufos;
	if(genpick!=nullptr) delete genpick;
}

void LevelGenerator::Generate() {
	GeneratePlatforms();
	GenerateSquirrel();
	GenerateEnemies();
	GeneratePickups();

	// "Generate" water
	SceneNode *water1 = Scene::GetSceneNode(Scene::AddSceneNode("water1"));
    
	int doodadID1 = ObjectManager::GetDoodadTypeIDByName("Water1");
	DoodadType doodad1 = ObjectManager::GetDoodads()[doodadID1];
    
    water1->SetMesh ( ResourceManager::Create<MESH>(new MeshResourceDescriptor(doodad1.modelFilepath))->MeshObject );
    water1->SetTranslation(0,-350,0);
	water1->SetScale(Vector3(doodad1.scale, 1, doodad1.scale));
}

void LevelGenerator::Clear() {
	// TODO
}

void LevelGenerator::GeneratePlatforms() {
	PlatformGeneratorParam param;
	param.MIN_PLATFORM_DIST = args.MIN_PLATFORM_DIST;
	param.MAX_PLATFORM_DIST = args.MAX_PLATFORM_DIST;
    param.DIST_SCALE_RATIO    = args.DIST_SCALE_RATIO_PLATS;
    param.DIST_SCALE_RATIO_ON = args.DIST_SCALE_RATIO_PLATS_ON;
	param.MIN_ELEV_ANGLE = args.MIN_ELEV_ANGLE;
	param.MAX_ELEV_ANGLE = args.MAX_ELEV_ANGLE;
	param.MIN_X_POINT = args.MIN_X_POINT;
	param.MAX_X_POINT = args.MAX_X_POINT;
	param.MIN_Z_POINT = args.MIN_Z_POINT;
	param.MAX_Z_POINT = args.MAX_Z_POINT;
	param.N_PLATFORMS = args.N_PLATFORMS;

	this->genplat = new PlatformGenerator(param);
	this->genplat->Generate();
	this->platforms = this->genplat->GetGeneratedPlatforms();
}

void LevelGenerator::GenerateEnemies() {
	EnemyGeneratorParam param;
	param.MIN_ORBIT_RADIUS = args.MIN_ORBIT_RADIUS;
	param.MAX_ORBIT_RADIUS = args.MAX_ORBIT_RADIUS;
    param.DIST_SCALE_RATIO    = args.DIST_SCALE_RATIO_UFO;
    param.DIST_SCALE_RATIO_ON = args.DIST_SCALE_RATIO_UFO_ON;
    param.MIN_ENEMIES = args.MIN_ENEMIES;
	param.MAX_ENEMIES = args.MAX_ENEMIES;
    param.UFO_RADIUS = args.UFO_RADIUS;
    param.UFO_ATK_RADIUS = args.UFO_ATK_RADIUS;
    param.UFO_ATK_RATE   = args.UFO_ATK_RATE;
    param.MIN_SPEED = args.MIN_SPEED;
    param.MAX_SPEED = args.MAX_SPEED;

	this->genufos = new EnemyGenerator(param);
	this->genufos->Generate(this->platforms, args.N_PLATFORMS);
	this->ufos = this->genufos->GetGeneratedEnemies();
}

void LevelGenerator::GeneratePickups() {
	PickupGeneratorParam param;
	param.MAX_FP_GAP = args.MAX_FP_GAP;
	param.MIN_FP_GAP = args.MIN_FP_GAP;
	param.MAX_HP_GAP = args.MAX_HP_GAP;
	param.MIN_HP_GAP = args.MIN_HP_GAP;
    param.MAX_ORB_GAP = args.MAX_ORB_GAP;
	param.MIN_ORB_GAP = args.MIN_ORB_GAP;

	param.PROB_1EURO = args.PROB_1EURO;
	param.PROB_2EURO = args.PROB_2EURO;
	param.PROB_5EURO = args.PROB_5EURO;
	param.PROB_GEM   = args.PROB_GEM;

	this->genpick = new PickupGenerator(param);
	this->genpick->Generate(platforms, args.N_PLATFORMS);
	this->fpacks = this->genpick->GetGeneratedFuelPacks();
	this->hpacks = this->genpick->GetGeneratedHealthPacks();
}

void LevelGenerator::GenerateSquirrel() {
    int i = Math::Random(0, platforms.size()-1);
    
    CharacterType conkerChar = ObjectManager::GetCharacterTypeByName("Conker");
	SceneNode *conker = Scene::GetSceneNode(Scene::AddSceneNode("conker", platforms[i]->GetSceneNode()->GetID()));
	Mesh * conkermesh = ResourceManager::Create<MESH> (new MeshResourceDescriptor(conkerChar.modelFilepath))->MeshObject;
	conker->SetMesh(conkermesh, -1); // Conker needs no collider... he does NOTHING!! :P
	conker->SetScale(Vector3(conkerChar.scale));
    conker->ScaleFromParent = false;
	PlatformType& platType = ObjectManager::GetPlatformTypes()[platforms[i]->GetObjectTypeID()];
	Vector3 spawnPt = platType.critterSpawn;
	conker->SetTranslation(spawnPt);
	conker->GetMesh()->Play("eat", ANIM_LOOP);
    conker->audioSources["chunk"] = new AudioSource (GameResources::squirrelSound);
}