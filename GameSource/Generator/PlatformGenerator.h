#ifndef PLATFORM_GENERATOR_H_DEF
#define PLATFORM_GENERATOR_H_DEF

#include <GameApplication.h>
#include <Objects/ObjectManager.h>

/**
 * RECOMMENDED CONSTRAINTS:
 *
 * #  MIN_PLATFORM_DIST > MAX_ORBIT_RADIUS + (1/2) * radius of ufo
 * #  MAX_ORBIT_RADIUS  > 2 * radius of ufo * (MAX_ENEMIES + 1)
 * #  MAX_X_POINT - MIN_X_POINT > MAX_PLATFORM_DIST
 * #  MAX_Y_POINT - MIN_Y_POINT > MAX_PLATFORM_DIST
 * #  MIN_ENEMIES = 0
 */

struct PlatformGeneratorParam {
	// Defines the rectangle in which platforms can be placed.
	float MIN_X_POINT;
	float MAX_X_POINT;
	float MIN_Z_POINT;
	float MAX_Z_POINT;

	// Defines the minimum / maximum distance between adjacent platforms.
	float MIN_PLATFORM_DIST;
	float MAX_PLATFORM_DIST;
    float DIST_SCALE_RATIO;
    float DIST_SCALE_RATIO_ON;

	// Define the minimum / maximum elevation angles.
	float MIN_ELEV_ANGLE;
	float MAX_ELEV_ANGLE;

	// Defines the number of platforms to place.
	int N_PLATFORMS;
};

class PlatformGenerator {
public:
	PlatformGenerator(PlatformGeneratorParam args);
	~PlatformGenerator();

	int Generate();
	Vector<Object*>& GetGeneratedPlatforms() { return platforms; }

private:
	PlatformGeneratorParam args;
	Vector<Object*> platforms;
    float nextScaleFactor;

	Vector3 GetRandomVector();
	Object* GetRandomPlatform(Vector3 position, int id);
    Object* GetPlatformByType(Vector3 position, int id, int platType);
};

#endif