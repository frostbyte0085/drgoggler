#include "EnemyGenerator.h"

#include <GameApplication.h>
#include <States/Behaviours/UfoAIBehaviour.h>

#include <Objects/SharedResources.h>

#define MAX_ATTEMPTS 1000

EnemyGenerator::EnemyGenerator(EnemyGeneratorParam args) {
	this->args = args;
}

EnemyGenerator::~EnemyGenerator() {
	ufos.clear();
}

int EnemyGenerator::Generate(const Vector<Object*> &platforms, int nPlats) {
	indexUfos = 0;
	
    // ignore the first two platforms
	for(int i=2; i<nPlats; i++) {
		AddEnemies(platforms[i], Math::Random(args.MIN_ENEMIES, args.MAX_ENEMIES));
	}

	return indexUfos;
}

void EnemyGenerator::AddEnemies(Object* platform, int n) {
	Vector<float> radiuses;

	for(int i=0; i<n; i++) {
		// (Try) Get radius
		float radius = GetRandomOrbitRadius(radiuses, i, platform);
		if(radius == -1.0f) continue; // couldn't find a place for this ufo. ditch it.

        radiuses.push_back(radius);
		int dir = RandClockwise(); // Clockwise or Anti-clockwise?
		
		AddAnEnemy(platform, radius, dir);
	}

	radiuses.clear();
}

void EnemyGenerator::AddAnEnemy(Object* platform, float radius, int dir) {
	// Get Strings:
	int charTypeID = ObjectManager::GetCharacterTypeIDByName("UFO");
	CharacterType ufo = ObjectManager::GetCharacterTypes()[charTypeID];
	String sceneNodeName = "ufo" + StringOperations::ToString<int>(indexUfos++);
	String ufoFileName = ufo.modelFilepath;

	// Add the ufo and AI:
	SceneNode *ufoNode = Scene::GetSceneNode( Scene::AddSceneNode(sceneNodeName,0) );
	ufoNode->SetMesh ( ResourceManager::Create<MESH> (new MeshResourceDescriptor(ufoFileName))->MeshObject, CT_SPHERE); 
	ufoNode->SetParent(platform->GetSceneNode()->GetID());
    Vector3 pos = Vector3(radius,20,0);
    
    if(args.DIST_SCALE_RATIO_ON) {
        pos.y += glm::length (platform->GetSceneNode()->GetGlobalScale()) * args.DIST_SCALE_RATIO;
    }
    
    // Get random spwn pt and speed:
    float speed = Math::RandomF(args.MIN_SPEED, args.MAX_SPEED);
    float compassAngle = Math::RandomF(0,360);
    pos = glm::rotate(pos, compassAngle, Vector3(0,1,0));
    
    ufoNode->SetTranslation(pos);
    ufoNode->SetTag("ufo", 0);
    ufoNode->SetScale(Vector3(ufo.scale));
    ufoNode->ScaleFromParent = false;
    UfoAIBehaviour *ai = new UfoAIBehaviour(radius, speed, dir, args.UFO_ATK_RADIUS, args.UFO_ATK_RATE);
	ufoNode->AddBehaviour("ufoAi", ai);
    ufoNode->SetCollisionGroup (OCG_UFO);
    
    ufoNode->audioSources["laser"] = new AudioSource(GameResources::ufoLaserSound);
    
    Log("Added an enemy");

	ufos.push_back(new Object(ufoNode, OTYPE_CHAR, charTypeID));
}

float EnemyGenerator::GetRandomOrbitRadius(Vector<float> radiuses, int n, Object* platform) {
	// Loop until a valid orbit is found:
	for(int i=0; i<MAX_ATTEMPTS; i++) {
        float minR=args.MIN_ORBIT_RADIUS, maxR=args.MAX_ORBIT_RADIUS,
        diff = maxR - minR;
        
        // Get distance rescale factor:
        if(args.DIST_SCALE_RATIO_ON) {
            PlatformType& platformType = ObjectManager::GetPlatformTypeByID(platform->GetObjectTypeID());
            float fact = platformType.scale * args.DIST_SCALE_RATIO;
            minR *= fact;
            maxR = minR + diff;
        }
        
		// Get random orbit
		float radius = Math::RandomF(minR,  maxR);
        
		// Does this orbit collide with others?
		bool collides = false;
		for(int i=0; i<n; i++) {
			if( fabs(radiuses[i] - radius) <  args.UFO_RADIUS * 2) {
				collides = true;
				break;
			}
		}
		if(!collides) return radius;
	}
	return -1.0f;
}

int EnemyGenerator::RandClockwise() {
	int coin = Math::Random(0,1);
	if(coin==0) return -1;
	else        return 1;
}