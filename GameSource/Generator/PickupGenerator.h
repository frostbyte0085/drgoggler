#ifndef PICKUP_GENERATOR_H_DEF
#define PICKUP_GENERATOR_H_DEF

#include <GameApplication.h>
#include <Objects/ObjectManager.h>

struct PickupGeneratorParam {
	// Defines the minimum / maximum # of platforms before encountering a HealthPack.
	int MIN_HP_GAP;
	int MAX_HP_GAP;

	// Defines the minimum / maximum # of platforms before encountering a FuelPack.
	int MIN_FP_GAP;
	int MAX_FP_GAP;
    
    // Defines the minimum / maximum # of platforms before encountering a Orb refill.
	int MIN_ORB_GAP;
	int MAX_ORB_GAP;

	// Define the probabilities [0,1] of the money appearances.
	// Note: 1 - sum of these = probability that no money spawns on this platforms
	// The generator doesn't check for the probabilities to make sense. (so don't
	// set two of these to 1.0 for instance, that will cause undesired results)
	float PROB_1EURO;
	float PROB_2EURO;
	float PROB_5EURO;
	float PROB_GEM;
};

class PickupGenerator {
public:
	PickupGenerator(PickupGeneratorParam args);
	~PickupGenerator();

	int Generate(const Vector<Object*>& platforms, int nPlats);
	Vector<Object*>& GetGeneratedHealthPacks() { return healthpacks; }
	Vector<Object*>& GetGeneratedFuelPacks() { return fuelpacks; }
	Vector<Object*>& GetGeneratedMoney() {return money;}

private:
	float spwn1euro, spwn2euro, spwn5euro, spwngem;

	PickupGeneratorParam args;
	Vector<Object*> healthpacks;
	Vector<Object*> fuelpacks;
	Vector<Object*> money;

	void addHealth(Object * plat);
	void addFuel(Object * plat);
    void addOrb(Object * plat);
	void addMoney(Object * plat);
    void addStar(Object * plat);

	int getRandomMoney();
};

#endif