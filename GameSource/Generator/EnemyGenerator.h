#ifndef ENEMY_GENERATOR_H_DEF
#define ENEMY_GENERATOR_H_DEF

#include <GameApplication.h>
#include <Objects/ObjectManager.h>

struct EnemyGeneratorParam {
	// Defines the minimum / maximum radius of orbit of the UFOs.
	float MIN_ORBIT_RADIUS;
	float MAX_ORBIT_RADIUS;
    float DIST_SCALE_RATIO;
    bool  DIST_SCALE_RATIO_ON;
    
    // Defines the minimum / maximum rotational speed:
    float MIN_SPEED;
    float MAX_SPEED;
    
	// Defines the minimum / maximum number of enemies per platform.
	int MIN_ENEMIES;
	int MAX_ENEMIES;
    
    // Define the minimum distance between the centers of enemies.
    float UFO_RADIUS;
    
    // Define the distance from the player when UFOs start shooting + rate.
    float UFO_ATK_RADIUS;
    float UFO_ATK_RATE;   // Attacks / second
};

class EnemyGenerator {
public:
	EnemyGenerator(EnemyGeneratorParam args);
	~EnemyGenerator();

	int Generate(const Vector<Object*> &platforms, int nPlats);
	Vector<Object*>& GetGeneratedEnemies() { return ufos; }

private:
	EnemyGeneratorParam args;
	int indexUfos;
	Vector<Object*> ufos;

	void AddEnemies(Object* platform, int n);
	void AddAnEnemy(Object* platform, float radius, int dir);

	float GetRandomOrbitRadius(Vector<float> radiuses, int n, Object* platform);
	int RandClockwise();
};

#endif