#ifndef GAME_APPLICATION_H_DEF
#define GAME_APPLICATION_H_DEF

#include <EngineInclude.h>

class GameApplication : public Application {
public:
    GameApplication();
    ~GameApplication();
    
    virtual void SetupStates();
    
	static void SetupKeyboardAndMouse(int invertY);
	static void SetupGamePad(int invertY);
};

#endif
