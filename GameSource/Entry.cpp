#include "GameApplication.h"
#include "console.h"

// we have split the main functions:
/*
 1) On Windows: This file
 2) On OSX: main.mm
*/
#if PLATFORM == PLATFORM_WINDOWS
#ifdef _CONSOLE
int main(int argc, char *argv[]) {
#else
int WINAPI WinMain(          HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow) {
#endif

#if CONFIG == CONFIG_DEBUG
	ConsoleWindow console;
	console.Open();
#endif

	PHYSFS_init (nullptr);

	GameApplication app;
	int exitStatus =app.Run();

	return exitStatus;
}

#endif//