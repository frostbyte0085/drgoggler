#include "GameApplication.h"

#include <States/AnimationTestState.h>
#include <States/UITestState.h>
#include <States/AudioTestState.h>
#include <States/GeneratorTestState.h>
#include <States/MainMenuState.h>
#include <States/LoadingState.h>
#include <States/IntroState.h>

#include <Objects/ObjectManager.h>
#include <Objects/SharedResources.h>

GameApplication::GameApplication() {
    
}

GameApplication::~GameApplication() {
    
}

void GameApplication::SetupKeyboardAndMouse(int invertY) {
	Input::ClearActions();
	Input::SetKeyAction("Quit", GLFW_KEY_ESC, 1.0f);
    
    Input::SetKeyAction("Conker", 'P', 1.0f);
    Input::SetKeyAction("Forward", 'W', 1.0f);
    Input::SetKeyAction("Backward", 'S', -1.0f);
	Input::SetKeyAction("StrafeL",'A',-1.0f);
	Input::SetKeyAction("StrafeR",'D',1.0f);
	Input::SetKeyAction("Up", GLFW_KEY_SPACE, 1.0f);
	Input::SetKeyAction("Down", GLFW_KEY_LCTRL, -1.0f);

    Input::SetMouseButtonAction("Fire", GLFW_MOUSE_BUTTON_1, 1.0f);
    Input::SetMouseButtonAction("Lock", GLFW_MOUSE_BUTTON_2, 1.0f);
    
	Input::SetMouseButtonAction("MenuClick", GLFW_MOUSE_BUTTON_1, 1.0f);
	Input::SetMouseAxisAction("Right", "Left", MOUSE_AXIS_X, AXIS_NORMAL);
	Input::SetMouseAxisAction("LookUp", "LookDown", MOUSE_AXIS_Y, invertY);
}

void GameApplication::SetupGamePad(int invertY) {
	Input::ClearActions();
	Input::SetJoyButtonAction("Quit", X360_BUTTON_BACK, 1.0f);
    
	Input::SetJoyAxisAction("Forward", "Backward", X360_AXIS_LS_Y,     AXIS_NORMAL);
    Input::SetJoyAxisAction("Left",    "Right",    X360_AXIS_RS_X,     AXIS_NORMAL);
	Input::SetJoyAxisAction("LookUp",  "LookDown", X360_AXIS_RS_Y,     invertY);
	Input::SetJoyAxisAction("StrafeL", "StrafeR",  X360_AXIS_LS_X,     AXIS_NORMAL);
	Input::SetJoyAxisAction("Up",      "Down",     X360_AXIS_TRIGGERS, AXIS_INVERTED);

	Input::SetJoyButtonAction("Fire", X360_BUTTON_LB, 1.0f);
	Input::SetJoyButtonAction("Lock", X360_BUTTON_RB, 1.0f);
    
	Input::SetMouseButtonAction("MenuClick", GLFW_MOUSE_BUTTON_1, 1.0f);
}

void GameApplication::SetupStates() {
    Application::SetupStates();
    
	SetupKeyboardAndMouse(AXIS_NORMAL);

	ObjectManager::LoadContent();
    SharedResources::Initialize();
    
    Scene::SetUILayer(new UILayer("Data/UI/UITextureAtlas.xml"));
    Scene::GetUILayer()->SetClickAudioSource(SharedResources::menuClickSource);
    
    AddState("UITest", new UITestState());
	AddState("AudioTest", new AudioTestState());
	AddState("AnimationTestState", new AnimationTestState());
	AddState("GeneratorTestState", new GeneratorTestState());
    AddState("MainMenuState", new MainMenuState());
    AddState("LoadingState", new LoadingState());
    AddState("IntroState", new IntroState());
    
    SetState("IntroState");
}