//
//  PauseMenu.h
//  UEAGame
//
//  Created by Olivier Legat on 02/12/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef PAUSE_MENU_H_
#define PAUSE_MENU_H_

#include <GameApplication.h>

class PauseMenu {
public:
    PauseMenu();
    ~PauseMenu();
    
    // Return false on exit of the pause menu.
    bool Update();
    void SetOn();
    
private:
    Font *font;
    UILayer *ui;
};

#endif
