#ifndef NUMBER_TICKER_H_
#define NUMBER_TICKER_H_

#define NUMBER_TICKER_DEFAULT_TICK_RATE 200

#include <GameApplication.h>

class NumberTicker {
public:
	NumberTicker(UILayer *ui, Font *font, int digits);
	~NumberTicker();

	void SetDigits(int digits) {this->digits = digits;}
	int GetDigits() {return this->digits;}
	
	void SetValue(int value, bool tick=true);
	int Increment(int add, bool tick=true);
	int GetValueTargetted() {return this->valueTargetted;}
	int GetValueDisplayed() {return this->valueDisplayed;}

	void SetTickRate(float tickRate) {this->tickRate = tickRate;}
	float GetTickRate() {return this->tickRate;}

	void SetPosition(Vector2 position) {this->position = position;}
	Vector2 GetPosition() {return this->position;}

	void Update();

private:
	int digits;
	int valueTargetted;
	int valueDisplayed;

	float timeRemainder;
	float tickRate; // Number of changes / second

	UILayer *ui;
    Font *font;
	Vector2 position;

	void UpdateValueDisplayed();
};

#endif