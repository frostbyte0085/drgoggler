//
//  MainMenu.cpp
//  UEAGame
//
//  Created by Olivier Legat on 24/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "MainMenu.h"

#include <Objects/SharedResources.h>
#include <Objects/ObjectManager.h>
#include <States/Behaviours/PlayerController.h>

const int MainMenu::logoOffx = 600;
const int MainMenu::logoOffy = 70;
const int MainMenu::textOffx = 800;
const int MainMenu::textOffy = 350;
const int MainMenu::offx = 760;
const int MainMenu::offy = 450;
const int MainMenu::height = 80;
const int MainMenu::width  = 450;
const int MainMenu::sep = 50;

const int MainMenu::windowX = 550;
const int MainMenu::windowY = 320;

MainMenu::InputMode MainMenu::inputMode = MainMenu::INPUT_KEYMOUSE;
MainMenu::VideoMode MainMenu::videoMode = MainMenu::VIDEO_WINDOWED;
int MainMenu::invertY = AXIS_NORMAL;
int MainMenu::sensitivity = MainMenu::SENSIVITY_MODE_NORMAL;

MainMenu::MainMenu() {
    ui = Scene::GetUILayer();
}
MainMenu::~MainMenu() {
    
}

void MainMenu::Start() {
    
    logo = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/Logo.png"))->TextureObject;
    
	ctrlsKeyMouse = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/Controls/key-mouse.png"))->TextureObject;
	ctrlsX360 = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/Controls/x360-ctrls.png"))->TextureObject;

	font = SharedResources::arial_small;
    
    
    state = MENU_SECTION_MAIN;
}

bool MainMenu::Update(bool &changeNow) {
    
    changeNow = false;
    
    switch(state) {
        case MENU_SECTION_MAIN:
            ui->Window(Vector2(windowX, windowY), Vector2(ui->GetVirtualWidth()-windowX, ui->GetVirtualHeight()-100), Vector4(1,1,1,0.5f));
            return UpdateMain();

        case MENU_SECTION_OPTIONS:
            ui->Window(Vector2(windowX, windowY), Vector2(ui->GetVirtualWidth()-windowX, ui->GetVirtualHeight()-100), Vector4(1,1,1,0.5f));
            return UpdateOptions();

		case MENU_SECTION_SELECT_DIFF:
			ui->Window(Vector2(windowX, windowY), Vector2(ui->GetVirtualWidth()-windowX, ui->GetVirtualHeight()-100), Vector4(1,1,1,0.5f));
			return UpdateSelectDiff();

		case MENU_SECTION_SHOW_GENERATING:
            ui->Window(Vector2(windowX, windowY), Vector2(ui->GetVirtualWidth()-windowX, ui->GetVirtualHeight()-100), Vector4(1,1,1,0.5f));
            return UpdateGenerating();

		case MENU_SECTION_START_GENERATING:
//			Application::GameApp->SetState("LoadingState");
            changeNow = true;
			return true;
        default: return true;
    }
}

bool MainMenu::UpdateMain() {
    ui->TexturedQuad(logo, Vector2(logoOffx, logoOffy), Vector2(1));

    int mainMenuOffset = (int)font->GetTextWidth("Main Menu");
    
	ui->TextLabel(font,  Vector2(ui->GetVirtualWidth()/2 - mainMenuOffset/2, textOffy), "Main Menu");

	int x, y;
    bool r = true;

	x = offx;
	y = offy;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Start")) {
		state = MENU_SECTION_SELECT_DIFF;
    }
    
	y = offy+(height+sep)*1;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Controls")) {
		state = MENU_SECTION_OPTIONS;
    }

	y = offy+(height+sep)*2;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Exit")) {
		return false;
    }
    
    return r;
}

bool MainMenu::UpdateSelectDiff() {
	ui->TexturedQuad(logo, Vector2(logoOffx, logoOffy), Vector2(1));

    int mainMenuOffset = (int)font->GetTextWidth("Select difficulty");
    
	ui->TextLabel(font,  Vector2(ui->GetVirtualWidth()/2 - mainMenuOffset/2, textOffy), "Select difficulty");

	int x, y;

	x = offx;
	y = offy;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Novice")) {
		ObjectManager::SetDefaultGeneratorByID(0);
		state = MENU_SECTION_SHOW_GENERATING;
    }

	y = offy+height+sep;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Famine")) {
		ObjectManager::SetDefaultGeneratorByID(1);
		state = MENU_SECTION_SHOW_GENERATING;
    }

	y = offy+(height+sep)*2;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Invasion")) {
		ObjectManager::SetDefaultGeneratorByID(2);
		state = MENU_SECTION_SHOW_GENERATING;
    }

	y = offy+(height+sep)*3;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Back")) {
		state = MENU_SECTION_MAIN;
    }
    
    return true;
}

bool MainMenu::UpdateOptions() {
	ui->TexturedQuad(logo, Vector2(logoOffx, logoOffy), Vector2(1));

    int optionsMenuOffset = (int)font->GetTextWidth("Controls");
    
	ui->TextLabel(font, Vector2(ui->GetVirtualWidth()/2-optionsMenuOffset/2, textOffy), "Controls");
	
	// Show ctrls
	Texture* ctrls = nullptr;
	switch(inputMode) {
	case INPUT_JOYSTICK:
		ctrls = ctrlsX360;
		break;
	case INPUT_KEYMOUSE:
		ctrls = ctrlsKeyMouse;
		break;
	}
	float k = 1.65f;
	int ctrlsX = ui->GetVirtualWidth() - ctrls->GetWidth() * k;
	int ctrlsY = ui->GetVirtualHeight() * 2.0f / 3.0f - ctrls->GetHeight() * k * 2.0f / 3.0f;
	ui->TexturedQuad(ctrls, Vector2(ctrlsX, ctrlsY), Vector2(k));
    
	int x=offx, y=offy;
    String text;
    
    // Draw video mode option button:
    /*
    y = offy+(height+sep/3.0f)*0;
    switch(videoMode) {
        case VIDEO_WINDOWED: text = "Windowed"; break;
        case VIDEO_FULLSCREEN: text = "Fullscreen"; break;
        default: text="Oh oh... error"; break;
	}
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), text)) {
		switch(videoMode) {
            case VIDEO_WINDOWED: 
                videoMode = VIDEO_FULLSCREEN;
                GraphicsContext::SetFastFullscreenMode();
                Input::ShowMouse(true);
                break;
            case VIDEO_FULLSCREEN: 
                videoMode= VIDEO_WINDOWED;
                GraphicsContext::SetFastWindowedMode();
                Input::ShowMouse(true);
                break;
            default: break;
		}
    }*/
    
	// Draw ctrls button.
    y = offy+(height+sep)*0;
    switch(inputMode) {
        case INPUT_JOYSTICK: text = "Gamepad"; break;
        case INPUT_KEYMOUSE: text = "Keyboard / Mouse"; break;
        default: text="Oh oh... error"; break;
	}
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), text)) {
		switch(inputMode) {
		case INPUT_JOYSTICK: 
			inputMode = INPUT_KEYMOUSE;
			GameApplication::SetupKeyboardAndMouse(invertY);
			break;
		case INPUT_KEYMOUSE: 
			inputMode = INPUT_JOYSTICK;
			GameApplication::SetupGamePad(invertY);
			break;
		default: break;
		}
    }
    
	// Invert Y Button
	y = offy+(height+sep)*1;
	text = invertY == AXIS_NORMAL ? "Y-Normal" : "Y-Inverted";
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), text)) {
		invertY = (invertY == AXIS_NORMAL) ? AXIS_INVERTED : AXIS_NORMAL;
		switch(inputMode) {
			case INPUT_KEYMOUSE: 
				GameApplication::SetupKeyboardAndMouse(invertY);
				break;
			case INPUT_JOYSTICK:
				GameApplication::SetupGamePad(invertY);
				break;
			default: break;
		}
    }

	// Sensitivity button:
	y = offy+(height+sep)*2;
	switch(sensitivity) {
	case SENSIVITY_MODE_LOW:
		text = "Low sensitivity";
		break;
	case SENSIVITY_MODE_NORMAL:
		text = "Normal sensitivity";
		break;
	case SENSIVITY_MODE_HIGH:
		text = "High sensitivity";
		break;
	default: break;
	}
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), text)) {
	switch(sensitivity) {
	case SENSIVITY_MODE_LOW:
		sensitivity = SENSIVITY_MODE_NORMAL;
		PlayerController::lookSensitivity = PlayerController::PLAYER_LOOK_SENSITIVITY_NORMAL;
		break;
	case SENSIVITY_MODE_NORMAL:
		sensitivity = SENSIVITY_MODE_HIGH;
		PlayerController::lookSensitivity = PlayerController::PLAYER_LOOK_SENSITIVITY_HIGH;
		break;
	case SENSIVITY_MODE_HIGH:
		sensitivity = SENSIVITY_MODE_LOW;
		PlayerController::lookSensitivity = PlayerController::PLAYER_LOOK_SENSITIVITY_LOW;
		break;
	default: break;
	}
    }

	// Back button:
	y = offy+(height+sep)*3;
	if(ui->Button(font, Vector2(x, y), Vector2(x+width, y+height), "Back")) {
		state = MENU_SECTION_MAIN;
    }

    return true;
}

bool MainMenu::UpdateGenerating() {

	/*ui->TexturedQuad(logo, Vector2(logoOffx, logoOffy), Vector2(1));

	ui->TextLabel(font, Vector2(textOffx, textOffy), "Genarating level...");
	ui->TextLabel(font, Vector2(textOffx, textOffy+sep+sep), "(patience your majesty)");

     */
	state = MENU_SECTION_START_GENERATING;

	return true;
}