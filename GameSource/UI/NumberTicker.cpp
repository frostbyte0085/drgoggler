#include "NumberTicker.h"

/*Texture* NumberTicker::NUMBER[10] = {nullptr};

void NumberTicker::initStatics() {
	if(NUMBER[0]!=nullptr) return;

	for(int i=0; i<10; i++) {
		String file = "Data/UI/Numbers/" +  StringOperations::ToString<int>(i) + ".png";
		NUMBER[i] = ResourceManager::Create<TEXTURE>( new TextureResourceDescriptor(file))->TextureObject;
	}
}*/

NumberTicker::NumberTicker(UILayer *ui, Font *font, int digits) {
	//initStatics();
	this->ui = ui;
    this->font = font;
	this->digits = digits;
	this->valueDisplayed = 0;
	this->valueTargetted = 0;
	this->tickRate = NUMBER_TICKER_DEFAULT_TICK_RATE;
	this->timeRemainder = 0.0f;
	this->position = Vector2(0);
}
NumberTicker::~NumberTicker() {
	//this->numbers.clear();
}

void NumberTicker::SetValue(int value, bool tick) {
	if(tick==false) {
		this->valueTargetted = value;
		this->valueDisplayed = value;
	}
	else {
		this->valueTargetted = value;
	}
}
int NumberTicker::Increment(int add, bool tick) {
	int oldValue = valueTargetted;
	if(tick==false) {
		this->valueTargetted += add;
		this->valueDisplayed += add;
	}
	else {
		this->valueTargetted += add;
	}
	return oldValue;
}
void NumberTicker::UpdateValueDisplayed() {
	if(valueTargetted != valueDisplayed)
	{
		// Get # of ticks for this frame
		timeRemainder += Timing::LastFrameTime;
		int ticks = (int) (timeRemainder * tickRate);
		timeRemainder -= ticks / tickRate;

		// Update the displayed value
		if(valueDisplayed < valueTargetted)
			valueDisplayed += ticks;
		else if(valueDisplayed > valueTargetted)
			valueDisplayed -= ticks;
	
		// Reset time remainer if target is reached
		if(valueDisplayed == valueTargetted)
			timeRemainder = 0.0f;
	}
}
void NumberTicker::Update() {
	UpdateValueDisplayed();
	int ax = valueDisplayed;
    String text = "Score: "+StringOperations::ToString<int>(ax);
	ui->TextLabel(font, position, text);
}