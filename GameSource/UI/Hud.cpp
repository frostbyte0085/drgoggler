#include "Hud.h"
#include "NumberTicker.h"
#include "MainMenu.h"

#include <Objects/SharedResources.h>

#include <States/Behaviours/PlayerController.h>

// Default Vres: 1920 x 1080

String Hud::deathClicheLine1[9] = {
    // Ground death cliches:
    "You have died a horrible and painful death!",
    "Dr. Goggler's heart was ripped out of his body!",
    "Oh no! Don't die! We're out of tomb stones!",
    
    // Air death cliches
    "You have died a horrible and painful death!",
    "   May day! May day!... Oh, you're dead.   ",
    "You died. Now you are falling to your death!",
    
    // Water death cliches
    "That thing on your back is not a scuba tank!",
    "Dr. Goggler tried to be a fish... and failed!",
    "This is a flight simulator. Why would you wanna swim?",
};
String Hud::deathClicheLine2[5] = {
    "\"You suck.\" --Dr.Goggler",
    "\"Dam, you're really bad at this game.\" --Dr.Goggler",
    "\"Don't worry, it's all your fault.\" --Dr.Goggler",
    "\"With other players, this never happens.\" --Dr.Goggler",
    "\"Please don't ever play again.\" --Dr.Goggler"
};

Hud::Hud(PlayerController *player) {
    this->player = player;
    
    this->ui = Scene::GetUILayer();
	this->score = new NumberTicker(ui, SharedResources::arial_small, 5);
    
    doomFace[0] = GetFace(0);
    doomFace[1] = GetFace(1);
    doomFace[2] = GetFace(2);
    doomFace[3] = GetFace(3);
    doomFace[4] = GetFace(4);
	hitpoint = GetTexture("Data/UI/Ingame/HealthPoint.png");
    
    fuelIcon  = GetTexture("Data/UI/Ingame/Jetpack.png");
	manapoint = GetTexture("Data/UI/Ingame/FuelPoint.png");
    crosshair = GetTexture("Data/UI/Ingame/Crosshair.png");
    
    clicheLine1Index = -1;
    clicheLine2Index = -1;
    
    bloodScreen = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("Data/UI/Ingame/HUD-bleed.png"))->TextureObject;
    bloodScreenAlpha = 0.0f;
}
Hud::~Hud() {
    delete score;
}

Texture* Hud::GetTexture(String file) {
    return ResourceManager::Create<TEXTURE>( new TextureResourceDescriptor(file))->TextureObject;
}

Texture* Hud::GetFace(int i) {
    String file = "Data/UI/Ingame/DoomFace" +  StringOperations::ToString<int>(i) + ".png";
    return ResourceManager::Create<TEXTURE>( new TextureResourceDescriptor(file))->TextureObject;
}

void Hud::Start() {
	this->score->SetValue(this->player->score, false);
}

void Hud::SetOn() {

}

void Hud::Update() {
    if (!Application::GameApp->IsPaused())
        DrawBloodScreen();
    
	DrawHealthPoints();
	DrawFuelPoints();
    
	DrawScore();
    
    if (!Application::GameApp->IsPaused())
        DrawCrosshair();
    
    if (!DrawYouAreDEAD())
        return;
    
    if (!DrawYouAreDONE())
        return;
    
    DrawScoreBoard();
}

void Hud::DrawBloodScreen() {
    if (player->hp > 0 && player->hp <= 3 && !player->IsDead()) {
        float scaleX = (float)ui->GetVirtualWidth() / (float)bloodScreen->GetWidth();
        float scaleY = (float)ui->GetVirtualHeight() / (float)bloodScreen->GetHeight();
        
        bloodScreenAlpha = fabs(sinf(Timing::CurrentTime)) / 2.0f;
        ui->TexturedQuad(bloodScreen, Vector2(0,0), Vector2(scaleX, scaleY), Vector4(1,1,1,bloodScreenAlpha));
    }
    else
        bloodScreenAlpha = 0.0f;
}

void Hud::DrawHealthPoints() {
    Texture* whichFace;
    if(player->hp >= 9)
        whichFace = doomFace[0];
    else if(player->hp >= 7)
        whichFace = doomFace[1];
    else if(player->hp >= 5)
        whichFace = doomFace[2];
    else if(player->hp >= 3)
        whichFace = doomFace[3];
	else 
		whichFace = doomFace[4];
    
    ui->TexturedQuad(whichFace, Vector2(0,870), Vector2(0.7));
    
	int x = 250;
	for(int i=0; i<player->hp; i++) {
		ui->TexturedQuad(hitpoint, Vector2(x,950), Vector2(1));
		x += 30;
	}
}

void Hud::DrawFuelPoints() {
    ui->TexturedQuad(fuelIcon, Vector2(1675,870), Vector2(0.7));
    
	int x = 1600;
	for(int i=0; i<player->mana; i++) {
		ui->TexturedQuad(manapoint, Vector2(x,950), Vector2(1));
		x -= 30;
	}
}

void Hud::DrawScore() {
    if(!player->IsDead() && !player->GotStar()) {
        // In-game score:
        
        score->SetValue(player->score);
        score->Update();
    
        String text = "Kills: " + StringOperations::ToString(player->kills);
        float killsPosition = ui->GetVirtualWidth() - SharedResources::arial_small->GetTextWidth(text) - SharedResources::arial_small->GetLineHeight();
        ui->TextLabel(SharedResources::arial_small, Vector2(killsPosition, 0), text);

        text = "Level: " + StringOperations::ToString(player->level);
        float levelPosition = (ui->GetVirtualWidth() - SharedResources::arial_small->GetTextWidth(text)) / 2.0f;
        ui->TextLabel(SharedResources::arial_small, Vector2(levelPosition, 0), text);
    }
}

void Hud::DrawCrosshair() {
    if (!player->renderCrossHair)
        return;
    
    if (player->IsDoomed() || player->GotStar()) return;
    
    float crosshairPosX = ui->GetVirtualWidth()/2 - crosshair->GetWidth()/2;
    float crosshairPosY = ui->GetVirtualHeight()/2 - crosshair->GetHeight()/2;
    
    ui->TexturedQuad(crosshair, Vector2(crosshairPosX, crosshairPosY));
}

bool Hud::DrawYouAreDEAD() {
    if(player->IsDead()) {
        if(clicheLine1Index == -1) {
            // Pick a random line:
            switch (player->state) {
                case PS_DEAD_GROUND: clicheLine1Index = Math::Random(0, 2); break;
                case PS_DEAD_AIR:    clicheLine1Index = Math::Random(3, 5); break;
                case PS_DEAD_DROWN:  clicheLine1Index = Math::Random(6, 8); break;
                default: clicheLine1Index=0;  break;
            }
           
        }
        if(clicheLine2Index == -1) {
            // Pick a random line:
            clicheLine2Index = Math::Random(0, 4);
        }
        
        Font *font = SharedResources::arial_small;
        String cliche1 = deathClicheLine1[clicheLine1Index];
        String cliche2 = deathClicheLine2[clicheLine2Index];
        
        float clicheMaxLength = std::max (SharedResources::arial_small->GetTextWidth(cliche1), SharedResources::arial_small->GetTextWidth(cliche2));
        
        float clicheX = ui->GetVirtualWidth() / 2 - clicheMaxLength/2;

        float x = (float)MainMenu::offx;
        float y =(float) MainMenu::offy + (float)MainMenu::sep + (float)MainMenu::height + 200;
        
        float windowEndY = clicheX + clicheMaxLength + 40;
        
        ui->Window (Vector2 (clicheX - 40, 100), Vector2(windowEndY, y + 40 + MainMenu::height));
        
        ui->TextLabel(font, Vector2(clicheX, 620), cliche1);
        ui->TextLabel(font, Vector2(clicheX, 685), cliche2);
        

        if(ui->Button(font, Vector2(clicheX, y), Vector2(clicheX + MainMenu::width, y+MainMenu::height), "Give up")) {
            ui->Clear();
            Application::GameApp->SetState("MainMenuState");
            return false;
        }
        
        if(ui->Button(font, Vector2(windowEndY-MainMenu::width-40, y), Vector2(windowEndY-40, y+MainMenu::height), "Retry!")) {
            ui->Clear();
            Application::GameApp->SetState("LoadingState");
            return false;
        }
    }
    return true;
}

bool Hud::DrawYouAreDONE() {
    if(player->GotStar()) {
        Font *font = SharedResources::arial_small;
        
        String text = "Brave warrior, you collected the precious artifact!";
        float textWidth = SharedResources::arial_small->GetTextWidth(text);
        
        float textX = ui->GetVirtualWidth()/2 - textWidth/2;
        float textEndY = ui->GetVirtualWidth()/2 + textWidth/2;
        
        float x = (float)MainMenu::offx;
        float y = (float)MainMenu::offy;
        
        ui->Window (Vector2 (textX - 40, 100), Vector2(textEndY + 40, MainMenu::offy + 40 + MainMenu::height +320));
        
        ui->TextLabel(font, Vector2(ui->GetVirtualWidth()/2-textWidth/2 , MainMenu::textOffy+320), text);
        
        if(ui->Button(font, Vector2(x, y+320), Vector2(x+MainMenu::width, y+MainMenu::height+320), "Continue")) {
            ui->Clear();
			// Remember the score:
			PlayerController::previousScore = this->player->score;
			PlayerController::previousLevel = this->player->level;
			PlayerController::previousKills = this->player->kills;
            PlayerController::previousRoundsFired = this->player->roundsFired;
            Application::GameApp->SetState("LoadingState");
            return false;
        }
    }
    return true;
}

void Hud::DrawScoreBoard() {
    if(player->IsDead() || player->GotStar()) {
        // Post-game carnage:
        int x0 = (int) (
                        (ui->GetVirtualWidth()
                         -SharedResources::arial_small->GetTextWidth("Aliens slaughtered:   00:00") ) / 2.0f
                        );
        int x1 = x0+(int)SharedResources::arial_small->GetTextWidth("Aliens slaughtered:   ");
        int y = 150;
        int dy = SharedResources::arial_small->GetLineHeight();
        String fieldName;
        String fieldValue;
        Vector4 color;
        
        fieldName = "Levels completed: ";
        int lvls = player->level; if(player->IsDoomed()) lvls--;
        fieldValue = StringOperations::ToString(lvls);
        color = Vector4(1,1,1,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Health remaining: ";
        int hp = player->hp*10; 
        if(hp < 0) {
            hp = -hp;
            fieldName = "Overkilled by: ";
        }
        fieldValue = StringOperations::ToString(hp) + "%";
        color = Vector4(1,0,0,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Fuel remaining: ";
        fieldValue = StringOperations::ToString(player->mana*10) + "%";
        color = Vector4(0.4,0.4,1,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Orbs remaining: ";
        fieldValue = StringOperations::ToString(player->orbsLeft);
        color = Vector4(0,1,0,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Rounds fired: ";
        fieldValue = StringOperations::ToString(player->roundsFired);
        color = Vector4(0,1,1,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Aliens slaughtered: ";
        fieldValue = StringOperations::ToString(player->kills);
        color = Vector4(0,1,1,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Accuracy: ";
        int percentage = player->roundsFired == 0 ? 0 : // avoid /0
        (int)( (float)player->kills * 100 / (float)player->roundsFired );
        fieldValue = StringOperations::ToString(percentage) + "%";
        color = Vector4(0,1,1,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        fieldName = "Time taken: ";
        int secs = (int) player->stoppedAt - player->startedAt;
        int mins = secs / 60;
        secs -= mins * 60;
        fieldValue =  StringOperations::ToString(secs);
        if(secs < 10) fieldValue = "0" + fieldValue;
        fieldValue = StringOperations::ToString(mins) + ":" + fieldValue;
        if(mins < 10) fieldValue = "0" + fieldValue;
        color = Vector4(1,1,0,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
        
        y += dy;
        fieldName = "Score: ";
        fieldValue = StringOperations::ToString(player->score);
        color = Vector4(1,1,1,1);
        ui->TextLabel(SharedResources::arial_small, Vector2(x0,y), fieldName,  color);
        ui->TextLabel(SharedResources::arial_small, Vector2(x1,y), fieldValue, color);
        y += dy;
    }
}