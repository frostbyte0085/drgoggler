//
//  MainMenu.h
//  UEAGame
//
//  Created by Olivier Legat on 24/11/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#ifndef MAIN_MENU_H_
#define MAIN_MENU_H_

#include <GameApplication.h>

class MainMenu {
public:
    MainMenu();
    ~MainMenu();

    void Start();
    bool Update(bool &changeNow);
    
    static const int logoOffx;// = 600;
    static const int logoOffy;// = 70;
    static const int textOffx;// = 600;
    static const int textOffy;// = 350;
    static const int offx;// = 720;
    static const int offy;// = 450;
    static const int height;// = 80;
    static const int width;//  = 420;
    static const int sep;// = 50;*/
    static const int windowX;
    static const int windowY;
    static const int windowWidth;
    static const int windowHeight;
    
private:
	enum InputMode {
		INPUT_JOYSTICK = 0,
		INPUT_KEYMOUSE = 1
	};
	enum VideoMode {
		VIDEO_FULLSCREEN = 0,
		VIDEO_WINDOWED = 1
	};
	enum SensivityMode {
		SENSIVITY_MODE_LOW = 0,
		SENSIVITY_MODE_NORMAL = 1,
		SENSIVITY_MODE_HIGH = 2
	};
	enum MenuSection {
		MENU_SECTION_MAIN = 0,
		MENU_SECTION_OPTIONS = 1,
		MENU_SECTION_SELECT_DIFF = 2,
		MENU_SECTION_SHOW_GENERATING = 3,
		MENU_SECTION_START_GENERATING = 4
	};

	static InputMode inputMode;
	static VideoMode videoMode;
	static int invertY;
	static int sensitivity;

    MenuSection state;
    
    Texture *logo;
	Texture *ctrlsKeyMouse;
	Texture *ctrlsX360;
    UILayer *ui;
	Font *font;
    
    bool UpdateMain();
	bool UpdateSelectDiff();
    bool UpdateOptions();
	bool UpdateGenerating();
};

#endif
