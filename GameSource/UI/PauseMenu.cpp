//
//  PauseMenu.cpp
//  UEAGame
//
//  Created by Olivier Legat on 02/12/11.
//  Copyright (c) 2011 UCL. All rights reserved.
//

#include "PauseMenu.h"
#include "MainMenu.h"  // for pixel width/height and offsets

#include "../Objects/SharedResources.h"

PauseMenu::PauseMenu() {
    this->ui = Scene::GetUILayer();
    
    font = SharedResources::arial_small;
    
}

PauseMenu::~PauseMenu() {
    
}

void PauseMenu::SetOn() {
    //Scene::SetUILayer(this->ui);
}

bool PauseMenu::Update() {
    bool ret = true;
    
    String text = "Game paused";
    float textWidth = SharedResources::arial_small->GetTextWidth(text);

    float x = (float)MainMenu::offx;
	float y = (float)MainMenu::offy;
    
    
    float windowX = (float)(ui->GetVirtualWidth()/2 - 500);
    float windowY =(float)MainMenu::textOffy-40;
    float windowToX = (float)(ui->GetVirtualWidth()/2 + 500);
    float windowToY = y + (float)MainMenu::height + 40;
    
    ui->Window(Vector2(windowX, windowY), Vector2(windowToX, windowToY));
    ui->TextLabel(font, Vector2(ui->GetVirtualWidth()/2-textWidth/2, MainMenu::textOffy), text);
    

	if(ui->Button(font, Vector2(windowX + 100, y), Vector2(windowX+MainMenu::width, y+MainMenu::height), "Continue")) {
        ret = false;
    }
    
	if(ui->Button(font, Vector2(windowToX - MainMenu::width, y), Vector2(windowToX - 100, y+MainMenu::height), "Give up")) {
        ui->Clear();
        Application::GameApp->SetState("MainMenuState");
        ret = true;
    }
    
    return ret;
}