#ifndef HUD_H_
#define HUD_H_

#include <GameApplication.h>

class PlayerController;
class NumberTicker;
class Hud {
public:
    Hud(PlayerController *player);
    ~Hud();
    
    void Start();
    void Update();
    
    void SetOn();
    
private:
    static String deathClicheLine1[9];
    static String deathClicheLine2[5];
    Texture *bloodScreen;
    float bloodScreenAlpha;
    
    UILayer *ui;
	NumberTicker *score;
    Texture *doomFace[5];
	Texture *hitpoint;
    Texture *fuelIcon;
	Texture *manapoint;
    Texture *crosshair;
    
    Texture* GetTexture(String filename);
    Texture* GetFace(int i);

    void DrawBloodScreen();
	void DrawHealthPoints();
    void DrawScore();
    void DrawScoreBoard();
	void DrawFuelPoints();
    void DrawCrosshair();
    
    int clicheLine1Index;
    int clicheLine2Index;
    bool DrawYouAreDEAD();
    bool DrawYouAreDONE();
    
    PlayerController* player;
};

#endif