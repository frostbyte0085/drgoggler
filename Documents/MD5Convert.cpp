// main.cpp
#include <stdio.h>
#include <vector>
#include <string>
#include <assert.h>

std::string Process(const std::string &input);

int main(int argc, char *argv[]) {

	if (argc != 3) {
		printf("Usage: %s input output\n", argv[0]);
		return -1;
	}

	printf("Converting %s --> %s\n", argv[1], argv[2]);

	FILE *fp = fopen(argv[1], "r");
	if (!fp) {
		printf("File open failed\n");
		return 0;
	}

	std::string output;

	fseek(fp, 0, SEEK_END);
	size_t sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::string current;
	bool inside = false;

	std::string input_file_as_string;

	for (size_t i=0; i<sz; i++) {
		char c = getc(fp);

		input_file_as_string += c;

		if (c == ')') {
			assert(inside == true);
			inside = false;
			output += Process(current);
			current = "";
		}

		if (!inside) {
			output += c;
		} else {
			current += c;
		}

		if (c == '(') {
			assert(inside == false);
			inside = true;
		}
	}

	fclose(fp);

	FILE *ofp = fopen(argv[2], "w");
	if (fwrite(output.c_str(), 1, output.size(), ofp) != output.size()) {
		printf("Failed to write output file\n");
	}
	fclose(ofp);

	return 0;
}

std::string Process(const std::string &input) {
	
	// format is *_ t1 *_ t2 *_ t3 *_
	int space_index = 0;
	int space_count[4] = {0, 0, 0, 0};
	std::vector<std::string> tokens;
	bool in_token = false;
	std::string current_token;

	for (int i=0; i<(int) input.size(); i++) {
		char c = input[i];
		if (c == ' ') {
			if (in_token) {
				if (current_token.size()) tokens.push_back(current_token);
				current_token = "";
			}
			in_token = false;
			space_count[space_index] ++;
		} else {
			if (!in_token) {
				space_index++;
				if (space_index >= 4) {
					return input; // too large parenthesis
				}
				in_token = true;
			}
			current_token += c;
		}
		
	}

	if (tokens.size() == 3) {
		std::swap(tokens[1], tokens[2]);
		std::string output;
		for (int i=0; i<4; i++) {
			for (int j=0; j<space_count[i]; j++) output += ' ';
			if (i < 3) output += tokens[i];
		}
		return output;
	}

	return input;
}