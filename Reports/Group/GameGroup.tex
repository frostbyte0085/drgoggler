\documentclass[]{ueacmpstyle}

\title{CMPSME27 - Computer Games Development \\ Dr. Goggler : Attack of the Mad ZuKu}

\author{Olivier Legat\\Pantelis Lekakis\\Manuel Feria}

\begin{document}

\maketitle

\section{Concept}
The concept of the game is to escalate a set of levitating platforms in order to pick up the green star at the very top with the use of a jet-pack. Unfortunately this is no easy task because the player is faced with violent alien enemies that fire at the player. 

The good news is that the player is not defence-less. The player has two kinds of weapons:
\begin{itemize}
\item \textbf{Dual Red Laser:} Infinite and fire at a high rate. However they are slow moving and non-homing.
\item \textbf{Orbs:} Orbs are powerful glowing spheres at will lock-on to enemies and home. Orbs are a valuable limited resource.
\end{itemize}

On the platforms the player will find special collectibles to aid the character across his journey.
\begin{itemize}
\item \textbf{Health-packs:} As the player gets shot by aliens he will take damage. These items will seal the wounds and restore his health-points.
\item \textbf{Fuel tanks:} The jet-pack requires fuel for combustion. It only has a very limited amount of fuel reserve and needs to be refilled frequently.
\item \textbf{Orbs:} Orbs can be found and the player can use these against the enemy aliens.
\item \textbf{Coins and Gems:} These items will instantly increase the player's score.
\end{itemize}

\begin{figure} [h!]
\centering
\includegraphics[scale=0.4]{Data/pickups.png}
\label{All the available pick-ups}
\end{figure}


Various game difficulties have been implemented:
\begin{itemize}
\item \textbf{Novice:} For new-comers the game can be quite challenging at first. This game mode provides the player with few and weak aliens to face, few platforms to escalate and lots of pick-ups
\item \textbf{Famine:} Famine is a little bit more challenging. The player has two times more platforms to escalate and resources are sparse. The enemies are also more frequent and more aggressive.
\item \textbf{Invasion:} This difficulty is highly challenging. Although resources appear far more often than in other modes, the enemies are highly frequent and extremely aggressive. If the player loses his guard for one seconds the aliens quickly wipe him out.
\end{itemize}

\section{Virtual Environment}
The virtual environment rather simple in our game. It is composed merely of:
\begin{itemize}
\item A water mesh on the bottom which drowns the character if he gets too close.
\item Platforms which the player can land on and collide with.
\item A sky-box.
\end{itemize}
The interesting thing about our virtual environment is that the platforms are placed using a random generator, meaning that the environment is completely different for each execution of the game.

\section{Characters and Animation}
The game has 3 characters:

\begin{figure} [h!]
\centering
\includegraphics[scale=0.4]{Data/chars.png}
\caption{All characters in the game}
\end{figure}


\begin{itemize}
\item \textbf{Dr Goggler:} This is player controlled character. The art-style of this character is inspired by Fanta commercials. This character has numerous animations such as standing, running, landing, flying, drowning and dying. He also has animations for when he gets hurt, if you land too hard he will play a ``hard-land" animation, and if he hits a platform or a UFO he will twitch then shake his head.
\item \textbf{Conker:} This is a little critter that appears on a random platform. He doesn't do anything other than eat nuts and decorate the scene. This character has an ``eating" animation
\item \textbf{UFO:} This the enemy unit that comes in great numbers. They orbit around platforms waiting for the character to get in range to start firing! The aliens all have a spinning animation.
\end{itemize}

\paragraph{UFO AI} The UFOs have a simple AI algorithm. They calculate the distance between themselves and the player. If the player is within range then the alien will start firing.

\section{Camera Modes}
The game is played from a 3rd-person perspective. The camera is kept at a fixed distance away from the character. The yaw of the camera is always the same as the character's. When the player turns the character the camera will move respectively. When the player looks up the pitch of the camera will change and the position will be rotated based on that angle:

Let \(X_p\) be the position of the player, \(X_c\) is the position of the camera and \(x\) is the position of the camera relative to the player:

\[X_c = X_p + x\]

When the camera is rotated along the \(x\) axis (pitch) by an angle of \(\theta\) the position of the camera is defined by

\[X_c = X_p + \textrm{rotateX}(\theta, x)\]

\section{Physics and collisions}
The character's flying state is updated using a simple physical model implementing acceleration and air drag. Please note that this physical model is applied only when Dr Goggler is flying.

\begin{figure} [h!]
\centering
\includegraphics[scale=0.4]{Data/2states.png}
\caption{The two main states of the player's character}
\end{figure}

For collisions, we use a 2-way approach, using raycastings and bounding volume intersections, each serving a different purpose:
\begin{itemize}
\item \textbf{Character - Platform collision}\\
Dr Goggler can collide with the platforms using simple bounding box to bounding box intersection tests. If a OnCollideEnter event is fired in the SceneNodeBehaviour, then Dr Goggler is becoming very dizzy and is pushed towards the opposite direction:
\begin{figure} [H]
\centering
\includegraphics[scale=0.6]{Data/uea-collision.png}
\caption{Responding a collision event}
\end{figure}

\item \textbf{Character - Platform landing}\\
Dr Goggler can see when there is a platform beneath him by raycasting down from his feet. If the ray hits a platform SceneNode bounding volume, then it continues internally to ray-triangle intersection to find the exact hit point. The structure that is returned is of type RayHitInfo and it can be used to retrieve apart from the intersection point, the normal, texture coordinate and whatever else we would want to support:\\
\begin{figure} [h!]
\centering
\includegraphics[scale=0.6]{Data/uea-rayhitinfo.png}
\caption{RayHitInfo}
\end{figure}

The Scene::Raycast method is called each frame to update the character position on the platform:
\begin{figure} [h!]
\centering
\includegraphics[scale=0.6]{Data/uea-raycast.png}
\caption{Raycasting }
\end{figure}
where origin is just above Dr Goggler's feet world position and node-\(>\)GetID() is the ignored character node in the raycasting process (we do not want to hit Dr Goggler!). If a platform triangle is hit, then we set the character on the point specified by the \textit{RayHitInfo} structure.\\
\linebreak

Finally, to determine the state of the character, we are using a simple finite state machine, with three main states; flying, walking and dead\_air:

\begin{figure} [h!]
\centering
\includegraphics[scale=0.6]{Data/uea-fsm.png}
\caption{FSM Update}
\end{figure}

\item \textbf{Character - UFO laser}\\
The enemies shoot at Dr Goggler, the projectiles being little rectangular meshes with a box bounding volume. Thanks to the engine's collision response event system, checking for \textit{laser - character} collision is simple. The \textit{laser} node has a specific tag, with tag name being ``enemylaser". We check if the collided node has this tag this way:
\begin{figure} [h!]
\centering
\includegraphics[scale=0.6]{Data/uea-lasers.png}
\caption{Lasers hitting Dr Goggler}
\end{figure}

A similar concept is implemented for the \textit{UFO - Character laser} collision detection, but it's implemented in the \textit{UfoAiBehaviour}.

\end{itemize}

\section{Scoring}
The game scoring is based around collecting valuable items from platforms and killing UFOs, more specifically:\\
\linebreak
1 UFO kill = 100pts\\
1 Euro coin = 100pts\\
2 Euro coin = 200pts\\
5 Euro coin = 500pts\\
Gem = 1000pts\\
\linebreak
Moreover, since the game allows for progressions through levels, the score is level-persistent.

\section{User Manual}

\begin{figure} [h!]
\centering
\includegraphics[scale=0.6]{Data/ctrls.png}
\caption{The control mappings for the game}
\end{figure}

The controls are indicated in-game in the Controls menu. Use the keyboard (or left thumb-stick) to move the character and the mouse (or right thumb-stick) to aim. You can lock-on to enemies by pressing the right mouse button (or the left bumper). Make sure that the UFOs are in sight. The Head-up display (HUD) will highlight which enemy you are targeting. Press the fire key to fire a homing orb. When you are out of orbs you can still use lasers, simply press and hold the fire key to emit lasers.

You will have to eventually land on platforms from time to time. To do so, simple lower the character by pressing Ctrl (or Left-Trigger). Make sure not to land to fast or the character will get stunned and hurt. To pick up items simply walk over them, you like hear a sound and see particles as you pick them up.

Watch your HUD carefully for information about your score and your character's status.

\begin{figure} [h!]
\centering
\includegraphics[scale=0.6]{Data/hud.png}
\caption{The in-game HUD}
\end{figure}

\end{document}