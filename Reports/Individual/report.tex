\documentclass[]{ueacmpstyle}
\usepackage{cite}
\usepackage{graphics}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{float}

\begin{document}

\title{Games Development \linebreak {\normalsize Dr Goggler Individual Technical Report}}

\author{Pantelis Lekakis\\CMP 5910471}

\maketitle

\tableofcontents

\pagebreak

\section{Prologue}
My main task in this project was to design a stable and solid cross-platform infrastructure to build the game upon, so I started developing the engine after having learnt from previous projects and design flaws. The way I structured the system is by maintaining a high level of isolation between modules and ensuring an error free environment by implementing only features needed for the game. More specifically, the major components of the engine which I developed include a hierarchical scenegraph, mesh and particle renderers, a reference counted resource manager and a fully data driven material system. Even though impossible to cover all areas of development, I will try to be precise and detailed on the aforementioned components.

\section{Engine Structure}
Below I am presenting a simplified diagram of the engine, however it contains all the major components that we will discuss in this report.
\begin{figure}[H]
  \caption{UEA Engine Diagram}
  \centering
    \includegraphics[scale=0.55]{Data/uea-engine.png}
\end{figure}

\pagebreak

\section{Scenegraph}
To represent the objects in our game we use a hierarchical scenegraph based on ideas deriving from Unity3D, more importantly, there are no specialised nodes for representing Mesh, Particles or whatever other renderable element. Instead, one SceneNode can include instances of meshes, particles, audio sources and everything that the engine supports or will support.

\subsection{SceneNode}
The \textit{SceneNode} is the basic element in the scene and apart from  attaching meshes, particle systems or other child nodes to it, it can also have a \textit{Collider} object for bounding volume versus bounding volume. The \textit{Collider} is an abstract encapsulation of a bounding volume and it can be subclassed as \textit{SphereCollider} or \textit{BoxCollider}. Below I am presenting the base  class declaration and the \textit{Collide} method for the \textit{SphereCollider}:
\begin{figure}[H]
\begin{minipage}[t]{0.5\linewidth}
\centering
\includegraphics[scale=0.47]{Data/uea-collider.png}
\caption{Collider base class}
\label{fig:figure3}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[t]{0.5\linewidth}
\centering
\includegraphics[scale=0.47]{Data/uea-collider2.png}
\caption{SphereCollider::Collide method}
\label{fig:figure4}
\end{minipage}
\end{figure}

Setting weather a node can collide or not with other nodes can be achieved by two methods:\\
1) Adding the node to a \textit{CollisionGroup}, which is required for nodes to collide with each other,\\
2) Explicitly selecting to ignore nodes in the collision process.\\
\linebreak
I will go into further details regarding the \textit{CollisionGroups} in the next section.

\subsection{Scene}
The \textit{Scene} is responsible for managing the \textit{SceneNodes} and their behaviours, handling collisions between nodes and culling invisible ones based on their bounding sphere. Following is a diagram showing the \textit{Scene} update sequence:
\begin{figure}[H]
  \caption{Updating the scene}
  \centering
    \includegraphics[scale=0.47]{Data/uea-sceneupdate.png}
\end{figure}

\subsubsection{SceneNodeBehaviour}
As seen in the following code snippet, a \textit{SceneNodeBehaviour} is a class which the developer can subclass to provide his own functionality for the node. The most important ones are the \textit{Update} and \textit{OnCollide} / \textit{OnTrigger} methods.
\begin{figure}[H]
  \caption{SceneNodeBehaviour base class}
  \centering
    \includegraphics[scale=0.47]{Data/uea-behaviour.png}
\end{figure}

A node can have one or more behaviours, stored by name in a hash-map, so when the \textit{Scene} comes to update the nodes, it also iterates through each behaviour, calling its \textit{Update} method. In the example of a character, you could assign a behaviour to its node to separate character update logic from the rest of the game.\\
The \textit{OnCollide} and \textit{OnTrigger} methods are essentially events called whenever a collision occurs. More specifically:\\
\linebreak
1) \textit{OnCollideEnter / OnTriggerEnter} is called at the moment the collision happened and is only called once.\\
2) \textit{OnCollideStay / OnTriggerStay} is called while the collision keeps happening.\\
3) \textit{OnCollideExit / OnTriggerExit} is called when the collision stops.\\
Collision response is implemented in this manner, so the game logic side can handle those events and act accordingly.

\subsubsection{Collisions}
To check for collisions between nodes we use a method that was introduced by NVIDIA PHYSX, \textit{CollisionGroups}. The main idea is that each node can belong to a group, and those groups are mapped in a 32 by 32 symmetrical matrix of boolean values, to set which groups can collide.\\
\begin{figure}[H]
  \caption{CollisionCombinations class declaration}
  \centering
    \includegraphics[scale=0.47]{Data/uea-colgroups.png}
\end{figure}
By default all collision group combinations are set the false, so no collisions will happen. It's up to the game logic to define its own collision groups and set which need to collide with each other. In our game for example, the character group is colliding with the platform, ufo, powerups and laser groups, however the ufo group is colliding only with the lasers and the character. As a result the number of intersection tests is drastically decreased.\\

Let's see what the sequence of collision detection is:
\begin{figure}[H]
  \caption{Scene handling collisions}
  \centering
    \includegraphics[scale=0.47]{Data/uea-collisionsource.png}
\end{figure}
The extreme situation would be that all nodes have a collider, every collider is enabled, and all the nodes are included in groups that have collisions enabled with each other, however this never happens. Usually, only about 1/4 of the nodes will be colliding with each after some tests we did while developing this method.

\section{Resource Management}
Resources such as textures, meshes, materials and so on are loaded in the engine's structures using a reference counted resource system. As we can see from the following diagram, the ResourceManager has two main jobs:\\
\linebreak
1) Create and handle loadable resources\\
2) Keep track of resources that need to be restored after a display mode change, such as textures, shaders and vertex buffer objects and invalidate/restore them when required.\\

\begin{figure}[H]
  \caption{Resource Manager Flow Chart}
  \centering
    \includegraphics[scale=0.5]{Data/uea-resources.png}
\end{figure}
A resource type agnostic, template based approach was used, based on Armin Ronacher's stacked resource manager. Since all engine resources inherit from the Resource base class, it's easy to invalidate and restore them, considering the following code snippet:\\

\begin{figure}[H]
\begin{minipage}[t]{0.5\linewidth}
\centering
\includegraphics[scale=0.47]{Data/invalidate.png}
\caption{Invalidating the resources}
\label{fig:figure3}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[t]{0.5\linewidth}
\centering
\includegraphics[scale=0.47]{Data/restore.png}
\caption{Restoring the resources}
\label{fig:figure4}
\end{minipage}
\end{figure}

Then it's up to each resource to invalidate and restore the video based data as it sees fit. For example, a Texture keeps the system memory copies so that it can restore the OpenGL texture later.\\
A direct result of using such a resource manager is low memory consumption, especially when using the same resources many times, such as textures and shaders being shared between materials.

\section{Rendering}
For the rendering we are using an OpenGL 2.1 context and a fully shader driven design, bypassing the fixed function pipeline. We will examine the different components used in the scene rendering from a bottom up view, starting from the low level, moving up to high level classes.

\subsection{Shader}
In the engine, a shader file describes the GLSL vertex and fragment shader pair that this shader instance will use when bound using glUseProgram. Consider the following diagram for shader creation:
\begin{figure}[H]
  \caption{Shader Compilation}
\begin{center}
\includegraphics[scale=0.5]{Data/uea-shaders.png}
\end{center}
\end{figure}

The most important part of the aforementioned sequence is the enumeration of the active uniforms so that we can set each uniform by name without worrying if the shader has it defined or not. If it doesn't use it, then nothing bad will happen, which is exactly what we want for the renderers and the material system. Consider the following code snippet for setting a 4x4 matrix uniform:
\begin{figure}[H]
  \caption{Shader::SetUniformMatrix4v method}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-shaders-uniform4v.png}
\end{center}
\end{figure}

\subsection{Texture}
A Texture in the engine can either be a cubemap (used for the skybox in the game), or a typical 2D texture. Creating empty textures and manipulating them directly is also possible, and those too are handled by the resource manager.
\subsection{Material}
The engine features a fully data driven material system, setting each shader's parameters. Apart from setting the shader uniform variables, the material also directs OpenGL as to what blending type to use, weather to allow for depth writing, and last but not least textures and their corresponding shader uniform. Let's see how a sample material file looks like:
\begin{figure}[H]
  \caption{User Interface material definition file}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-material.png}
\end{center}
\end{figure}
In the above example, the material is to be used with the UI elements, so when the \textit{UIRenderer} comes to process this material, it will use the \textit{UI.shader} along with the \textit{UITextureAtlas} texture to display our widget. Moreover it sets the blending type to \textit{alpha transparency} and the texture address type to \textit{clamp}.\\

\subsection{Mesh}
For representing 3D objects we are using the Doom 3 MD5 mesh format, supporting many submeshes and one material per submesh. Meshes can be either created at runtime by manually adding vertex, normal, texture coordinate, index data or even animation blend weights and joints, or loaded as a resource from file. Once built, the various vertex buffer objects are created and populated with the required submesh data:
\begin{figure}[H]
  \caption{Populating the submesh VBOs}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-vbo.png}
\end{center}
\end{figure}

\subsubsection{Animation}
It's noticeable that we are using 5 VBOs instead of the normal 3, that is because we are skinning the mesh in the GPU, so we need to send the bone weights and indices to the GLSL vertex shader through the texture coordinates 1 \& 2. the following diagram shows how the animation is updated on each frame:
\begin{figure}[H]
  \caption{Calculating the joints bone matrices}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-joints.png}
\end{center}
\end{figure}

Along with the bone weights and indices, the bone matrices are passed to the skinning shader:
\begin{figure}[H]
  \caption{Skinning on the GPU}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-skinning.png}
\end{center}
\end{figure}

Other simple animation features are loop-able / single shot animations and setting the playback speed.

\subsection{Renderers}
In the engine there 3 renderer static classes which fetch data from the scenegraph depending on the \textit{SceneNode} contents. This is done to keep the rendering separated from the scene representation and the game logic. The renderers themselves are scenegraph and resource agnostic, trying to render the vertex data with the most efficient way, most of the times sorting by texture or shader.

\subsubsection{MeshRenderer}
The MeshRenderer is responsible of rendering static and animated meshes. The meshes rendered are fetched by querying the \textit{SceneGraph} for nodes which include a Mesh instance, excluding nodes which are culled by the frustum, transforming them into \textit{RenderableElements}. Eventually, the renderer only cares about the VBOs, OpenGL shader and texture ids, and material properties assigned to each \textit{RenderableElement}. By using pointers to the original structures, the memory overhead is minimal and optimisation techniques such as sorting by texture is easy without keeping duplicate data.\\
The sequence that the \textit{MeshRenderer} takes for fetching and drawing the different meshes is illustrated in the following diagram:
\begin{figure}[H]
  \caption{MeshRenderer pipeline}
\begin{center}
\includegraphics[scale=0.5]{Data/uea-meshrenderer.png}
\end{center}
\end{figure}

\subsubsection{ParticleRenderer}
The \textit{ParticleRenderer} works in a similar fashion as the \textit{MeshRenderer} with the difference that it fetches \textit{SceneNodes} with \textit{ParticleSystem} instances. With regard to the particle system in the engine, it is processed in the CPU, with the billboarding happening in the GPU using a specialised particle vertex shader, the billboarding code following:
\begin{figure}[H]
  \caption{Billboarding the particle}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-particleshader.png}
\end{center}
\end{figure}

By giving to the \textit{ParticleRenderer} only the particle position 4 times, and sending the particle size to the GLSL shader, we can expand the particle quad by using the inverse view matrix.

\subsubsection{UIRenderer}
The \textit{UIRenderer} works not on the scenegraph, but on the \textit{UILayer} instance which keeps hold of current frame's UI widgets. The engine's UI system is a scalable, texture atlas-based system running on a virtual resolution of 1920x1080 pixels to achieve resolution independence. The renderer takes care of setting the orthographic projection for this resolution, code following:
\begin{figure}[H]
  \caption{Setting up an orthographic projection}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-ortho.png}
\end{center}
\end{figure}

where \textit{orthoProjectionData} is a float array holding the virtual resolution x, y, width and height information.\\
Having a virtual resolution also means that when we place the UI widgets, we think in virtual resolution space, so we also need to transform the mouse cursor from screen space to virtual resolution space used for clicking specific widgets such as Buttons.\\
The texture atlas for the UI was assembled using the utility Zwoptex by Zwopple LLC, and parsing the output xml file containing the subtexture positions and size in the engine. A great advantage in using a texture atlas is that it allows for less texture bindings and switching between widget states easily.
\begin{figure}[H]
  \caption{Parsing the texture atlas XML file}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-textureatlas.png}
\end{center}
\end{figure}

Once the texture atlas XML is parsed, the different subtextures are stored in a hash-map for fast retrieval by name. So, if we want to draw a \textit{UIButton} with a customisable state, we would select the state like this:
\begin{figure}[H]
  \caption{Setting up texture coordinates for a widget state}
\begin{center}
\includegraphics[scale=0.47]{Data/uea-uistate.png}
\end{center}
\end{figure}

\section{3rd party code and ideas used}
The engine makes use of:\\
1) MD5 Mesh loader  (\textit{http://3dgep.com/?p=1053})\\
2) GLM - OpenGL Mathematics Library (\textit{http://glm.g-truc.net})\\
3) GLFW for window and opengl context creation (\textit{http://www.glfw.org})\\
4) SOIL for image loading (\textit{http://www.lonesock.net/soil.html})\\
5) PHYSFS for virtual filesystem (\textit{http://icculus.org/physfs})\\
6) XML parser developed by Frank Vanden Berghen (\textit{http://www.applied-mathematics.net/tools/xmlParser.html})\\
7) Particle spawn logic based on codesampler billboarding particle tutorial (\textit{http://www.codesampler.com/oglsrc/oglsrc\_6.htm})\\
8) Scenegraph representation based on how Unity3D describes their scenegraph (\textit{http://unity3d.com})\\
9) Directional light GLSL implementation based on lighthouse3d  (\textit{http://www.lighthouse3d.com/tutorials/glsl-tutorial/?dirlightpix})\\


\end{document}