\contentsline {section}{\numberline {1}Prologue}{2}
\contentsline {section}{\numberline {2}Engine Structure}{2}
\contentsline {section}{\numberline {3}Scenegraph}{3}
\contentsline {subsection}{\numberline {3.1}SceneNode}{3}
\contentsline {subsection}{\numberline {3.2}Scene}{3}
\contentsline {subsubsection}{\numberline {3.2.1}SceneNodeBehaviour}{4}
\contentsline {subsubsection}{\numberline {3.2.2}Collisions}{4}
\contentsline {section}{\numberline {4}Resource Management}{5}
\contentsline {section}{\numberline {5}Rendering}{6}
\contentsline {subsection}{\numberline {5.1}Shader}{7}
\contentsline {subsection}{\numberline {5.2}Texture}{7}
\contentsline {subsection}{\numberline {5.3}Material}{8}
\contentsline {subsection}{\numberline {5.4}Mesh}{8}
\contentsline {subsubsection}{\numberline {5.4.1}Animation}{8}
\contentsline {subsection}{\numberline {5.5}Renderers}{9}
\contentsline {subsubsection}{\numberline {5.5.1}MeshRenderer}{9}
\contentsline {subsubsection}{\numberline {5.5.2}ParticleRenderer}{10}
\contentsline {subsubsection}{\numberline {5.5.3}UIRenderer}{10}
\contentsline {section}{\numberline {6}3rd party code and ideas used}{11}
