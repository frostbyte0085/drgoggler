// EngineDriver.cpp

#include <stdio.h>
#include <Platform.h>
#include <Graphics/GraphicsContext/GraphicsContext.h>

bool Init();
void Cleanup();
bool MainLoop();

int main(int argc, char **argv) {
	if (!Init()) {
		printf("Failed to initialize\n");
		return 0;
	}

	printf("Engine initialized!\n");

	while (MainLoop());


	Cleanup();


	return 0;
}

bool Init() {
	PHYSFS_init (nullptr);

	try {
		GraphicsContext::Initialize();
		GraphicsContext::SetFastWindowedMode();
	} catch (const char *message) {
		printf("Exception: %s\n", message);
		return false;
	}

	printf("OpenGL version: %s\n", GraphicsContext::GetGLVersion().c_str());

	return true;
}

void Cleanup() {
	GraphicsContext::Destroy();
}

bool MainLoop() {
	static int i=200;
	GraphicsContext::SwapBuffers();
	return i-- > 0;
}