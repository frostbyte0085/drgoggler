/*
 Author: 
 Manuel Miguel Feria Gonzalez
 
 Description:
 An AudioClip represends the FMOD loaded sound and it is attached to an AudioSource to be played.
*/

#ifndef AUDIOCLIP_H_DEF
#define AUDIOCLIP_H_DEF

#include <Platform.h>
#include <fmod/fmod.hpp>
#include <fmod/fmod_errors.h>
#include <string>

class AudioClip {
    friend class AudioResource;
    friend class AudioSource;
    
    AudioClip();
    ~AudioClip();
    
public:

private:
    bool createAudioClip(const String fullfilename, bool sound3d, bool streamed);
	FMOD::Sound     *sound;
	FMOD_MODE mode;

	bool audioClipCreated;
};

#endif
