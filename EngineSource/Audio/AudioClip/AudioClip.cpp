#include "AudioClip.h"

#include <Utilities/Exception.h>
#include <Audio/AudioSystem/AudioSystem.h>
#include <string>

AudioClip::AudioClip() {
    audioClipCreated = false;
    
    sound = nullptr;
}

AudioClip::~AudioClip() {
	// Free sound's memory
    if (sound)
        sound->release();
    
    audioClipCreated = false;
}


//This function create a new audioclip with a filename given
bool AudioClip::createAudioClip(const String fullfilename, bool sound3d, bool streamed) {
    if (sound3d)
        mode = FMOD_3D;
    else
        mode = FMOD_DEFAULT;
    
    mode |= FMOD_HARDWARE;
    
	//Convert string to Char *
	char * cfullfilename; 
	cfullfilename = new char [fullfilename.size()+1];
    strcpy (cfullfilename, fullfilename.c_str());

    if (!streamed) {
        if (AudioSystem::system->createSound(cfullfilename, mode, 0, &sound) != FMOD_OK)
            return false;
    }
    else {
        
        FMOD_CREATESOUNDEXINFO exInfo;
        memset (&exInfo, 0, sizeof(exInfo));

        exInfo.cbsize = sizeof(exInfo);
        exInfo.length = 0;
        
        // create the audio clip using a nonblocking streaming method.
        mode |= FMOD_CREATESTREAM | FMOD_NONBLOCKING;
        
        if (AudioSystem::system->createStream(cfullfilename, mode, 0, &sound) != FMOD_OK)
            return false;
         
    }

    audioClipCreated = true;
    return true;
}