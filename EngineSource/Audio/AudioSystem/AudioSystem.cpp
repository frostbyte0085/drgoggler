#include "AudioSystem.h"
#include "Utilities/Exception.h"
#include "../AudioSource/AudioSource.h"

FMOD::System* AudioSystem::system;
unsigned int AudioSystem::version;
int AudioSystem::numdrivers;
int AudioSystem::numchannels;
FMOD_SPEAKERMODE AudioSystem::speakermode;
FMOD_CAPS AudioSystem::caps;
char AudioSystem::device_name[256];

//System listener attributes
FMOD_VECTOR AudioSystem::listenerpos;
FMOD_VECTOR AudioSystem::forward;
FMOD_VECTOR AudioSystem::up;
FMOD_VECTOR AudioSystem::vel;

float AudioSystem::minDistance = 0.0f;
float AudioSystem::maxDistance = 100.0f;

bool AudioSystem::paused = false;

Set<AudioSource*> AudioSystem::sources;

//This functions initialize the audio sytem.
void AudioSystem::Initialize() {
	
	//Create a System object and initialize.
	FMOD::System_Create(&system);

    // do not use a multithreaded version of FMOD, it crashes on application exit even though we stop and release all the
    // sounds.
	system->init(500, FMOD_INIT_STREAM_FROM_UPDATE, 0);
    system->set3DSettings(1.0f, 500.0f, 1.0f);
    
	Log ("AudioSystem::Initialize: Success");
}

//This function destroy the audiosystem
void AudioSystem::Destroy(){
    Stop();
	sources.clear();
    
	if (system)
        system->close();

	//Free the object's memory.
    if (system)
        system->release();

	Log ("AudioSystem::Destroy: Success");
}

//Update the Audiosystem
void AudioSystem::Update()
{
    if (system)
        system->update();
}

/*
 Iterate through all the registered AudioSources and Stop playing them.
*/
void AudioSystem::Stop(AudioSource *ignoreSource) {
    Set<AudioSource*>::iterator it;
    for (it=sources.begin(); it!=sources.end(); it++) {
        AudioSource *source = *it;
        if (source == ignoreSource)
            continue;
        
        if (source) {
            source->Stop();
        }
    }
}

/*
 Iterate through all the registered AudioSources and Pause/Resume playing them.
*/
void AudioSystem::SetPause(bool p, AudioSource *ignoreSource) {
    paused = p;

    Set<AudioSource*>::iterator it;
    for (it=sources.begin(); it!=sources.end(); it++) {
        AudioSource *source = *it;
        if (source == ignoreSource)
            continue;
        
        if (source) {
            source->SetPaused(paused);
        }
    }    
}

//This function updates the listener attributes
void AudioSystem::UpdateListener(const Vector3 &position, const Vector3 &velocity, const Vector3 &forwardOrientation, const Vector3 &upOrientation){

    
    listenerpos.x = position.x;
	listenerpos.y = position.y;
	listenerpos.z = position.z;
    
	vel.x = velocity.x;
	vel.y = velocity.y;
	vel.z = velocity.z;
    
	forward.x = forwardOrientation.x;
	forward.y = forwardOrientation.y;
	forward.z = forwardOrientation.z;
    
	up.x = upOrientation.x;
	up.y = upOrientation.y;
	up.z = upOrientation.z;
    
	if (system)
        system->set3DListenerAttributes(0, &listenerpos, &vel, &forward, &up);
}

void AudioSystem::PutDefaultlistener(){
	const FMOD_VECTOR aux = {0.0f,0.0f,0.0f};
	//SetListenerAttributes(aux, aux, aux, aux);
	if (system)
        system->set3DListenerAttributes(0, &aux, &aux, &aux, &aux);
}