/*
 Author: 
 Manuel Miguel Feria Gonzalez
 
 Description:
 An FMOD based sound system supporting 3D positional audio sources and streamable audio clips.
 
*/
#ifndef AUDIO_SYSTEM_H_DEF
#define AUDIO_SYSTEM_H_DEF

#include "Platform.h"
#include <fmod/fmod.hpp>
#include <fmod/fmod_errors.h>
#include <vector>

class AudioSource;

class AudioSystem {
    friend class AudioClip;
    friend class AudioSource;
    
public:
    // System lifecycle
    static void Initialize();
    static void Destroy();

    static void SetDefault3DMinMaxDistance (float minimum, float maximum) {minDistance = minimum; maxDistance=maximum; }
    
    static void UpdateListener(const Vector3 &position, const Vector3 &velocity, const Vector3 &forwardOrientation, const Vector3 &upOrientation);
	static void PutDefaultlistener();

    static void SetPause(bool p, AudioSource *ignoreSource=nullptr);
    static bool IsPaused() { return paused; }
    static void Stop(AudioSource *ignoreSource=nullptr);
    
	static void Update();
    
private:
	//System variables
    static float minDistance, maxDistance;
    
    static bool paused;
    
    static Set<AudioSource*> sources;
    
	static FMOD::System *system;
	static unsigned int version;
	static int numdrivers;
	static int numchannels;
	static FMOD_SPEAKERMODE speakermode;
	static FMOD_CAPS caps;
	static char device_name[256];

	//System listener attributes
	static FMOD_VECTOR listenerpos;
	static FMOD_VECTOR forward;
	static FMOD_VECTOR up;
	static FMOD_VECTOR vel;

	//System sounds result for errors
	static FMOD_RESULT result;
};

#endif
