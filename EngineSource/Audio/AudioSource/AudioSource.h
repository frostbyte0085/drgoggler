/*
 Author: 
 Manuel Miguel Feria Gonzalez
 
 Description:
 The AudioSource is an encapsulation for playing and applying operations on an attached AudioClip.
 
*/
#ifndef AUDIO_SOURCE_H_DEF
#define AUDIO_SOURCE_H_DEF

#include <Platform.h>

class AudioClip;

class AudioSource {
public:
    AudioSource(AudioClip *clip);
    ~AudioSource();
    
    void SetAudioClip (AudioClip *clip);
    AudioClip* GetAudioClip() const { return clip; }
    
    void SetMinMaxDistance (float minimum, float maximum);
    void Update (const Vector3 &position, const Vector3 &velocity);
    void Play(bool loop=false, bool force=true);
    void Stop();
    bool IsPaused() const { return paused; }
    void SetPaused(bool p);
    bool IsPlaying();
    void SetVolume (float vol);
    float GetVolume() const;
    
private:
    AudioClip *clip;
    
    FMOD::Sound *sound;
    FMOD::System *system;
    FMOD::Channel *channel;
    
    float minDistance;
    float maxDistance;
    
    Vector3 position;
    Vector3 velocity;
    
    bool paused;
};

#endif
