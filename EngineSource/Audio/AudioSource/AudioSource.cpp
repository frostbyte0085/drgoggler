#include "AudioSource.h"
#include "../AudioClip/AudioClip.h"
#include "../AudioSystem/AudioSystem.h"

AudioSource::AudioSource (AudioClip *clip) {
    assert (clip);
    this->clip = clip;
    
    paused = false;
    
    sound = clip->sound;
    system = AudioSystem::system;
    
    // get a channel for this sound, but stop it immediately.
    system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel);
    if (channel) {
        channel->set3DMinMaxDistance(AudioSystem::minDistance, AudioSystem::maxDistance);
        channel->stop();
    }
    
    AudioSystem::sources.insert (this);
}

AudioSource::~AudioSource() {
    AudioSystem::sources.erase (this);
    
    Stop();
}

void AudioSource::SetMinMaxDistance (float minimum, float maximum) {
    minDistance = minimum;
    maxDistance = maximum;
}

void AudioSource::SetVolume (float vol) {
    if (channel) {
        channel->setVolume (vol);
    }
}

float AudioSource::GetVolume () const {
    float vol = 0.0f;
    if (channel) {
        channel->getVolume(&vol);
    }
    return vol;
}

void AudioSource::Play(bool loop, bool force) {
    
    if (IsPlaying() && !force)
        return;
    
    system->playSound(FMOD_CHANNEL_FREE, sound, false, &channel);
    
    if (channel) {
        if (loop) {
            channel->setMode(FMOD_LOOP_NORMAL);
        }
        channel->set3DMinMaxDistance (minDistance, maxDistance);
    }
}

bool AudioSource::IsPlaying() {
    bool playing = false;
    if (channel)
        channel->isPlaying(&playing);
    
    return playing && !paused;
}

void AudioSource::SetPaused(bool p) {
    if (channel) {
        paused = p;
        channel->setPaused(paused);
    }
}

void AudioSource::Stop() {
    if (!IsPlaying())
        return;
    
    if (channel)
        channel->stop();
}

void AudioSource::Update (const Vector3 &position, const Vector3 &velocity) {
    if (!channel)
        return;
    
    this->position = position;
    this->velocity = velocity;
    
    FMOD_VECTOR pos;
    pos.x = position.x;
    pos.y = position.y;
    pos.z = position.z;
    
    FMOD_VECTOR vel;
    vel.x = velocity.x;
    vel.y = velocity.y;
    vel.z = velocity.z;
    
    channel->set3DAttributes(&pos, &vel);
}