#import "Platform.h"
#import "BundleSupport.h"
#import <Cocoa/Cocoa.h>
#include <ApplicationServices/ApplicationServices.h>

/*
void SetWorkingDir()
{
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    [[NSFileManager defaultManager]
    changeCurrentDirectoryPath:resourcePath];
}

void OSXShowCursor()
{
    CGDisplayShowCursor (kCGDirectMainDisplay);
}

void OSXHideCursor()
{
    CGDisplayHideCursor (kCGDirectMainDisplay);
}

 * */

String Bundle_GetApplicationName() {
    NSProcessInfo *pi = [NSProcessInfo processInfo];
    NSString *appName = [pi processName];
    
    return [appName UTF8String];
}

String Bundle_GetContentsDirectory() {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle bundlePath];
    
    return [path UTF8String];
}