#include "ParticleSystem.h"
#include <Application/Timing.h>
#include <Mathematics/Math.h>

ParticleSystem::ParticleSystem() {
    aliveParticles = nullptr;
    freeParticles = nullptr;
    
    material = nullptr;
    
    psCreated = false;
    lastUpdate = 0.0f;
    currentTime = 0.0f;
    aliveParticlesCount = 0;
    totalParticlesCreated = 0;
    emittedOne = false;
    emit = false;
}

ParticleSystem::~ParticleSystem() {
    destroyParticleLists();
    
    particleSizes.clear();
    particleTexCoords.clear();
    particleVertices.clear();
    particleColors.clear();
    
    indexBuffer.clear();
}

bool ParticleSystem::createParticleSystem() {
    psCreated = true;

    particleColors.reserve (Emitter.MaxEmission);
    particleVertices.reserve(Emitter.MaxEmission);
    particleTexCoords.reserve(Emitter.MaxEmission);
    particleSizes.reserve(Emitter.MaxEmission);
    indexBuffer.reserve (Emitter.MaxEmission * 6);
    
    return Build();
}

void ParticleSystem::restoreVideoBasedData() {
    glGenBuffers(4, vbos);

}

void ParticleSystem::destroyVideoBasedData() {
    glDeleteBuffers (4, vbos);
}

bool ParticleSystem::Build() {
    restoreVideoBasedData();
    
    return true;
}

void ParticleSystem::Emit() {
    emit = true;
}

void ParticleSystem::Stop() {
    emit = false;
    emittedOne = false;
    destroyParticleLists();
}

void ParticleSystem::destroyParticleLists() {
    totalParticlesCreated = 0;
    
    while(aliveParticles) {
        Particle *p = aliveParticles;
        aliveParticles = p->NextParticle;
        delete p;
    }
    aliveParticles = nullptr;
    
    while(freeParticles) {
        Particle *p = freeParticles;
        freeParticles = p->NextParticle;
        delete p;
    }
    freeParticles = nullptr;    
}

void ParticleSystem::Update() {
    particleTexCoords.resize(0);
    particleVertices.resize(0);
    particleColors.resize(0);
    particleSizes.resize(0);
    indexBuffer.resize(0);

    if (!emit)
        return;
    
    Particle *particle;
    Particle **particleList = &aliveParticles;
    
    currentTime += Timing::LastFrameTime;
    
    int randomEmission = Math::Random(Emitter.MinEmission, Emitter.MaxEmission);
    
    int p=0; 
    while (*particleList) {
        particle = *particleList;

        if (particle->Age > particle->DyingAge) {
            *particleList = particle->NextParticle;
            particle->NextParticle = freeParticles;
            freeParticles = particle;
            
            aliveParticlesCount--;
            
            if (totalParticlesCreated >= randomEmission && Emitter.EmitOnce) {
                if (aliveParticlesCount == 0) {
                    Stop();
                    return;
                }
            }
            
        }
        else {
            // update here
            particle->Velocity += Emitter.Gravity * Timing::LastFrameTime;
            particle->Position += particle->Velocity * Timing::LastFrameTime;
            particle->Age += Timing::LastFrameTime;
            particle->Size += Animator.SizeGrow * Timing::LastFrameTime;
            particle->Size = Math::Clamp (particle->Size, Animator.MinimumSize, Animator.MaximumSize);
            
            // color interpolation
            float age = 1.0f - (particle->DyingAge - particle->Age) / particle->DyingAge;
            
            float startAge = 0.0f;
            float endAge = 0.2f;
            
            Vector4 color1 = Animator.Color1, color2=Animator.Color2;
            
            if (age > 0.2f && age <= 0.4f) {
                color1 = Animator.Color2;
                color2 = Animator.Color3;
                startAge = 0.2f;
                endAge = 0.4f;
            }
            else if (age > 0.4f && age <= 0.6f) {
                color1 = Animator.Color3;
                color2 = Animator.Color4;
                startAge = 0.4f;
                endAge = 0.6f;
            }
            else if (age > 0.6f && age <= 0.8f) {
                color1 = Animator.Color4;
                color2 = Animator.Color5;
                startAge = 0.6f;
                endAge = 0.8f;
            }
            else if (age > 0.8f && age <= 1.0f) {
                color1 = Animator.Color5;
                color2 = Animator.Color6;
                startAge = 0.8f;
                endAge = 1.0f;
            }
            
            float colorT = (age - startAge) / (endAge - startAge);
            colorT = Math::Clamp(colorT, 0.0f, 1.0f);
            
            if (age >= 0.0f && age < 1.0f)
                particle->Color = glm::mix(color1, color2, colorT);
            else
                particle->Color = Vector4(0,0,0,0);
            
            float size = particle->Size;
            
            // add the vertices, colors, sizes, texture coordinates and index information to the vectors
            //
            for (int j=0; j<4; j++) {
                particleVertices.push_back(particle->Position);
                particleColors.push_back(particle->Color);
                particleSizes.push_back(Vector2(size, size));
            }
            
            particleTexCoords.push_back(Vector2(0,0));
            particleTexCoords.push_back(Vector2(1,0));
            particleTexCoords.push_back(Vector2(1,1));
            particleTexCoords.push_back(Vector2(0,1));
            
            indexBuffer.push_back((p*4) + 0);
            indexBuffer.push_back((p*4) + 1);
            indexBuffer.push_back((p*4) + 2);
            indexBuffer.push_back((p*4) + 2);
            indexBuffer.push_back((p*4) + 3);
            indexBuffer.push_back((p*4) + 0);
            
            p++;
            
            particleList = &particle->NextParticle;
        }
    }
    
    if ( (currentTime - lastUpdate > Emitter.Interval) && !emittedOne) {
        lastUpdate = currentTime;
        
        for (int i=0; i<Emitter.ParticlesPerEmission; i++) {
            if (freeParticles) {
                particle = freeParticles;
                freeParticles = particle->NextParticle;
            }
            else {
                if (aliveParticlesCount < (unsigned int)randomEmission) {
                    particle = new Particle();
                }
                else {
                    emittedOne = Emitter.EmitOnce;
                }
            }
            
            if (aliveParticlesCount < (unsigned int)randomEmission && emit) {
                totalParticlesCreated ++;
                
                particle->NextParticle = aliveParticles;
                aliveParticles = particle;
                
                particle->DyingAge = Math::RandomF(Emitter.MinEnergy, Emitter.MaxEnergy);
                particle->Position = Vector3(0,0,0);
                particle->Size = Math::RandomF(Emitter.MinSize, Emitter.MaxSize);
                
                float randomVelX = Math::RandomF(-Emitter.RandomVelocity.x, Emitter.RandomVelocity.x);
                float randomVelY = Math::RandomF(-Emitter.RandomVelocity.y, Emitter.RandomVelocity.y);
                float randomVelZ = Math::RandomF(-Emitter.RandomVelocity.z, Emitter.RandomVelocity.z);
                
                particle->Velocity = Emitter.Velocity + Vector3(randomVelX, randomVelY, randomVelZ);
                particle->Age = 0.0f;
                particle->Color = Animator.Color1;
                
                aliveParticlesCount++;
            }
        }
    }
}