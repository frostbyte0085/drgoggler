/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The ParticleSystem handles creation and destruction of particles, updating their parameters based on their age and 
 interpolating colors.
 
 */

//
// Update sequence based roughly on http://www.codesampler.com/oglsrc/oglsrc_6.htm
//
#ifndef PARTICLE_SYSTEM_H_DEF
#define PARTICLE_SYSTEM_H_DEF

#include "Particle.h"

class Material;

class ParticleSystem {
    friend class ParticleSystemResource;
    friend class ParticleRenderer;
    
public:
    /*
     The ParticleAnimator handles color and size changes over age
     */
    class ParticleAnimator {
    public:
        ParticleAnimator();
        ~ParticleAnimator();
        
        Vector4 Color1;
        Vector4 Color2;
        Vector4 Color3;
        Vector4 Color4;
        Vector4 Color5;
        Vector4 Color6;
        
        float SizeGrow;
        float MinimumSize;
        float MaximumSize;
    private:
        
    };
    
    /*
     The ParticleEmitter contains emitter specific parameters such as emission rate,
     particle size, random velocity, etc.
     
     Emit once means that the particle system will stop emitting once all the particles are dead.
     */
    class ParticleEmitter {
    public:
        ParticleEmitter();
        ~ParticleEmitter();
        
        float Interval;
        int ParticlesPerEmission;
        
        // size
        float MinSize;
        float MaxSize;
        
        // energy - lifetime of the particle
        float MinEnergy;
        float MaxEnergy;
        
        // emission - how many particles are we spawning?
        int MinEmission;
        int MaxEmission;
        
        // velocity
        Vector3 Velocity;
        Vector3 RandomVelocity;
        
        // gravity
        Vector3 Gravity;
        
        bool EmitOnce;
        
    private:
        
    };
    ~ParticleSystem();
    
    ParticleAnimator Animator;
    ParticleEmitter Emitter;
    
    void SetMaterial (Material *material) { assert(material); this->material = material; }
    Material* GetMaterial() const { return material; }
    
    unsigned int GetAliveParticlesCount() const { return aliveParticlesCount; }
    bool Build();
    void Emit();
    bool IsEmitting() const { 
        return emit;
    }
    void Stop();
    void Update();
    
    
private:
    ParticleSystem();
    
    void destroyParticleLists();
    
    bool createParticleSystem();
    
    void destroyVideoBasedData();
    void restoreVideoBasedData();
    
    int totalParticlesCreated;
    
    bool psCreated;
    
    Particle *aliveParticles;
    Particle *freeParticles;
    unsigned int aliveParticlesCount;
    
    float currentTime;
    float lastUpdate;
    
    bool emit;
    bool emittedOne;
    
    GLuint vbos[4]; // position, color, texcoord and size (as texcoord1)
    
    Vector<Vector3> particleVertices;
    Vector<Vector2> particleTexCoords;
    Vector<Vector2> particleSizes;
    Vector<Vector4> particleColors;

    Material *material;
    
    Vector<GLuint> indexBuffer;
};

#endif
