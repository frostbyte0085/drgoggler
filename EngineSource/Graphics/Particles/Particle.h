/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Defines the particle paramters.
 
 */
#ifndef PARTICLE_H_DEF
#define PARTICLE_H_DEF

#include <Platform.h>

class Particle {
    friend class ParticleSystem;
    friend class ParticleRenderer;
public:
    Particle();
    ~Particle();

    float       InitTime;
    float       Size;
    Vector3     Velocity;
    Vector3     Position;
    Vector4     Color;
    float       Age;
    float       DyingAge;
    
    Particle *NextParticle;
protected:
};

#endif
