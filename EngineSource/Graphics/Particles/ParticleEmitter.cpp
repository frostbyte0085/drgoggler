#include "ParticleSystem.h"

ParticleSystem::ParticleEmitter::ParticleEmitter() {
    Interval = 1.0f;
    ParticlesPerEmission = 1;
    
    MinSize = 5;
    MaxSize = 5;
    
    MinEnergy = 1.0f;
    MaxEnergy = 1.0f;
    
    MinEmission = 50;
    MaxEmission = 50;
    
    Velocity = Vector3(0,0,0);
    RandomVelocity = Vector3(100,100,100);
    
    Gravity = Vector3(0,0,0);
    
    EmitOnce = false;
}

ParticleSystem::ParticleEmitter::~ParticleEmitter() {
    
}