#include "ParticleSystem.h"

ParticleSystem::ParticleAnimator::ParticleAnimator() {
    Color1 = Vector4(1,1,1,0.0f);
    Color2 = Vector4(1,1,1,1.0f);
    Color3 = Vector4(1,1,1,1.0f);
    Color4 = Vector4(1,1,1,0.8f);
    Color5 = Vector4(1,1,1,0.6f);
    Color6 = Vector4(1,1,1,0.0f);
    
    SizeGrow = 0.0f;
    MinimumSize = 0.0f;
    MaximumSize = 1000.0f;
}

ParticleSystem::ParticleAnimator::~ParticleAnimator() {
    
}