/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The DisplayMode which can be used to set the resolution when the application starts.
 
 */
#ifndef DISPLAYMODE_H_DEF
#define DISPLAYMODE_H_DEF

#include <Platform.h>
#include <Utilities/StringOperations.h>

class DisplayMode
{
public:
    DisplayMode() : Width(0), Height(0) {}
    DisplayMode(int width, int height)
    {
        SetMode (width, height);
    }
    
    // should be widthxheight
    DisplayMode(const String &displayString)
    {
        Width = getDisplayWidthFromString (displayString);
        Height = getDisplayHeightFromString (displayString);
    }
    
    void SetMode(int width, int height) {
        Width = width;
        Height = height;
    }
    
    int Width;
    int Height;
    
    inline DisplayMode operator = ( const DisplayMode& other )
    {
        return DisplayMode (other.Width, other.Height);
    }
    
    inline bool operator == (const DisplayMode& other ) const
    {
        return ( (Width == other.Width ) && (Height == other.Height) );
    }
    
private:
    int getDisplayWidthFromString(const String &displayMode)
    {
        int xIndex = (int)displayMode.find_first_of("x");
        String swwidth = displayMode.substr (0, xIndex);
        return StringOperations::FromString<int>(swwidth);
    }
    
    int getDisplayHeightFromString(const String &displayMode)
    {
        int xIndex = displayMode.find_first_of("x");
        String swheight = displayMode.substr(xIndex+1);
        return StringOperations::FromString<int>(swheight);
    }
};

#endif
