#include "GraphicsContext.h"
#include <Utilities/Exception.h>
#include <Resources/ResourceManager.h>
#include <Input/Input.h>
#include <Renderers/MeshRenderer/MeshRenderer.h>
#include <Renderers/UIRenderer/UIRenderer.h>
#include <Renderers/PostProcessRenderer/PostProcessRenderer.h>

DisplayMode GraphicsContext::desktopDisplayMode;
DisplayMode GraphicsContext::currentDisplayMode;
Vector<DisplayMode> GraphicsContext::displayModes;
bool GraphicsContext::isFullscreen = false;
bool GraphicsContext::hasOpenedWindow = false;
String GraphicsContext::renderer = "";
String GraphicsContext::version = "";
bool GraphicsContext::displayModeChanged = false;
bool GraphicsContext::firstRun = true;
Vector4 GraphicsContext::BackgroundColor = Vector4(0,0,0.3f,1);
int GraphicsContext::ClearFlags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;

void GraphicsContext::Initialize() {
    if (!glfwInit()) {
        Log ("GraphicsContext::Initialize: Failed (cannot created graphics context)");
        throw new Exception ("Cannot initialize graphics context!");
    }
    
    // enumerate our display mode and keep them in our own DisplayMode classes.
    GLFWvidmode glfwModes[500];
    int modeCount = glfwGetVideoModes (glfwModes, 500);
    
    for (int i=0; i<modeCount; i++) {
        GLFWvidmode glfwMode = glfwModes[i];
        
        // ignore really low resolutions
        if (glfwMode.Width < 640)
            continue;
        
        // only allow for 32bit display modes (24 + 8 for alpha)
        int displayModeBit = glfwMode.RedBits + glfwMode.GreenBits + glfwMode.BlueBits;
        
        if (displayModeBit < 24)
            continue;
        
        displayModes.push_back (DisplayMode(glfwMode.Width, glfwMode.Height));
    }
    
    // get the desktop resolution and keep it our own DisplayMode class.
    GLFWvidmode glfwDesktopDisplayMode;
    glfwGetDesktopMode (&glfwDesktopDisplayMode);
    
    desktopDisplayMode.SetMode (glfwDesktopDisplayMode.Width, glfwDesktopDisplayMode.Height);

    glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
    // force opengl 2.1 context
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 2);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 1);
    
    Log ("GraphicsContext::Initialize: Success");

}

void GraphicsContext::SetFastFullscreenMode() {
    SetDisplayMode(desktopDisplayMode, true);
}

void GraphicsContext::SetFastWindowedMode() {
    // find a suitable wide screen 16/9 resolution based on the desktop display mode.
    float candidateWidth0 = (float)(3 * desktopDisplayMode.Width / 4);
	float candidateHeight0 = (float)(9 * candidateWidth0 / 16);
    
	float candidateHeight1 = (float)(3 * desktopDisplayMode.Height / 4);
	float candidateWidth1 = (float)(16 * candidateHeight1 / 9);
    
    DisplayMode windowedMode;
    
    if (candidateWidth0 < candidateWidth1)
        windowedMode.SetMode((int)candidateWidth0, (int)candidateHeight0);
	else
        windowedMode.SetMode((int)candidateWidth1, (int)candidateHeight1);
    
    SetDisplayMode(windowedMode, false);
}

void GraphicsContext::SwitchFastWindowedFullscreen() {
    if (isFullscreen)
        SetFastWindowedMode();
    else
        SetFastFullscreenMode();
}

/*
 SetDisplayMode does a number of things:
    1) Call the ResourceManager's invalidateResources method to prepare the video based resources for a display mode change
    2) Destroys the renderers
    3) Re-opens a glfw window with the new resolution
    4) Re-maps the input handles for mouse, joystick and keyboard
    5) Restores renderers and calls the ResourceManager's restoreResources to restore the video based resources after the display mode change.
*/
void GraphicsContext::SetDisplayMode(const DisplayMode &displayMode, bool fullscreen) {
    String modeString = "[" + StringOperations::ToString<int>(displayMode.Width) + "x" + StringOperations::ToString<int>(displayMode.Height) + "]";
    
    if (fullscreen && !DisplayModeSupported(displayMode.Width, displayMode.Height))
        throw new Exception ("Display mode " + modeString + " is not supported for fullscreen mode!");
        
    int windowMode = GLFW_WINDOW;
    if (fullscreen)
        windowMode = GLFW_FULLSCREEN;
    
    displayModeChanged = false;
    
    if (!firstRun) {
        // invalidate video based resources here.
        ResourceManager::InvalidateVideoBasedResources();
        MeshRenderer::Destroy();
        PostProcessRenderer::Destroy();
        UIRenderer::Destroy();
    }
    
	// close the glfw window and re-open it with the new resolution.
    if (hasOpenedWindow) {
        Destroy();
        Initialize();
	}
    
    if (!glfwOpenWindow(displayMode.Width, displayMode.Height, 8, 8, 8, 0, 24, 8, windowMode)) {
        throw new Exception("Video mode changed failed!");
        Log ("GraphicsContext::SetDisplayMode: Failed");
    }
    
    // need to remap the keyboard and mouse callbacks
    Input::remapInputHandlers();
    
    version = (char*)glGetString(GL_VERSION);
    renderer = (char*)glGetString(GL_RENDERER);
    
    std::cout << version << std::endl;
    
	// re-init glew
#if PLATFORM == PLATFORM_WINDOWS
	if (glewInit() == GLEW_OK) {
        if (!GLEW_VERSION_2_1) {
            glewExperimental = GL_TRUE;
            if (glewInit() == GLEW_OK) {
                if (!GLEW_VERSION_2_1) {
                    Log ("GraphicsContext::SetDisplayMode: Failed (opengl 2.1 is not supported)");
                    throw new Exception ("Driver does not support OpenGL 2.1");
                }
            }
        }
    }
    else {
        Log ("GraphicsContext::SetDisplayMode: Failed (cannot expose opengl extensions)");
        throw new Exception ("Driver failed to expose OpenGL extensions.");
    }
#endif

	hasOpenedWindow = true;

    isFullscreen = fullscreen;
    currentDisplayMode = displayMode;
    currentDisplayMode.SetMode (displayMode.Width, displayMode.Height);
    
    if (!firstRun) {
        // restore video based resources here.
        MeshRenderer::Initialize();
        UIRenderer::Initialize();
        PostProcessRenderer::Initialize();
        ResourceManager::RestoreVideoBasedResources();
    }
    
    firstRun = false;
    displayModeChanged = true;
}

void GraphicsContext::Clear() {
    glViewport(0,0,GetCurrentDisplayMode().Width, GetCurrentDisplayMode().Height);
    
    glClearColor (BackgroundColor.r, BackgroundColor.g, BackgroundColor.b, BackgroundColor.a);
    glClear (ClearFlags);
}

void GraphicsContext::SetWindowTitle(const String &title) {
    glfwSetWindowTitle(title.c_str());
}

/*
 See if the specified display mode is enumerated.
*/
bool GraphicsContext::DisplayModeSupported(int width, int height) {
    
    Vector<DisplayMode>::const_iterator it;
    for (it=displayModes.begin(); it!=displayModes.end(); it++) {
        DisplayMode mode = *it;
        
        if (mode.Width == width && mode.Height == height)
            return true;
    }
    
    return false;
}

void GraphicsContext::Update() {
    displayModeChanged = false;
}

void GraphicsContext::Destroy() {
    displayModes.clear();
    
    glfwCloseWindow();
    
    glfwTerminate();
    Log ("GraphicsContext::Destroy: Success");
}

DisplayMode GraphicsContext::GetDesktopDisplayMode() {
    return desktopDisplayMode;
}

void GraphicsContext::SwapBuffers() {
    
    glfwSwapBuffers();
}