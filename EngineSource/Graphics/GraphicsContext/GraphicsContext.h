/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The GraphicsContext uses GLFW to initialise the opengl window, handle display modes and display mode enumeration
 and clear / present the backbuffer.
 
 Supported opengl version is 2.1.
 
 */
#ifndef GFX_CONTEXT_H_DEF
#define GFX_CONTEXT_H_DEF

#include "DisplayMode.h"

class GraphicsContext {

public:
    // context lifecycle
    static void Initialize();
    static void Destroy();
    
    // display modes
    static void Update();
    static void SetDisplayMode (const DisplayMode &displayMode, bool fullscreen);
    static void SetFastWindowedMode ();
    static void SetFastFullscreenMode ();
    static void SwitchFastWindowedFullscreen();
    static DisplayMode GetDesktopDisplayMode();
    static DisplayMode GetCurrentDisplayMode() { return currentDisplayMode; }
    static bool IsFullscreen() { return isFullscreen; }
    static bool DisplayModeSupported (int width, int height);
    static void SetWindowTitle (const String& title);
    static String GetGLVersion() { return version; }
    static String GetGLRenderer() { return renderer; }
    static bool IsModeChanged() { return displayModeChanged; }
    
    // context operations
    static int ClearFlags;
    static Vector4 BackgroundColor;
    
    static void Clear();
    static void SwapBuffers();
    
private:
    static bool firstRun;
    static bool displayModeChanged;
    
    static DisplayMode desktopDisplayMode;
    static Vector<DisplayMode> displayModes;
    static DisplayMode currentDisplayMode;
    static bool isFullscreen;
	static bool hasOpenedWindow;
    static String version;
    static String renderer;
    
};

#endif
