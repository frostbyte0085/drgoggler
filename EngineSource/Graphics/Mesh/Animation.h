/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The MeshAnimation class contains animation specific data such as joint position and orientation as well as
 animation playback parameters, speed and loop type.
 
 The skeletons are interpolated and the bone matrices are passed to the skinning shader through the MeshRenderer for
 animation.
 
 */
#ifndef ANIMATION_H_DEF
#define ANIMATION_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

enum AnimationLoopType {
    ANIM_ONCE = 0,
    ANIM_LOOP = 1
};

class MeshAnimation
{
    friend class Mesh;
    friend class MeshResource;
    friend class MeshRenderer;
    
public:
    MeshAnimation();
    ~MeshAnimation();

    // Update this animation's joint set.
    void Update();

    // The JointInfo stores the information necessary to build the 
    // skeletons for each frame
    struct JointInfo
    {
        String      name;
        int         parentID;
        int         flags;
        int         startIndex;
    };
    typedef Vector<JointInfo> JointInfoList;

    struct Bound
    {
        Vector3   min;
        Vector3   max;
    };
    typedef Vector<Bound> BoundList;

    struct BaseFrame
    {
        Vector3     position;
        Quaternion  orientation;
    };
    typedef Vector<BaseFrame> BaseFrameList;

    struct FrameData
    {
        int             frameID;
        Vector<float>   frameData;
    };
    typedef Vector<FrameData> FrameDataList;

    // A Skeleton joint is a joint of the skeleton per frame
    struct SkeletonJoint
    {
        SkeletonJoint()
            : parent(-1)
            , position(0)
        {}

        SkeletonJoint( const BaseFrame& copy )
            : position( copy.position )
            , orientation( copy.orientation)
        {}

        int             parent;
        Vector3         position;
        Quaternion      orientation;
    };
    typedef Vector<SkeletonJoint> SkeletonJointList;
    typedef Vector<Matrix44> SkeletonMatrixList;

    // A frame skeleton stores the joints of the skeleton for a single frame.
    struct FrameSkeleton
    {
        SkeletonMatrixList  boneMatrices;
        SkeletonJointList   joints;
    };
    typedef std::vector<FrameSkeleton> FrameSkeletonList;

    const FrameSkeleton& GetSkeleton() const
    {
        return animatedSkeleton;
    }

    const SkeletonMatrixList& GetSkeletonMatrixList() const
    {
        return animatedSkeleton.boneMatrices;
    }

    int GetJointCount() const
    {
        return (int)jointInfos.size();
    }

    const JointInfo& GetJointInfo( unsigned int index ) const
    {
        assert( index < jointInfos.size() );
        return jointInfos[index];
    }

    float GetDuration() const { return animationDuration; }
    float GetAnimationTime() const { return animationTime; }
    bool  IsFinished() const { return cycleFinished; }
    int   GetLoopType() const { return loopType; }
    
    void Play(AnimationLoopType loopType);
    void Stop();
    void Pause();
    void Resume();
    void TogglePause();
    bool IsPaused() const;
    
    float Speed;
    
private:
    int md5Version;
    int frameRate;
    int animatedComponentCount;

    float animationDuration;
    float frameDuration;
    float animationTime;
    
    bool cycleFinished;
    bool canPlay;
    bool isPaused;
    
    
    JointInfoList       jointInfos;
    BoundList           bounds;
    BaseFrameList       baseFrames;
    FrameDataList       frames;
    FrameSkeletonList   skeletons;    // All the skeletons for all the frames
    
    FrameSkeleton       animatedSkeleton;
    
    // Build the frame skeleton for a particular frame
    void buildFrameSkeleton( Matrix44 axisChange, FrameSkeletonList& skeletons, const JointInfoList& jointInfo, const BaseFrameList& baseFrames, const FrameData& frameData );
    void interpolateSkeletons( FrameSkeleton& finalSkeleton, const FrameSkeleton& skeleton0, const FrameSkeleton& skeleton1, float interpolate );
    
    AnimationLoopType loopType;

};

#endif//