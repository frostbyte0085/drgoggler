/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Mesh is based on md5mesh file format, however custom meshes can be created as well by manually adding vertex and index information, even bones.
 
 The mesh is meant to be skinned on the gpu using a glsl vertex program, and the MeshRenderer does exactly that with a limit of 4 bones per vertex passed as a texture coordinate.
 
 */
#ifndef MESH_H_DEF
#define MESH_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>
#include "Animation.h"

class Material;
class MeshAnimation;

// Loader based on: http://3dgep.com/?p=1053
// MD5 specs: http://tfc.duke.free.fr/coding/md5-specs-en.html
class Mesh {
    friend class MeshResource;
    friend class MeshRenderer;
    
    Mesh();
public:
    ~Mesh();
    
    void Build();
    void UpdateAnimations();
    
    void MakePlane(float width, float height);
    void MakeSkyBox();
    
    typedef Vector<Vector3> PositionBuffer;
    typedef Vector<Vector3> NormalBuffer;
    typedef Vector<Vector4> WeightBuffer;
    typedef Vector<Vector4> BoneIndexBuffer;
    typedef Vector<Vector2> TexCoordBuffer;
    typedef Vector<GLuint> IndexBuffer;
    

    struct MeshVertex {
        Vector3 position;
        Vector3 normal;
        Vector2 textureCoordinate0;

        Vector4 boneWeights0;
        Vector4 boneIndices0;
        
        int     startWeight;
        int     weightCount;
    };
    
    typedef Vector<MeshVertex> VertexList;
    
    struct Triangle {
        int indices[3];
    };
    
    typedef Vector<Triangle> TriangleList;
    
    struct Weight {
        int     jointID;
        float   bias;
        Vector3 position;
    };
    
    typedef Vector<Weight> WeightList;
    
    struct Joint {
        String      name;
        int         parentID;
        Vector3     position;
        Quaternion  orientation;
    };
    
    typedef Vector<Joint> JointList;
    
    struct Submesh {
        Submesh() {
            material = nullptr;
        }
        Material        *material;
        
        VertexList      vertices;
        TriangleList    triangles;
        WeightList      weights;
        
        PositionBuffer      positionBuffer;
        NormalBuffer        normalBuffer;
        TexCoordBuffer      texCoordBuffer;
        WeightBuffer        boneWeightsBuffer0;
        BoneIndexBuffer     boneIndexBuffer0;
        
        IndexBuffer     indexBuffer;
        
        // vbos
        GLuint          vbos[5]; // vertex, normal, texcoord, bone weights(0,1), bone weight indices(0,1) and index buffer
    };

    typedef Vector<Submesh*> SubmeshList;
    
    unsigned int GetSubmeshCount() const { return (unsigned int)submeshes.size(); }
    
    Submesh* GetSubmesh (int id) { if (id>=0 && id<(int)submeshes.size()) return submeshes[id]; return nullptr; }
    void AddSubmesh (Submesh *mesh) { submeshes.push_back (mesh); }

    MeshAnimation* GetAnimation(const String &name);
    MeshAnimation* GetCurrentAnimation() { return currentAnimation; }
    
    Joint& GetJoint(int id) {return joints[id];}
    
    void Play (const String &name, AnimationLoopType loop);
    void TogglePause();
    void Stop ();
    
private:
    void destroyVideoBasedData();
    void restoreVideoBasedData();
    
    bool            meshCreated;
    
    int             md5Version;
    HashMap<String, MeshAnimation*> animations;
    MeshAnimation *currentAnimation;
    
    JointList       joints;
    SubmeshList     submeshes;
    
    bool isAnimationCompatible (const MeshAnimation &anim);
    void buildBindPose (Matrix44 axisChange);
    
    void prepareSubmesh (Submesh *sm);
    void prepareNormals (Submesh *sm);
    void prepareSubmeshVBO (Submesh *sm);
    
    Vector<Matrix44> bindPose;
    Vector<Matrix44> inverseBindPose;
    Vector<Matrix44> animatedBones;
    
};

#endif
