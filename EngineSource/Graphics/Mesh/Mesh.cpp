#include "Mesh.h"
#include "Animation.h"

#include <Graphics/Material/Material.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/Shader/Shader.h>
#include <Mathematics/Math.h>

Mesh::Mesh() {
    md5Version = -1;
    
    meshCreated = false;
    currentAnimation = nullptr;
}

Mesh::~Mesh() {
    destroyVideoBasedData();
    
    HashMap<String, MeshAnimation*>::iterator it;
    
    for (it=animations.begin(); it!=animations.end(); it++) {
        MeshAnimation *ma = it->second;
        assert (ma);
        
        delete ma;
    }
    animations.clear();
    
    for (int i=0; i<(int)submeshes.size(); i++) {
        Submesh *sm = submeshes[i];
        
        assert(sm);
        
        sm->positionBuffer.clear();
        sm->normalBuffer.clear();
        sm->texCoordBuffer.clear();
        sm->boneIndexBuffer0.clear();
        sm->boneWeightsBuffer0.clear();
        sm->indexBuffer.clear();
            
        sm->vertices.clear();
        sm->triangles.clear();
        sm->weights.clear();
        
        delete sm;
    }
    submeshes.clear();
    joints.clear();
}

void Mesh::Play (const String &name, AnimationLoopType loop) {
    HashMap<String, MeshAnimation*>::iterator it = animations.find (name);
    
    if (it != animations.end()) {
        assert (it->second);
        
        if (it->second != currentAnimation) {
            currentAnimation = it->second;
            currentAnimation->Play (loop);
        }
    }
}

void Mesh::TogglePause() {
    if (currentAnimation) {
        currentAnimation->TogglePause();
    }
}

void Mesh::Stop() {
    if (currentAnimation) {
        currentAnimation->Stop();
    }
}

void Mesh::restoreVideoBasedData() {
    for (int i=0; i<(int)submeshes.size(); i++) {
        Submesh *sm = submeshes[i];
        assert (sm);
        
        prepareSubmeshVBO(sm);
    }
}

void Mesh::destroyVideoBasedData() {
    for (int i=0; i<(int)submeshes.size(); i++) {
        Submesh *sm = submeshes[i];
        assert (sm);
        
        glDeleteBuffers(5, sm->vbos);
    }
}

MeshAnimation* Mesh::GetAnimation(const String &name) {
    HashMap<String, MeshAnimation*>::const_iterator it = animations.find (name);
    if (it != animations.end()) {
        assert (it->second);
        return it->second;
    }
    return nullptr;
}

void Mesh::UpdateAnimations() {

    if (currentAnimation) {
        currentAnimation->Update();
        
        const Vector<Matrix44> &animatedSkeleton = currentAnimation->GetSkeletonMatrixList();
        for (int j=0; j<(int)joints.size(); j++){
            animatedBones[j] = animatedSkeleton[j] * inverseBindPose[j];
        }
    }
    else {
        animatedBones.assign (joints.size(), Matrix44(1.0f));
    }
}

void Mesh::MakePlane(float width, float height) {
    Submesh *sm = new Submesh();
    
    MeshVertex verts[4];
    Triangle tris[2];
    
    verts[0].position = Vector3(-width/2,0,height/2);
    verts[0].textureCoordinate0 = Vector2(0,0);
    verts[1].position = Vector3(width/2,0,height/2);
    verts[1].textureCoordinate0 = Vector2(1,0);
    verts[2].position = Vector3(width/2,0,-height/2);
    verts[2].textureCoordinate0 = Vector2(1,1);
    verts[3].position = Vector3(-width/2,0,-height/2);
    verts[3].textureCoordinate0 = Vector2(0,1);
    
    tris[0].indices[0] = 0;     tris[0].indices[1] = 1;     tris[0].indices[2] = 2;
    tris[1].indices[0] = 2;     tris[1].indices[1] = 3;     tris[1].indices[2] = 0;
    
    for (int i=0; i<4; i++) {
        verts[i].weightCount = 0;
        sm->vertices.push_back(verts[i]);
    }
    
    for (int i=0; i<2; i++) {
        sm->triangles.push_back(tris[i]);
        
        sm->indexBuffer.push_back((GLuint)tris[i].indices[0]);
        sm->indexBuffer.push_back((GLuint)tris[i].indices[1]);
        sm->indexBuffer.push_back((GLuint)tris[i].indices[2]);
    }
    
    AddSubmesh(sm);
    Build();
}

void Mesh::MakeSkyBox() {
    Submesh *sm = new Submesh();
    
    MeshVertex verts[8];
    Triangle tris[12];
    
    float dim = 1.0f;
	verts[0].position.x = -dim;   verts[0].position.y = -dim;  verts[0].position.z = -dim;
	verts[1].position.x = -dim;   verts[1].position.y =  dim;  verts[1].position.z = -dim;
	verts[2].position.x =  dim;   verts[2].position.y =  dim;  verts[2].position.z = -dim;
	verts[3].position.x =  dim;   verts[3].position.y = -dim;  verts[3].position.z = -dim;
    
	verts[4].position.x = -dim;   verts[4].position.y = -dim;  verts[4].position.z = dim;
	verts[5].position.x = -dim;   verts[5].position.y =  dim;  verts[5].position.z = dim;
	verts[6].position.x =  dim;   verts[6].position.y =  dim;  verts[6].position.z = dim;
	verts[7].position.x =  dim;   verts[7].position.y = -dim;  verts[7].position.z = dim;
    
    tris[0].indices[0]=0; tris[0].indices[1]=1; tris[0].indices[2]=2;
	tris[1].indices[0]=0; tris[1].indices[1]=2; tris[1].indices[2]=3;
	tris[2].indices[0]=4; tris[2].indices[1]=6; tris[2].indices[2]=5;
	tris[3].indices[0]=4; tris[3].indices[1]=7; tris[3].indices[2]=6;
	tris[4].indices[0]=1; tris[4].indices[1]=5; tris[4].indices[2]=6;
	tris[5].indices[0]=1; tris[5].indices[1]=6; tris[5].indices[2]=2;
	tris[6].indices[0]=0; tris[6].indices[1]=7; tris[6].indices[2]=4;
	tris[7].indices[0]=0; tris[7].indices[1]=3; tris[7].indices[2]=7;
	tris[8].indices[0]=0; tris[8].indices[1]=5; tris[8].indices[2]=1;
	tris[9].indices[0]=0; tris[9].indices[1]=4; tris[9].indices[2]=5;
	tris[10].indices[0]=3; tris[10].indices[1]=2; tris[10].indices[2]=7;
	tris[11].indices[0]=2; tris[11].indices[1]=6; tris[11].indices[2]=7;
    
    for (int i=0; i<8; i++) {
        verts[i].weightCount = 0;
        sm->vertices.push_back(verts[i]);
    }
    
    for (int i=0; i<12; i++) {
        sm->triangles.push_back(tris[i]);
        
        sm->indexBuffer.push_back((GLuint)tris[i].indices[0]);
        sm->indexBuffer.push_back((GLuint)tris[i].indices[1]);
        sm->indexBuffer.push_back((GLuint)tris[i].indices[2]);
    }
    
    AddSubmesh(sm);
    Build();
}

void Mesh::Build() {
    for (int i=0; i<(int)submeshes.size(); i++) {
        Submesh *sm = submeshes[i];
        assert (sm);
        
        prepareSubmesh(sm);
        prepareNormals(sm);
        
        prepareSubmeshVBO(sm);
    }
    meshCreated = true;
}

void Mesh::buildBindPose(Matrix44 axisChange) {
    bindPose.clear();
    inverseBindPose.clear();
    
    JointList::iterator it;
    for (it=joints.begin(); it!=joints.end(); it++) {
        Joint &joint = *it;
        
        // convert the joint axis
        joint.position = Vector3(axisChange * Vector4(joint.position, 1.0f));
        joint.orientation = glm::toQuat(axisChange) * joint.orientation;
        
        Matrix44 boneTranslation = glm::translate (joint.position);
        Matrix44 boneRotation = glm::toMat4 (joint.orientation);
        Matrix44 boneMatrix = boneTranslation * boneRotation;        
        Matrix44 inverseBoneMatrix = glm::inverse (boneMatrix);
        
        bindPose.push_back (boneMatrix);
        inverseBindPose.push_back (inverseBoneMatrix);
    }
    
}

void Mesh::prepareNormals(Submesh *sm) {
    sm->normalBuffer.clear();
    
    for (int i=0; i<(int)sm->triangles.size(); i++) {
        Vector3 v0 = sm->vertices[sm->triangles[i].indices[0]].position;
        Vector3 v1 = sm->vertices[sm->triangles[i].indices[1]].position;
        Vector3 v2 = sm->vertices[sm->triangles[i].indices[2]].position;
        
        Vector3 normal = glm::cross(v2 - v0, v1 - v0);
        
        sm->vertices[sm->triangles[i].indices[0]].normal += normal;
        sm->vertices[sm->triangles[i].indices[1]].normal += normal;
        sm->vertices[sm->triangles[i].indices[2]].normal += normal;
    }
    
    // normalize the normals
    for (int i=0; i<(int)sm->vertices.size(); i++) {
        MeshVertex &vertex = sm->vertices[i];
        
        vertex.normal = glm::normalize (vertex.normal);
        sm->normalBuffer.push_back(vertex.normal);
    }
}

void Mesh::prepareSubmesh(Submesh *sm) {
    
    sm->positionBuffer.clear();
    sm->texCoordBuffer.clear();
    sm->boneIndexBuffer0.clear();
    sm->boneWeightsBuffer0.clear();
    
    for (int i=0; i<(int)sm->vertices.size(); i++) {
        Vector3 finalPosition;
        MeshVertex &vertex = sm->vertices[i];
        
        if (vertex.weightCount > 0) {
            vertex.position = Vector3(0,0,0);
            vertex.normal = Vector3(0,0,0);
        
            // reset the bone weights and indices
            for (int b=0; b<4; b++) {
                
                vertex.boneIndices0[b] = 0.0f;
                vertex.boneWeights0[b] = 0.0f;
            }

			for (int j=0; j<(int)vertex.weightCount; j++) {

				assert(j <= 4);

                Weight &weight = sm->weights[vertex.startWeight + j];
                Joint &joint = joints[weight.jointID];
            
                Vector3 rotationPosition = Math::TransformVectorByQuaternion(joint.orientation, weight.position);
                vertex.position += (joint.position + rotationPosition) * weight.bias;
  
                vertex.boneIndices0[j] = (float)weight.jointID;
                vertex.boneWeights0[j] = weight.bias;

            }
        
        }
        sm->positionBuffer.push_back(vertex.position);
        sm->texCoordBuffer.push_back(vertex.textureCoordinate0);
        sm->boneIndexBuffer0.push_back(vertex.boneIndices0);
        sm->boneWeightsBuffer0.push_back(vertex.boneWeights0);
    }    
}

bool Mesh::isAnimationCompatible(const MeshAnimation &anim) {
    if (joints.size() != anim.GetJointCount())
        return false;
    
    for (int i=0; i<(int)joints.size(); i++) {
        const Joint &meshJoint = joints[i];
        const MeshAnimation::JointInfo &animJoint = anim.GetJointInfo(i);
        
        if (meshJoint.name != animJoint.name || meshJoint.parentID != animJoint.parentID)
            return false;
    }
    return true;
}

void Mesh::prepareSubmeshVBO(Submesh *sm) {
    glGenBuffers(5, sm->vbos);
    
    // position buffer
    glBindBuffer(GL_ARRAY_BUFFER, sm->vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, sm->positionBuffer.size()*sizeof(Vector3), &sm->positionBuffer[0], GL_STATIC_DRAW);
    
    // normal buffer
    glBindBuffer(GL_ARRAY_BUFFER, sm->vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, sm->normalBuffer.size()*sizeof(Vector3), &sm->normalBuffer[0], GL_STATIC_DRAW);
    
    // texture coordinate buffer
    glBindBuffer(GL_ARRAY_BUFFER, sm->vbos[2]);
    glBufferData(GL_ARRAY_BUFFER, sm->texCoordBuffer.size()*sizeof(Vector2), &sm->texCoordBuffer[0], GL_STATIC_DRAW);
    
    // bone weights buffer 0
    glBindBuffer(GL_ARRAY_BUFFER, sm->vbos[3]);
    glBufferData(GL_ARRAY_BUFFER, sm->boneWeightsBuffer0.size()*sizeof(Vector4), &sm->boneWeightsBuffer0[0], GL_STATIC_DRAW);
    
    // bone index buffer 0
    glBindBuffer(GL_ARRAY_BUFFER, sm->vbos[4]);
    glBufferData(GL_ARRAY_BUFFER, sm->boneIndexBuffer0.size()*sizeof(Vector4), &sm->boneIndexBuffer0[0], GL_STATIC_DRAW);
}