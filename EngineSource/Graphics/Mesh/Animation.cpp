#include "Animation.h"
#include <Mathematics/Math.h>
#include <Application/Timing.h>

MeshAnimation::MeshAnimation() {
    md5Version = 0;
    frameRate = 0;
    animatedComponentCount = 0;
    animationDuration = 0;
    frameDuration = 0;
    animationTime = 0;
    Speed = 1.0f;
    isPaused = false;
    canPlay = false;
}

MeshAnimation::~MeshAnimation() {
    jointInfos.clear();
    bounds.clear();
    baseFrames.clear();
    frames.clear();
    animatedSkeleton.joints.clear();
}

void MeshAnimation::buildFrameSkeleton( Matrix44 axisChange, FrameSkeletonList& skeletons, const JointInfoList& jointInfos, const BaseFrameList& baseFrames, const FrameData& frameData ) {
    FrameSkeleton skeleton;

    for (int i = 0; i < (int)jointInfos.size(); i++)
    {
        int j = 0;

        const JointInfo& jointInfo = jointInfos[i];
        // Start with the base frame position and orientation.
        SkeletonJoint animatedJoint = baseFrames[i];

        animatedJoint.parent = jointInfo.parentID;

        if ( jointInfo.flags & 1 ) // Pos.x
        {
            animatedJoint.position.x = frameData.frameData[ jointInfo.startIndex + j++ ];
        }
        if ( jointInfo.flags & 2 ) // Pos.y
        {
            animatedJoint.position.y = frameData.frameData[ jointInfo.startIndex + j++ ];
        }
        if ( jointInfo.flags & 4 ) // Pos.x
        {
            animatedJoint.position.z  = frameData.frameData[ jointInfo.startIndex + j++ ];
        }
        if ( jointInfo.flags & 8 ) // Orient.x
        {
            animatedJoint.orientation.x = frameData.frameData[ jointInfo.startIndex + j++ ];
        }
        if ( jointInfo.flags & 16 ) // Orient.y
        {
            animatedJoint.orientation.y = frameData.frameData[ jointInfo.startIndex + j++ ];
        }
        if ( jointInfo.flags & 32 ) // Orient.z
        {
            animatedJoint.orientation.z = frameData.frameData[ jointInfo.startIndex + j++ ];
        }

        animatedJoint.orientation = Math::ComputeQuaternionW( animatedJoint.orientation );
        
        if ( animatedJoint.parent >= 0 ) // Has a parent joint
        {
            SkeletonJoint& parentJoint = skeleton.joints[animatedJoint.parent];
            Vector3 rotPos = parentJoint.orientation * animatedJoint.position;

            animatedJoint.position = parentJoint.position + rotPos;
            animatedJoint.orientation = parentJoint.orientation * animatedJoint.orientation;
            
            
            animatedJoint.orientation = glm::normalize( animatedJoint.orientation );
        }
        else {
            animatedJoint.position = Vector3(axisChange * Vector4(animatedJoint.position, 1.0f));
            animatedJoint.orientation = glm::toQuat(axisChange) * animatedJoint.orientation;
        }
        
        skeleton.joints.push_back(animatedJoint);

        // Build the bone matrix for GPU skinning.
        Matrix44 boneTranslate = glm::translate( animatedJoint.position );
        Matrix44 boneRotate = glm::toMat4( animatedJoint.orientation );
        Matrix44 boneMatrix = boneTranslate * boneRotate;
        
        skeleton.boneMatrices.push_back( boneMatrix );
    }

    skeletons.push_back(skeleton);

}

void MeshAnimation::Play(AnimationLoopType loopType) {
    this->loopType = loopType;
    animationTime = 0.0f;
    canPlay = true;
    isPaused = false;
    cycleFinished = false;
}

void MeshAnimation::Stop() {
    animationTime = 0.0f;
    cycleFinished = false;
    canPlay = false;
    isPaused = false;
}

void MeshAnimation::Pause() {
    isPaused = true;
}

void MeshAnimation::Resume() {
    isPaused = false;
}

void MeshAnimation::TogglePause() {
    isPaused = !isPaused;
}

bool MeshAnimation::IsPaused() const {
    return isPaused;
}

void MeshAnimation::Update() {
    
    
    float deltaTime = Timing::LastFrameTime;
    
    deltaTime *= Speed;
    
    if (IsFinished()) {
        switch (loopType) {
            case ANIM_ONCE:
                canPlay = false;
                break;
                
            case ANIM_LOOP:
                canPlay = true;
                cycleFinished = false;
                break;
        }
    }
    
    if ( frames.size() < 1  || !canPlay || isPaused) return;
    
    animationTime += deltaTime;

    
    while ( animationTime > animationDuration ) {
            animationTime -= animationDuration;
    }
    
    while ( animationTime < 0.0f ) {
        animationTime += animationDuration;
    }

    // Figure out which frame we're on
    float frameNum = animationTime * (float)frameRate;
    
    int frame0 = (int)floorf( frameNum );
    int frame1 = (int)ceilf( frameNum );
    frame0 = frame0 % (int)frames.size();
    frame1 = frame1 % (int)frames.size();
    
    float interpolate = fmodf( animationTime, frameDuration ) / frameDuration;

    interpolateSkeletons( animatedSkeleton, skeletons[frame0], skeletons[frame1], interpolate );
    
    // is our cycle finished?
    int roundedFrame = (int)roundf(frameNum);
    cycleFinished = (roundedFrame == frames.size());
    
    if (cycleFinished && loopType == ANIM_ONCE) {
        frame0 = (int)frames.size()-1;
        frame1 = (int)frames.size()-1;
        interpolate = 1.0f;
        interpolateSkeletons( animatedSkeleton, skeletons[frame0], skeletons[frame1], interpolate );
    }
}

void MeshAnimation::interpolateSkeletons( FrameSkeleton& finalSkeleton, const FrameSkeleton& skeleton0, const FrameSkeleton& skeleton1, float interpolate ) {

    
    for ( int i = 0; i < (int)jointInfos.size(); i++ )
    {
        SkeletonJoint& finalJoint = finalSkeleton.joints[i];
        Matrix44& finalMatrix = finalSkeleton.boneMatrices[i];

        const SkeletonJoint& joint0 = skeleton0.joints[i];
        const SkeletonJoint& joint1 = skeleton1.joints[i];

        finalJoint.parent = joint0.parent;

        finalJoint.position = glm::lerp( joint0.position, joint1.position, interpolate );
        finalJoint.orientation = glm::slerp( joint0.orientation, joint1.orientation, interpolate );
        
        // Build the bone matrix for GPU skinning.
        finalMatrix = glm::translate( finalJoint.position ) * glm::toMat4(finalJoint.orientation);
    }
}