/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Material class is an encapsulation for all the effects that the engine's sader system supports.
 
 */
#ifndef MATERIAL_H_DEF
#define MATERIAL_H_DEF

#include <Platform.h>

#include <Graphics/Texture/Texture.h>

class Shader;

typedef Pair<String, Texture*> TexturePair;

class Material {
    friend class MaterialResource;
    
    Material();
    
public:
    ~Material();
    Material (Material *other);
    
    Shader* GetShader() const { return shader; }
    void SetShader(Shader *shader) { this->shader = shader; }
    
    bool HasDiffuseMap() const {return diffuseMap.second != nullptr; }
    TexturePair* GetDiffuseMap() { return &diffuseMap; }
    void SetDiffuseMap(Texture *tex);
    
    void SetDiffuseMapUniform (const String &u) { diffuseMap.first = u; }
    
    bool HasNormalMap() const { return normalMap.second != nullptr; }
    TexturePair* GetNormalMap()  { return &normalMap; }
    void SetNormalMap(Texture *tex);
    
    void SetNormalMapUniform (const String &u) { normalMap.first = u; }
    
    bool HasSpecularMap() const { return specularMap.second != nullptr; }
    TexturePair* GetSpecularMap() { return &specularMap; }
    void SetSpecularMap(Texture *tex);
    
    void SetSpecularMapUniform (const String &u) { specularMap.first = u; }
    
    bool CanDepthWrite() const { return depthWrite; }
    void SetDepthWrite (bool enable);
    
    bool IsCubemap() const { return cubemap; }
    void SetCubemap (bool enable);
    
    bool HasBlending() const { return blending; }
    void SetBlending (bool enabled);
    
    bool HasLighting() const { return lighting; }
    void SetLighting(bool enable);
    
    unsigned int GetSrcBlend() const { return blendSrc; }
    unsigned int GetDestBlend() const { return blendDest; }
    void SetBlendingFunction (unsigned int src, unsigned int dest);
    
    Vector4 GetDiffuseColor() const { return diffuseColor; }
    void SetDiffuseColor(const Vector4 &c);
    
    Vector4 GetAmbientColor() const { return ambientColor; }
    void SetAmbientColor(const Vector4 &c);
    
    Vector4 GetSpecularColor() const { return specularColor; }
    void SetSpecularColor(const Vector4 &c);
    
    float GetShininess() const { return shininess; }
    void SetShininess(float s);
    
    void SetDiffuseMapRepeat(const Vector2 &repeat);
    Vector2 GetDiffuseMapRepeat() const { return diffuseMapRepeat; }
    
    void SetNormalMapRepeat(const Vector2 &repeat);
    Vector2 GetNormalMapRepeat() const { return normalMapRepeat; }
    
    void SetSpecularMapRepeat(const Vector2 &repeat);
    Vector2 GetSpecularMapRepeat() const { return specularMapRepeat; }

    void SetDiffuseMapScroll(const Vector2 &scroll);
    Vector2 GetDiffuseMapScroll() const { return diffuseMapScroll; }
    
    void SetNormalMapScroll(const Vector2 &scroll);
    Vector2 GetNormalMapScroll() const { return normalMapScroll; }
    
    void SetSpecularMapScroll(const Vector2 &scroll);
    Vector2 GetSpecularMapScroll() const { return specularMapScroll; }
    
	void SetAddressMode(TextureAddressMode addressMode) { this->addressMode = addressMode; }
	TextureAddressMode GetAddressMode() const { return addressMode; }

private:
    Shader *shader;
    
	TextureAddressMode addressMode;

    TexturePair diffuseMap;
    TexturePair normalMap;
    TexturePair specularMap;
    
    Vector2 diffuseMapRepeat;
    Vector2 normalMapRepeat;
    Vector2 specularMapRepeat;

    Vector2 diffuseMapScroll;
    Vector2 normalMapScroll;
    Vector2 specularMapScroll;
    
    bool lighting;
    bool depthWrite;
    bool cubemap;
    unsigned int blendSrc, blendDest;
    bool blending;
    
    float shininess;
    
    Vector4 ambientColor;
    Vector4 diffuseColor;
    Vector4 specularColor;
};



#endif
