#include "Material.h"

#include <Graphics/Texture/Texture.h>

Material::Material() {
    shader = nullptr;
    
    depthWrite = true;
	cubemap = false;
    shininess = 1.0f;
    
    lighting = true;
    
    blending = false;
    blendSrc = GL_ONE;
    blendDest = GL_ZERO;
    
    diffuseColor = Vector4(1,1,1,1);
    ambientColor = Vector4(0.35f, 0.35f, 0.35f, 1.0f);
    specularColor = Vector4(1,1,1,1);
    
    diffuseMapRepeat = Vector2(1,1);
    normalMapRepeat = Vector2(1,1);
    specularMapRepeat = Vector2(1,1);
    
    diffuseMapScroll = Vector2(0,0);
    normalMapScroll = Vector2(0,0);
    specularMapScroll = Vector2(0,0);

	addressMode = TAM_REPEAT;
}

Material::~Material() {

}

Material::Material(Material* other) {
    shader = other->shader;
    
    depthWrite = other->depthWrite;
    cubemap = other->cubemap;
    shininess = other->shininess;
    
    blending = other->blending;
    blendSrc = other->blendSrc;
    blendDest = other->blendDest;
    
    diffuseColor = other->diffuseColor;
    ambientColor = other->ambientColor;
    specularColor = other->specularColor;
    
    diffuseMapRepeat = other->diffuseMapRepeat;
    diffuseMapScroll = other->diffuseMapScroll;
    
    normalMapRepeat = other->normalMapRepeat;
    normalMapScroll = other->normalMapScroll;
    
    specularMapRepeat = other->specularMapRepeat;
    specularMapScroll = other->specularMapScroll;
    
    diffuseMap = other->diffuseMap;
    normalMap = other->normalMap;
    specularMap = other->specularMap;
}

void Material::SetDiffuseMap (Texture *tex) {
    assert (tex);
    
    diffuseMap.second = tex;
}

void Material::SetNormalMap (Texture *tex) {
    assert (tex);
    
    normalMap.second = tex;
}

void Material::SetSpecularMap (Texture *tex) {
    assert (tex);
    
    specularMap.second = tex;
}

void Material::SetBlending (bool enabled) {
    blending = enabled;
}

void Material::SetLighting (bool enabled) {
    lighting = enabled;
}

void Material::SetBlendingFunction (unsigned int src, unsigned int dest) {
    blendSrc = src;
    blendDest = dest;
}

void Material::SetCubemap(bool enable) {
    cubemap = enable;
}

void Material::SetDepthWrite(bool enable) {
    depthWrite = enable;
}

void Material::SetShininess (float s) {
    if (s > 1.0f)
        s = 1.0f;
    if (s < 0.0f)
        s = 0.0f;
    
    shininess = s;
}

void Material::SetDiffuseColor(const Vector4 &c) {
    diffuseColor = c;
}

void Material::SetAmbientColor(const Vector4 &c) {
    ambientColor = c;
}

void Material::SetSpecularColor(const Vector4 &c) {
    specularColor = c;
}

void Material::SetDiffuseMapRepeat(const Vector2 &repeat) {
    diffuseMapRepeat = repeat;
}

void Material::SetNormalMapRepeat(const Vector2 &repeat) {
    normalMapRepeat = repeat;
}

void Material::SetSpecularMapRepeat(const Vector2 &repeat) {
    specularMapRepeat = repeat;
}

void Material::SetDiffuseMapScroll(const Vector2 &scroll) {
    diffuseMapScroll = scroll;
}

void Material::SetNormalMapScroll(const Vector2 &scroll) {
    normalMapScroll = scroll;
}

void Material::SetSpecularMapScroll(const Vector2 &scroll) {
    specularMapScroll = scroll;
}