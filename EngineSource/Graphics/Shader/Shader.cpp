#include "Shader.h"
#include <Utilities/Exception.h>
#include <Utilities/StringOperations.h>

Shader::Shader() {
    
}

Shader::~Shader() {
    destroyVideoBasedData();
}

void Shader::restoreVideoBasedData() {
    
    bool vsSuccess = createVertexShader(vpSource);
    bool fsSuccess = createFragmentShader(fpSource);
    bool programSuccess = link();
    if (!vsSuccess || !fsSuccess || !programSuccess)
        throw new Exception ("Failed to restore the shader!");
}

void Shader::destroyVideoBasedData() {
	if (!shaderCreated)
		return;
    
    glUseProgram(0);
    
    HashMap<String, Uniform*>::iterator it;
    for (it=uniforms.begin(); it!=uniforms.end(); it++) {
        Uniform *data = it->second;
        if (data)
            delete data;
    }
    uniforms.clear();

    glDetachShader(shaderId, shaderFP);
    glDetachShader(shaderId, shaderVP);
    
    glDeleteShader(shaderFP);
    glDeleteShader(shaderVP);
    glDeleteProgram(shaderId);
    
    shaderCreated = false;
}

bool Shader::createVertexShader(const String &programSource) {
    // remember our source
    vpSource = programSource;
    
    shaderVP = glCreateShader(GL_VERTEX_SHADER);
    
    const char* source = programSource.c_str();
    
    glShaderSource(shaderVP, 1, &source, 0);
    glCompileShader(shaderVP);
    
    printInfoLog(shaderVP);
    
    validateShader(shaderVP);
        
    return true;
}

bool Shader::createFragmentShader(const String &programSource) {
    // remember our source
    fpSource = programSource;
    
    shaderFP = glCreateShader(GL_FRAGMENT_SHADER);
    
    const char* source = programSource.c_str();
    
    glShaderSource(shaderFP, 1, &source, 0);
    glCompileShader(shaderFP);
    
    printInfoLog(shaderFP);
    
    validateShader(shaderFP);
    
    return true;
}

bool Shader::link() {
    shaderId = glCreateProgram();
    glAttachShader(shaderId, shaderVP);
    glAttachShader(shaderId, shaderFP);
    glLinkProgram(shaderId);
    
    printInfoLog(shaderId);
    
    validateProgram(shaderId);
    
    int totalUniforms = -1;
    glGetProgramiv(shaderId, GL_ACTIVE_UNIFORMS, &totalUniforms);
        
    for (int i=0; i<totalUniforms; i++) {
        int nameLen = -1;
        int num = -1;
        GLenum type = GL_ZERO;
        char name[256];
            
        glGetActiveUniform(shaderId, GLuint(i), sizeof(name)-1, &nameLen, &num, &type, name);
        name[nameLen] = 0;
        GLuint location = glGetUniformLocation(shaderId, name);
        
        // add these to our structure
        Uniform *uniform = new Uniform();
        
        uniform->Location = location;
        uniform->Name = name;
        uniform->Type = type;
        
        
        // if name includes the [0] prefix (some drivers expose array uniforms like that),
        // let's remove this prefix.
        if (StringOperations::EndsWith(uniform->Name, "[0]")) {
            uniform->Name = uniform->Name.substr(0, uniform->Name.find_first_of("["));
        }
        uniforms[uniform->Name] = uniform;
    }
    
    shaderCreated = true;
    return true;
}

bool Shader::validateShader(GLuint shader) {
    
    const unsigned int bufferSize = 512;
    char buffer[bufferSize];
    memset(buffer, 0, bufferSize);
    GLsizei length = 0;
    
    glGetShaderInfoLog(shader, bufferSize, &length, buffer);
    if (length > 0)
        Log (buffer);
    return true;
}

bool Shader::validateProgram(GLuint program) {
    
    const unsigned int bufferSize = 512;
    char buffer[bufferSize];
    memset(buffer, 0, bufferSize);
    GLsizei length = 0;
    
    memset(buffer, 0, bufferSize);
    glGetProgramInfoLog(program, bufferSize, &length, buffer);
    if (length > 0)
        Log (buffer);
    
    glValidateProgram(program);
    GLint status;
    glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
    if (status == GL_FALSE)
        return false;
    
    return true;
}

void Shader::printInfoLog(GLuint obj) {
    int infologLength = 0;
    int maxLength;
    if(glIsShader(obj))    
        glGetShaderiv(obj,GL_INFO_LOG_LENGTH,&maxLength);
    else
        glGetProgramiv(obj,GL_INFO_LOG_LENGTH,&maxLength);
    
	std::string infoLogStr;
	infoLogStr.resize(maxLength);
	char *infoLog = &infoLogStr[0];

    if (glIsShader(obj))
        glGetShaderInfoLog(obj, maxLength, &infologLength, infoLog);
    else
        glGetProgramInfoLog(obj, maxLength, &infologLength, infoLog);
    
    if (infologLength > 0)
        Log (infoLog);
}

void Shader::Use() {
    glUseProgram(shaderId);
}

void Shader::SetUniformMatrix4(const String &uniformVariable, Matrix44 matrix) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        glUniformMatrix4fv (uniform->Location, 1, GL_FALSE, glm::value_ptr(matrix));
    }
}

void Shader::SetUniform1i(const String &uniformVariable, int i) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        glUniform1i(uniform->Location, i);
    }
}

void Shader::SetUniform1f(const String &uniformVariable, float f) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        glUniform1f(uniform->Location, f);
    }
}

void Shader::SetUniformMatrix4v(const String &uniformVariable, Vector<Matrix44> matrixArray) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        
        glUniformMatrix4fv(uniform->Location, matrixArray.size(), GL_FALSE, glm::value_ptr(matrixArray[0]));
    }
}

void Shader::SetUniformMatrix4v(const String &uniformVariable, Matrix44 *matrixArray, unsigned int arrayCount) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        
        glUniformMatrix4fv(uniform->Location, arrayCount, GL_FALSE, glm::value_ptr(matrixArray[0]));
    }
}

void Shader::SetUniform4f (const String &uniformVariable, const Vector4 &v) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        glUniform4f(uniform->Location, v[0], v[1], v[2], v[3]);
    }
}

void Shader::SetUniform3f (const String &uniformVariable, const Vector3 &v) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        glUniform3f(uniform->Location, v[0], v[1], v[2]);
    }
}

void Shader::SetUniform2f (const String &uniformVariable, const Vector2 &v) {
    Uniform *uniform = uniforms[uniformVariable];
    if (uniform) {
        glUniform2f(uniform->Location, v[0], v[1]);
    }
}