/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Shader class contains a vertex and fragment GLSL shader combined in one, enumerating exposed uniforms.
 SetUniform* methods check if the uniform is available, otherwise the function returns.
 */
#ifndef SHADER_H_DEF
#define SHADER_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

struct Uniform {
   
    GLuint Location;
    unsigned int Type;
    String Name;
    
};

class Shader {
    friend class ShaderResource;
    
    Shader();
public:
    ~Shader();
    
    void Use();
    unsigned int GetShaderID() const { return shaderId; }
    void SetUniformMatrix4 (const String &uniformVariable, Matrix44 matrix);
    void SetUniformMatrix4v (const String &uniformVariable, Vector<Matrix44> matrixArray);
    void SetUniformMatrix4v (const String &uniformVariable, Matrix44 *matrixArray, unsigned int arrayCount);
    void SetUniform1i (const String &uniformVariable, int i);
    void SetUniform1f (const String &uniformVariable, float f);
    void SetUniform4f (const String &uniformVariable, const Vector4 &v);
    void SetUniform3f (const String &uniformVariable, const Vector3 &v);
    void SetUniform2f (const String &uniformVariable, const Vector2 &v);
    
private:
    bool createVertexShader (const String &programSource);
    bool createFragmentShader (const String &programSource);
    bool link();
    
    void printInfoLog (GLuint obj);
    
    void destroyVideoBasedData();
    void restoreVideoBasedData();
    
    bool validateShader (GLuint shader);
    bool validateProgram (GLuint program);
    
    bool shaderCreated;
    unsigned int shaderId, shaderVP, shaderFP;
    HashMap<String, Uniform*> uniforms;
    String vpSource, fpSource;
};

#endif
