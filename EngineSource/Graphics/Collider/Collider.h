/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Collider base class.
 
 Contains 2 sets of collindedNodes and previouslyCollidedNodes.  For information refer to the Scene header description.
 */
#ifndef COLLIDER_H_DEF
#define COLLIDER_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

enum ColliderType {
    CT_SPHERE = 0,
    CT_BOX = 1
};


class Collider {
    friend class Scene;
public:
    Collider();
    virtual ~Collider();
    
    virtual ColliderType GetColliderType() const =0;
    bool IsTrigger;
    bool Enabled;
    
    virtual void Transform(Matrix44 &matrix)=0;
    virtual bool Collide(Collider *other)=0;
    
private:
    Set<int> *otherCollidedNodes, *prevOtherCollidedNodes;
};


#endif
