#include "SphereCollider.h"
#include "BoxCollider.h"
#include <Graphics/Mesh/Mesh.h>

SphereCollider::SphereCollider() {

}

SphereCollider::~SphereCollider() {
    
}

SphereCollider::SphereCollider(const Sphere &sphere) {
    SetRadius( sphere.GetRadius() );
    SetCenter( sphere.GetCenter() );
    
    worldBoundingSphere = Sphere (sphere);
}

SphereCollider::SphereCollider (Mesh *mesh) {
    Vector<Vector3> vertices;
    
    for (unsigned int i=0; i<mesh->GetSubmeshCount(); i++) {
        Mesh::Submesh *sm = mesh->GetSubmesh(i);
        assert (sm);
        
        for (int j=0; j<(int)sm->positionBuffer.size(); j++) {
            vertices.push_back(sm->positionBuffer[j]);
        }
    }
    
    boundingSphere = Sphere(vertices);
    worldBoundingSphere = Sphere(boundingSphere);
    
    vertices.clear();
}

void SphereCollider::SetRadius(float radius) {
    boundingSphere.SetRadius(radius);
}

void SphereCollider::SetCenter(const Vector3 &v) {
    boundingSphere.SetCenter(v);
}

Vector3 SphereCollider::GetCenter() const {
    return boundingSphere.GetCenter();
}

float SphereCollider::GetRadius() const {
    return boundingSphere.GetRadius();
}

ColliderType SphereCollider::GetColliderType() const {
    return CT_SPHERE;
}

void SphereCollider::Transform (Matrix44 &matrix) {
    worldBoundingSphere = Sphere(boundingSphere);
    
    worldBoundingSphere.Transform(matrix);
}

/*
 Check against all possible collider types.
*/
bool SphereCollider::Collide(Collider *other) {
    assert (other);
    
    ColliderType type = other->GetColliderType();
    bool collisionStatus = false;
    
    if (type == CT_SPHERE) {
        SphereCollider *otherSphere = (SphereCollider*)other;
        
        collisionStatus = Math::Intersect(GetGlobalBoundingSphere(),
                                otherSphere->GetGlobalBoundingSphere());
    }
    else if (type == CT_BOX) {
        BoxCollider *otherBox = (BoxCollider*)other;
        
        collisionStatus = Math::Intersect(GetGlobalBoundingSphere(),
                                otherBox->GetGlobalBoundingBox());
    }
    
    return collisionStatus;
}