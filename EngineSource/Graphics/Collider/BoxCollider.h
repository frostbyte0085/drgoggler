/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The BoxCollider implements a Box bounding volume, keeping local and global copies of it.
 The global bounding box is transformed so that it can be checked against other colliders' bounding volumes
 for intersections.
 */
#ifndef BOX_COLLIDER_H_DEF
#define BOX_COLLIDER_H_DEF

#include "Collider.h"

#include <Mathematics/Box.h>

class Mesh;

class BoxCollider : public Collider {
public:
    BoxCollider();
    BoxCollider(const Box &box);
    BoxCollider(Mesh *mesh);
    ~BoxCollider();
     
    Box& GetGlobalBoundingBox() { return worldBoundingBox; }
    Box& GetBoundingBox() { return boundingBox; }
    
    ColliderType GetColliderType() const;
    
    void Transform(Matrix44 &matrix);
    bool Collide(Collider *other);
    
private:
    Box boundingBox;
    Box worldBoundingBox;
};

#endif
