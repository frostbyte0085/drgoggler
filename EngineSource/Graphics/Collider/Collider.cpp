#include "Collider.h"

Collider::Collider() {
    IsTrigger = false;
    Enabled = true;
    
    otherCollidedNodes = nullptr;
    prevOtherCollidedNodes = nullptr;
    
    otherCollidedNodes = new Set<int>();
    prevOtherCollidedNodes = new Set<int>();
}

Collider::~Collider() {
    otherCollidedNodes->clear();
    if (otherCollidedNodes)
        delete otherCollidedNodes;
    otherCollidedNodes = nullptr;
    
    prevOtherCollidedNodes->clear();
    if (prevOtherCollidedNodes)
        delete prevOtherCollidedNodes;
    prevOtherCollidedNodes = nullptr;
}