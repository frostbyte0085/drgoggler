#include "BoxCollider.h"
#include "SphereCollider.h"
#include <Graphics/Mesh/Mesh.h>

BoxCollider::BoxCollider() {
    
}

BoxCollider::~BoxCollider() {
    
}

BoxCollider::BoxCollider(const Box &box) {
    this->boundingBox = Box(box);
    this->worldBoundingBox = Box(box);
}

BoxCollider::BoxCollider (Mesh *mesh) {
    Vector<Vector3> vertices;
    
    for (unsigned int i=0; i<mesh->GetSubmeshCount(); i++) {
        Mesh::Submesh *sm = mesh->GetSubmesh(i);
        assert (sm);
        
        for (int j=0; j<(int)sm->positionBuffer.size(); j++) {
            vertices.push_back(sm->positionBuffer[j]);
        }
    }
    
    boundingBox = Box(vertices);
    worldBoundingBox = Box(boundingBox);
    
    vertices.clear();
}

ColliderType BoxCollider::GetColliderType() const {
    return CT_BOX;
}

void BoxCollider::Transform (Matrix44 &matrix) {
    worldBoundingBox = Box (boundingBox);
    
    worldBoundingBox.Transform (matrix);
}

/*
 Check against all possible collider types.
 */
bool BoxCollider::Collide(Collider *other) {
    assert (other);
    
    ColliderType type = other->GetColliderType();
    bool collisionStatus = false;
    
    if (type == CT_SPHERE) {
        SphereCollider *otherSphere = (SphereCollider*)other;
        
        collisionStatus = Math::Intersect(otherSphere->GetGlobalBoundingSphere(), GetGlobalBoundingBox());
    }
    else if (type == CT_BOX) {
        BoxCollider *otherBox = (BoxCollider*)other;
        
        collisionStatus = Math::Intersect(GetGlobalBoundingBox(), otherBox->GetGlobalBoundingBox());
    }
    
    return collisionStatus;
}