/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The SphereCollider implements a Sphere bounding volume, keeping local and global copies of it.
 The global bounding sphere is transformed so that it can be checked against other colliders' bounding volumes
 for intersections.
 */
#ifndef SPHERE_COLLIDER_H_DEF
#define SPHERE_COLLIDER_H_DEF

#include "Collider.h"
#include <Mathematics/Sphere.h>

class Mesh;

class SphereCollider : public Collider {
public:
    SphereCollider();
    SphereCollider(const Sphere &sphere);
    SphereCollider(Mesh *mesh);
    ~SphereCollider();
    
    void SetRadius(float radius);
    float GetRadius() const;
    
    void SetCenter(const Vector3 &v);
    Vector3 GetCenter() const;
    
    Sphere& GetGlobalBoundingSphere() { return worldBoundingSphere; }
    Sphere& GetBoundingSphere() { return boundingSphere; }
    
    ColliderType GetColliderType() const;
    
    void Transform(Matrix44 &matrix);
    bool Collide(Collider *other);
    
private:
    Sphere boundingSphere;
    Sphere worldBoundingSphere;
};

#endif
