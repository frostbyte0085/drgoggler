#include "Font.h"

#include <Utilities/KeyValueList.h>

Font::Font() {
    fontCreated = false;
}

Font::~Font() {
    pages.clear();
}

void Font::destroyVideoBasedData() {
}

void Font::restoreVideoBasedData() {
}

bool Font::createFont() {
    fontCreated = true;
    return true;
}

MathRect Font::GetTextRect(const String &text, const Vector2 &pos) {
    float w = GetTextWidth(text);
    float h = (float)GetLineHeight();
    
    return MathRect (pos.x, pos.y, w, h);
}

Font::CharData Font::charLineToCharData(const String &line) {
    KeyValueList values(line);
    Font::CharData ret;
    
    ret.index = values.GetIntValue("id");
    ret.x = values.GetIntValue("x");
    ret.y = values.GetIntValue("y");
    ret.width = values.GetIntValue("width");
    ret.height = values.GetIntValue("height");
    ret.xOffset = values.GetIntValue("xoffset");
    ret.yOffset = values.GetIntValue("yoffset");
    ret.xAdvance = values.GetIntValue("xadvance");
    ret.page = values.GetIntValue("page");
    
    ret.valid = true;
    
    return ret;
}

Font::KerningData Font::kerningLineToKerningData(const String &line) {
    KeyValueList values(line);
    Font::KerningData ret;
    
    ret.first = values.GetIntValue("first");
    ret.second = values.GetIntValue("second");
    ret.displace = values.GetIntValue("amount");
    
    ret.valid = true;
    
    return ret;
}

Font::CharData Font::GetChar(int index) {
    Font::CharData ret;
    for (int i=0; i<(int)chars.size(); i++) {
        if (chars[i].index == index) return chars[i];
    }
    return ret;
}

Texture* Font::GetPage(int index) {
    return pages[index];
}

int Font::GetKerning(int first, int second) {
    return kernings[first][second];
}

int Font::GetLineHeight() {
    return lineHeight;
}

void Font::SetLineHeight(int lh) {
    lineHeight = lh;
}

/*
 Get the text width based on the character kerning
*/
float Font::GetTextWidth(const String &text) {
    String msg = text;
    float posx = 0;
    for (int i=0; i<(int)msg.size(); i++) {
        int kern = 0;
        if (i > 0) {
            kern = GetKerning((int)msg[i-1], (int)msg[i]);
        }
        
        posx += kern;
        Font::CharData cd = GetChar((int)msg[i]);
        if (cd.valid) {
            posx += cd.xAdvance;
        }
        else {
            // just move the position a little
            posx += 10;
        }
    }
    
    return posx;
}