/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Font class contains all the data required to render single line text using a bitmapped font.
 
 */
#ifndef FONT_H_DEF
#define FONT_H_DEF

#include <Platform.h>
#include <Mathematics/Rectangle.h>

class Texture;

class Font {
    friend class FontResource;
    
    
public:
    class CharData {
        friend class FontResource;
    public:
        bool valid;
        int index;
        int page;
        int x, y, width, height;
        int xOffset, yOffset, xAdvance;
        
        CharData() {
            valid = false;
            index = -1;
            page = 0;
            x = 0; y = 0; width = 0; height = 0;
            xOffset = 0; yOffset = 0; xAdvance = 0;
        }
    };
    
    class KerningData {
        friend class FontResource;
    public:
        bool valid;
        int first, second, displace;
        
        KerningData() {
            valid = false;
            first = -1; second = -1; displace = 0;
        }
    };
    
    Font();
    
public:
    ~Font();
    
    Texture* GetPage(int index);
    CharData GetChar(int index);
    int GetKerning(int first, int second);
    
    MathRect GetTextRect(const String &text, const Vector2 &pos);
    
    int GetLineHeight();
    void SetLineHeight(int lh);
    float GetTextWidth(const String &text);
    
private:
    
    bool createFont();
    
    void destroyVideoBasedData();
    void restoreVideoBasedData();
    
    bool fontCreated;
    
    CharData charLineToCharData(const String &line);
    KerningData kerningLineToKerningData(const String &line);
    
    int lineHeight;
    
    Vector<Texture*> pages;
    Vector<CharData> chars;
    Map<int, Map<int, int> > kernings;
    
};

#endif
