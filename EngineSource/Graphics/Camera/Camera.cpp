#include "Camera.h"
#include <Graphics/GraphicsContext/GraphicsContext.h>

Camera::Camera() {
    fov = 45.0f;
    aspect = 1.77f;
    nearClipPlane = 0.01f;
    farClipPlane = 10000.0f;
}

Camera::~Camera() {
    
    
}

void Camera::SetupCamera(float fov, float aspect, float nearClipPlane, float farClipPlane) {
    this->fov = fov;
    this->aspect = aspect;
    this->nearClipPlane = nearClipPlane;
    this->farClipPlane = farClipPlane;
}

void Camera::update() {
    projectionMatrix = glm::perspective(fov, aspect, nearClipPlane, farClipPlane);
}