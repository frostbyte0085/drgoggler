/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Camera is a simple encapsulation for a perspective projection matrix.
 
 */
#ifndef CAMERA_H_DEF
#define CAMERA_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class Camera {
    friend class Scene;
public:
    Camera();
    virtual ~Camera();
    
    void SetupCamera (float fov, float aspect, float nearClipPlane, float farClipPlane);
    float GetNearClipPlane() const { return nearClipPlane; }
    float GetFarClipPlane() const { return farClipPlane; }
    
    
    Matrix44& GetProjectionMatrix() { return projectionMatrix; }
    
private:
    void update();
    
    float fov, aspect, nearClipPlane, farClipPlane;
    Matrix44 projectionMatrix;
    
};

#endif
