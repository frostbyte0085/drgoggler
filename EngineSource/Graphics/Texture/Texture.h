/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Texture class creates an opengl texture id from the newly created system copies.
 Supports cubemaps and various texture address modes.
 */
#ifndef TEXTURE_H_DEF
#define TEXTURE_H_DEF

#include <Platform.h>

enum TextureAddressMode {
    TAM_REPEAT,
    TAM_CLAMP
};

class Texture {
    friend class TextureResource;
    
    Texture();
    
public:
    ~Texture();

    GLuint GetTextureID() const {return textureID; }
    int GetWidth() const { return width; }
    int GetHeight() const { return height; }
    bool IsCubemap() const { return cubemap; }
    bool HasMipmaps() const { return mipmaps; }
    
private:
    bool createTexture(bool mipmaps, bool cubemap, TextureAddressMode addressMode);
    bool createEmptyTexture();
    
    void destroyVideoBasedData();
    void restoreVideoBasedData();
    
    bool textureCreated;
    
    bool mipmaps;
    bool cubemap;
    TextureAddressMode addressMode;
    
    int width;
    int height;
    int channels;
    int originalTextureFormat;
    int internalTextureFormat;
    
    Vector< unsigned char* > systemMemoryCopies;
    GLuint textureID;
    bool emptyTexture;
};

#endif
