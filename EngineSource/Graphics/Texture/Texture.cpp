#include "Texture.h"

Texture::Texture() {
    width = 0;
    height = 0;
    textureID = -1;
    textureCreated = false;
    mipmaps = false;
    cubemap = false;
    emptyTexture = false;
    addressMode = TAM_REPEAT;
}

Texture::~Texture() {
    destroyVideoBasedData();
    
    for (int i=0; i<(int)systemMemoryCopies.size(); i++){
        if (systemMemoryCopies[i])
            SOIL_free_image_data(systemMemoryCopies[i]);
    }
    systemMemoryCopies.clear();
}

void Texture::destroyVideoBasedData() {
    glBindTexture (GL_TEXTURE_CUBE_MAP, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glDeleteTextures(1, &textureID);
    
    textureID = -1;
    textureCreated = false;
}

void Texture::restoreVideoBasedData() {
    if (!emptyTexture)
        createTexture(mipmaps, cubemap, addressMode);
    else
        createEmptyTexture();
}

bool Texture::createEmptyTexture () {
    glGenTextures(1, &textureID);
    if (textureID <= 0)
        return false;
    
    textureCreated = true;
    return true;
}

bool Texture::createTexture(bool mipmaps, bool cubemap, TextureAddressMode addressMode) {
    this->mipmaps = mipmaps;
    this->cubemap = cubemap;
    this->addressMode = addressMode;
    
    glGenTextures(1, &textureID);
    
    if (textureID <= 0 )
        return false;
    
    switch( channels )
    {
		case 1:
			originalTextureFormat = GL_LUMINANCE;
			break;
		case 2:
			originalTextureFormat = GL_LUMINANCE_ALPHA;
			break;
		case 3:
			originalTextureFormat = GL_RGB;
			break;
		case 4:
			originalTextureFormat = GL_RGBA;
			break;
    }
    internalTextureFormat = originalTextureFormat;
    
    if (!cubemap) {
        unsigned char* firstSysMemCopy = systemMemoryCopies[0];
        
        glBindTexture(GL_TEXTURE_2D, textureID);
    
        if (addressMode == TAM_REPEAT) {
            glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
            glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        }
        else {
            glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
            glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
        }
                    
        glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, 
              GL_MODULATE );
    
        if (!mipmaps) {
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        
            glTexImage2D(GL_TEXTURE_2D, 0, internalTextureFormat, width, height, 0, originalTextureFormat, GL_UNSIGNED_BYTE, &firstSysMemCopy[0]);
        }
        else {
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        
            gluBuild2DMipmaps( GL_TEXTURE_2D, internalTextureFormat, width, height, 
                          originalTextureFormat, GL_UNSIGNED_BYTE, &firstSysMemCopy[0]);
        }
    }
    else {
        glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
        
        if (!mipmaps) {
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }
        else {
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);           
        }
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  

        // default cubemap address mode is clamp to edge
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        
        for (int i=0; i<6; i++) {
            unsigned char* sysMemCopy = systemMemoryCopies[i];
            assert (sysMemCopy);
            
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP, GL_TRUE);
            
            if (!mipmaps) {
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalTextureFormat, width, height, 0, originalTextureFormat, GL_UNSIGNED_BYTE, &sysMemCopy[0]);
            }
            else {
                gluBuild2DMipmaps( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, internalTextureFormat, width, height,
                                  originalTextureFormat, GL_UNSIGNED_BYTE, &sysMemCopy[0]);
            }
        }
    }
    
    textureCreated = true;
    return true;
}