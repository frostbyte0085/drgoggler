/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Simple singleton class, we do not use this in the engine currently, but leaving it here for reference and future use.
 
 */
#ifndef SINGLETON_H_DEF
#define SINGLETON_H_DEF

#include <Platform.h>

template <class T> class Singleton
{
	static T* instance;
protected:
	Singleton(){};
	Singleton operator= (const Singleton& other) {}
	Singleton(const Singleton& other){}
public:
	static T& GetSingleton()
	{
		if (!instance)
			instance = new T;
		return *instance;
	}
	static T* GetSingletonPtr()
	{
		if (!instance)
			instance = new T;
		return instance;
	}
	static void Release()
	{
		if (instance)
			delete instance;
		instance = nullptr;
	}
};
template <class T> T* Singleton<T>::instance = nullptr;

#endif