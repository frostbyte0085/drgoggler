/*
 Author: 
 Pantelis Lekakis
 
 Description:
 StringOperations is a static class containing useful string manipulation methods
 
 */
#ifndef STRINGOPERATIONS_H_DEF
#define STRINGOPERATIONS_H_DEF

#include <Platform.h>
#include <iomanip>

class StringOperations
{
public:
    // use stringstream to convert value to a string
	template <class T> static String ToString(const T& value, int precision=2)
	{
        std::stringstream out;
		out << std::fixed << std::setprecision(precision) << value;
		return out.str();
	}

    // use istringstream to convert string to value
	template <class T> static T FromString(const String& s)
	{
        std::istringstream is(s);
		T t;
		is >> t;
		return t;
	}

	static String ToLower(const String &s);
	static String ToUpper(const String &s);

	static Vector<String> SplitString(const String &s, char separator);

    static String RemoveQuotes(const String & s);
	static bool StartsWith (const String &check, const String &s);
	static bool StartsWith (const String &check, char c);
	static bool EndsWith (const String &check, const String &s);
	static bool EndsWith (const String &check, char c);
	static String Trim(const String &s);
	static String Trim(const String &s, const String &delims);

	static String ReplaceAll(const String &s, char old_char, char new_char);
};
#endif /* STRINGOPERATIONS_H_ */
