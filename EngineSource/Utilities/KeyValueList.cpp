#include "KeyValueList.h"

// initializes the list by a line of tokens
KeyValueList::KeyValueList(const String &line)
{
    ProcessLine(line);
}


KeyValueList::~KeyValueList() {
    values.clear();
}

String KeyValueList::GetValueNoQuotes(const String &key)
{
    String str = GetValue(key);
    String ret = "";
    for (int i=0; i<(int)str.size(); i++)
    {
        if (str[i] != '\"') ret += str[i];
    }
    return ret;
}

String KeyValueList::GetValue(const String &key)
{
    return values[key];
}

int KeyValueList::GetIntValue(const String &key)
{
    return atoi(GetValue(key).c_str());
}

float KeyValueList::GetFloatValue(String key)
{
    return (float)atof(GetValue(key).c_str());
}

void KeyValueList::ProcessToken(const String &t)
{
    String key = "";
    String value = "";
    bool eq = false;
    for (int i=0; i<(int)t.size(); i++)
    {
        if (t[i] == '=')
        {
            eq = true;
            continue;
        }

        if (!eq) key += t[i];
        else value += t[i];
    }
    values[key] = value;
}

void KeyValueList::ProcessLine(const String &line)
{
    String curr = "";
    // tokenize
    for (int i=0; i<(int)line.size(); i++)
    {
        if (line[i] == ' ')
        {
            if (curr.size())
            {
                ProcessToken(curr);
                curr = "";
            }
        }
        else
        {
            curr += line[i];
        }
    }
    if (curr.size()) ProcessToken(curr);
}
