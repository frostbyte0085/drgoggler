#include "StringOperations.h"


String StringOperations::ToLower(const String &s)
{
	String ret = s;
	for(unsigned int i=0;i<ret.length();++i)
	{
		ret[i] = tolower(ret[i]);
	}
	return ret;
}

String StringOperations::ToUpper(const String &s)
{
	String ret = s;
	for(unsigned int i=0;i<ret.length();++i)
	{
		ret[i] = toupper(ret[i]);
	}
	return ret;
}

String StringOperations::ReplaceAll (const String &s, char old_char, char new_char)
{
	String ret = s;
	for (unsigned int i=0; i<ret.size(); i++)
	{
		if ((char)ret[i] == old_char)
			ret[i] = new_char;
	}
	return ret;
}

String StringOperations::RemoveQuotes(const String &s) {
    String ret = s;
    size_t n;
    while ((n=ret.find('\"')) != String::npos) {
        ret.erase(n,1);
    }
    return ret;
}

/*
 Split a string by a separator character and return a vector of all the elements.
*/
Vector<String> StringOperations::SplitString(const String &s, char separator)
{
	String tmp = s;
	Vector<String> ret;

	while (tmp.size())
	{
		size_t f = tmp.find_first_of(separator);
		if (f == tmp.npos)
		{
			ret.push_back(tmp);
			break;
		}

		ret.push_back(tmp.substr(0, f));
		if (f+1 >= tmp.size()) break;
		tmp = tmp.substr(f + 1);
	}

	return ret;
}

bool StringOperations::StartsWith(const String &check, char c)
{
	if (check.empty())
		return false;

	return ((char)(check[0]) == c);
}

bool StringOperations::StartsWith(const String &check, const String &s)
{
	if (check.empty())
		return false;

	size_t thisLen = check.length();
	size_t patLen = s.length();
	if (thisLen < patLen || patLen == 0)
		return false;
	String startThis = check.substr (0, patLen);

	return (startThis == s);
}

bool StringOperations::EndsWith(const String &check, char c)
{
	if (check.empty())
		return false;

	return ((char)(check[check.size()-1]) == c);
}

bool StringOperations::EndsWith(const String &check, const String &s)
{
	if (check.empty())
		return false;

	size_t thisLen = check.length();
	size_t patLen = s.length();
	if (thisLen < patLen ||	patLen == 0)
		return false;
	String endThis = check.substr (thisLen - patLen, patLen);
	return (endThis == s);
}

/*
 Remove tabs and spaces from the string's left and right side.
*/
String StringOperations::Trim (const String &s)
{
	return Trim (s, " \t\r");
}

String StringOperations::Trim(const String &s, const String &delims)
{
	String str = s;
	str.erase (str.find_last_not_of (delims) + 1); //right
	str.erase (0, str.find_first_not_of (delims)); //left
	return str;
}
