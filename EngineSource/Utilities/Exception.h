/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Exception class, this is thrown when a fatal error is detected.
 
 */
#ifndef EXCEPTION_H_DEF
#define EXCEPTION_H_DEF

#include <Platform.h>

class Exception
{
public:
	Exception(const String &error)
	{
		this->error = error;
		this->longDescription = "";
	}
	Exception(const String &error, const String &longDescription)
	{
		this->error = error;
		this->longDescription = longDescription;
	}

	String GetError() const
	{
		return error;
	}

	String GetLongDescription() const
	{
		return longDescription;
	}

protected:
	String error;
	String longDescription;
};

#endif//