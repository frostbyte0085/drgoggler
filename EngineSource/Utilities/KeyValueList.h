/*
 Author: 
 Pantelis Lekakis
 
 Description:
 KeyValueList is a simple text file parser which contains String, int/float pairs. Primarily used
 to parse the Font file.
 
 */
#ifndef KEYVALUELIST_H_
#define KEYVALUELIST_H_

#include <Platform.h>

class KeyValueList {
public:
    // initializes the list by a line of tokens
	KeyValueList(const String &line);
    ~KeyValueList();

    String GetValueNoQuotes(const String &key);
	String GetValue(const String &key);
	int GetIntValue(const String &key);
	float GetFloatValue(String key);
	void ProcessToken(const String &t);
	void ProcessLine(const String &line);

private:
   	Map<String, String> values;
};

#endif
