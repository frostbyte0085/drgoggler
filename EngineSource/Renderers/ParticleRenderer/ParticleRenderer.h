#ifndef PARTICLE_RENDERER_H_DEF
#define PARTICLE_RENDERER_H_DEF

#include "RenderableParticle.h"
#include "ParticleRendererVariables.h"

class Shader;
class Material;
class Transform;

class ParticleRenderer {
public:
    static void Initialize();
    static void Destroy();
    
    static void Render();
    
private:
    static void clearRenderables();
    static RenderableParticle& allocateRenderable();
    
    static ParticleRendererVariables ParticleVars;
    
    static void fetchData();
    
    static void initRendererState();
    static void applyMaterials(Material *material);
    static void drawElements(const ParticleRenderData &rd);
    static void resetRendererState();
    
    
    static void sort();
    static bool sortPredicate (const RenderableParticle& object1, const RenderableParticle &object2);
    
    static List<RenderableParticle> renderables;
};

#endif
