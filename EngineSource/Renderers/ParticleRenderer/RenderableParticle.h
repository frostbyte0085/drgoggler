#ifndef RENDERABLE_PARTICLE_H_DEF
#define RENDERABLE_PARTICLE_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class Material;

struct ParticleRenderData {
    GLuint positionVBO;
    Vector3 *positionData;
    unsigned int positionCount;
    
    GLuint colorVBO;
    Vector4 *colorData;
    unsigned int colorCount;
    
    GLuint texCoord0VBO;
    Vector2 *texCoord0Data;
    unsigned int texCoord0Count;
    
    GLuint texCoord1VBO;
    Vector2 *texCoord1Data;
    unsigned int texCoord1Count;

    GLuint *indexBuffer;
    unsigned int indexCount;
};

// renderable particle, this is passed to the renderer
class RenderableParticle {
public:
    RenderableParticle();
    ~RenderableParticle();
    
    Material *material;
    Matrix44 modelMatrix;
    ParticleRenderData renderData;
    
};

#endif
