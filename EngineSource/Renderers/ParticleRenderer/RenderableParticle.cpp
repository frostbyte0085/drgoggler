#include "RenderableParticle.h"

RenderableParticle::RenderableParticle() {
    material = nullptr;
    
    renderData.positionVBO = 0;
    renderData.positionData = nullptr;
    renderData.positionCount = 0;
    
    renderData.colorVBO = 0;
    renderData.colorData = nullptr;
    renderData.colorCount = 0;
    
    renderData.texCoord0VBO = 0;
    renderData.texCoord0Data = nullptr;
    renderData.texCoord0Count = 0;
    
    renderData.texCoord1VBO = 0;
    renderData.texCoord1Data = nullptr;
    renderData.texCoord1Count = 0;
    
    renderData.indexBuffer = nullptr;
    renderData.indexCount = 0;
}

RenderableParticle::~RenderableParticle() {
    
}

