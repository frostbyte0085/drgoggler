#ifndef PARTICLE_RENDERER_VARIABLES_H_DEF
#define PARTICLE_RENDERER_VARIABLES_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class ParticleRendererVariables {
public:
    ParticleRendererVariables();
    ~ParticleRendererVariables();
    
    Matrix44 ProjectionMatrix;
    Matrix44 ViewMatrix;
    Matrix44 InvViewMatrix;
    
private:
    
};

#endif
