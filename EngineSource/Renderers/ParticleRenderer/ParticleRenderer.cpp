#include "ParticleRenderer.h"
#include <Graphics/Material/Material.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/Particles/ParticleSystem.h>

#include <Graphics/Mesh/Mesh.h>
#include <Graphics/Camera/Camera.h>

#include <Graphics/GraphicsContext/GraphicsContext.h>

#include <Mathematics/Math.h>
#include <Mathematics/Box.h>
#include <Mathematics/Quad.h>
#include <Mathematics/Sphere.h>

#include <Scenegraph/Scene.h>
#include <Scenegraph/SceneNode.h>
#include <Scenegraph/QueryResult.h>
#include <Application/Timing.h>


ParticleRendererVariables ParticleRenderer::ParticleVars;
List<RenderableParticle> ParticleRenderer::renderables;


void ParticleRenderer::Initialize() {
    Log ("MeshParticleRenderer::Initialize: Success");
}

void ParticleRenderer::Destroy() {
    renderables.clear();
    
    Log ("MeshParticleRenderer::Destroy: Success");
}

void ParticleRenderer::sort() {
    
    renderables.sort (sortPredicate);
}

bool ParticleRenderer::sortPredicate (const RenderableParticle &object1, const RenderableParticle &object2) {
    assert (object1.material);
    assert (object2.material);
    
    Material *material1 = object1.material;
    Texture *diffuse1 = material1->GetDiffuseMap()->second;
    assert (diffuse1);
    
    //Shader *shader1 = material1->GetShader();
    
    Material *material2 = object2.material;
    Texture *diffuse2 = material2->GetDiffuseMap()->second;
    assert (diffuse2);
    
    //Shader *shader2 = material2->GetShader();
    
    if (!diffuse1 || !diffuse2)
        return false;
    
    return diffuse1->GetTextureID() < diffuse2->GetTextureID();
    //return shader1->GetShaderID() < shader2->GetShaderID();
}

void ParticleRenderer::applyMaterials(Material *material) {
    if (!material)
        return;
    
    Shader *shader = material->GetShader();
    if (!shader)
        return;
    
    if (material->HasBlending())
        glEnable(GL_BLEND);
    else
        glDisable(GL_BLEND);
        
    glBlendFunc(material->GetSrcBlend(), material->GetDestBlend());
    //glBlendFuncSeparate(material->GetSrcBlend(), material->GetDestBlend(), GL_ONE, GL_ZERO);
    
    // texture repeat
    shader->SetUniform2f("diffuseMapRepeat", material->GetDiffuseMapRepeat());
    
    // texture scroll
    shader->SetUniform2f("diffuseMapScroll", material->GetDiffuseMapScroll());
    
    // textures
    if (material->HasDiffuseMap()) {
        TexturePair *diffusePair = material->GetDiffuseMap();
        glEnable (GL_TEXTURE_2D);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffusePair->second->GetTextureID());
        shader->SetUniform1i(diffusePair->first, 0);
    }
    
    glDepthMask(material->CanDepthWrite());
}

void ParticleRenderer::drawElements(const ParticleRenderData &rd) {
    // position buffer
    glBindBuffer(GL_ARRAY_BUFFER, rd.positionVBO);
    glBufferData(GL_ARRAY_BUFFER, rd.positionCount * sizeof(Vector3), rd.positionData, GL_STREAM_DRAW);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    
    // color //buffer
    glBindBuffer(GL_ARRAY_BUFFER, rd.colorVBO);
    glBufferData(GL_ARRAY_BUFFER, rd.colorCount * sizeof(Vector4), rd.colorData, GL_STREAM_DRAW);
    glColorPointer(4, GL_FLOAT, 0, 0);
    
    // texture coordinate buffer
    glClientActiveTexture(GL_TEXTURE0);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, rd.texCoord0VBO);
    glBufferData(GL_ARRAY_BUFFER, rd.texCoord0Count*sizeof(Vector2), rd.texCoord0Data, GL_STREAM_DRAW);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);

    // particle sizes as texture coordinate 1
    glClientActiveTexture(GL_TEXTURE1);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, rd.texCoord1VBO);
    glBufferData(GL_ARRAY_BUFFER, rd.texCoord1Count*sizeof(Vector2), rd.texCoord1Data, GL_STREAM_DRAW);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);
    
    glDrawElements(GL_TRIANGLES, rd.indexCount, GL_UNSIGNED_INT, rd.indexBuffer);
}

void ParticleRenderer::initRendererState() {
    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ENABLE_BIT | GL_TEXTURE_BIT | GL_TRANSFORM_BIT | GL_VIEWPORT_BIT | GL_LIGHTING_BIT | GL_CURRENT_BIT);
    
    glPushClientAttrib (GL_CLIENT_VERTEX_ARRAY_BIT | GL_CLIENT_PIXEL_STORE_BIT);
    
    glEnableClientState( GL_VERTEX_ARRAY );
    glEnableClientState( GL_TEXTURE_COORD_ARRAY );
    glEnableClientState( GL_COLOR_ARRAY );
    
    glDisable(GL_CULL_FACE);
}

void ParticleRenderer::resetRendererState() {
    glClientActiveTexture(GL_TEXTURE0);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glUseProgram(0);
    
    glDisableClientState( GL_VERTEX_ARRAY );
    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
    glDisableClientState( GL_COLOR_ARRAY );
    
    glDepthMask(GL_TRUE);
}

void ParticleRenderer::fetchData() {
    // setup global uniform variables
    Matrix44 cameraModelMatrix;
    
    if (Scene::activeCameraNode) {
        Camera *camera = Scene::activeCameraNode->camera;
        assert (Scene::activeCameraNode->camera);
        
        ParticleVars.ProjectionMatrix = camera->GetProjectionMatrix();
        ParticleVars.ViewMatrix = glm::matrix::inverse(Scene::activeCameraNode->GetMatrix());
        ParticleVars.InvViewMatrix = glm::inverse (ParticleVars.ViewMatrix);
        
        cameraModelMatrix = Scene::activeCameraNode->GetMatrix();
    }

    
    // get all scene particles
    const QueryResult &result = Scene::GetSceneNodesWithParticleSystems();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++){
        SceneNode *node = Scene::GetSceneNode(result.nodeIds[i]);
        if (!node)
            continue;

        ParticleSystem *ps = node->GetParticleSystem();
        if (ps->GetAliveParticlesCount() == 0)
            continue;
        
        RenderableParticle &rp = ParticleRenderer::allocateRenderable();
        rp.material = ps->GetMaterial();
        rp.renderData.positionVBO = ps->vbos[0];
        rp.renderData.colorVBO = ps->vbos[1];
        rp.renderData.texCoord0VBO = ps->vbos[2];
        rp.renderData.texCoord1VBO = ps->vbos[3];
        
		if(ps->particleVertices.size() != 0) {
			rp.renderData.positionData = &ps->particleVertices[0];
			rp.renderData.positionCount = ps->particleVertices.size();
		}
		if(ps->particleColors.size() != 0) {
			rp.renderData.colorData = &ps->particleColors[0];
			rp.renderData.colorCount = ps->particleColors.size();
		}
		if(ps->particleTexCoords.size() != 0) {
			rp.renderData.texCoord0Data = &ps->particleTexCoords[0];
			rp.renderData.texCoord0Count = ps->particleTexCoords.size();
		}
		if(ps->particleSizes.size() != 0) {
			rp.renderData.texCoord1Data = &ps->particleSizes[0];
			rp.renderData.texCoord1Count = ps->particleSizes.size();
		}
		if(ps->indexBuffer.size() != 0) {
			rp.renderData.indexBuffer = &ps->indexBuffer[0];
			rp.renderData.indexCount = ps->indexBuffer.size();
		}
        rp.modelMatrix = node->GetMatrix();
    }
    Scene::FreeQueryResult(result);
}

void ParticleRenderer::Render() {
    
    clearRenderables();
    
    fetchData();
    
    // enable various opengl attributes
    initRendererState();
    
    sort();
    
    List<RenderableParticle>::iterator it;
    
    for (it=renderables.begin(); it!=renderables.end(); it++) {
        RenderableParticle *renderable = &(*it);
        if (!renderable)
            continue;
        
        // must have a material
        Material *material = renderable->material;
        if (!material)
            continue;
        
        // must have a shader
        Shader *shader = renderable->material->GetShader();
        if (!shader)
            continue;
        
        shader->Use();
        
        // set the projection and modelview matrices
        shader->SetUniformMatrix4("projectionMatrix", ParticleVars.ProjectionMatrix);
        shader->SetUniformMatrix4("viewMatrix", ParticleVars.ViewMatrix);
        shader->SetUniformMatrix4("invViewMatrix", ParticleVars.InvViewMatrix);
        
        shader->SetUniformMatrix4("modelMatrix", renderable->modelMatrix);
        
        // set some global shader variables
        shader->SetUniform1f ("currentTime", Timing::CurrentTime);
                
        // apply the material to the shader
        applyMaterials(material);
        
        // draw our vbo with glDrawElements
        drawElements(renderable->renderData);
    }
    
    // reset those attributes
    resetRendererState();
}

RenderableParticle& ParticleRenderer::allocateRenderable() {
    renderables.push_back(RenderableParticle());
    
    return renderables.back();
}

void ParticleRenderer::clearRenderables() {
    renderables.resize(0);
}