#ifndef POST_PROCESS_RENDERER_H_DEF
#define POST_PROCESS_RENDERER_H_DEF

#include <Platform.h>

class Material;
class Texture;

enum PostProcessEffect {
    PPE_NONE = 0,
    PPE_DREAMSCAPE = 1
};

class PostProcessRenderer {
public:
    static void Initialize();
    static void Destroy();
    
    static void Begin();
    static void DoPostProcess();
    
    static PostProcessEffect Effect;
private:
    
    static Material *material;
    static Material *materialDownsampled;
    static Material *materialBlurred;
    
    static Texture *texture;
    static Texture *textureDownsampled;
    static Texture *textureBlurred;
    
    static float downsampledWidth;
    static float downsampledHeight;
    
    static GLuint fbo, rbo;
    
    static GLuint fboDownsampled, rboDownsampled;
    
    static GLuint fboBlurred, rboBlurred;
};

#endif
