#include "PostProcessRenderer.h"
#include <Graphics/Material/Material.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/GraphicsContext/GraphicsContext.h>

#include <Resources/ResourceManager.h>
#include <Resources/MaterialResource.h>
#include <Resources/TextureResource.h>

Material* PostProcessRenderer::material = nullptr;
Texture* PostProcessRenderer::texture = nullptr;
GLuint PostProcessRenderer::fbo = -1;
GLuint PostProcessRenderer::rbo = -1;

Material* PostProcessRenderer::materialDownsampled = nullptr;
Texture* PostProcessRenderer::textureDownsampled = nullptr;
GLuint PostProcessRenderer::fboDownsampled = -1;
GLuint PostProcessRenderer::rboDownsampled = -1;

Material* PostProcessRenderer::materialBlurred = nullptr;
Texture* PostProcessRenderer::textureBlurred = nullptr;
GLuint PostProcessRenderer::fboBlurred = -1;
GLuint PostProcessRenderer::rboBlurred = -1;

float PostProcessRenderer::downsampledWidth = 0.0f;
float PostProcessRenderer::downsampledHeight = 0.0f;

PostProcessEffect PostProcessRenderer::Effect = PPE_NONE;

void PostProcessRenderer::Initialize() {
    downsampledWidth = (float)GraphicsContext::GetCurrentDisplayMode().Width/4;
    downsampledHeight = (float)GraphicsContext::GetCurrentDisplayMode().Height/4;
    
    material = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor("Data/Materials/Downsample.mat", false))->MaterialObject;
    assert (material);
    
    texture = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("framebuffer", false, false, TAM_CLAMP, true))->TextureObject;
    
    assert (texture);
    
    // whole screen fbo & rbo
    glGenFramebuffers(1, &fbo);
    glGenRenderbuffers(1, &rbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    
    glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, GraphicsContext::GetCurrentDisplayMode().Width, GraphicsContext::   GetCurrentDisplayMode().Height, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, nullptr);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->GetTextureID(), 0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, GraphicsContext::GetCurrentDisplayMode().Width, GraphicsContext::GetCurrentDisplayMode().Height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
    
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    // 1/4 screen fbo & rbo: downsampled.
    materialDownsampled = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor("Data/Materials/Blur.mat", false))->MaterialObject;
    assert (materialDownsampled);
    
    textureDownsampled = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("framebufferDownsampled", false, false, TAM_CLAMP, true))->TextureObject;
    
    glGenFramebuffers(1, &fboDownsampled);
    glGenRenderbuffers(1, &rboDownsampled);
    glBindFramebuffer(GL_FRAMEBUFFER, fboDownsampled);
    
    glBindTexture(GL_TEXTURE_2D, textureDownsampled->GetTextureID());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)downsampledWidth, (GLsizei)downsampledHeight, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, nullptr);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureDownsampled->GetTextureID(), 0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, rboDownsampled);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, (GLsizei)downsampledWidth, (GLsizei)downsampledHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDownsampled);
    
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    // 1/4 screen fbo & rbo: blurred.
    materialBlurred = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor("Data/Materials/GlowCombine.mat", false))->MaterialObject;
    assert (materialBlurred);
    
    textureBlurred = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor("framebufferBlurred", false, false, TAM_CLAMP, true))->TextureObject;
    
    glGenFramebuffers(1, &fboBlurred);
    glGenRenderbuffers(1, &rboBlurred);
    glBindFramebuffer(GL_FRAMEBUFFER, fboBlurred);
    
    glBindTexture(GL_TEXTURE_2D, textureBlurred->GetTextureID());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)downsampledWidth, (GLsizei)downsampledHeight, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, nullptr);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureBlurred->GetTextureID(), 0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, rboBlurred);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, (GLsizei)downsampledWidth, (GLsizei)downsampledHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboBlurred);
    
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProcessRenderer::Destroy() {
    glDeleteFramebuffers(1, &fbo);
    glDeleteRenderbuffers(1, &rbo);
    
    glDeleteFramebuffers(1, &fboDownsampled);
    glDeleteRenderbuffers(1, &rboDownsampled);
    
    glDeleteFramebuffers(1, &fboBlurred);
    glDeleteRenderbuffers(1, &rboBlurred);
}

void PostProcessRenderer::Begin() {
    glBindTexture (GL_TEXTURE_2D, 0);
    glBindTexture (GL_TEXTURE_CUBE_MAP, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void PostProcessRenderer::DoPostProcess() {
    assert (material);
    assert (texture);

    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ENABLE_BIT | GL_TEXTURE_BIT | GL_TRANSFORM_BIT | GL_VIEWPORT_BIT);

    glPushClientAttrib (GL_CLIENT_VERTEX_ARRAY_BIT | GL_CLIENT_PIXEL_STORE_BIT);
    
    glDisable(GL_LIGHTING);
    
    
    
    GLfloat viewport[4];
    glGetFloatv(GL_VIEWPORT, viewport);
  
    Matrix44 orthoProjection = glm::ortho(-1.0f,
                                          1.0f,
                                          1.0f,
                                          -1.0f, -1.0f, 1.0f);
    
    Vector2 vertices[] = {
        Vector2(-1,1),
        Vector2(1,1),
        Vector2(1,-1),
        Vector2(-1,-1)
    };
    
    float texoffsetX = (1.0f / viewport[2]) * 0.5f;
    float texoffsetY = (1.0f / viewport[3]) * 0.5f;
    
    float u0 = 0+texoffsetX;
    float v0 = 0+texoffsetY;
    
    float u1 = 1.0f - texoffsetX;
    float v1 = 1.0f - texoffsetY;
    
    Vector2 texcoords[] = {
        Vector2(u0,v0),
        Vector2(u1,v0),
        Vector2(u1,v1),
        Vector2(u0,v1)
    };

    unsigned short indices[] = {
        0, 1, 2, 2, 3, 0
    };
    
    glBindFramebuffer(GL_FRAMEBUFFER, fboDownsampled);
            
    GraphicsContext::Clear();
    
    material->SetDiffuseMap(texture);
    
    Shader *shader = material->GetShader();
    assert (shader);
    
    shader->Use();
    shader->SetUniformMatrix4("projectionMatrix", orthoProjection);
    shader->SetUniform1f ("rtWidth", viewport[2]);
    shader->SetUniform1f ("rtHeight", viewport[3]);
    
    if (material->HasBlending())
        glEnable(GL_BLEND);
    else
        glDisable(GL_BLEND);
    
    glBlendFunc(material->GetSrcBlend(), material->GetDestBlend());
    
    if (material->HasDiffuseMap()) {
        TexturePair *diffusePair = material->GetDiffuseMap();
        glEnable (GL_TEXTURE_2D);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffusePair->second->GetTextureID());
        
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_POINT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_POINT);
        
        shader->SetUniform1i(diffusePair->first, 0);
    }
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer( 2, GL_FLOAT, 0, &vertices[0]);
    
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, &texcoords[0]);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, &indices[0]);
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    // downsampled textured quad, render it in the fboBlurred
	for (int blurPass=0; blurPass<10; blurPass++) {
		glBindFramebuffer(GL_FRAMEBUFFER, fboBlurred);
    
		glViewport(0, 0, (GLsizei)downsampledWidth, (GLsizei)downsampledHeight);
		glGetFloatv(GL_VIEWPORT, viewport);
    
		GraphicsContext::BackgroundColor = Vector4(0,0,0,1);
		GraphicsContext::Clear();
    
		materialDownsampled->SetDiffuseMap(textureDownsampled);
    
		Shader *shaderDownsampled = materialDownsampled->GetShader();
		assert (shaderDownsampled);
    
		shaderDownsampled->Use();
		shaderDownsampled->SetUniformMatrix4("projectionMatrix", orthoProjection);
		shaderDownsampled->SetUniform1f ("rtWidth", downsampledWidth);
		shaderDownsampled->SetUniform1f ("rtHeight", downsampledHeight);
    
		if (materialDownsampled->HasBlending())
			glEnable(GL_BLEND);
		else
			glDisable(GL_BLEND);
    
		glBlendFunc(materialDownsampled->GetSrcBlend(), materialDownsampled->GetDestBlend());
    
		if (materialDownsampled->HasDiffuseMap()) {
			TexturePair *diffusePair = materialDownsampled->GetDiffuseMap();
			glEnable (GL_TEXTURE_2D);
        
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, diffusePair->second->GetTextureID());
        
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_POINT);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_POINT);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
			shaderDownsampled->SetUniform1i(diffusePair->first, 0);
		}
    
		texoffsetX = (1.0f / viewport[2]) * 0.0525f;
		texoffsetY = (1.0f / viewport[3]) * -0.0625f;
    
		u0 = 0 - texoffsetX;
		v0 = 0 - texoffsetY;
    
		u1 = 1.0f - texoffsetX;
		v1 = 1.0f - texoffsetY;
    
		Vector2 texcoords2[] = {
			Vector2(u0,v0),
			Vector2(u1,v0),
			Vector2(u1,v1),
			Vector2(u0,v1)
		};
    
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 2, GL_FLOAT, 0, &vertices[0]);
    
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, 0, &texcoords2[0]);
    
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, &indices[0]);
    
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		std::swap(textureDownsampled, textureBlurred);
		std::swap(fboDownsampled, fboBlurred);
	}
	std::swap(textureDownsampled, textureBlurred);
	std::swap(fboDownsampled, fboBlurred);
    //////////////////////////////////////////////////////////////////////////////

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    //glViewport(0, 0, (GLsizei)downsampledWidth, (GLsizei)downsampledHeight);
	glViewport(0,0,GraphicsContext::GetCurrentDisplayMode().Width, GraphicsContext::GetCurrentDisplayMode().Height);
    glGetFloatv(GL_VIEWPORT, viewport);
    
    GraphicsContext::BackgroundColor = Vector4(0,0,0,1);
    GraphicsContext::Clear();
    
    materialBlurred->SetDiffuseMap(textureBlurred);
    
    Shader *shaderBlurred = materialBlurred->GetShader();
    assert (shaderBlurred);
    
    shaderBlurred->Use();
    shaderBlurred->SetUniformMatrix4("projectionMatrix", orthoProjection);
    shaderBlurred->SetUniform1f ("rtWidth", downsampledWidth);
    shaderBlurred->SetUniform1f ("rtHeight", downsampledHeight);
    
    if (materialBlurred->HasBlending())
        glEnable(GL_BLEND);
    else
        glDisable(GL_BLEND);
    
    glBlendFunc(materialBlurred->GetSrcBlend(), materialBlurred->GetDestBlend());

    
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
    
    shaderBlurred->SetUniform1i("sceneTexture", 0);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textureDownsampled->GetTextureID());
    
    shaderBlurred->SetUniform1i("blurTexture", 1);
    
    texoffsetX = (1.0f / viewport[2]) * 0.5f;
    texoffsetY = (1.0f / viewport[3]) * 0.5f;
    
    u0 = 0+texoffsetX;
    v0 = 0+texoffsetY;
    
    u1 = 1.0f - texoffsetX;
    v1 = 1.0f - texoffsetY;
    
    Vector2 texcoords3[] = {
        Vector2(u0,v0),
        Vector2(u1,v0),
        Vector2(u1,v1),
        Vector2(u0,v1)
    };
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer( 2, GL_FLOAT, 0, &vertices[0]);
    
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, &texcoords3[0]);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, &indices[0]);
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glPopClientAttrib();
    glPopAttrib();
    
    glFlush();
    
    glDisable(GL_TEXTURE_2D);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
