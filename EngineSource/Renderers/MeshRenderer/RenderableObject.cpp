#include "RenderableObject.h"

RenderableObject::RenderableObject() {
    material = nullptr;
    
    renderData.positionVBO = 0;
    renderData.normalVBO = 0;
    renderData.texCoord0VBO = 0;
    renderData.boneWeightsVBO0 = 0;
    renderData.boneIndicesVBO0 = 0;
    
    renderData.animatedBones = nullptr;
    renderData.animatedBonesCount = 0;
    
    renderData.indexBuffer = nullptr;
    renderData.indexCount = 0;
}

RenderableObject::~RenderableObject() {

}

