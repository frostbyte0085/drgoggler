#ifndef MESH_RENDERER_H_DEF
#define MESH_RENDERER_H_DEF

#include "RenderableObject.h"
#include "MeshRendererVariables.h"

class RenderableObject;
class Shader;
class Material;
class Transform;
class Collider;

struct RenderableBoundingVolume {
    Collider *collider;
    Matrix44  modelMatrix;
};

class MeshRenderer {
public:
    static void Initialize();
    static void Destroy();
    
    static void Render(bool drawDebug);
    
private:
    static void clearRenderables();
    static RenderableObject& allocateRenderable();
    static RenderableBoundingVolume& allocateBoundingVolume();
    
    static MeshRendererVariables MeshVars;
    
    static void fetchData();
    static void fetchDebugData();
    
    static void initRendererState();
    static void applyMaterials(Material *material);
    static void drawElements(const MeshRenderData &rd);
    static void drawBoundingVolumes();
    static void resetRendererState();
    
    
    static void sort();
    static bool sortPredicate (const RenderableObject& object1, const RenderableObject &object2);
    
    static Vector< RenderableBoundingVolume > boundingVolumes;
    static List<RenderableObject> renderables;
    
    static GLUquadricObj *quadric;
};

#endif
