#ifndef MESH_RENDERER_VARIABLES_H_DEF
#define MESH_RENDERER_VARIABLES_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class MeshRendererVariables {
public:
    MeshRendererVariables();
    ~MeshRendererVariables();
    
    Matrix44 ProjectionMatrix;
    Matrix44 ViewMatrix;
    
    // world light
    Vector3 WorldLightDirection;
    Vector4 WorldLightAmbient;
    Vector4 WorldLightDiffuse;
    
    //blob shadow
    Vector3 BlobShadowCenter;
    float BlobShadowRadius;
    Vector4 BlobShadowColor;
    float BlobShadowDistance;
    float BlobShadowMaxDistance;
private:

};

#endif
