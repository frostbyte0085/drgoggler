#include "MeshRenderer.h"
#include <Graphics/Material/Material.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/Collider/SphereCollider.h>
#include <Graphics/Collider/BoxCollider.h>

#include <Graphics/Mesh/Mesh.h>
#include <Graphics/Camera/Camera.h>

#include <Mathematics/Math.h>
#include <Mathematics/Box.h>
#include <Mathematics/Quad.h>
#include <Mathematics/Sphere.h>

#include <Graphics/GraphicsContext/GraphicsContext.h>

#include <Scenegraph/Scene.h>
#include <Scenegraph/SceneNode.h>
#include <Scenegraph/QueryResult.h>
#include <Application/Timing.h>

MeshRendererVariables MeshRenderer::MeshVars;
List<RenderableObject> MeshRenderer::renderables;
Vector<RenderableBoundingVolume> MeshRenderer::boundingVolumes;

GLUquadric* MeshRenderer::quadric = nullptr;

void MeshRenderer::Initialize() {
    Log ("MeshRenderer::Initialize: Success");
    quadric = gluNewQuadric();
}

void MeshRenderer::Destroy() {
    renderables.clear();
    boundingVolumes.clear();
    
    gluDeleteQuadric(quadric);
    Log ("MeshRenderer::Destroy: Success");
}

void MeshRenderer::sort() {
    
    renderables.sort (sortPredicate);
}

bool MeshRenderer::sortPredicate (const RenderableObject &object1, const RenderableObject &object2) {
    assert (object1.material);
    assert (object2.material);
    
    Material *material1 = object1.material;
    Texture *diffuse1 = material1->GetDiffuseMap()->second;
    assert (diffuse1);
    
    //Shader *shader1 = material1->GetShader();
    
    Material *material2 = object2.material;
    Texture *diffuse2 = material2->GetDiffuseMap()->second;
    assert (diffuse2);
    
    //Shader *shader2 = material2->GetShader();
    
    if (!diffuse1 || !diffuse2)
        return false;
    
    return diffuse1->GetTextureID() < diffuse2->GetTextureID();
    //return shader1->GetShaderID() < shader2->GetShaderID();
}

void MeshRenderer::applyMaterials(Material *material) {
    if (!material)
        return;

    Shader *shader = material->GetShader();
    if (!shader)
        return;
    
    if (material->HasBlending())
        glEnable(GL_BLEND);
    else
        glDisable(GL_BLEND);
    
    // world light
    Vector4 lightPosition = Vector4(MeshVars.WorldLightDirection.x, MeshVars.WorldLightDirection.y, MeshVars.WorldLightDirection.z, 0.0f);
    
    float shininess = material->GetShininess() * 128.0f;
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glm::value_ptr(material->GetDiffuseColor()));
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, glm::value_ptr(material->GetAmbientColor()));
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, glm::value_ptr(material->GetSpecularColor()));
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &shininess);
    
    glLightfv (GL_LIGHT0, GL_DIFFUSE, glm::value_ptr(MeshVars.WorldLightDiffuse));
    glLightfv (GL_LIGHT0, GL_AMBIENT, glm::value_ptr(MeshVars.WorldLightAmbient));
    glLightfv (GL_LIGHT0, GL_POSITION, glm::value_ptr(lightPosition));
    
    glBlendFunc(material->GetSrcBlend(), material->GetDestBlend());
    
    shader->SetUniform1i("lighting", (int)material->HasLighting());
    
    // texture repeat
    shader->SetUniform2f("diffuseMapRepeat", material->GetDiffuseMapRepeat());
    shader->SetUniform2f("normalMapRepeat", material->GetNormalMapRepeat());
    shader->SetUniform2f("specularMapRepeat", material->GetSpecularMapRepeat());
    
    // texture scroll
    shader->SetUniform2f("diffuseMapScroll", material->GetDiffuseMapScroll());
    shader->SetUniform2f("normalMapScroll", material->GetNormalMapScroll());
    shader->SetUniform2f("specularMapScroll", material->GetSpecularMapScroll());
    
    // textures
    if (material->HasDiffuseMap()) {
        TexturePair *diffusePair = material->GetDiffuseMap();
        if (!material->IsCubemap()) {
            
            glEnable (GL_TEXTURE_2D);
            
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, diffusePair->second->GetTextureID());
            shader->SetUniform1i(diffusePair->first, 0);
        
            if (material->HasNormalMap()) {
                TexturePair *normalPair = material->GetNormalMap();
            
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, normalPair->second->GetTextureID());
                shader->SetUniform1i(normalPair->first, 1);
            
                if (material->HasSpecularMap()) {
                    TexturePair *specPair = material->GetSpecularMap();
                
                    glActiveTexture(GL_TEXTURE2);
                    glBindTexture(GL_TEXTURE_2D, specPair->second->GetTextureID());
                    shader->SetUniform1i(specPair->first, 2);
                }
            }
        }
        else {
            glEnable(GL_TEXTURE_CUBE_MAP);
            
            glDisable(GL_TEXTURE_GEN_S);
            glDisable(GL_TEXTURE_GEN_T);
            glDisable(GL_TEXTURE_GEN_R);
            
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_CUBE_MAP, diffusePair->second->GetTextureID());
            
            shader->SetUniform1i(diffusePair->first, 0);
        }
    }
    glDepthMask(material->CanDepthWrite());
}

void MeshRenderer::drawElements(const MeshRenderData &rd) {
    // position buffer
    glBindBuffer(GL_ARRAY_BUFFER, rd.positionVBO);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    
    // normal buffer
    glBindBuffer(GL_ARRAY_BUFFER, rd.normalVBO);
    glNormalPointer(GL_FLOAT, 0, 0);
    
    // texture coordinate buffer
    glClientActiveTexture(GL_TEXTURE0);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, rd.texCoord0VBO);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);
    
    // texture coordinate 1: weights buffer 0
    glClientActiveTexture(GL_TEXTURE1);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, rd.boneWeightsVBO0);
    glTexCoordPointer(4, GL_FLOAT, 0, 0);
    
    // texture coordinate 2: weights index buffer 0
    glClientActiveTexture(GL_TEXTURE2);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, rd.boneIndicesVBO0);
    glTexCoordPointer(4, GL_FLOAT, 0, 0);

     
    glDrawElements(GL_TRIANGLES, rd.indexCount, GL_UNSIGNED_INT, rd.indexBuffer);

}

void MeshRenderer::initRendererState() {
    glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    glEnableClientState( GL_VERTEX_ARRAY );
    glEnableClientState( GL_TEXTURE_COORD_ARRAY );
    glEnableClientState( GL_NORMAL_ARRAY );
    
    glBindTexture (GL_TEXTURE_CUBE_MAP, 0);
    glBindTexture (GL_TEXTURE_2D, 0);
}

void MeshRenderer::resetRendererState() {
    
    glClientActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    
    glDisable(GL_TEXTURE_CUBE_MAP);
    glDisable(GL_TEXTURE_2D);
    
    glTexCoordPointer(2, GL_FLOAT, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);

    
    glDisableClientState( GL_VERTEX_ARRAY );
    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
    glDisableClientState( GL_NORMAL_ARRAY );
}

void MeshRenderer::fetchData() {
    // setup global uniform variables
    if (Scene::activeCameraNode) {
        Camera *camera = Scene::activeCameraNode->camera;
        assert (Scene::activeCameraNode->camera);
        
        MeshVars.ProjectionMatrix = camera->GetProjectionMatrix();
        MeshVars.ViewMatrix = glm::matrix::inverse(Scene::activeCameraNode->GetMatrix());
    }
        
    // world light parameters
    MeshVars.WorldLightDirection = Scene::env.worldLightDirection;
    MeshVars.WorldLightAmbient = Scene::env.worldLightAmbient;
    MeshVars.WorldLightDiffuse = Scene::env.worldLightDiffuse;
    
    // blob shadow parameters
    MeshVars.BlobShadowCenter = Scene::env.blobShadowCenter;
    
    if (Scene::env.blobShadowEnable)
        MeshVars.BlobShadowColor = Scene::env.blobShadowColor;
    else
        MeshVars.BlobShadowColor = Vector4(1,1,1,1);
    
    MeshVars.BlobShadowRadius = Scene::env.blobShadowRadius;
    MeshVars.BlobShadowDistance = Scene::env.blobShadowDistance;
    MeshVars.BlobShadowMaxDistance = Scene::env.blobShadowMaxDistance;
        
    // get all scene meshes
    const QueryResult &result = Scene::GetSceneNodesWithMeshes();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++){
        SceneNode *node = Scene::GetSceneNode(result.nodeIds[i]);
        if (!node || node->IsFrustumCulled()) {
            continue;
		}
        
        Mesh *mesh = node->GetMesh();
        
        for (int j=0; j<(int)mesh->GetSubmeshCount(); j++) {
            RenderableObject &ro = MeshRenderer::allocateRenderable();
            Mesh::Submesh *sm = mesh->GetSubmesh(j);
            
            ro.material = sm->material;
            
            ro.renderData.positionVBO = sm->vbos[0];
            ro.renderData.normalVBO = sm->vbos[1];
            ro.renderData.texCoord0VBO = sm->vbos[2];
            ro.renderData.boneWeightsVBO0 = sm->vbos[3];
            ro.renderData.boneIndicesVBO0 = sm->vbos[4];
            
            ro.renderData.indexBuffer = &sm->indexBuffer[0];
            ro.renderData.indexCount = (int)sm->indexBuffer.size();
            
            if(mesh->animatedBones.size() > 0) {
				ro.renderData.animatedBones = &mesh->animatedBones[0];
				ro.renderData.animatedBonesCount = (int)mesh->animatedBones.size();
			}    
            ro.modelMatrix = node->GetMatrix();
        }
    }
    Scene::FreeQueryResult(result);
}

void MeshRenderer::fetchDebugData() {
    // get all the scene colliders - add the boundingVolumes to the list to render
    const QueryResult &resultColliders = Scene::GetSceneNodesWithColliders();
    
    for (int i=0; i<(int)resultColliders.nodeIds.size(); i++) {
        SceneNode *node = Scene::GetSceneNode(resultColliders.nodeIds[i]);
        assert (node);
        
        RenderableBoundingVolume &bv = MeshRenderer::allocateBoundingVolume();
        bv.collider = node->GetCollider();
        bv.modelMatrix = node->GetMatrix();
    }
    
    Scene::FreeQueryResult(resultColliders);
}

void MeshRenderer::Render(bool drawDebug) {

    clearRenderables();
    
    fetchData();
    if (drawDebug)
        fetchDebugData();
    
    // enable various opengl attributes
    initRendererState();
    
    sort();
    
    List<RenderableObject>::iterator it;
    
    for (it=renderables.begin(); it!=renderables.end(); it++) {
        RenderableObject *renderable = &(*it);
        if (!renderable)
            continue;
        
        // must have a material
        Material *material = renderable->material;
        if (!material)
            continue;
        
        // must have a shader
        Shader *shader = renderable->material->GetShader();
        if (!shader)
            continue;
             
        shader->Use();
        
        // set the projection and modelview matrices
        shader->SetUniformMatrix4("projectionMatrix", MeshVars.ProjectionMatrix);
        shader->SetUniformMatrix4("viewMatrix", MeshVars.ViewMatrix);
        
        shader->SetUniformMatrix4("modelMatrix", renderable->modelMatrix);
        
        // set some global shader variables
        shader->SetUniform1f ("currentTime", Timing::CurrentTime);
        
        // blob shadow
        shader->SetUniform1f("blobShadowRadius", MeshVars.BlobShadowRadius);
        shader->SetUniform4f("blobShadowColor", MeshVars.BlobShadowColor);
        shader->SetUniform3f("blobShadowCenter", MeshVars.BlobShadowCenter);
        shader->SetUniform1f("blobShadowDistance", MeshVars.BlobShadowDistance);
        shader->SetUniform1f("blobShadowMaxDistance", MeshVars.BlobShadowMaxDistance);
        
        // skinned mesh bone matrices
        if (renderable->renderData.animatedBonesCount > 0) {
            
            shader->SetUniformMatrix4v("boneMatrix", renderable->renderData.animatedBones, renderable->renderData.animatedBonesCount);
            
        }

        // apply the material to the shader
        applyMaterials(material);
        
        // draw our vbo with glDrawElements
        drawElements(renderable->renderData);
    }
    
    // reset those attributes
    resetRendererState();
    
    // draw the debug bounding volumes for our colliders
    drawBoundingVolumes();
}

void MeshRenderer::drawBoundingVolumes() {
    if (!boundingVolumes.size() )
        return;
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glPushMatrix();
    
    glDisable(GL_LIGHTING);
    
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(glm::value_ptr( MeshVars.ProjectionMatrix) );
    
    Vector<RenderableBoundingVolume>::iterator it;
    
    glColor4f(1,0,0,1);
    
    for (it=boundingVolumes.begin(); it!=boundingVolumes.end(); it++) {
        RenderableBoundingVolume &bv = *it;
        
        glMatrixMode(GL_MODELVIEW);
        Matrix44 modelView = MeshVars.ViewMatrix;
        glLoadMatrixf (glm::value_ptr(modelView));
    
        if (bv.collider->GetColliderType() == CT_SPHERE) {
            SphereCollider *sc = (SphereCollider*)bv.collider;
            Sphere &sphere = sc->GetGlobalBoundingSphere();

            glTranslatef (sphere.GetCenter().x, sphere.GetCenter().y, sphere.GetCenter().z );
            gluSphere(quadric, sphere.GetRadius(), 16, 16);
        }
        else if (bv.collider->GetColliderType() == CT_BOX) {
            BoxCollider *bc = (BoxCollider*)bv.collider;
            Box &b = bc->GetGlobalBoundingBox();

            glTranslatef (0,0,0);
            
            glBegin(GL_QUADS);
            for (int i=0; i<6; i++) {
                Quad quad = b.GetQuad(i);
                
                for (int j=0; j<4; j++) {
                    glVertex3f (quad.GetVertex(j).x, quad.GetVertex(j).y, quad.GetVertex(j).z);
                }
            }
            
            glEnd();
        }
    
    }
    glPopMatrix();
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

RenderableObject& MeshRenderer::allocateRenderable() {
    renderables.push_back(RenderableObject());
    
    return renderables.back();
}

RenderableBoundingVolume& MeshRenderer::allocateBoundingVolume() {
    boundingVolumes.push_back(RenderableBoundingVolume());
    
    return boundingVolumes[boundingVolumes.size()-1];
}

void MeshRenderer::clearRenderables() {
    renderables.resize(0);
    boundingVolumes.resize(0);
}