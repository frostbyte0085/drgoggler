#ifndef RENDERABLE_OBJECT_H_DEF
#define RENDERABLE_OBJECT_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class Material;

struct MeshRenderData {
    GLuint positionVBO;
    GLuint normalVBO;
    GLuint texCoord0VBO;
    GLuint boneWeightsVBO0;
    GLuint boneIndicesVBO0;
    
    Matrix44 *animatedBones;
    unsigned int animatedBonesCount;
    
    GLuint *indexBuffer;
    unsigned int indexCount;
};

// renderable object, this is passed to the renderer
class RenderableObject {
public:
    RenderableObject();
    ~RenderableObject();
    
    Material *material;
    Matrix44 modelMatrix;
    MeshRenderData renderData;
    
};

#endif
