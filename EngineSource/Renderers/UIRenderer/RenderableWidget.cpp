#include "RenderableWidget.h"

RenderableWidget::RenderableWidget() {
    vertices = nullptr;
    verticesCount = 0;
    indices = nullptr;
    indexCount = 0;
    textureCoordinates = nullptr;
    textureCoordinatesCount = 0;
}

RenderableWidget::~RenderableWidget() {
    
}