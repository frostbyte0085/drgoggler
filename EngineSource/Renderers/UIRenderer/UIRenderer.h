/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The UIRenderer retrieves all the UIWidgets from the current UILayer, converts them into RenderableWidgets, sorts them
 by texture and draws them using glDrawElements
*/

#ifndef UI_RENDERER_H_DEF
#define UI_RENDERER_H_DEF

#include <Platform.h>
#include "RenderableWidget.h"

class UIRenderer {
public:
    static void Initialize();
    static void Destroy();
    
    static void Render();
    
private:
    static GLfloat *orthoProjectionData;
    
    static RenderableWidget& allocateRenderable();
    static void clearRenderables();
    static List<RenderableWidget> renderables;
    
    static void fetchData();
    
    static void sort();
    static bool sortPredicate (const RenderableWidget& object1, const RenderableWidget &object2);
    
    static void initRendererState();
    static void resetRendererState();
    static void setupOrthoProjection();
    static void applyMaterials(Material *material);
    static void drawElements(RenderableWidget *rd);
    
    static Matrix44 orthoProjection;
};

#endif
