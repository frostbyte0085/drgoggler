#include "UIRenderer.h"
#include "RenderableWidget.h"
#include <UI/UILayer.h>

#include <UI/Widgets/UIButton.h>
#include <UI/Widgets/UITexture.h>
#include <UI/Widgets/UITextLabel.h>

#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/Material/Material.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>

#include <Scenegraph/Scene.h>

List<RenderableWidget> UIRenderer::renderables;
Matrix44 UIRenderer::orthoProjection;
GLfloat* UIRenderer::orthoProjectionData = nullptr;

void UIRenderer::Initialize() {
    Log ("UIRenderer::Initialize: Success");
}

void UIRenderer::Destroy() {
    renderables.clear();
    
    Log ("UIRenderer::Destroy: Success");
}

/*
 Fetch data works on pointer data to avoid copying vectors around.
*/
void UIRenderer::fetchData() {
    UILayer *ui = Scene::GetUILayer();
    if (!ui)
        return;
    
    UIRenderer::orthoProjectionData = ui->orthoProjection;
    
    for (int w=0; w<(int)ui->widgets.size(); w++) {
        UIWidget *widget = ui->widgets[w];
        assert (widget);
        
        RenderableWidget &ro = UIRenderer::allocateRenderable();
        
        ro.vertices = &widget->vertices[0];
        ro.verticesCount = (int)widget->vertices.size();
        
        ro.textureCoordinates = &widget->textureCoordinates[0];
        ro.textureCoordinatesCount = (int)widget->textureCoordinates.size();
        
        ro.indices = &widget->indices[0];
        ro.indexCount = widget->indices.size();
        
        ro.color = widget->color;
        ro.material = widget->material;
    }
}

void UIRenderer::sort() {
    
    renderables.sort (sortPredicate);
}

/*
Sort the two objects by texture id.
*/
bool UIRenderer::sortPredicate (const RenderableWidget &object1, const RenderableWidget &object2) {
    assert (object1.material);
    assert (object2.material);
    
    Material *material1 = object1.material;
    Texture *diffuse1 = material1->GetDiffuseMap()->second;
    assert (diffuse1);
    
    Material *material2 = object2.material;
    Texture *diffuse2 = material2->GetDiffuseMap()->second;
    assert (diffuse2);
    
    if (!diffuse1 || !diffuse2)
        return false;
    
    return diffuse1->GetTextureID() < diffuse2->GetTextureID();
}

void UIRenderer::initRendererState() {
    setupOrthoProjection();

	glEnable (GL_SCISSOR_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
}

void UIRenderer::resetRendererState() {
    glClientActiveTexture(GL_TEXTURE0);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glUseProgram(0);
    
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
	glDisable(GL_SCISSOR_TEST);
    
    glDepthMask(GL_TRUE);
    
}

void UIRenderer::setupOrthoProjection() {
    orthoProjection = glm::ortho(orthoProjectionData[0] - 0.5f, 
                                 orthoProjectionData[0] + orthoProjectionData[2],
                                 orthoProjectionData[1] + orthoProjectionData[3],
                                 orthoProjectionData[1] - 0.5f, -1.0f, 1.0f);
}


void UIRenderer::applyMaterials(Material *material) {
    if (!material)
        return;

    // get the shader the material is using
    Shader *shader = material->GetShader();
    if (!shader)
        return;
    
    // for the UI, this is probably always true, so enable blending
    if (material->HasBlending())
        glEnable(GL_BLEND);
    else
        glDisable(GL_BLEND);
    
    glBlendFunc(material->GetSrcBlend(), material->GetDestBlend());
    
    // set the diffuse texture
    if (material->HasDiffuseMap()) {
        TexturePair *diffusePair = material->GetDiffuseMap();
        glEnable (GL_TEXTURE_2D);
            
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffusePair->second->GetTextureID());
        shader->SetUniform1i(diffusePair->first, 0);
    }
}

/*
 Get the RenderableWidget data and draw it using glDrawElements
*/
void UIRenderer::drawElements(RenderableWidget *rd) {
    assert (rd);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer( 2, GL_FLOAT, 0, &rd->vertices[0]);
    
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, &rd->textureCoordinates[0]);
    
    glDrawElements(GL_TRIANGLES, rd->indexCount, GL_UNSIGNED_SHORT, &rd->indices[0]);
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*
 Fetch widgets, apply materials/shader global variables and render
*/
void UIRenderer::Render() {
    if (!Scene::GetUILayer())
        return;
    
    clearRenderables();
    
    fetchData();
    
    initRendererState();
    
    //sort();
    
    List<RenderableWidget>::iterator it;
    
    for (it=renderables.begin(); it!=renderables.end(); it++) {
        RenderableWidget *renderable = &(*it);
        if (!renderable)
            continue;
        
        // must have a material
        Material *material = renderable->material;
        if (!material)
            continue;
        
        // must have a shader
        Shader *shader = renderable->material->GetShader();
        if (!shader)
            continue;
        
        shader->Use();
        
        // set the projection and modelview matrices
        shader->SetUniformMatrix4("projectionMatrix", orthoProjection);
        shader->SetUniform4f ("uiColor", renderable->color);
        
        // apply the material to the shader
        applyMaterials(material);
        
        // draw with glDrawElements
        drawElements(renderable);
        
    }
    
    resetRendererState();
    Scene::GetUILayer()->Clear();
}

/*
 Return a reference to the newly created RenderableWidget
*/
RenderableWidget& UIRenderer::allocateRenderable() {
    renderables.push_back(RenderableWidget());
    
    return renderables.back();
}

void UIRenderer::clearRenderables() {
    renderables.resize(0);
}