/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The RenderableWidget represents a UIWidget data using the least possible information the renderer needs to know.
 
*/
#ifndef RENDERABLE_WIDGET_H_DEF
#define RENDERABLE_WIDGET_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class Material;
class Texture;

class RenderableWidget {
public:
    RenderableWidget();
    ~RenderableWidget();
    
    Vector2 *vertices;
    unsigned int verticesCount;
    
    unsigned short *indices;
    unsigned short indexCount;
    
    Vector2 *textureCoordinates;
    unsigned int textureCoordinatesCount;
    
    Vector4 color;
    
    Material *material;
};

#endif
