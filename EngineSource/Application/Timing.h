/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Two timing classes, one for when the game is paused, and one for when the game is not paused.
 That way the menu can have animations if we want to, even if the game is paused.
 
 */
#ifndef TIMING_H_DEF
#define TIMING_H_DEF

#include <Platform.h>

/*
 CurrentTime: time since the application was launched.
 LastFrameTime: elapsed delta time between frame begin and frame end.
*/
class Timing {
public:
	static float CurrentTime;
	static float LastFrameTime;
};

/*
 CurrentTime: time since the application was launched.
 LastFrameTime: elapsed delta time between frame begin and frame end.
 FramesPerSecond: frames per second, updated every 1 second.
 */
class IndependentTiming {
public:
    static float CurrentTime;
    static float LastFrameTime;
    static unsigned int FramesPerSecond;
};

#endif
