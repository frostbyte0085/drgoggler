/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The main application base class, it handles state management, timing and of course initialisation, shutdown
 and update / render of the various subsystems.
 */

#ifndef APPLICATION_H_DEF
#define APPLICATION_H_DEF

#include <Platform.h>

class State;

class Application {
public:
    Application();
    virtual ~Application();
    
    int Run();
    virtual void SetupStates() {};
    
    void AddState (const String &name, State *state);
    void SetState (const String &name);
    String GetCurrentState() const { 
    
        String name = "";
        HashMap<String, State*>::const_iterator it;
        for (it=states.begin(); it!=states.end(); it++) {
            if (it->second == currentState) {
                name = it->first;
                break;
            }
        }
        return name;
    }
    
    void TogglePause();
    void SetPause (bool p);
    bool IsPaused() const { return isPaused; }
    
    static Application *GameApp;
private:
    State *currentState;
    HashMap<String, State*> states;
    
    bool isPaused;
    bool exitRequested;
};

#endif
