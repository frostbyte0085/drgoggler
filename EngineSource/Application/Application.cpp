#include "Application.h"
#include <Graphics/GraphicsContext/GraphicsContext.h>
#include <Utilities/Exception.h>
#include <Utilities/MacMessageBox.h>
#include <Filesystem/Filesystem.h>
#include <Filesystem/File.h>
#include <Resources/ResourceManager.h>
#include <Resources/ShaderResource.h>
#include <Resources/TextureResource.h>
#include <Resources/MeshResource.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Mesh/Mesh.h>
#include <Graphics/Texture/Texture.h>
#include "State.h"
#include <Mathematics/Math.h>
#include <Scenegraph/Scene.h>
#include <Scenegraph/SceneNode.h>
#include <Audio/AudioSystem/AudioSystem.h>
#include <Input/Input.h>
#include "Timing.h"
#include <Renderers/UIRenderer/UIRenderer.h>
#include <Renderers/MeshRenderer/MeshRenderer.h>
#include <Renderers/ParticleRenderer/ParticleRenderer.h>
#include <Renderers/PostProcessRenderer/PostProcessRenderer.h>
#include <Graphics/Camera/Camera.h>

float Timing::LastFrameTime = 0.0f;
float Timing::CurrentTime = 0.0f;

float IndependentTiming::LastFrameTime = 0.0f;
float IndependentTiming::CurrentTime = 0.0f;
unsigned int IndependentTiming::FramesPerSecond = 0;

Application* Application::GameApp = nullptr;

Application::Application() {
    Application::GameApp = this;
    
    currentState = nullptr;

    isPaused = false;
    exitRequested = false;
}

Application::~Application() {
    if (currentState)
        currentState->Exit();
    
    HashMap<String, State*>::iterator it;
    for (it=states.begin(); it!=states.end(); it++) {
        State *state = it->second;
        if (state)
            delete state;
        state = nullptr;
    }
    states.clear();
    
    Application::GameApp = nullptr;
    
    Log ("Application::~Application: Success");
}

void Application::SetState(const String &name) {
    State* state = states[name];
    
    if (state) {
        if (currentState)
            currentState->Exit();
            
        state->Enter();
        currentState = state;
        Log ("Application::SetState: Success (" + name + ")");
        return;
    }
    Log ("Application::SetState: Failed (" + name + " cannot be found)");
}

void Application::AddState(const String &name, State *state) {
    // see if the state already exists
    State *existingState = states[name];
    
    if (!existingState) {
        states[name] = state;
        Log ("Application::AddState: Success (" + name + ")");
        return;
    }
    Log ("Application::AddState: Failed (" + name + " already exists)");
}

void Application::SetPause (bool p) {
    isPaused = p;
}

void Application::TogglePause() {
    isPaused = !isPaused;
}

int Application::Run() {
    try {
        srand((unsigned int)time(0));
        // initialize filesystem
        Filesystem::Initialize();
        // initialize resource manager
        ResourceManager::Initialize();
        
        // initialize our graphics context
        GraphicsContext::Initialize();
        // read the config file here
        // as simple as reading the resolution. if the file is empty, run in windowed mode using the default wide screen
        // resolution
        File *f = new File ("Data/start.txt", File::ReadFile);
        char buffer[128];
        memset(buffer, 0, sizeof(buffer));
        if (f) {
            f->ReadToEOF(buffer);
            delete f;
        }
		String startContents = buffer;
        if (startContents != "0") {
            Vector<String> startContentsSplit = StringOperations::SplitString (startContents, 'x');
            if (startContentsSplit.size() > 0) {
                GraphicsContext::SetDisplayMode (DisplayMode(StringOperations::FromString<int>(startContentsSplit[0]),
                                            StringOperations::FromString<int>(startContentsSplit[1])), true);

            }
            else
                GraphicsContext::SetFastWindowedMode();
        }
        else
            GraphicsContext::SetFastWindowedMode();

        // initialize our fmod device
        AudioSystem::Initialize();
        
        // initialize the input system
        Input::Initialize();
        
        // initialize the renderer
        MeshRenderer::Initialize();
        
        // initialize the particle renderer
        ParticleRenderer::Initialize();
        
        // initialize the ui renderer
        UIRenderer::Initialize();
        
        // initialize the post processing renderer
        PostProcessRenderer::Initialize();
        
        // initialize the scenegraph
        Scene::Initialize();

        // state setup
        SetupStates();
        
        bool running = true;

        float timeElapsed = 0.0f;
        float currentTime = 0.0f;
        float indCurrentTime = 0.0f;
    
        unsigned int numFrames=0;
        
        while (running) {
            float timePreFrame = (float)glfwGetTime();
            indCurrentTime += timeElapsed;
            
            // if the game is paused we stop updating the Timing variables
            if (!isPaused) {
                currentTime += timeElapsed;
                Timing::CurrentTime = currentTime;
                Timing::LastFrameTime = timeElapsed;
            }
            else {
                Timing::LastFrameTime = 0.0f;
                
            }
            // but carry on with the IndependentTiming ones, we need those.
            IndependentTiming::CurrentTime = indCurrentTime;
            IndependentTiming::LastFrameTime = timeElapsed;
            
            GraphicsContext::Update();
            Input::Update ();
            
            // first, update the current state
            if (currentState) {
                if (!currentState->Update()) {
                    exitRequested = true;
                }
            }
            
            // update the audio
            AudioSystem::Update();
            
            // update the scenegraph
            Scene::Update();
           
            GraphicsContext::Clear();
            
            // transform the mesh sceneNodes into renderables and pass them to the mesh renderer
            MeshRenderer::Render(false);
            
            // transform the particle sceneNodes into renderables and pass them to the particle renderer
            ParticleRenderer::Render();
            
            // pass the ui widgets to the ui renderer
            UIRenderer::Render();
            
            GraphicsContext::SwapBuffers();
            glfwSleep(0.01f);
           
            float timePostFrame = (float)glfwGetTime();
            timeElapsed = timePostFrame - timePreFrame;
            
            // calculate the frames per second and store them in the IndepenedentTiming static class
            numFrames++;
            static float elapsedFps = 0;
            elapsedFps += timeElapsed;
            if (elapsedFps >= 1.0f) {
                IndependentTiming::FramesPerSecond = numFrames;
                elapsedFps = 0.0f;
                numFrames = 0;
            }
            
			running = glfwGetWindowParam(GLFW_OPENED) ? true : false;
            
            if (exitRequested)
                running = false;
        }
    
        // destroy the scenegraph
        Scene::Destroy();
        
        // destroy the post processing renderer
        PostProcessRenderer::Destroy();
        
        // destroy the ui renderer
        UIRenderer::Destroy();
        
        // destroy the renderer
        MeshRenderer::Destroy();
        
        // destroy the input system
        Input::Destroy();
        
        // destroy graphics context
        GraphicsContext::Destroy();

        // destroy resource manager
        ResourceManager::Destroy();
        
		// destroy the fmod device
        AudioSystem::Destroy();

        // destroy filesystem
        Filesystem::Destroy();
    }
    catch (Exception &exc) {
#if PLATFORM == PLATFORM_WINDOWS

#elif PLATFORM == PLATFORM_MACOSX
        
#endif
		std::cout << exc.GetLongDescription() << std::endl;
        return -1;
    }
    
    return 0;
}