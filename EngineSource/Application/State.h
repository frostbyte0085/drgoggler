/*
 Author: 
 Pantelis Lekakis
 
 Description:
 A application's state. This could be a MenuState or a GameState, etc, changeable from Application class.
 
 */
#ifndef STATE_H_DEF
#define STATE_H_DEF

#include <Platform.h>
#include "Timing.h"

class State {
public:
    State();
    virtual ~State();
    
    virtual void Enter()=0;
    virtual void Exit()=0;
    virtual bool Update()=0;
    
};

#endif
