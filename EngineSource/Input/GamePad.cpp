#include "GamePad.h"

GamePadState** GamePad::controllers;

GamePadState* GamePad::getState(int playernum) {
	controllers[playernum]->Update();
	return controllers[playernum];
}

void GamePad::Initialize() {
	controllers = new GamePadState *[MAX_NUM_CONTROLLERS];
	for(int i=0; i<MAX_NUM_CONTROLLERS; i++)
		controllers[i] = new GamePadState(i);
}


/**
 *     IMPLEMENTATION OF GamePadState
 */
	
GamePadState::GamePadState(int playernum) {
	// Get the GLFW playernum :
	int glfw_playernum = 0;
	switch(playernum) {
	case 0: glfw_playernum = GLFW_JOYSTICK_1; break;
	case 1: glfw_playernum = GLFW_JOYSTICK_2; break;
	case 2: glfw_playernum = GLFW_JOYSTICK_3; break;
	case 3: glfw_playernum = GLFW_JOYSTICK_4; break;
	default: break;
	}

	// Find out what the properties of this joystick
	this->glfw_playernum = glfw_playernum;
	this->max_buttons = glfwGetJoystickParam(glfw_playernum, GLFW_BUTTONS);
	this->max_axes = glfwGetJoystickParam(glfw_playernum, GLFW_AXES);
	this->IsConnected = glfwGetJoystickParam(glfw_playernum, GLFW_PRESENT);

	// Nothing is pressed at the start:
	for(int i=0; i<GAMEPAD_MAX_BUTTONS; i++) {
		buttons[i] = GLFW_RELEASE;
	}
	for(int i=0; i<GAMEPAD_MAX_AXES; i++) {
		axes[i] = 0.0f;
	}
}
GamePadState::~GamePadState() {
	//delete buttons;
	//delete axes;
}

bool GamePadState::IsButtonDown(int buttonNum) {
	return buttons[buttonNum] == GLFW_PRESS;
}

bool GamePadState::IsButtonUp(int buttonNum) {
	return buttons[buttonNum] == GLFW_RELEASE;
}

float GamePadState::GetAxis(int axis) {
	return axes[axis];
}

void GamePadState::Update() {
	bool WasConnected = IsConnected;
	this->IsConnected = glfwGetJoystickParam(glfw_playernum, GLFW_PRESENT);
	if(!IsConnected) return;
	if(!WasConnected) {
		this->max_buttons = glfwGetJoystickParam(glfw_playernum, GLFW_BUTTONS);
		this->max_axes = glfwGetJoystickParam(glfw_playernum, GLFW_AXES);
		this->IsConnected = glfwGetJoystickParam(glfw_playernum, GLFW_PRESENT);
	}

	glfwGetJoystickButtons(this->glfw_playernum, this->buttons, this->max_buttons);
	glfwGetJoystickPos(this->glfw_playernum, this->axes, this->max_axes);

	IgnoreDeadzoneValues();
	// Comment this if you never plan on using the fields Thumbsticks and Triggers:
	UpdateX360Interface();
}

#define deadzone(n) ( (n) < -X360_DEADZONE || (n) > X360_DEADZONE) ? (n) : 0.0f; 
void GamePadState::IgnoreDeadzoneValues() {
	this->axes[X360_AXIS_LS_X] = deadzone(this->axes[X360_AXIS_LS_X]);
	this->axes[X360_AXIS_LS_Y] = deadzone(this->axes[X360_AXIS_LS_Y]);
	this->axes[X360_AXIS_RS_X] = deadzone(this->axes[X360_AXIS_RS_X]);
	this->axes[X360_AXIS_RS_Y] = deadzone(this->axes[X360_AXIS_RS_Y]);
	this->axes[X360_AXIS_TRIGGERS] = deadzone(this->axes[X360_AXIS_TRIGGERS]);
}

void GamePadState::UpdateX360Interface() {
	// Ignore values in deadzone
	this->Thumbsticks.Left.x  = this->axes[X360_AXIS_LS_X];
	this->Thumbsticks.Left.y  = this->axes[X360_AXIS_LS_Y];
	this->Thumbsticks.Right.x = this->axes[X360_AXIS_RS_X];
	this->Thumbsticks.Right.y = this->axes[X360_AXIS_RS_Y];

	// @note LT is +ve, RT is -ve
	if(this->axes[X360_AXIS_TRIGGERS] > 0) {
		// Left trigger is pressed.
		this->Triggers.Left = this->axes[X360_AXIS_TRIGGERS];
		this->Triggers.Right = 0.0f;
	}
	else {
		// Right trigger is pressed.
		this->Triggers.Left = 0.0f;
		this->Triggers.Right = -this->axes[X360_AXIS_TRIGGERS];
	}
}