#ifndef INPUT_H_DEF
#define INPUT_H_DEF

#include <Platform.h>
#include <Application/Timing.h>

#include <Mathematics/Math.h>
#include "GamePad.h"

#define MOUSE_AXIS_X 0x10
#define MOUSE_AXIS_Y 0x11

#define AXIS_INVERTED -1
#define AXIS_NORMAL   1

enum AxisDirection {NEGATIVE, POSITIVE, NO_DIRECTION};
AxisDirection getDirectionOfConstant(float k);
float getValueInDirection(float v, AxisDirection dir);

class InputAction
{
public:
    
    InputAction() {
        KeyCode = -1;
        MouseButton = -1;
		MouseAxis = -1;
		JoyButton = -1;
		JoyAxis = -1;
		ConstValue = true;
        ResetAll();
    }
    
    void SetKey (int key, float value) {
        KeyCode = key;
		ConstValue = true; Value = value;
        MouseButton = -1;
		MouseAxis = -1;
		JoyButton = -1;
		JoyAxis = -1;
		Direction = NO_DIRECTION;
		Inverted = 1;
        ResetAll();
    }
    void SetMouseButton (int button, float value) {
        MouseButton = button;
		ConstValue = true; Value = value;
        KeyCode = -1;
		MouseAxis = -1;
		JoyButton = -1;
		JoyAxis = -1;
		Direction = NO_DIRECTION;
		Inverted = 1;
        ResetAll();    
    }
	void SetMouseAxis(int axis, AxisDirection dir, int inverted) {
		MouseAxis = axis;
		ConstValue = false;
        KeyCode = -1;
        MouseButton = -1;
		JoyButton = -1;
		JoyAxis = -1;
		Direction = dir;
		Inverted = inverted;
		ResetAll();   
	}
	void SetJoyButton(int button, float value) {
		JoyButton = button;
		ConstValue = true; Value = value;
		KeyCode = -1;
        MouseButton = -1;
		MouseAxis = -1;
		JoyAxis = -1;
		Direction = NO_DIRECTION;
		Inverted = 1;
		ResetAll();
	}
	void SetJoyAxis(int axis, AxisDirection dir, int inverted) {
		JoyAxis = axis;
		ConstValue = false;
		KeyCode = -1;
        MouseButton = -1;
		MouseAxis = -1;
		JoyButton = -1;
		Direction = dir;
		Inverted = inverted;
		ResetAll();
	}
    
    void ResetAll()
    {
        Pressed = false;
        Clicked = false;
        untouched = true;
        downTimeStamp = 0;
        hasRepeated = false;
    }
    
	bool ConstValue; float Value;
    int KeyCode;
    int MouseButton;
	int MouseAxis;
	int JoyButton;
	int JoyAxis;
	AxisDirection Direction;
	int Inverted;
    
    bool Pressed;
    bool Clicked;
    bool hasRepeated;
    
    bool untouched;
    float downTimeStamp;
};

class Input {
    friend class GraphicsContext;
	friend class Keyboard;
    
public:
    static void Initialize();
    static void Destroy();
    
    static void SetKeyAction (const String &name, int key, float value);
    static void SetMouseButtonAction (const String &name, int button, float value);
	static void SetMouseAxisAction (const String &pos, const String &neg, int axis, int orient);
	static void SetJoyButtonAction (const String &name, int button, float value);
	static void SetJoyAxisAction (const String &pos, const String &neg, int axis, int orient);

    static void RemoveAction (const String &name);
    static void ClearActions();
    
    static bool IsPressed (const String &name);
    static bool IsReleased (const String &name);
    static bool IsClicked (const String &name);
	static float GetValue (const String &name);
    
    static Vector2 GetMousePosition();
    static Vector2 GetRelativeMousePosition();
    static bool IsMouseMoving();
    static void ShowMouse(bool mouseCursorOn);
    
    static InputAction GetState (const String &name);

	// glfw callbacks
    static void keyCallback(int key, int action);
    static void buttonCallback (int button, int action);
    static void mouseCallback (int x, int y);

	static void joystickUpdate();

    static void Update();

private:
    static void remapInputHandlers();
    
    static HashMap<String, InputAction*> keyMappings;

	/* Mouse Handling variables: */
	static bool mouseButtons[2];
    static Vector2 mousePosition, oldMousePosition;
    static Vector2 relativeMousePosition;
    static bool mouseCursorOn, mouseCursorOnChangeFlag;
	static bool isMouseMoving;
    static bool mouseButtonPressed;
	static float mouseSensitivity;
	static InputAction* mouseAxisXp; // Use for optimisation. Avoid sequentially searching the
	static InputAction* mouseAxisXn; // hash table.
	static InputAction* mouseAxisYp;
	static InputAction* mouseAxisYn;

	/* Keyboard Handling variables: */
	static bool keys[GLFW_KEY_LAST];
	static bool keyPressed;

	/* GamePad Handling variables: */
	static GamePadState* gamePadState;
};

#endif
