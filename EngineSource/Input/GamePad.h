/**
 * Triggers are considered an Axis on x360 controllers. RT is -ve and LT is +ve.
 * @author Olivier Legat
 */
#ifndef GAMEPAD_H_DEF
#define GAMEPAD_H_DEF

#include <Platform.h>

#define MAX_NUM_CONTROLLERS 4
#define GAMEPAD_MAX_BUTTONS 20
#define GAMEPAD_MAX_AXES 20

#define X360_AXIS_LS_X     0
#define X360_AXIS_LS_Y     1
#define X360_AXIS_TRIGGERS 2  // LT+, RT-
#define X360_AXIS_RS_Y     3
#define X360_AXIS_RS_X     4

#define X360_BUTTON_A     0
#define X360_BUTTON_B     1
#define X360_BUTTON_X     2
#define X360_BUTTON_Y     3
#define X360_BUTTON_RB    4
#define X360_BUTTON_LB    5
#define X360_BUTTON_BACK  6
#define X360_BUTTON_START 7
#define X360_BUTTON_LS    8
#define X360_BUTTON_RS    9

#define X360_DEADZONE 0.12f

struct GamePadThumbsticks {
	Vector2 Left;
	Vector2 Right;
};

struct GamePadTriggers {
	float Left;
	float Right;
};

class GamePadState {
private:
	int glfw_playernum;

	unsigned char buttons[GAMEPAD_MAX_BUTTONS];
	float axes [GAMEPAD_MAX_AXES];

	int max_axes;
	int max_buttons;

	void IgnoreDeadzoneValues();
	void UpdateX360Interface();

public:
	GamePadState(int playernum);
	~GamePadState();

	// Makes this class call GLFW functions to update the state.
	void Update();

	bool IsButtonDown(int buttonNum);
	bool IsButtonUp(int buttonNum);
	bool IsConnected;

	// Generic axis function:
	float GetAxis(int axis);

	// X360 axis interface:
	GamePadThumbsticks Thumbsticks;
	GamePadTriggers Triggers;
};

class GamePad {
private:
	// array of 4 controllers (or more... )
	static GamePadState** controllers;
public:
	static void Initialize();

	static GamePadState* getState(int playernum);
};

#endif