/**
 * @author Pantelis Lekakis
 * @author Olivier Legat
 */
#include "Input.h"

#define isMouseAxis(action) (action) == mouseAxisXp || (action)==mouseAxisYp \
	|| (action) == mouseAxisXn || (action)==mouseAxisYn

AxisDirection getDirectionOfConstant(float k) {
	if(k < 0.0f) return NEGATIVE;
	else if(k > 0.0f) return POSITIVE;
	else return NO_DIRECTION;
}
float getValueInDirection(float v, AxisDirection dir) {
	switch(dir) {
	case NEGATIVE: if(v > 0.0f) v=0.0f; break;
	case POSITIVE: if(v < 0.0f) v=0.0f; break;
	case NO_DIRECTION:
	default: /* Accept whatever the value is */ break;
	}
	return v;
}

/********************************************************************
 *						Variable Declarations						*
 ********************************************************************/
HashMap<String, InputAction*> Input::keyMappings;

bool Input::keys[GLFW_KEY_LAST] = {false};
bool Input::keyPressed = false;

bool Input::mouseButtons[2] = {false};
bool Input::mouseButtonPressed = false;
bool Input::isMouseMoving = false;
Vector2 Input::mousePosition;
Vector2 Input::oldMousePosition;
Vector2 Input::relativeMousePosition;
InputAction* Input::mouseAxisXp = new InputAction();
InputAction* Input::mouseAxisXn = new InputAction();
InputAction* Input::mouseAxisYp = new InputAction();
InputAction* Input::mouseAxisYn = new InputAction();
float Input::mouseSensitivity = 20.0f;
bool Input::mouseCursorOn = true;
bool Input::mouseCursorOnChangeFlag = false;

GamePadState* Input::gamePadState;

/********************************************************************
 *					INITIALIZATION and DESTRUCTION					*
 ********************************************************************/
void Input::Initialize() {
	Log ("Input::Initialize: Success");
    
    remapInputHandlers();
    
    mousePosition = oldMousePosition = Vector2(0,0);
    relativeMousePosition = Vector2(0,0);

	mouseAxisXp->SetMouseAxis(MOUSE_AXIS_X, POSITIVE, AXIS_NORMAL);
	mouseAxisXn->SetMouseAxis(MOUSE_AXIS_X, NEGATIVE, AXIS_NORMAL);
	mouseAxisYp->SetMouseAxis(MOUSE_AXIS_Y, POSITIVE, AXIS_NORMAL);
	mouseAxisYn->SetMouseAxis(MOUSE_AXIS_Y, NEGATIVE, AXIS_NORMAL);
    
	GamePad::Initialize();
	gamePadState = GamePad::getState(0);

    //EnableInput();
}
void Input::Destroy() {
    
    HashMap<String, InputAction*>::iterator it;
	for (it=keyMappings.begin(); it!=keyMappings.end(); it++)
	{
		if (it->second != nullptr)
			delete it->second;
		it->second = nullptr;
	}
    
    glfwEnable( GLFW_MOUSE_CURSOR );
	
    keyMappings.clear();
    Log ("Input::Destroy: Success");
}

/********************************************************************
 *					Hashtable modification methods					*
 ********************************************************************/
void Input::SetKeyAction(const String &name, int key, float value) {
    InputAction *action = new InputAction();
    action->SetKey(key, value);
    keyMappings[name] = action;
}
void Input::SetMouseButtonAction(const String &name, int button, float value) {
    InputAction *action = new InputAction();
    action->SetMouseButton(button, value);
    keyMappings[name] = action;
}
void Input::SetMouseAxisAction (const String &pos, const String &neg, int axis, int orient) {
	if(axis==MOUSE_AXIS_X) {
		keyMappings[pos] = mouseAxisXp;
		keyMappings[neg] = mouseAxisXn;
		mouseAxisXp->Inverted = orient;
		mouseAxisXn->Inverted = orient;
	}
	else if (axis==MOUSE_AXIS_Y) {
		keyMappings[pos] = mouseAxisYp;
		keyMappings[neg] = mouseAxisYn;
		mouseAxisYp->Inverted = orient;
		mouseAxisYn->Inverted = orient;
	}
}
void Input::SetJoyButtonAction (const String &name, int button, float value) {
	InputAction *action = new InputAction();
	action->SetJoyButton(button, value);
	keyMappings[name] = action;
}
void Input::SetJoyAxisAction (const String &pos, const String &neg, int axis, int orient) {
	InputAction *action = new InputAction();
    action->SetJoyAxis(axis, POSITIVE, orient);
    keyMappings[pos] = action;

	action = new InputAction();
    action->SetJoyAxis(axis, NEGATIVE, orient);
    keyMappings[neg] = action;
}

void Input::RemoveAction (const String &name) {
    HashMap<String, InputAction*>::iterator it = keyMappings.find(name);
	if (it != keyMappings.end())
	{
        InputAction *action = it->second;
        assert (action);
        
		keyMappings.erase(it);

		if(isMouseAxis(action)) return;
		delete action;
        action = nullptr;
	}
}

void Input::ClearActions() {//mem leak here:
	//Input::keyMappings.clear();
	HashMap<String, InputAction*>::iterator it=keyMappings.begin();
	while (it!=keyMappings.end()) {
		InputAction *action = it->second;
        assert (action);
        
		keyMappings.erase(it);

		if(isMouseAxis(action)) return;
		delete action;
        action = nullptr;
		it=keyMappings.begin();
    }
}

/********************************************************************
 *						  Input state methods						*
 ********************************************************************/
bool Input::IsPressed(const String &name)
{
	HashMap<String, InputAction*>::iterator it = keyMappings.find(name);
	if (it != keyMappings.end())
	{
        InputAction *action = it->second;
        assert(action);
        
        return action->Pressed;
	}
	return false;
}
bool Input::IsReleased(const String &name)
{
	return !Input::IsPressed(name);
}
bool Input::IsClicked (const String &name)
{
	HashMap<String, InputAction*>::const_iterator it = keyMappings.find (name);
	if (it != keyMappings.end())
	{
        InputAction *action = it->second;
        assert(action);
        
        return action->Clicked;
	}
	return false;
}

float Input::GetValue (const String &name) {
	HashMap<String, InputAction*>::const_iterator it = keyMappings.find (name);
	if (it != keyMappings.end())
	{
        InputAction *action = it->second;
        assert(action);
        
		// Get value taking the direction into consideration:
		float val = getValueInDirection(action->Value, action->Direction);
		val *= action->Inverted;

		/* Actions with a direction update InputAction::Value dynamically.
		 * But actions with no directions have val as a constant.
		 * If a directionless action is an pressed it should return 0.
		 */
		if(action->Direction==NO_DIRECTION && !action->Pressed) // is an unpress directionless action?
			return 0.0f;
		else
			return val;
	}
	return 0.0f;
}
    
Vector2 Input::GetRelativeMousePosition() {
    return relativeMousePosition;
}
Vector2 Input::GetMousePosition() {
    return mousePosition;
}
bool Input::IsMouseMoving() {
	return isMouseMoving;
}
void Input::ShowMouse(bool mouseCursorOn) {
    mouseCursorOnChangeFlag = (Input::mouseCursorOn != mouseCursorOn);
    Input::mouseCursorOn = mouseCursorOn;
    if(mouseCursorOnChangeFlag) {
        if(mouseCursorOn) glfwEnable(GLFW_MOUSE_CURSOR);
        else              glfwDisable(GLFW_MOUSE_CURSOR);
    }
}

InputAction Input::GetState(const String &name)
{
	InputAction retState;
	HashMap<String, InputAction*>::iterator it = keyMappings.find (name);
	if (it != keyMappings.end())
	{
        InputAction *action = it->second;
        assert (action);
        
		
		retState = *action;
	}
	return retState;
}

/********************************************************************
 *							GLFW Callbacks							*
 ********************************************************************/
void Input::mouseCallback (int x, int y) {
	float dx = x - mousePosition.x;
	float dy = y - mousePosition.y;

    mousePosition.x = (float)x;
    mousePosition.y = (float)y;

	// TODO: clicked, pressed, release for mouse axis
    if(mouseCursorOnChangeFlag) {
        mouseCursorOnChangeFlag = false;
    }
    else {
        dx /= mouseSensitivity;
        dy /= mouseSensitivity;
        mouseAxisXp->Value = dx;
        mouseAxisXn->Value = dx;
        mouseAxisYp->Value = dy;
        mouseAxisYn->Value = dy;
    }
}

void Input::keyCallback(int key, int action) {
    keys[key] = (action == GLFW_PRESS);
    
    HashMap<String, InputAction*>::iterator it;
    
    for (it=keyMappings.begin(); it!=keyMappings.end(); it++) {
        InputAction *action = it->second;
        assert (action);
        
        // it's a key
        if (action->KeyCode == key) {
            if (keys[key]) {
                action->Clicked = false;
                action->Pressed = true;
                action->downTimeStamp = Timing::CurrentTime;
                action->hasRepeated = false;
            }
            else {
             action->Clicked = true;
             action->Pressed = false;
             }
        }
    }
}

void Input::buttonCallback(int button, int action) {
    mouseButtons[button] = (action == GLFW_PRESS);

    HashMap<String, InputAction*>::iterator it;
    for (it=keyMappings.begin(); it!=keyMappings.end(); it++) {
        InputAction *action = it->second;
        assert (action);

        if (action->MouseButton == button) {
            if (mouseButtons[button]) {
                action->Clicked = false;
                action->Pressed = true;
                action->downTimeStamp = Timing::CurrentTime;
                action->hasRepeated = false;
            }
            else {
                action->Clicked = true;							
                action->Pressed = false;
            }
        }
    }
}

// Not really a GLFW callback.. but it does the equivalent:
void Input::joystickUpdate() {
	gamePadState = GamePad::getState(0);

	// Iterate on all the elements of the hashtable
    HashMap<String, InputAction*>::iterator it;
    for (it=keyMappings.begin(); it!=keyMappings.end(); it++) 
	{
        InputAction *action = it->second;
        assert (action);
		int button, axis;

		// Is this element a Joystick button?
        if ( (button = action->JoyButton) != -1) 
		{
			bool wasPressed = action->Pressed;
			action->Pressed = gamePadState->IsButtonDown(button);
			
			// if it use to be pressed and is now released, then it's clicked:
			if(wasPressed && !action->Pressed) 
				action->Clicked = true;
			
			// if it use to be released and now it's pressed, update time stamp:
			if(!wasPressed && action->Pressed)
				action->downTimeStamp = Timing::CurrentTime;
		}

		// Is this element a Joystick axis?
		else if ( (axis = action->JoyAxis) != -1) 
		{
			bool wasPressed = action->Pressed;
			float val = gamePadState->GetAxis(axis);
			action->Pressed  = val!=0;
			action->Value = getValueInDirection(val, action->Direction);

			// if it use to be pressed and is now released, then it's clicked:
			if(wasPressed && !action->Pressed) 
				action->Clicked = true;
			
			// if it use to be released and now it's pressed, update time stamp:
			if(!wasPressed && action->Pressed)
				action->downTimeStamp = Timing::CurrentTime;
		}
    }
}

/********************************************************************
 *							Mischievious							*
 ********************************************************************/
void Input::Update() {
    
	// Refresh mouse
	if(oldMousePosition.x == mousePosition.x) { // GLFW doesn't callback if the mouse stays in place.
		mouseAxisXp->Value = 0.0f;              // So we need to set the "axis" to 0 when the mouse settles.
		mouseAxisXn->Value = 0.0f;
	}
	if(oldMousePosition.y == mousePosition.y) {
		mouseAxisYp->Value = 0.0f;
		mouseAxisYn->Value = 0.0f;
	}
    oldMousePosition = mousePosition;
    
	joystickUpdate();

    // Reset clicked keys
    HashMap<String, InputAction*>::iterator it;
    for (it=keyMappings.begin(); it!=keyMappings.end(); it++) { 
        InputAction *action = it->second;

        // Is the key clicked?
        if(action->Clicked) {
            // Has it been touch in a frame?
            if(!action->untouched) {
                action->Clicked = false;
                action->untouched = true;
            }
            else {
                // The clicked flag hasn't been used yet. Reset it on the next iteration
                action->untouched = false;
            }
        } 
    }
}

// this must be called on display mode change, as glfw is re-init there.
void Input::remapInputHandlers() {
    glfwEnable(GLFW_MOUSE_CURSOR);
    
    glfwSetKeyCallback(Input::keyCallback);
    glfwSetMouseButtonCallback(Input::buttonCallback);
    glfwSetMousePosCallback(Input::mouseCallback);
}
