#include "Physics.h"
#include <math.h>
#include <Scenegraph/SceneNode.h>

#define isFloatNegligable(f) ( (f) < NEGLIGABLE && (f) > -NEGLIGABLE )
#define neglectFloat(f) (if(IsFloatNegligable( (f) ))  (f) = 0.0f )

float PhysicalState::AirDensity = DEFAULT_AIR_DENSITY;
float PhysicalState::GravityExaggeration = 1.0f;

/********************************************************************
 *					Constructors and Destructors					*
 ********************************************************************/
PhysicalState::PhysicalState(SceneNode* node, float mass) {
	this->node = node;
    this->F.a = Vector3(0);
	this->F.m = mass;
    this->Cd.Xpos = 1.0f;
    this->Cd.Xneg = 1.0f;
    this->Cd.Ypos = 1.0f;
    this->Cd.Yneg = 1.0f;
    this->Cd.Zpos = 1.0f;
    this->Cd.Zneg = 1.0f;
	state.s = state.v = state.a = Vector3(0,0,0);
	this->effectedByGravity = false;
}

PhysicalState::PhysicalState(SceneNode* node, float volume, float density) {
	this->node = node;
	this->F.m = volume * density;
    this->Cd.Xpos = 1.0f;
    this->Cd.Xneg = 1.0f;
    this->Cd.Ypos = 1.0f;
    this->Cd.Yneg = 1.0f;
    this->Cd.Zpos = 1.0f;
    this->Cd.Zneg = 1.0f;
	state.s = state.v = state.a = Vector3(0,0,0);
	this->effectedByGravity = false;
}

PhysicalState::~PhysicalState() {
}

/********************************************************************
 *						 Set and Get methods						*
 ********************************************************************/
void PhysicalState::SetAtRest() {
	state.v = state.a = state.s = F.a = Vector3(0);
}

bool PhysicalState::IsSubjectToGravity() {return effectedByGravity;}
bool PhysicalState::SetSubjectToGravity(bool effected) {
	bool old = effectedByGravity;
	effectedByGravity = effected;
	return old;
}

float PhysicalState::GetMass() {return F.m;}
float PhysicalState::SetMass(float newMass) {
	float oldMass = F.m;
	F.m = newMass;
	return oldMass;
}

Vector3 PhysicalState::GetPosition() {return node->GetGlobalTranslation();}
Vector3 PhysicalState::SetPosition(Vector3 newPosition) {
	Vector3 oldPosition = node->GetGlobalTranslation();
	node->SetGlobalTranslation(newPosition);
	return oldPosition;
}

Vector3 PhysicalState::GetVelocity() {return state.v;}
Vector3 PhysicalState::SetVelocity(Vector3 newVelocity) {
	Vector3 oldVelocity = state.v;
	state.v = newVelocity;
	return oldVelocity;
}

float PhysicalState::GetSpeed() {
	return sqrtf(state.v.x*state.v.x  +  state.v.y*state.v.y  +  state.v.z*state.v.z);
}

Vector3 PhysicalState::GetAcceleration() {return state.a;}
Vector3 PhysicalState::SetAcceleration(Vector3 acceleration) {
	Vector3 oldAcc = state.a;
	state.a = acceleration;
	return oldAcc;
}

void PhysicalState::SetDragCoof(float Cd) {
    this->Cd.Xpos = Cd;
    this->Cd.Xneg = Cd;
    this->Cd.Ypos = Cd;
    this->Cd.Yneg = Cd;
    this->Cd.Zpos = Cd;
    this->Cd.Zneg = Cd;
}
void  PhysicalState::SetDragCoof(DragCoofs coofs) {
    this->Cd = coofs;
}
void  PhysicalState::SetDragCoof(float Cd, DragAxis ax) {
    switch(ax) {
        case DRAG_AXIS_X: this->Cd.Xpos = Cd; this->Cd.Xneg = Cd; break;
        case DRAG_AXIS_X_POS: this->Cd.Xpos = Cd; break;
        case DRAG_AXIS_X_NEG: this->Cd.Xneg = Cd; break;
            
        case DRAG_AXIS_Y: this->Cd.Ypos = Cd; this->Cd.Yneg = Cd; break;
        case DRAG_AXIS_Y_POS: this->Cd.Ypos = Cd; break;
        case DRAG_AXIS_Y_NEG: this->Cd.Yneg = Cd; break;
            
        case DRAG_AXIS_Z: this->Cd.Zpos = Cd; this->Cd.Zneg = Cd; break;
        case DRAG_AXIS_Z_POS: this->Cd.Zpos = Cd; break;
        case DRAG_AXIS_Z_NEG: this->Cd.Zneg = Cd; break;
            
        default: break;
    }
}

float PhysicalState::GetDragCoof(DragAxis ax) {
    switch(ax) {
        case DRAG_AXIS_X: return (Cd.Xpos + Cd.Xneg) / 2.0f;
        case DRAG_AXIS_X_POS: return Cd.Xpos;
        case DRAG_AXIS_X_NEG: return Cd.Xneg;
            
        case DRAG_AXIS_Y: return (Cd.Ypos + Cd.Yneg) / 2.0f;
        case DRAG_AXIS_Y_POS: return Cd.Ypos;
        case DRAG_AXIS_Y_NEG: return Cd.Yneg;
            
        case DRAG_AXIS_Z: return (Cd.Zpos + Cd.Zneg) / 2.0f;
        case DRAG_AXIS_Z_POS: return Cd.Zpos;
        case DRAG_AXIS_Z_NEG: return Cd.Zneg;
        default: return 0.0f;
    }
}
DragCoofs PhysicalState::GetDrafCoof() {
    return Cd;
}

/********************************************************************
 *						   Physics Methods							*
 ********************************************************************/
Vector3 PhysicalState::CalcDrag(Vector3 v) {
    Vector3 drag;
    drag.x = v.x * v.x;
    drag.y = v.y * v.y;
    drag.z = v.z * v.z;
    
    if(v.x > 0.0f)
        drag.x *= -Cd.Xpos;
    else
        drag.x *= Cd.Xneg;
    
    if(v.y > 0.0f)
        drag.y *= -Cd.Ypos;
    else
        drag.y *= Cd.Yneg;
    
    if(v.z > 0.0f)
        drag.z *= -Cd.Zpos;
    else
        drag.z *= Cd.Zneg;
    
	return drag * 0.5f * AirDensity;
	state.v = state.v / 1.15f;

	return Vector3(0);
}

Vector3 PhysicalState::ResultantForce(Force f1, Force f2) {
	Vector3 va1 = f1.a * f1.m;
	Vector3 va2 = f2.a * f2.m;

	return va1 + va2;
}

void PhysicalState::SetThrust(Vector3 acceleration) {
    F.a = acceleration;
}

PhysicalStateData* PhysicalState::Update(float dt) {
    
    // Calculate forces
    Vector3 thrust = F.m * F.a;
    Vector3 drag = CalcDrag(state.v);
    Vector3 resultant = thrust + drag;
    
    // Update physical state:
    state.a = resultant / F.m;
    if(effectedByGravity) 
        state.a += Vector3(0, ACC_GRAVITY * GravityExaggeration, 0); // add drag for grav.
    Vector3 ds = (state.v * dt)  +  (state.a) * (dt*dt) / 2.0f;
    state.s += ds;
    state.v += state.a * dt;
    
//#define LOG_PHYSICS
#ifdef LOG_PHYSICS
	printf("s     : %15.2f [%15.2f, %15.2f, %15.2f]\n", glm::length(state.s), state.s.x, state.s.y, state.s.z);
	printf("v     : %15.2f [%15.2f, %15.2f, %15.2f]\n", glm::length(state.v), state.v.x, state.v.y, state.v.z);
	printf("a     : %15.2f [%15.2f, %15.2f, %15.2f]\n", glm::length(state.a), state.a.x, state.a.y, state.a.z);
	printf("thrust: %15.2f [%15.2f, %15.2f, %15.2f]\n", glm::length(thrust), thrust.x, thrust.y, thrust.z);
	printf("drag  : %15.2f [%15.2f, %15.2f, %15.2f]\n", glm::length(drag), drag.x, drag.y, drag.z);
	printf("\n");
#endif
    
	node->Translate(
		ds.x,
		ds.y,
		ds.z, 
		SPACE_LOCAL);

	return &state;
}

/********************************************************************
 *						   Rotation Methods							*
 ********************************************************************/
void PhysicalState::RotateSceneNode(float x, float y, float z, Space space) {
    node->Rotate(x, y, x, space);
    RotateForce(-x, -y, -z);
}
Vector3 PhysicalState::RotateVector3(Vector3 v, float x, float y, float z) {
    Vector3 vr = v;
    vr = glm::rotate(vr, x, Vector3(1,0,0));
    vr = glm::rotate(vr, y, Vector3(0,1,0));
    vr = glm::rotate(vr, z, Vector3(0,0,1));
    return vr;
}
void PhysicalState::RotateForce(float x, float y, float z) {
    /*Frot = glm::rotate(Frot, x, Vector3(1,0,0));
    Frot = glm::rotate(Frot, y, Vector3(0,1,0));
    Frot = glm::rotate(Frot, z, Vector3(0,0,1));*/
    
    state.s = RotateVector3(state.s, x, y, z);
    state.v = RotateVector3(state.v, x, y, z);
    state.a = RotateVector3(state.a, x, y, z);
}
void PhysicalState::RotateSceneNodeAndForce(float x, float y, float z, Space space) {
    node->Rotate(x, y, z, space);
}