#ifndef PHYSICS_H_DEF
#define PHYSICS_H_DEF

#include <Platform.h>
#include <Scenegraph/SceneNode.h>

#define NEGLIGABLE  0.001f    // Values below this are set to 0.0f on Update.

#define ACC_GRAVITY -9.80665f // Acceleration due to gravity  [m/s^2]
#define ACC_DRAG    -10.0f    // Deacceleration due to air resistance [m/s^2]

#define DEFAULT_AIR_DENSITY 0.01f  // Increase this to increase air resistance

#define DRAG_COOF_SPHERE         0.47f
#define DRAG_COOF_HALF_SPHERE    0.42f
#define DRAG_COOF_CONE           0.50f
#define DRAG_COOF_CUDE           1.05f
#define DRAG_COOF_ANGLED_CUDE    0.80f
#define DRAG_COOF_LONG_CYLINDER  0.82f
#define DRAG_COOF_SHORT_CYLINDER 1.15f

/**
 * a   - Acceleration            [m/s^2]
 * A   - Reference surface area  [m^2]
 * Cd  - Drag cooefficent        []
 * Fd  - Force of the drag       [N = kg*m/s^2]
 * m   - Mass                    [kg]
 * rho - Density of air          [kg/m^3]
 * s   - Displacement            [m]
 * v   - Velocity of the node    [m/s]


 * Fd = 0.5 * rho * (v*v) * Cd * A
 */

class SceneNode;

struct Force {
	Vector3 a; // acceleration
	float m;   // mass
};

struct PhysicalStateData {
	Vector3 s; // Displacement
	Vector3 v; // Velocity
	Vector3 a; // Acceleration
};

enum DragAxis {
    DRAG_AXIS_X,
    DRAG_AXIS_X_POS,
    DRAG_AXIS_X_NEG,
    DRAG_AXIS_Y,
    DRAG_AXIS_Y_POS,
    DRAG_AXIS_Y_NEG,
    DRAG_AXIS_Z,
    DRAG_AXIS_Z_POS,
    DRAG_AXIS_Z_NEG
};
struct DragCoofs {
    float Xpos, Xneg, Ypos, Yneg, Zpos, Zneg;
};

class PhysicalState {
public:
	PhysicalState(SceneNode * node, float mass);
	PhysicalState(SceneNode * node, float volume, float density);
	~PhysicalState();

	float GetMass();              // get current mass
	float SetMass(float newMass); // set new mass. returns old mass.

    void SetThrust(Vector3 acceleration); // apply a force using the object's mass.
	static Vector3 ResultantForce(Force f1, Force f2);

	bool IsSubjectToGravity();
	bool SetSubjectToGravity(bool effected);

    // Set the drag coof (the same on all axes)
    void  SetDragCoof(float Cd);
    void  SetDragCoof(DragCoofs coofs);
    void  SetDragCoof(float Cd, DragAxis ax);
    
    float GetDragCoof(DragAxis ax);
    DragCoofs GetDrafCoof();
    
	Vector3 GetPosition();
	Vector3 GetVelocity();
	Vector3 GetAcceleration();
	float GetSpeed(); // = sqrt(v.x^2 + x.y^2 + x.z^2)

	Vector3 SetPosition(Vector3 pos);
	Vector3 SetVelocity(Vector3 vel);
	Vector3 SetAcceleration(Vector3 acc);
    
    //void RotateSceneNode(Quaternion q);
    //void RotateForce(Quaternion q);
    //void RotateSceneNodeAndForce(Quaternion q);
    
    void RotateSceneNode(float x, float y, float z, Space space);
    void RotateForce(float x, float y, float z);
    void RotateSceneNodeAndForce(float x, float y, float z, Space space);
    
	PhysicalStateData* Update(float dtime);
	void SetAtRest();

	static float AirDensity;
    static float GravityExaggeration;

	void GetAtRest();

private:
	SceneNode* node;
    bool effectedByGravity;
    
	Force F;         // thrust applied on the object.
    DragCoofs Cd;        // "Pernetration power"
    
	PhysicalStateData state;

	Vector3 CalcDrag(Vector3 v);
    
    Vector3 RotateVector3(Vector3 v, float x, float y, float z);
};

#endif