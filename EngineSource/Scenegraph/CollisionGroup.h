/*
 Author: 
 Pantelis Lekakis
 
 Description:
 CollisionGroups is an idea that PHYSX uses to distinguish between which objects they can collide by placing them
 in certain groups.
 
 The reason that we are using 32 by 32 arrays is to enable bitmask operations especially for raycasting. An example would
 be to test using Raycast against scene nodes in collision groups CG_OBJECT1 & CG_OBJECT2, ignoring all the rest.
 This is not implemented and we are just using collision groups to see which nodes can collide with others in a grouped
 manner. This also means that performance is increased because we dramatically decrease the number of intersection tests
 between nodes as not all nodes collide with all nodes anymore.
 */
#ifndef COLLISION_GROUP_H_DEF
#define COLLISION_GROUP_H_DEF

#include <Platform.h>

typedef unsigned int CollisionGroup;

class CollisionCombinations {
public:
    CollisionCombinations() { 
        for (CollisionGroup i=0; i<32; i++) {
            for (CollisionGroup j=0; j<32; j++) {
                combinations[i][j] = false;
            }
        }
    }
    
    ~CollisionCombinations() {}
    
    bool CanCollide (CollisionGroup g1, CollisionGroup g2) { return combinations[g1][g2]; }
    void SetCollide (CollisionGroup g1, CollisionGroup g2, bool collide) { combinations[g1][g2] = combinations[g2][g1] = collide; }
    
private:
    bool combinations[32][32];
};

#endif
