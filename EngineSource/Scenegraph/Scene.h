/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The Scene is a the heart of the engine's scenegraph, handling scene node behaviour updates, collision detection and response
 as well atmospheric effects such as lighting and blob shadows for platforms. The main concepts are based on Unity3D's
 scenegraph, more importantly the scene node representation and scene node behaviours are a key element in the scene graph.
 
 Querring the scene for nodes is very easy and fast due to the stack based node and query system, able to return a list of 
 specific nodes, such as nodes with meshes, or nodes with colliders, or even nodes by name or tag.
 
 Collision detection happens between scene nodes with Collider objects attached to them, and only between scene nodes
 with enabled collisions through the CollisionGroup notion, an idea implemented in NVIDIA PHYSX. By default all
 CollisionGroups are disabled, so no collisions occur.
 
 Operations such as WorldToScreen and ScreenToWorldRay are also available to transform a vector from screen space to world 
 space and vice versa.
 
 A scene can also have one skybox and one active camera assigned to it.
 */
#ifndef SCENE_H_DEF
#define SCENE_H_DEF

#include <Platform.h>

#include <Mathematics/Plane.h>

#include "CollisionGroup.h"

class Ray;
class SceneNode;
class QueryResult;
class Material;
class SceneNodeBehaviour;
class Collider;
class UILayer;
class ParticleSystem;

struct RayHitInfo {
    Collider *collider;
    SceneNode *node;
    float distance;
    Vector3 point;
    Vector3 normal;
    Vector2 textureCoord;
};

// use this to sort the nodes before raycasting
class SceneNodeSortDistance {
public:
    SceneNodeSortDistance(const Vector3 &origin) { this->origin = origin; }
    
    bool operator() (SceneNode *a, SceneNode *b);
    
private:
    Vector3 origin;
};


struct SceneEnvironment {
    // fog
    
    // blob shadow
    bool blobShadowEnable;
    Vector3 blobShadowCenter;
    float blobShadowRadius;
    Vector4 blobShadowColor;
    float blobShadowDistance;
    float blobShadowMaxDistance;
    
    // world light
    Vector3 worldLightDirection;
    Vector4 worldLightDiffuse;
    Vector4 worldLightAmbient;
};



class Scene {
    friend class MeshRenderer;
    friend class ParticleRenderer;
    friend class SceneNode;
    
public:
    static void Initialize();
    static void Destroy();
    
    static void Clear();
    static int AddSceneNode (const String &name, int parent=0);
    static void DestroySceneNode (int id, bool recursive=true);
    
    static void Update();
    
    static void SetActiveCamera (int id);
    static SceneNode* GetActiveCamera() {return activeCameraNode;}
    
    static void SetSkyBox (Material *skyboxMaterial);
    
    static void SetUILayer (UILayer *ui);
    static UILayer* GetUILayer() { return ui; }
    
    // scene queries
    static SceneNode* GetSceneNode (int id);
    static SceneNode* GetSceneNodeWithName (const String &name);
    static SceneNode* GetSceneNodeWithCollider (Collider *collider);
    static SceneNode* GetSceneNodeWithTag (const String &tag);
    static SceneNode* GetSceneNodeWithParticleSystem (ParticleSystem *particleSystem);
    
    static const QueryResult& GetSceneNodesWithName (const String &name);
    static const QueryResult& GetSceneNodesWithTag (const String &tag);
    static const QueryResult& GetSceneNodesWithCollider (Collider *collider);
    static const QueryResult& GetSceneNodesWithParticleSystem (ParticleSystem *particles);
    
    static const QueryResult& GetSceneNodesWithMeshes ();
    static const QueryResult& GetSceneNodesWithColliders();
    static const QueryResult& GetSceneNodesWithParticleSystems();
    
    static void FreeQueryResult(const QueryResult &result);
    
    // ray and project/unproject operations
    static Ray ScreenToWorldRay (const Vector2 &point);
    static Vector2 WorldToScreenPoint (const Vector3 &worldPoint);
    static bool Raycast (const Vector3 &origin, const Vector3 &direction, float maxDistance, RayHitInfo &hitInfo, int ignoreNode);
    
    // collision groups handling
    static void DisableCollisions (unsigned int g1, unsigned int g2);
    static void EnableCollisions (unsigned int g1, unsigned int g2);
    static bool IsCollisionEnabled (unsigned int g1, unsigned int g2);
    
    // lighting and shadows
    static void SetWorldLight (const Vector3 &direction, const Vector4 ambient, const Vector4 &diffuse);
    static void SetBlobShadow (const Vector3 &center, float radius, const Vector4 &color, float distance, float maxDistance);
    static void EnableBlobShadow () { env.blobShadowEnable = true; }
    static void DisableBlobShadow() { env.blobShadowEnable = false; }
    
    static const Vector<Plane>& GetCurrentFrustum() { return frustum; }

private:
    static SceneEnvironment env;
    
    static Vector<Plane> frustum;
    
    static void updateMeshAnimations();
    static void updateParticleSystems();
    static void updateBehaviours();
    static void createMatrices();
    static void destroyNodes();
    
    static void handleCollisionsAndTriggers();
    static void setupColliderObjects();
    static void updateCollisionAndTriggerCalls();
    
    static int addSceneQueryResult();
    
	typedef HashMap<String, Set<int> > NodeNameBindings_;
    static NodeNameBindings_ nodeNameBindings;

    typedef HashMap<String, Set<int> > NodeTagBindings_;
    static NodeTagBindings_ nodeTagBindings;
    
    static Vector<QueryResult> queryResults;
    static Stack<int> availableQuerySlots;
    
    static Vector<SceneNode*> nodes;
    static Stack<int> availableSlots;
    
    static void addToTagBindingMap(SceneNode* node);
    static void addToNameBindingMap(SceneNode* node, const String &name);
    static int getQueryAvailableSlot();
    static int getAvailableSlot();
    
    static CollisionCombinations collisionCombs;
    // special scene nodes
    static SceneNode *activeCameraNode;
    static SceneNode *skyBoxNode;
    
    // UI layer - it's a container for ui widgets
    static UILayer *ui;
};

#endif
