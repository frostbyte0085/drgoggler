/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The SceneNode represents a node with a parent and several children. The implementation is simple and effective:
 
 Each SceneNode cares only about itself and its parent. It was important to be able to tweak both local and global
 transformations so whenever something is changed, the global translation, rotation and scaling are calculated from
 the parent node (if any). This is a safe way to ensure that transformations are synced between the local and global
 transformations based on the node parent.
 
 A node's rotation and scaling can also be isolated from the parent's, so the parent transformation does not affect the
 node's. A fine example would be a satellite rotating around the earth in a geostatic orbit. The earth rotates, but the
 satellite doesn't, it only follows the earth's rotation by rotating around the earth node's center point.
 
 Instead of having specialised nodes for mesh, particle system, audio, etc like many implementations do, the engine
 uses a flat scene node containing elements such as Mesh, ParticleSystem, Collider, AudioSources, etc. That way we can
 combine Meshes with Particles without creating new nodes.
 
 A SceneNode can also have a number of SceneNodeBehaviour instances which are linked to game logic and collision response.
 Whenever a behaviour is attached, its Start method is called, a good place to load resources if needed.
 
 Please note that the model matrix is the final global matrix, including transformations from the root -> this node
 including its parents, so the Scene class does not need to iterate recursively through the nodes, thus reducing
 time complexity to O(1).
 
 */
#ifndef SCENE_NODE_H_DEF
#define SCENE_NODE_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>
#include "CollisionGroup.h"

#include <Mathematics/Sphere.h>

class Mesh;
class Camera;
class Collider;
class SceneNodeBehaviour;
class Box;
class AudioSource;
class ParticleSystem;

/*

*/

enum Space {
    SPACE_LOCAL = 0,
    SPACE_PARENT = 1,
    SPACE_GLOBAL = 2
};

class SceneNode {
    friend class Scene;
public:
    int GetID() const { return id; }
    const String& GetName() const { return name; }
    bool HasTag() { return tag != nullptr; }
    Pair<String, int>* GetTag() const { return tag; }
    void SetTag (const String &tag, int value, bool recursively=true);
    
    bool HasParent() const { return parent >= 0; }
    int GetParent() const { return parent; }
    void SetParent (int p);
    
    int GetChildCount() const { return (int)children.size(); }
    int GetChild(int id) const;
    
    void IgnoreCollisionsWith (SceneNode *other) { assert (other); this->ignoreCollisionNode = other; }
    void ClearIgnoreCollisionsWith() { ignoreCollisionNode = nullptr; }
    
    CollisionGroup GetCollisionGroup() const { return collisionGroup; }
    void SetCollisionGroup(CollisionGroup group, bool recursively=true);
    
    void AddBehaviour (const String &name, SceneNodeBehaviour *behaviour);
    SceneNodeBehaviour* GetBehaviour (const String &name);
    void RemoveBehaviour (const String &name);
    
    bool IsDestroying() const { return isDestroying; }
    
    // sceneNode components
    Camera *camera;
    
    Sphere* GetBoundingSphere() const { return boundingSphere; }
    bool IsFrustumCulled() const { return isFrustumCulled; }
    
    void SetMesh (Mesh *mesh, int collider=-1);
    Mesh* GetMesh() const { return mesh; }
    
    void SetParticleSystem (ParticleSystem *particles);
    ParticleSystem* GetParticleSystem() const { return particleSystem; }
    
    void SetCollider (Collider *collider);
    Collider* GetCollider() const { return collider; }
    
    HashMap<String, AudioSource*> audioSources;
    
    // transformations
    
    // translation
    void SetTranslation (const Vector3 &p);
    void SetTranslation (float x, float y, float z);
    
    void SetGlobalTranslation (const Vector3 &p);
    void SetGlobalTranslation (float x, float y, float z);
    
    void Translate (const Vector3 &p, Space relativeTo);
    void Translate (float x, float y, float z, Space relativeTo);
    
    const Vector3& GetTranslation();
    const Vector3& GetGlobalTranslation();
    
    // scaling
    void SetScale (const Vector3 &s);
    void SetScale (float x, float y, float z);
    
    void Scale (const Vector3 &s);
    void Scale (float x, float y, float z);
    
    bool ScaleFromParent;
    const Vector3& GetScale();
    const Vector3& GetGlobalScale();
    
    // rotation    
    void SetRotation (const Vector3 &r);
    void SetRotation (float x, float y, float z);
    void SetRotation (const Quaternion &q);
    void SetRotation (float w, float x, float y, float z);
    
    void Rotate (const Vector3 &r, Space relativeTo);
    void Rotate (float x, float y, float z, Space relativeTo);
    
    void RotateAroundPoint (const Vector3 &point, const Vector3 &pivot, const Quaternion &angle, Space relativeTo);
    
    bool RotateFromParent;
    const Quaternion& GetRotation();
    
    void SetGlobalRotation (const Vector3 &r);
    void SetGlobalRotation (float x, float y, float z);
    void SetGlobalRotation (const Quaternion &q);
    void SetGlobalRotation (float w, float x, float y, float z);
    
    const Quaternion& GetGlobalRotation();
    
    void LookAt(const Vector3 &target);
    
    // global matrix
    Matrix44& GetMatrix() { return modelMatrix; }
    
    Vector3 TransformPoint(const Vector3 &localPoint) { return localToWorldPosition (localPoint); }
    Vector3 InverseTransformPoint (const Vector3 &worldPoint) { return worldToLocalPosition (worldPoint); }
    
    
private:
    SceneNode(int id);
    ~SceneNode();
    
    SceneNode *ignoreCollisionNode;
    
    bool isFrustumCulled;
    
	void needUpdate(int node);

	bool needsUpdate;

    CollisionGroup collisionGroup;
    
    bool isDestroying;
    
    HashMap<String, SceneNodeBehaviour*> behaviours;
    
    // components
    Mesh *mesh;
    ParticleSystem *particleSystem;
    
    // collision and culling
    Collider *collider;
    Sphere *boundingSphere;
    Sphere worldBoundingSphere;
    
    void updateFromParent();
    
    Vector3 worldToLocalPosition(const Vector3 &worldPos);
    Vector3 localToWorldPosition(const Vector3 &localPos);
    Quaternion worldToLocalOrientation(const Quaternion &worldRotation);
    Quaternion localToWorldOrientation(const Quaternion &localRotation);
    
    int id;
    int parent;
    Vector<int> children;
    
    String name;
    Pair<String, int> *tag;
    
    // transformation variables
    // local
    Vector3 translation;
    Quaternion rotation;
    Vector3 scaling;
    
    // world
    Vector3 globalTranslation;
    Quaternion globalRotation;
    Vector3 globalScaling;

    Matrix44 modelMatrix;
};

#endif
