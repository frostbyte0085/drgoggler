/*
 Author: 
 Pantelis Lekakis
 
 Description:
 SceneNodeBehaviour is a key game logic and collision response element in the engine's scenegraph. SceneNodes that
 have some form of interaction or need to respond to collision detection must attach one or more behaviours.
 
 The most useful feature is the OnCollide and OnTrigger methods. More importantly:
 
 1) OnCollideEnter:  called when the scene node detects a collision, it's only called once.
 2) OnCollideStay:   called when the scene node keeps colliding, it's called until the collision stops happening.
 3) OnCollideExit:   called when the scene node stops colliding, again only called once.
 
 the same applies for Trigger events. The engine does not distinguish between Triggers and Collisions, however the
 game logic might care about it.
 */
#ifndef SCENE_NODE_BEHAVIOUR_H_DEF
#define SCENE_NODE_BEHAVIOUR_H_DEF

#include <Platform.h>

class SceneNode;
class Collider;

class SceneNodeBehaviour {
  
public:
    SceneNodeBehaviour();
    virtual ~SceneNodeBehaviour();
    
    virtual bool Start()=0;
    virtual bool Update()=0;
    
    virtual void OnTriggerEnter(SceneNode *otherNode)=0;
    virtual void OnTriggerExit(SceneNode *otherNode)=0;
    virtual void OnTriggerStay(SceneNode *otherNode)=0;
    
    virtual void OnCollideEnter(SceneNode *otherNode)=0;
    virtual void OnCollideExit(SceneNode *otherNode)=0;
    virtual void OnCollideStay(SceneNode *otherNode)=0;
    
    SceneNode *node;
    
};

#endif
