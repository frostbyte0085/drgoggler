#include "SceneNode.h"
#include "Scene.h"
#include "SceneNodeBehaviour.h"
#include <Mathematics/Box.h>
#include <Mathematics/Sphere.h>
#include <Graphics/Collider/BoxCollider.h>
#include <Graphics/Collider/SphereCollider.h>
#include <Graphics/Mesh/Mesh.h>
#include <Graphics/Particles/ParticleSystem.h>

#include <Audio/AudioSource/AudioSource.h>

int SceneNode::GetChild(int id) const {
    if (id < 0 || id >= (int)children.size())
        return -1;
    
    return children[id];
}

/*
 Change the node's parent.
 1) Preserve the old global translation and rotation.
 2) Remove the current parent (if any)
 3) Add this node to the parents children, effectively making this node a child of it.
 4) Restore the previous global translation and rotation so we do not see hickups in motion.
*/
void SceneNode::SetParent(int p) {
    
    Vector3 previousTranslation = globalTranslation;
    Quaternion previousRotation = globalRotation;
    
    updateFromParent();
    
    // remove the current parent
    if (HasParent()) {
        SceneNode *parentNode = Scene::GetSceneNode(parent);
        if (parentNode) {
            Vector<int>::iterator it;
            for (it=parentNode->children.begin(); it!=parentNode->children.end(); it++ ) {
                if (*it == id) {
                    parentNode->children.erase(it);
                    break;
                }
            }
        }
    }
    
    // add the new parent id
    parent = p;
    
    if (p >= 0) {
        
        SceneNode *newParentNode = Scene::GetSceneNode(p);
        if (newParentNode) {
            newParentNode->children.push_back(id);
        }
    }
    
    SetGlobalRotation(previousRotation);
    SetGlobalTranslation(previousTranslation);
}

SceneNode::SceneNode(int id) {
    this->id = id;
    parent = -1;
    
	needsUpdate = false;

    // transformations
    scaling = globalScaling = Vector3(1,1,1);
    translation = globalTranslation = Vector3(0,0,0);
    rotation = globalRotation = Quaternion(1,0,0,0);

    ScaleFromParent = false;
    RotateFromParent = true;
    
    isDestroying = false;
    
    collisionGroup = 0;
    
    // components
    particleSystem = nullptr;
    mesh = nullptr;
    camera = nullptr;
    collider = nullptr;
    boundingSphere = nullptr;
    tag = nullptr;
    
    ignoreCollisionNode = nullptr;
    isFrustumCulled = false;
}

SceneNode::~SceneNode() {
    HashMap<String, SceneNodeBehaviour*>::iterator it;
    
    for (it=behaviours.begin(); it!=behaviours.end(); it++) {
        SceneNodeBehaviour *behaviour = it->second;
        if (behaviour)
            delete behaviour;
        behaviour = nullptr;
    }
    
    if (tag)
        delete tag;
    tag = nullptr;
    
    if (collider)
        delete collider;
    collider = nullptr;
    
    if (boundingSphere)
        delete boundingSphere;
    boundingSphere = nullptr;
    
    HashMap<String, AudioSource*>::iterator it2;
    for (it2=audioSources.begin(); it2!=audioSources.end(); it2++) {
        AudioSource *source = it2->second;
        if (source)
            delete source;
        source = nullptr;
    }
    audioSources.clear();
    
    behaviours.clear();
}

void SceneNode::needUpdate (int node) {

}

/*
 By giving a sceneNode a tag and a value we can associate a node with a value, useful for missiles and
 lasers where the value would be the damage they cause.
*/
void SceneNode::SetTag (const String &tag, int value, bool recursively) {
    if (this->tag)
        delete this->tag;
    
    this->tag = new Pair<String, int>(tag, value);
    
    Scene::addToTagBindingMap(this);
    
    if (recursively) {
        for (int i=0; i<(int)children.size(); i++) {
            SceneNode *node = Scene::GetSceneNode( children[i] );
            if (!node)
                continue;
            
            node->SetTag (tag, value, true);
        }
    }
}

/*
 Set the collision group for this node and its children (if recursively)
*/
void SceneNode::SetCollisionGroup (CollisionGroup group, bool recursively) {
    collisionGroup = group;
    
    if (recursively) {
        for (int i=0; i<(int)children.size(); i++) {
            SceneNode *node = Scene::GetSceneNode( children[i] );
            if (!node)
                continue;
            
            node->SetCollisionGroup (group, true);
        }
    }
}

/*
 Set the collider. If the node has no collider, it will not take part in any collision detection checks.
*/
void SceneNode::SetCollider(Collider *collider) {
    assert (collider);
    this->collider = collider;
}

/*
 Set the node mesh and generate a collider for it (if not -1).
*/
void SceneNode::SetMesh (Mesh *mesh, int collider) {
    assert(mesh);
    
    this->mesh = mesh;
    
    switch (collider) {
        case CT_SPHERE:
            this->collider = new SphereCollider (mesh);
            break;
            
        case CT_BOX:
            this->collider = new BoxCollider (mesh);
            break;
            
        default:
            break;
    }
    
    // add the frustum bounding box for this mesh
    
    Vector<Vector3> vertices;
    
    for (unsigned int i=0; i<mesh->GetSubmeshCount(); i++) {
        Mesh::Submesh *sm = mesh->GetSubmesh(i);
        assert (sm);
        
        for (int j=0; j<(int)sm->positionBuffer.size(); j++) {
            vertices.push_back(sm->positionBuffer[j]);
        }
    }
        
    boundingSphere = new Sphere (vertices);

    vertices.clear();
}

void SceneNode::SetParticleSystem(ParticleSystem *particles) {
    assert (particles);
    particleSystem = particles;
}

/*
 Add a behaviour to the node and call the Start method
*/
void SceneNode::AddBehaviour (const String &name, SceneNodeBehaviour *behaviour) {
    if (!behaviour)
        return;
    
    HashMap<String, SceneNodeBehaviour*>::const_iterator it = behaviours.find (name);
    if (it == behaviours.end()) {
        behaviours[name] = behaviour;
        behaviour->node = this;
        behaviour->Start();
    }
}

void SceneNode::RemoveBehaviour (const String &name) {
    HashMap<String, SceneNodeBehaviour*>::const_iterator it = behaviours.find (name);
    if (it != behaviours.end()) {
        if (it->second)
            delete it->second;
        
        behaviours.erase (name);
    }
}

SceneNodeBehaviour* SceneNode::GetBehaviour(const String &name) {
    HashMap<String, SceneNodeBehaviour*>::const_iterator it = behaviours.find (name);
    if (it != behaviours.end())
        return it->second;
    
    return nullptr;
}

/*
 Update from parent concatenates the transformations of the parent with the local transformations of the node.
 1) If we allow rotation to be inherited from the parent, then we just multiply the parent rotation
    with this node's local rotation quaternion, otherwise the node's global rotation is its local rotation.
 2) Same applies for the scaling.
 3) For the translation, we also need to take into account the parent scaling and the parent rotation, so first we 
    multiply the globaltranslation by the rotation and the scaling of the parent, then with the local translation of this
    node. Finally, we can add the parentTranslation to the equation by simply adding it to the global translation of the node.
*/
void SceneNode::updateFromParent() {
    
    SceneNode *parentNode = nullptr;
    if (parent >= 0)
        parentNode = Scene::GetSceneNode(parent);
    
    if (parentNode) {
        // Update rotation
        const Quaternion& parentRotation = parentNode->globalRotation;
        if (RotateFromParent) {
            // Combine orientation with that of parent
            globalRotation = parentRotation * rotation;
        }
        else
            globalRotation = rotation;
        
        
        // Update scale
        const Vector3& parentScaling = parentNode->globalScaling;
        
        if (ScaleFromParent) {
            // Scale own position by parent scale
            globalScaling = parentScaling * scaling;
        }
        else
            globalScaling = scaling;
        
        
        // Update translation
        const Vector3 &parentTranslation = parentNode->globalTranslation;
        // Change position vector based on parent's orientation & scale
        globalTranslation = parentRotation * (parentScaling * translation);
        
        // Add altered position vector to parents
        globalTranslation += parentTranslation;
    } else {
        // Root node, no parent
        globalRotation = rotation;
        globalTranslation = translation;
        globalScaling = scaling;
    }
}

Vector3 SceneNode::worldToLocalPosition( const Vector3 &worldPos ) {
    updateFromParent();
    return glm::inverse(globalRotation) * (worldPos - globalTranslation) / globalScaling;
}

Vector3 SceneNode::localToWorldPosition( const Vector3 &localPos ) {
    updateFromParent();
    return (globalRotation * localPos * globalScaling) + globalTranslation;
}

Quaternion SceneNode::worldToLocalOrientation( const Quaternion &worldRotation) {
    updateFromParent();
    return glm::inverse(globalRotation) * worldRotation;
}

Quaternion SceneNode::localToWorldOrientation( const Quaternion &localRotation ) {
    updateFromParent();
    return globalRotation * localRotation;
}

// translation
void SceneNode::SetTranslation(const Vector3 &p) {
    translation = p;
    updateFromParent();
}

void SceneNode::SetTranslation(float x, float y, float z) {
    SetTranslation(Vector3(x,y,z));
}

void SceneNode::SetGlobalTranslation(const Vector3 &p) {
    SceneNode *parentNode = Scene::GetSceneNode(parent);
    if (parentNode)
        SetTranslation(parentNode->worldToLocalPosition(p));
    else
        SetTranslation(p);
}

void SceneNode::SetGlobalTranslation(float x, float y, float z) {
    SetTranslation(Vector3(x,y,z));
}


void SceneNode::Translate (const Vector3 &p, Space relativeTo) {
    SceneNode *parentNode = Scene::GetSceneNode(parent);

    if (relativeTo == SPACE_LOCAL)
        translation += rotation * p;
    else if (relativeTo == SPACE_GLOBAL) {
        if (parentNode)
            translation += ( glm::inverse(parentNode->GetGlobalRotation()) * p) / parentNode->GetGlobalScale();
        else
            translation += p;
    }
    else if (relativeTo == SPACE_PARENT) {
        translation += p;
    }
    
    updateFromParent();
}

void SceneNode::Translate (float x, float y, float z, Space relativeTo) {
    Translate (Vector3(x,y,z), relativeTo);
}


const Vector3& SceneNode::GetGlobalTranslation() {
    updateFromParent();
    return globalTranslation;
}

const Vector3& SceneNode::GetTranslation() {
    return translation;
}

// rotation
void SceneNode::SetRotation(const Vector3 &euler) {
    Vector3 eulerDegrees = Vector3(DEG2RAD(euler.x), DEG2RAD(euler.y), DEG2RAD(euler.z));
    SetRotation (Quaternion(eulerDegrees));
}

void SceneNode::SetRotation(float x, float y, float z) {
    SetRotation(Vector3(x,y,z));
}

void SceneNode::SetRotation(const Quaternion &q) {
    rotation = q;
    rotation = glm::normalize(rotation);
    updateFromParent();
}

void SceneNode::SetRotation(float w, float x, float y, float z) {
    SetRotation(Quaternion(w,x,y,z));
}

void SceneNode::SetGlobalRotation(const Vector3 &euler) {
    Vector3 eulerDegrees = Vector3(DEG2RAD(euler.x), DEG2RAD(euler.y), DEG2RAD(euler.z));
    SetGlobalRotation (Quaternion(eulerDegrees));
}

void SceneNode::SetGlobalRotation(float x, float y, float z) {
    SetGlobalRotation(Vector3(x,y,z));
}

void SceneNode::SetGlobalRotation(const Quaternion &q) {
    SceneNode *parentNode = Scene::GetSceneNode(parent);
    if (parent)
        SetRotation(parentNode->worldToLocalOrientation(q));
    else
        SetRotation(q);
}

void SceneNode::SetGlobalRotation(float w, float x, float y, float z) {
    SetGlobalRotation(Quaternion(w,x,y,z));
}

void SceneNode::Rotate(const Vector3 &r, Space relativeTo) {
    Vector3 eulerDegrees = Vector3(DEG2RAD(r.x), DEG2RAD(r.y), DEG2RAD(r.z));
    Quaternion quat(eulerDegrees);
    
    quat = glm::normalize(quat);
    
    if (relativeTo == SPACE_LOCAL) {
        rotation = rotation * quat;
    }
    else if (relativeTo == SPACE_GLOBAL) {
        rotation = rotation *  glm::inverse(GetGlobalRotation()) * quat * GetGlobalRotation();
    }
    else if (relativeTo == SPACE_PARENT) {
        rotation =quat * rotation;
    }
    
    updateFromParent();
}

void SceneNode::Rotate(float x, float y, float z, Space relativeTo) {
    Rotate (Vector3(x,y,z), relativeTo);
}

void SceneNode::RotateAroundPoint(const Vector3 &point, const Vector3 &pivot, const Quaternion &angle, Space relativeTo) {
    
    Vector3 finalPosition = (angle * (point-pivot)) + pivot;
    
    
    if (relativeTo == SPACE_LOCAL || relativeTo == SPACE_PARENT)
        SetTranslation(finalPosition);
    else 
        SetGlobalTranslation(finalPosition);
}

void SceneNode::LookAt (const Vector3 &target) {
    Matrix44 mat = glm::lookAt(GetGlobalTranslation(), target, Vector3(0,1,0));
    Quaternion rot=glm::quat_cast(mat);
    rot.w *= -1;
    
    SetGlobalRotation(rot);
}

const Quaternion& SceneNode::GetRotation() {
    return rotation;
}

const Quaternion& SceneNode::GetGlobalRotation() {
    updateFromParent();
    return globalRotation;
}

// scaling
void SceneNode::SetScale(const Vector3 &s) {
    scaling = s;
}

void SceneNode::SetScale(float x, float y, float z) {
    SetScale(Vector3(x,y,z));
}

void SceneNode::Scale(const Vector3 &s) {
    scaling *= s;
}

void SceneNode::Scale(float x, float y, float z) {
    Scale (Vector3(x,y,z));
}

const Vector3& SceneNode::GetScale() {
    return scaling;
}

const Vector3& SceneNode::GetGlobalScale() {
    updateFromParent();
    return globalScaling;
}