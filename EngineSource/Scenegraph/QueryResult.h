/*
 Author: 
 Pantelis Lekakis
 
 Description:
 QueryResult containing node ids from a recent scene node query.
 This must be freed after it's not required using Scene::FreeQueryResult !
 
 */
#ifndef QUERY_RESULT_H_DEF
#define QUERY_RESULT_H_DEF

#include <Platform.h>

class SceneNode;

class QueryResult {
	friend class Scene;
public:
    Vector<int> nodeIds;
    
private:
	QueryResult(int queryId);
    int id;
	int GetID() const { return id; }
};

#endif
