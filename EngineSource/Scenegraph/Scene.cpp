#include "Scene.h"
#include "SceneNode.h"
#include "QueryResult.h"

#include <Graphics/Camera/Camera.h>
#include <Resources/ResourceManager.h>
#include <Resources/MeshResource.h>
#include <Graphics/Mesh/Mesh.h>
#include <Graphics/Collider/Collider.h>
#include <Graphics/Collider/BoxCollider.h>
#include <Graphics/Collider/SphereCollider.h>
#include "SceneNodeBehaviour.h"

#include <Graphics/Particles/ParticleSystem.h>

#include <Audio/AudioClip/AudioClip.h>
#include <Audio/AudioSystem/AudioSystem.h>
#include <Audio/AudioSource/AudioSource.h>

#include <Mathematics/Sphere.h>
#include <Mathematics/Ray.h>
#include <Mathematics/Math.h>
#include <Mathematics/Box.h>
#include <Mathematics/Line.h>

#include <UI/UILayer.h>

SceneEnvironment Scene::env;

SceneNode* Scene::activeCameraNode = nullptr;
SceneNode* Scene::skyBoxNode = nullptr;

Scene::NodeNameBindings_ Scene::nodeNameBindings;
Scene::NodeTagBindings_ Scene::nodeTagBindings;

Vector<SceneNode*> Scene::nodes;
Stack<int> Scene::availableSlots;

Vector<QueryResult> Scene::queryResults;
Stack<int> Scene::availableQuerySlots;

UILayer* Scene::ui = nullptr;

Vector<Plane> Scene::frustum;

CollisionCombinations Scene::collisionCombs;

/*
sort the nodes based on the distance from the origin.
this is required for the raycasting to work properly.
*/
bool SceneNodeSortDistance::operator()(SceneNode *a, SceneNode *b)
{
    float distanceA = glm::distance(origin, a->GetGlobalTranslation());
    float distanceB = glm::distance(origin, b->GetGlobalTranslation());
    
    return distanceA < distanceB;
}

void Scene::Initialize() {
    if (nodes.size() != 0) {
        Log ("Scene::Initialize: Failed (Scene already contains nodes)");
        return;
    }
    
    // the root object
    SceneNode *root = new SceneNode(0);
    addToNameBindingMap(root, "__root_node__");
    nodes.push_back(root);
    
    SetWorldLight (Vector3(0,-1,0), Vector4(1,1,1,1), Vector4(1, 1, 1, 1.0f));

    Log ("Scene::Initialize: Success");
}

void Scene::Destroy() {
    for (int i=0; i<(int) nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (node)
            delete node;
    }
    nodes.clear();

    nodeNameBindings.clear();
    nodeTagBindings.clear();
    
    while (!availableSlots.empty()) {
        availableSlots.pop();
    }
    
    while (!availableQuerySlots.empty()) {
        availableQuerySlots.pop();
    }
    
    frustum.clear();
    
    if (ui)
        delete ui;
    
    Log ("Scene::Destroy: Success");
}

void Scene::SetUILayer(UILayer *ui) {
    assert (ui);
    
    if (Scene::ui)
        delete Scene::ui;
    
    Scene::ui = ui;
}

void Scene::DisableCollisions (unsigned int g1, unsigned int g2) {
    collisionCombs.SetCollide (g1, g2, false);
}

void Scene::EnableCollisions (unsigned int g1, unsigned int g2) {
    collisionCombs.SetCollide (g1, g2, true);
}

bool Scene::IsCollisionEnabled (unsigned int g1, unsigned int g2) {
    return collisionCombs.CanCollide (g1, g2);
}

void Scene::SetSkyBox(Material *skyboxMaterial) {
    // special scene nodes
    skyBoxNode = GetSceneNode(AddSceneNode("__skybox_node__", 0));
    skyBoxNode->mesh = ResourceManager::Create<MESH>(new MeshResourceDescriptor("SkyboxMesh", true))->MeshObject;
    skyBoxNode->mesh->MakeSkyBox();
    
    skyBoxNode->mesh->GetSubmesh(0)->material = skyboxMaterial;
}

void Scene::SetActiveCamera(int id) {
    SceneNode *cameraNode = GetSceneNode(id);
    assert (cameraNode);
    assert (cameraNode->camera);
    
    activeCameraNode = cameraNode;
}

/*
 Recursively remove the scene nodes, ignoring root.
 Also clears the active camera node and the frustum.
*/
void Scene::Clear() {
    // ignore root, start from 1
    for (int i=1; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || node->isDestroying)
            continue;
        
        DestroySceneNode(node->GetID(), false);
    }
    activeCameraNode = nullptr;
    frustum.clear();
}

/*
 Get all nodes containing a particle system and update them.
*/
void Scene::updateParticleSystems() {
    const QueryResult &result = Scene::GetSceneNodesWithParticleSystems();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++) {
        SceneNode *node = GetSceneNode(result.nodeIds[i]);
        if (!node || node->isDestroying)
            continue;
        
        ParticleSystem *particle = node->GetParticleSystem();
        particle->Update();
    }
    
    FreeQueryResult (result);
}

/*
 Get all nodes containing a mesh and if there is animation, update it.
*/
void Scene::updateMeshAnimations() {
    const QueryResult &result = Scene::GetSceneNodesWithMeshes();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++) {
        SceneNode *node = GetSceneNode(result.nodeIds[i]);
        if (!node || node->isDestroying)
            continue;

        if (node->GetMesh()->GetCurrentAnimation() != nullptr)
            node->GetMesh()->UpdateAnimations();
    }
    
    FreeQueryResult (result);
}

/*
 This method does two things:
 1) Call the scene node behaviour (if any) Update method
 2) Update the audio source position (if any), used for 3D sound.
*/
void Scene::updateBehaviours() {
    for (int i=0; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || node->isDestroying)
            continue;
        
        HashMap<String, SceneNodeBehaviour*>::const_iterator it;
        for (it=node->behaviours.begin(); it!=node->behaviours.end(); it++) {
            SceneNodeBehaviour *behaviour = it->second;
            
            if (behaviour) {
                behaviour->Update();
            }
        }
        
        HashMap<String, AudioSource*>::iterator it2;
        for (it2=node->audioSources.begin(); it2!=node->audioSources.end(); it2++) {
            AudioSource *source = it2->second;
            assert (source);
            
            Vector3 nodeTranslation = node->GetGlobalTranslation();
            nodeTranslation.z *= -1; // fmod uses +z for depth.
            
            source->Update(nodeTranslation, Vector3(0,0,0));
        }
    }
}

/*
 1) Compose the model matrix for each scene node's translation, rotation and scaling elements.
 2) Transform the collider with the model matrix
 3) Transform the bounding sphere with the model matrix for frustum culling
 4) Update the perspective projection matrix by updating the active camera.
*/
void Scene::createMatrices() {
    // then create the matrices
    for (int i=0; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || node->isDestroying)
            continue;
        
        Vector3 scaling = node->GetGlobalScale();
        Vector3 translation = node->GetGlobalTranslation();
        Quaternion rotation = node->GetGlobalRotation();
        
        Math::ComposeMatrix(node->modelMatrix, translation, rotation, scaling);
        if (node->collider)
            node->collider->Transform (node->modelMatrix);
        
        if (node->boundingSphere) {
            node->worldBoundingSphere = Sphere(*node->boundingSphere);
            node->worldBoundingSphere.Transform (node->GetMatrix());

            node->isFrustumCulled = !node->worldBoundingSphere.InsideFrustum(frustum);
        }
    }
    
    // update the main camera's projection matrix
    if (activeCameraNode && !activeCameraNode->isDestroying) {
        assert (activeCameraNode->camera);
        activeCameraNode->camera->update();
    }
}

/*
 Destroy nodes destined to be deleted.
 1) Remove the node from the Name bindings
 2) Remove the node from the Tag bindings
 3) Set the scene node in the nodes vector to nullptr and make that id available
 4) delete the scene node
*/
void Scene::destroyNodes() {
    // destroy nodes that need to be destroyed
    //
    for (int i=0; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || !node->isDestroying)
            continue;
        
        // make this node not available in the name hashmap anymore
        if (node->GetName() != "") {
            NodeNameBindings_::iterator it = nodeNameBindings.find (node->GetName());
            if (it != nodeNameBindings.end()) {
                it->second.erase (node->GetID());

                if (it->second.size() == 0) {
                    nodeNameBindings.erase(node->GetName());
                }
            }
        }
        
        // make this node not available in the tag hashmap anymore
        if (node->HasTag()) {
            String tag = node->GetTag()->first;
            
            NodeTagBindings_::iterator it = nodeTagBindings.find (tag);
            if (it != nodeTagBindings.end()) {
                it->second.erase (node->GetID());                

                if (it->second.size() == 0) {
                    nodeTagBindings.erase(tag);
                }
            }
        }
        
        nodes[node->GetID()] = nullptr;
        availableSlots.push (node->GetID());
        
        delete node;
        node = nullptr;
    }

}

/*
 Calls all the different update methods and calculates the frustum form the active camera.
 Destruction of nodes happens in the last moment.
*/
void Scene::Update() {
    frustum.clear();
    
    if (activeCameraNode) {
        Camera *cam = activeCameraNode->camera;
        
        Matrix44 viewMatrix = glm::inverse(activeCameraNode->GetMatrix());
        Matrix44 projectionMatrix = cam->GetProjectionMatrix();
    
        frustum = Math::CalculateViewFrustum(viewMatrix, projectionMatrix);
    
    }
    updateBehaviours();
    
    updateParticleSystems();
    updateMeshAnimations();
    
    createMatrices();

    setupColliderObjects();
    handleCollisionsAndTriggers();
    updateCollisionAndTriggerCalls();
    
    destroyNodes();
}

/*
 Handling collision response in the engine is done in 3 passes for scene nodes that have a behaviour attached to them:
 1) Gets the set difference between the current frame's collided nodes and the previous frame's collided nodes. This will
 give us the nodes that have just collided. On those nodes, we need to call the behaviour's OnCollideEnter or OnTriggerEnter
 depending on what kind of collider the scene node has.

 2) Gets the set difference between the previous frame's collided nodes and the current frame's collided nodes. This will
 give us the nodes that have just stopped collided. On those nodes, we need to call the behaviour's OnCollideExit
 or OnTriggerExit depending on what kind of collider the scene node has.
 
 3) Gets the set intersection between the current frame's collided nodes and the previous frame's collided nodes. This will
 give us the nodes that keep colliding. On those nodes, we need to call the behaviour's OnCollideStay or OnTriggerStay
 depending on what kind of collider the scene node has.
*/
void Scene::updateCollisionAndTriggerCalls() {
    const QueryResult &result = GetSceneNodesWithColliders();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++) {
        SceneNode *node = GetSceneNode(result.nodeIds[i]);
        if (!node || node->isDestroying)
            continue;
        //assert (node);
        
        Collider *collider = node->GetCollider();
        if (!collider)
            continue;
        //assert (collider);
        
        Set<int> temp;
        Set<int>::iterator it;
        HashMap<String, SceneNodeBehaviour*>::iterator behIt;
        
        // enter
        std::set_difference(collider->otherCollidedNodes->begin(), collider->otherCollidedNodes->end(), collider->prevOtherCollidedNodes->begin(), collider->prevOtherCollidedNodes->end(), std::inserter(temp, temp.end()));
        
        for (it=temp.begin(); it!=temp.end(); it++) {
            SceneNode *otherNode = GetSceneNode(*it);
            if (!otherNode)
                continue;
            
            Collider *otherCollider = otherNode->GetCollider();
            if (!otherCollider)
                continue;
            
            //assert (otherCollider);
            
            for (behIt=node->behaviours.begin(); behIt!=node->behaviours.end(); behIt++) {
                SceneNodeBehaviour *behaviour = behIt->second;
                assert (behaviour);
                
                bool trigger = otherCollider->IsTrigger || collider->IsTrigger;
                if (!trigger)
                    behaviour->OnCollideEnter(otherNode);
                else
                    behaviour->OnTriggerEnter(otherNode);
            }
        }

        temp.clear();
        
        // exit
        std::set_difference(collider->prevOtherCollidedNodes->begin(), collider->prevOtherCollidedNodes->end(), collider->otherCollidedNodes->begin(), collider->otherCollidedNodes->end(), std::inserter(temp, temp.end()));
        
        for (it=temp.begin(); it!=temp.end(); it++) {
            SceneNode *otherNode = GetSceneNode(*it);
            if (!otherNode)
                continue;
            
            Collider *otherCollider = otherNode->GetCollider();
            if (!otherCollider)
                continue;
            
            //assert (otherCollider);
            
            for (behIt=node->behaviours.begin(); behIt!=node->behaviours.end(); behIt++) {
                SceneNodeBehaviour *behaviour = behIt->second;
                assert (behaviour);
                
                bool trigger = otherCollider->IsTrigger || collider->IsTrigger;
                if (!trigger)
                    behaviour->OnCollideExit(otherNode);
                else
                    behaviour->OnTriggerExit(otherNode);
            }
        }
        
        temp.clear();
        
        // stay
        std::set_intersection(collider->prevOtherCollidedNodes->begin(), collider->prevOtherCollidedNodes->end(), collider->otherCollidedNodes->begin(), collider->otherCollidedNodes->end(), std::inserter(temp, temp.end()));
        
        for (it=temp.begin(); it!=temp.end(); it++) {
            SceneNode *otherNode = GetSceneNode(*it);
            if (!otherNode)
                continue;
            
            Collider *otherCollider = otherNode->GetCollider();
            if (!otherCollider)
                continue;
            
            //assert (otherCollider);
            
            for (behIt=node->behaviours.begin(); behIt!=node->behaviours.end(); behIt++) {
                SceneNodeBehaviour *behaviour = behIt->second;
                assert (behaviour);
                
                bool trigger = otherCollider->IsTrigger || collider->IsTrigger;
                if (!trigger)
                    behaviour->OnCollideStay(otherNode);
                else
                    behaviour->OnTriggerStay(otherNode);
            }
            
        }
        
        temp.clear();
    }
    
    FreeQueryResult(result);
}

/*
 For scene nodes that are eligible for collision (that is, those who have a collider and those whose CollisionGroups are
 enabled for collisions), check for Collider vs Collider collision. Each node's collider now keeps a list of all collided
 nodes.
*/
void Scene::handleCollisionsAndTriggers() {
    const QueryResult &result = GetSceneNodesWithColliders();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++) {
        SceneNode *node1 = GetSceneNode(result.nodeIds[i]);
        if (!node1 || node1->isDestroying)
            continue;

        Collider *collider1 = node1->GetCollider();
        
        if (!collider1 || !collider1->Enabled)
            continue;
        
        for (int j=0; j<(int)result.nodeIds.size(); j++) {
            if (i==j)
                continue;
            
            SceneNode *node2 = GetSceneNode(result.nodeIds[j]);
            if (node2->ignoreCollisionNode == node1)
                continue;
            
            if (!node2 || node2->isDestroying)
                continue;
            
            if (!collisionCombs.CanCollide(node1->GetCollisionGroup(), node2->GetCollisionGroup() ) )
                continue;
            
            Collider *collider2 = node2->GetCollider();
            if (!collider2 || !collider2->Enabled)
                continue;
            
            if (collider1->Collide(collider2) ) {
                collider1->otherCollidedNodes->insert(node2->GetID());
                collider2->otherCollidedNodes->insert(node1->GetID());
            }
        }
    }
    FreeQueryResult(result);
}

/*
 For all scene nodes that have a collider, swap their otherCollidedNodes and prevOtherCollidedNodes. By doing so each frame,
 we can perform intersections and differences on those Set<int> and see which behaviours' collision methods need to be called.
 
 This is the core of the collision response system.
*/
void Scene::setupColliderObjects() {
    const QueryResult &result = GetSceneNodesWithColliders();
    
    for (int i=0; i<(int)result.nodeIds.size(); i++) {
        SceneNode *node = GetSceneNode(result.nodeIds[i]);
        if (!node || node->isDestroying)
            continue;
        
        Collider *collider = node->GetCollider();
        if (!collider)
            continue;
        
        std::swap (collider->otherCollidedNodes, collider->prevOtherCollidedNodes);
        collider->otherCollidedNodes->clear();
    }
    
    FreeQueryResult(result);
    
}

/*
 1) For raycasting, we first sort the scene nodes with colliders based on their distance from the origin.
 2) Check if our ray hit a bounding volume, if it did, then continue to ray-triangle testing.
 3) If the ray hit a triangle in the scene node's mesh, then return a RayHitInfo structure with the 
    normal, point, sceneNode instance that we hit.
*/
bool Scene::Raycast (const Vector3 &origin, const Vector3 &direction, float maxDistance, RayHitInfo &hitInfo, int ignoreNode) {
    
    SceneNode *ignoreNodeObject = Scene::GetSceneNode(ignoreNode);
    assert (ignoreNodeObject);
    
    Ray ray (origin, direction);
    Line line (ray, maxDistance);
    
    // sort the nodes based on distance from the origin
    List<SceneNode*> sortedNodes;
    for (int i=0; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || node->GetID() == ignoreNode)
            continue;
    
        if (!node->collider || !node->collider->Enabled)
            continue;
        
        if (glm::distance (origin, node->GetGlobalTranslation()) > maxDistance )
            continue;
        
        sortedNodes.push_back(nodes[i]);
    }
    
    sortedNodes.sort(SceneNodeSortDistance(origin));
    
    
    bool ret = false;
    
    List<SceneNode*>::const_iterator it;
    // check if the ray intersects with anything
    for (it=sortedNodes.begin(); it!=sortedNodes.end(); it++) {
        SceneNode *node = *it; // this is valid for sure, we checked for non-valid pointer in the previous loop
        
        Collider *collider = node->collider; // we checked if the collider is valid in the previous loop
        
        Mesh *mesh = node->GetMesh();
        if (!mesh)
            continue;
        
        bool boxIntersected=false, sphereIntersected=false;
        
        if (collider->GetColliderType() == CT_BOX) {
            BoxCollider *boxCollider = (BoxCollider*)collider;
            
            boxIntersected = Math::Intersect(line, boxCollider->GetGlobalBoundingBox());
		}
        else if (collider->GetColliderType() == CT_SPHERE) {
            SphereCollider *sphereCollider = (SphereCollider*)collider;
            
            
            sphereIntersected = Math::Intersect(line, sphereCollider->GetGlobalBoundingSphere());
		}
        if ( !(boxIntersected || sphereIntersected) )
            continue;
        
                
        for (unsigned int i=0; i<mesh->GetSubmeshCount(); i++) {                    
            Mesh::Submesh *submesh = mesh->GetSubmesh(i);
                    
            for (int j=0; j<(int)submesh->triangles.size(); j++) {
                Mesh::Triangle &triangle = submesh->triangles[j];
                        
                Vector3 v0 = submesh->vertices[triangle.indices[0]].position;
                Vector3 v1 = submesh->vertices[triangle.indices[1]].position;
                Vector3 v2 = submesh->vertices[triangle.indices[2]].position;
                

                v0 = glm::transformVector3ByMatrix(v0, node->GetMatrix());
                v1 = glm::transformVector3ByMatrix(v1, node->GetMatrix());
                v2 = glm::transformVector3ByMatrix(v2, node->GetMatrix());
                        
                Triangle mathTri (v0, v1, v2);
                        
                if (Math::Intersect(line.GetRay(), mathTri) ) {
                    Vector3 p = Math::PointOfIntersection(line, mathTri);
					float d = glm::distance(p, origin);

					if (!ret  ||  d < hitInfo.distance) {
						hitInfo.node = node;
						hitInfo.collider = collider;
						hitInfo.point = p;
						hitInfo.distance = d;
                        hitInfo.normal = mathTri.GetNormal();
                            
		                ret = true;
					}
                }
            }
        }
                

    }
    return ret;
}

void Scene::SetWorldLight (const Vector3 &direction, const Vector4 ambient, const Vector4 &diffuse) {
    
    env.worldLightDirection = glm::normalize (direction);
    env.worldLightAmbient = ambient;
    env.worldLightDiffuse = diffuse;
}

void Scene::SetBlobShadow (const Vector3 &center, float radius, const Vector4 &color, float distance, float maxDistance) {
    
    env.blobShadowCenter = center;
    env.blobShadowRadius = radius;
    env.blobShadowColor = color;
    env.blobShadowDistance = distance;
    env.blobShadowMaxDistance = maxDistance;
}

/*
 Get an available scene node slot. This is implemented using a stack to reduce the number of allocations when scene nodes
 are created/deleted during the game loop.
*/
int Scene::getAvailableSlot() {
    if (availableSlots.empty()) {
        nodes.push_back(nullptr);
        return (int)nodes.size()-1;
    }
    
    int ret = availableSlots.top();
    availableSlots.pop();
    return ret;
}

/*
 Get an available scene node query slot. This is implemented using a stack to reduce the number of allocations when 
 scene node queries are created/deleted during the game loop.
 
 Freeing the queries is important so that the stack doesn't increase in size!
 */
int Scene::getQueryAvailableSlot() {
    if (availableQuerySlots.empty()) {
		int id = (int)queryResults.size();
		assert(id < 1000); // someone forgot to free his queries!
        queryResults.push_back(QueryResult(id));
        return id;
    }
    
    int ret = availableQuerySlots.top();
    availableQuerySlots.pop();
    return ret;
}

/*
 Mark the scene node for destruction. This will be done before this frame ends.
*/
void Scene::DestroySceneNode(int id, bool recursive) {
    if (id >= 0) {
        SceneNode *node = nodes[id];
        assert (node);
        
        node->isDestroying = true;
        
        if (recursive) {
            for (int i=0; i<node->GetChildCount(); i++) {
                DestroySceneNode(node->GetChild(i));
            }
        }
    }
}

void Scene::addToNameBindingMap(SceneNode *node,const String &name) {
    // add it to the name hashmap
    assert(name != "");
    assert(node);
    
    node->name = name;
    nodeNameBindings[name].insert(node->GetID());
}

void Scene::addToTagBindingMap(SceneNode *node) {
    // add it to the tag hashmap
    assert (node);
    assert (node->HasTag());
    
    String tag = node->GetTag()->first;
    
    nodeTagBindings[tag].insert(node->GetID());
}

int Scene::AddSceneNode(const String &name, int parent) {
    SceneNode *node = new SceneNode(getAvailableSlot());
    
    nodes[node->GetID()] = node;
    node->SetParent(parent);
    
    addToNameBindingMap(node, name);
    
    return node->GetID();
}

SceneNode* Scene::GetSceneNodeWithName(const String &name) {
    SceneNode *node = nullptr;
    
    NodeNameBindings_::const_iterator it = nodeNameBindings.find (name);
    if (it != nodeNameBindings.end()) {
        Set<int> names = it->second;
        node = GetSceneNode (*names.begin());
        if (node && node->isDestroying)
            return nullptr;
    }
    
    return node;
}

SceneNode* Scene::GetSceneNodeWithTag(const String &tag) {
    SceneNode *node = nullptr;
    
    NodeTagBindings_::const_iterator it = nodeTagBindings.find (tag);
    if (it != nodeTagBindings.end()) {
        Set<int> tags = it->second;
        node = GetSceneNode (*tags.begin());
        if (node && node->isDestroying)
            return nullptr;
    }
    
    return node;
}

SceneNode* Scene::GetSceneNodeWithCollider(Collider *collider) {
    assert (collider);
    
    SceneNode *node = nullptr;
    
    for (int i=0; i<(int)nodes.size(); i++) {
        if (!nodes[i] || nodes[i]->isDestroying)
        //if (!nodes[i])
            continue;
    
        if (nodes[i]->collider == collider) {
            node = nodes[i];
            break;
        }
    }
    
    return node;
}

SceneNode* Scene::GetSceneNodeWithParticleSystem(ParticleSystem *particleSystem) {
    assert (particleSystem);
    
    SceneNode *node = nullptr;
    
    for (int i=0; i<(int)nodes.size(); i++) {
        if (!nodes[i] || nodes[i]->isDestroying)
            //if (!nodes[i])
            continue;
        
        if (nodes[i]->particleSystem == particleSystem) {
            node = nodes[i];
            break;
        }
    }
    
    return node;
}

SceneNode* Scene::GetSceneNode(int id) {
//    if (id >= 0 && id < (int) nodes.size()) {
        if (nodes[id] && !nodes[id]->isDestroying)
            return nodes[id];
//    }
    
    return nullptr;
}

void Scene::FreeQueryResult(const QueryResult &result) {
	queryResults[result.GetID()].nodeIds.resize(0);
    availableQuerySlots.push(result.GetID());
}

const QueryResult& Scene::GetSceneNodesWithName (const String &name) {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    NodeNameBindings_::iterator it = nodeNameBindings.find (name);
    if (it != nodeNameBindings.end()) {
		for (Set<int>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2 ++) {
            int nodeId = *it2;
            if (!nodes[nodeId] || nodes[nodeId]->isDestroying)
            //if (!nodes[nodeId])
                continue;
            
			assert(nodeId >= 0);
            result.nodeIds.push_back(nodeId);
        }
    }
    
    return result;
}

const QueryResult& Scene::GetSceneNodesWithTag (const String &tag) {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    NodeTagBindings_::iterator it = nodeTagBindings.find (tag);
    if (it != nodeTagBindings.end()) {
		for (Set<int>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2 ++) {
            int nodeId = *it2;
            if (!nodes[nodeId] || nodes[nodeId]->isDestroying)
            //if (!nodes[nodeId])
                continue;
            
			assert(nodeId >= 0);
            result.nodeIds.push_back(nodeId);
        }
    }
    
    return result;
}

const QueryResult& Scene::GetSceneNodesWithCollider(Collider *collider) {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    if (!collider)
        return result;
    
    for (int i=0; i<(int)nodes.size(); i++) {
        // if this node is to be destroyed, ignore it
        if (!nodes[i] || nodes[i]->isDestroying)
        //if (!nodes[i])
            continue;
    
        if (nodes[i]->collider == collider)
            result.nodeIds.push_back (nodes[i]->GetID());
        
    }
    
    return result;
}

const QueryResult& Scene::GetSceneNodesWithParticleSystem(ParticleSystem *particle) {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    if (!particle)
        return result;
    
    for (int i=0; i<(int)nodes.size(); i++) {
        // if this node is to be destroyed, ignore it
        if (!nodes[i] || nodes[i]->isDestroying)
        //if (!nodes[i])
            continue;
        
        if (nodes[i]->particleSystem == particle)
            result.nodeIds.push_back (nodes[i]->GetID());
        
    }
    
    return result;
}

const QueryResult& Scene::GetSceneNodesWithMeshes() {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    for (int i=0; i<(int)nodes.size(); i++) {
        // if this node is to be destroyed, ignore it
        if (!nodes[i] || nodes[i]->isDestroying)
        //if (!nodes[i])
            continue;
        
        if (nodes[i]->mesh)
            result.nodeIds.push_back(nodes[i]->GetID());
    }
    return result;
}

const QueryResult& Scene::GetSceneNodesWithColliders() {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    for (int i=0; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || node->isDestroying)
//        if (!node)
            continue;
        
        if (node->collider) {
            result.nodeIds.push_back(node->GetID());
        }
    }
    return result;
}

const QueryResult& Scene::GetSceneNodesWithParticleSystems() {
    QueryResult &result = queryResults[getQueryAvailableSlot()];
    
    for (int i=0; i<(int)nodes.size(); i++) {
        SceneNode *node = nodes[i];
        if (!node || node->isDestroying)
        //if (!node)
            continue;
        
        if (node->particleSystem)
            result.nodeIds.push_back(node->GetID());
    }
    return result;
}

/*
 Implementation of gluProject using glm
*/
Vector2 Scene::WorldToScreenPoint (const Vector3 &worldPoint) {
    assert (activeCameraNode);
    assert (activeCameraNode->camera);
    
    float viewport[4];
    glGetFloatv(GL_VIEWPORT, viewport);
    
    Vector3 screenPos = glm::project(worldPoint, glm::inverse(activeCameraNode->GetMatrix()), activeCameraNode->camera->GetProjectionMatrix(), Vector4(viewport[0], viewport[1], viewport[2], viewport[3]));

    screenPos.y = viewport[3] - screenPos.y;
    
    return Vector2(screenPos.x, screenPos.y);
}

/*
 Implementation of gluUnproject using glm
*/
Ray Scene::ScreenToWorldRay(const Vector2 &point) {
    assert (activeCameraNode);
    assert (activeCameraNode->camera);
    
    Vector2 oglWindowPoint = point;
    
    float viewport[4];
    glGetFloatv(GL_VIEWPORT, viewport);

    oglWindowPoint.y = viewport[3] - oglWindowPoint.y;
    
    // this difference is enough to construct a ray with correct direction.
    Vector3 depth1 = Vector3(oglWindowPoint.x, oglWindowPoint.y, activeCameraNode->camera->GetNearClipPlane());
    Vector3 depth2 = Vector3(oglWindowPoint.x, oglWindowPoint.y, activeCameraNode->camera->GetNearClipPlane()*2);
    
    Matrix44 invTransProj = activeCameraNode->camera->GetProjectionMatrix();
    
    Vector3 unprojectedDepth1 = glm::unProject(depth1, glm::inverse(activeCameraNode->GetMatrix()), invTransProj, Vector4(viewport[0], viewport[1], viewport[2], viewport[3]));

    Vector3 unprojectedDepth2 = glm::unProject(depth2, glm::inverse(activeCameraNode->GetMatrix()), invTransProj, Vector4(viewport[0], viewport[1], viewport[2], viewport[3]));
    
    return Ray(unprojectedDepth1, unprojectedDepth2);
}