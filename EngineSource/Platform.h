/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The engine's platform header defines cross-platform precompiler definitions and includes specific headers for
 OSX and Windows. Compiler specific features such as hash_map are also addressed to be accessed in a uniform way.
 
 Some commonly used std containers such as string, vector, list, etc are redefined using a capital starting letter
 to conform with the rest of the engine's coding style.
 
 */

#ifndef PLATFORM_H_DEF
#define PLATFORM_H_DEF

// find which plaform we are running and exclude those not supported
#define PLATFORM_MACOSX 1
#define PLATFORM_WINDOWS 2
#define PLATFORM_UNSUPPORTED 0

#if defined (__WIN32__) || defined (_WIN32)
#	define WIN32_LEAN_AND_MEAN
#	define WIN32_EXTRALEAN
#	define PLATFORM PLATFORM_WINDOWS
#	define NOMINMAX

#	include <windows.h>
#	include <shlobj.h>
#elif defined (__APPLE_CC__) || defined (__APPLE__)
#	define PLATFORM PLATFORM_MACOSX

#else
#	define PLATFORM PLATFORM_UNSUPPORTED
#endif


// find which compiler we are using and exclude those not supported
#define COMPILER_MSVC 100
#define COMPILER_GCC 200
#define COMPILER_UNSUPPORTED 0

#ifdef _MSC_VER
#   define COMPILER COMPILER_MSVC
#	pragma warning(disable:4800) // int to bool performance warning

#elif defined (__GNUC__)
#   define COMPILER COMPILER_GCC
#else
#   define COMPILER COMPILER_UNSUPPORTED
#endif//

// external libraries
#if PLATFORM == PLATFORM_WINDOWS
#   include <glew/glew.h>
#endif

#include <fmod/fmod.hpp>
#include <xmlparser/xmlParser.h>

#include <glfw/glfw.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#if COMPILER == COMPILER_MSVC
#	pragma warning (push)
#	pragma warning (disable: 4244)
#	pragma warning (disable: 4018)
#endif

#include <soil/SOIL.h>

#if COMPILER == COMPILER_MSVC
#	pragma warning (pop)
#endif

#if COMPILER == COMPILER_MSVC
#	pragma warning (push)
#	pragma warning (disable: 4996)
#endif

#include <physfs/physfs.h>

#if COMPILER == COMPILER_MSVC
#	pragma warning (pop)
#endif

// standard headers
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <memory>
#include <sstream>
#include <cmath>
#include <string>
#include <map>
#include <ctime>
#include <stack>
#include <vector>
#include <assert.h>
#include <iterator>
#include <typeinfo>
#include <list>
#include <algorithm>
#include <cctype>
#include <set>
#include <fstream>
#include <utility>

#include <fstream>
#include <iostream>


// c++ 0x null standard. Use this to conform to the future :)
#ifndef nullptr
#	ifdef NULL
#		define nullptr NULL
#	else
#		define nullptr 0
#	endif
#endif

// debug / release configuration defines
#define CONFIG_DEBUG 1
#define CONFIG_RELEASE 2

#if defined (_DEBUG)
#	define CONFIG CONFIG_DEBUG
#else
#	define CONFIG CONFIG_RELEASE
#endif

#if CONFIG == CONFIG_DEBUG
#define Log(x) std::cout << (x) << std::endl
#else
#define Log(x) 
#endif

// MSVC and other compilers have different implementations of the HashMap. Fix this here.
#if COMPILER == COMPILER_GCC
// not msvc
#include <ext/hash_map>

typedef signed long long int64;
typedef unsigned long long uint64;

namespace std
{
    
	using namespace __gnu_cxx;
}

namespace __gnu_cxx
{
    template<> struct hash< std::string >
    {
        size_t operator()( const std::string& x ) const
        {
            return hash< const char* >()( x.c_str() );
        }
    };
}

#elif COMPILER == COMPILER_MSVC
// msvc
#include <hash_map>

using namespace stdext;
typedef __int64 int64;
typedef unsigned __int64 uint64;

#endif

// redefine some commonly used glm classes:
#ifndef GLM_REDEFINED
#define GLM_REDEFINED

#   define Vector2 glm::vec2
#   define Vector3 glm::vec3
#   define Vector4 glm::vec4
#   define Quaternion glm::quat

#   define Matrix44 glm::mat4
#   define Matrix33 glm::mat3
#   define Matrix22 glm::mat2

#endif

// redefine some commonly used stl classes:
#ifndef STL_REDEFINED
#define STL_REDEFINED

#define Stack std::stack
#define String std::string
#define Vector std::vector
#define List std::list
#define Map std::map
#define Pair std::pair
#define Set std::set

#if COMPILER == COMPILER_MSVC
#   define HashMap stdext::hash_map
#elif COMPILER == COMPILER_GCC
#   define HashMap std::hash_map
#endif

#endif//

// define some data types which can be different according to the compiler used

#endif//