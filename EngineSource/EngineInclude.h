/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Include this file from the game side to get all the engine features and goodies.
 
 */
#ifndef ENGINEINCLUDE_H_DEF
#define ENGINEINCLUDE_H_DEF

// engine header files go here
#include <Platform.h>
#include <Input/Input.h>
#include <Input/GamePad.h>
#include <Scenegraph/Scene.h>
#include <Scenegraph/SceneNodeBehaviour.h>
#include <Scenegraph/SceneNode.h>
#include <Scenegraph/QueryResult.h>
#include <Audio/AudioSystem/AudioSystem.h>
#include <Audio/AudioClip/AudioClip.h>
#include <Audio/AudioSource/AudioSource.h>
#include <Application/Application.h>
#include <Application/Timing.h>
#include <Application/State.h>
#include <Mathematics/Math.h>
#include <Mathematics/Ray.h>
#include <Mathematics/Sphere.h>
#include <Mathematics/Box.h>
#include <Mathematics/Quad.h>
#include <Mathematics/Plane.h>
#include <Mathematics/Triangle.h>
#include <Mathematics/GlmExtensions.h>
#include <Physics/Physics.h>
#include <Resources/Resource.h>
#include <Resources/ResourceManager.h>
#include <Resources/FontResource.h>
#include <Resources/ShaderResource.h>
#include <Resources/ParticleSystemResource.h>
#include <Resources/MeshResource.h>
#include <Resources/AudioResource.h>
#include <Resources/MaterialResource.h>
#include <Resources/TextureResource.h>
#include <Filesystem/Filesystem.h>
#include <Filesystem/File.h>
#include <Graphics/Collider/SphereCollider.h>
#include <Graphics/Collider/BoxCollider.h>
#include <Graphics/Font/Font.h>
#include <Graphics/GraphicsContext/GraphicsContext.h>
#include <Graphics/Material/Material.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/Camera/Camera.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Particles/ParticleSystem.h>
#include <Graphics/Mesh/Mesh.h>
#include <Graphics/Mesh/Animation.h>
#include <Utilities/Exception.h>
#include <Utilities/StringOperations.h>
#include <Utilities/KeyValueList.h>
#include <UI/UILayer.h>
#include <Renderers/UIRenderer/UIRenderer.h>
#include <Renderers/MeshRenderer/MeshRenderer.h>
#include <Renderers/ParticleRenderer/ParticleRenderer.h>
#include <Renderers/PostProcessRenderer/PostProcessRenderer.h>

#endif
