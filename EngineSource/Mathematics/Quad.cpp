#include "Quad.h"

Quad::Quad() {
    for (int i=0; i<4; i++) {
        vertices[i] = Vector3(0,0,0);
    }
}

Quad::Quad(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& d) {
    vertices[0] = a;
    vertices[1] = b;
    vertices[2] = c;
    vertices[3] = d;
}

Quad::Quad(const Vector4& a, const Vector4& b, const Vector4& c, const Vector4& d) {
    vertices[0] = Vector3(a.x, a.y, a.z);
    vertices[1] = Vector3(b.x, b.y, b.z);
    vertices[2] = Vector3(c.x, c.y, c.z);
    vertices[3] = Vector3(d.x, d.y, d.z);
}

Quad::~Quad() {
    
}

Vector3 Quad::GetVertex(unsigned int index) const {
	if (index < 4) {
		return vertices[index];
	}
	return Vector3();
}

void Quad::SetVertex(unsigned int index, const Vector3& v) {
	if (index < 4)
	{
		vertices[index] = v;
	}
}

Triangle Quad::GetTriangle(unsigned int tri) const
{
	switch (tri)
	{
		case 0:
			return Triangle(vertices[0], vertices[1], vertices[2]);
		case 1:
			return Triangle(vertices[0], vertices[2], vertices[3]);
	}
	return Triangle();
}

Vector3 Quad::GetNormal() const {
	return GetTriangle(0).GetNormal();
}

Plane Quad::GetPlane() const {
	return GetTriangle(0).GetPlane();	
}

Plane Quad::GetEdgePlane(unsigned int i) const {
	if (i < 4)
	{
		Line line = GetLine(i);
		Vector3 c = line.GetVertex(0) + GetNormal();
		Triangle tri = Triangle(line.GetVertex(0), c, line.GetVertex(1));
		return tri.GetPlane();
	}
	return Plane();
}

void Quad::Flip() {
    Vector3 old[4];
	memcpy(old, vertices, 4 * sizeof(Vector3));
	for (unsigned int i=0; i<4; i++)
	{
		vertices[i] = old[3 - i];
	}
}