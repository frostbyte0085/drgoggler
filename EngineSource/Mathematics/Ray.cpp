#include "Ray.h"


Ray::Ray (const Vector3 &origin, const Vector3 &direction) {
    this->origin = origin;
    this->direction = glm::normalize(direction);
}

Ray::Ray() {
    
}

Vector3 Ray::GetPosition(float distance) const{
    return origin + (distance * direction);
}

float Ray::GetDistance(const Vector3 &position) const {
    return glm::dot(position - origin, direction);
}

Vector3 Ray::Project (const Vector3 &v) const {
    return origin + GetDistance(v) * direction;
}