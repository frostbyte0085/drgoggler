#ifndef SPHERE_H_DEF
#define SPHERE_H_DEF

#include <Platform.h>
#include "Plane.h"

class Sphere {
public:
    Sphere();
    Sphere (const Sphere &other);
    Sphere (const Vector3 &center, float radius);
    Sphere (const Vector<Vector3> &vertices);
    ~Sphere();
    
    const Vector3& GetCenter() const { return center; }
    float GetRadius() const { return radius; }
    
    void SetCenter(const Vector3 &center) { this->center = center; }
    void SetRadius(float radius) { this->radius = radius; }
    
    Vector3 GetTop() const;
    Vector3 GetBottom() const;
    
    void Transform(Matrix44 &m);
    
    bool InsideFrustum(const Vector<Plane> &frustum);
    
private:
    Vector3 center;
    float radius;
};

#endif
