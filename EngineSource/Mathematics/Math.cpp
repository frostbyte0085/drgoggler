#include "Math.h"

#include "Sphere.h"
#include "Plane.h"
#include "Line.h"
#include "Box.h"
#include "Triangle.h"

int Math::Random(int from, int to) {
	return rand() % (to - from +1) + from;
}

float Math::RandomF(float low, float high) {
	float temp;

	/* swap low & high around if the user makes no sense */
	if (low > high)
	{
		temp = low;
		low = high;
		high = temp;
	}

	/* calculate the random number & return it */
	temp = ((float)rand() / (static_cast<float>(RAND_MAX) + 1.0f))
	* (high - low) + low;
	return temp;
}

double Math::RandomD(double low, double high) {
	double temp;

	/* swap low & high around if the user makes no sense */
	if (low > high)
	{
		temp = low;
		low = high;
		high = temp;
	}

	/* calculate the random number & return it */
	temp = (rand() / (static_cast<double>(RAND_MAX) + 1.0))
	* (high - low) + low;
	return temp;
}

Vector3 Math::TransformVectorByQuaternion(const Quaternion &q, const Vector3 &v) {

    float x2 = q.x + q.x; 
    float y2 = q.y + q.y; 
    float z2 = q.z + q.z;    
    float xx2 = q.x * x2; 
    float yy2 = q.y * y2; 
    float zz2 = q.z * z2; 
    float xy2 = q.x * y2; 
    float yz2 = q.y * z2; 
    float zx2 = q.z * x2; 
    float xw2 = q.w * x2; 
    float yw2 = q.w * y2; 
    float zw2 = q.w * z2; 
    
    return Vector3( 
                    (1.f - yy2 - zz2) * v[0] + (xy2 - zw2) * v[1] + (zx2 + yw2) * v[2], 
                    (xy2 + zw2) * v[0] + (1.f - zz2 - xx2) * v[1] + (yz2 - xw2) * v[2], 
                    (zx2 - yw2) * v[0] + (yz2 + xw2) * v[1] + (1.f - xx2 - yy2) * v[2] 
                    ); 
}

Quaternion Math::ComputeQuaternionW(const Quaternion &q) {
    Quaternion ret = q;
    float t = 1.0f - (ret[0] * ret[0]) - (ret[1] * ret[1]) - (ret[2] * ret[2]);
    if (t < 0.0f)
        ret[3] = 0.0f;
    else
        ret[3] = -sqrtf(t);
    
    return ret;
}

void Math::ComposeMatrix(Matrix44 &output, Vector3 translation, Quaternion rotation, Vector3 scaling) {
    Matrix44 rotationMatrix = glm::gtc::quaternion::mat4_cast(rotation);
    
    // translation matrix
    Matrix44 translationMatrix = glm::gtx::transform::translate (translation);
    
    // scale matrix
    Matrix44 scaleMatrix = glm::gtx::transform::scale(scaling);
    
    output = translationMatrix * rotationMatrix * scaleMatrix;
}

void Math::DecomposeMatrix(Matrix44 &input, Vector3 &translation, Quaternion &rotation, Vector3 &scaling) {
    
    ExtractRotation(input, rotation);
    ExtractScaling(input, scaling);
    ExtractTranslation(input, translation);
}

void Math::ExtractTranslation (Matrix44 &mat, Vector3 &extracted) {
    
    float *m = glm::value_ptr(mat);
    extracted = Vector3 (m[12], m[13], m[14]);
    
}

void Math::ExtractScaling (Matrix44 &mat, Vector3 &extracted) {
    
    float *m = glm::value_ptr(mat);
    Vector3 v1 = Vector3 (m[0], m[1], m[2]);
    Vector3 v2 = Vector3 (m[4], m[5], m[6]);
    Vector3 v3 = Vector3 (m[8], m[9], m[10]);
    
    extracted = Vector3(glm::length(v1), glm::length(v2), glm::length(v3));
}

void Math::ExtractRotation (Matrix44 &mat, Quaternion &extracted) {
    
    float *m = glm::value_ptr(mat);
    
    Vector3 v1 = Vector3 (m[0], m[1], m[2]);
    Vector3 v2 = Vector3 (m[4], m[5], m[6]);
    Vector3 v3 = Vector3 (m[8], m[9], m[10]);
    
    v1 = glm::normalize(v1);
    v2 = glm::normalize(v2);
    v3 = glm::normalize(v3);
    
    Matrix33 m1 (v1[0], v1[1], v1[2],
                v2[0], v2[1], v2[2],
                v3[0], v3[1], v3[2]);
    
    m1 = glm::transpose(m1);
    
    glm::gtc::quaternion::quat_cast(m1);
}

//http://www.crownandcutlass.com/features/technicaldetails/frustum.html
Vector<Plane> Math::CalculateViewFrustum (const Matrix44 &view, const Matrix44 &projection) {
    Vector<Plane> frustum;
    
    const float   *proj = glm::value_ptr(projection);
    const float   *modl = glm::value_ptr(view);
    float   clip[16];
        
    /* Combine the two matrices (multiply projection by modelview) */
    clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
    clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
    clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
    clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];
    
    clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
    clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
    clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
    clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];
    
    clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
    clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
    clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
    clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];
    
    clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
    clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
    clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
    clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];
    
    /* Extract the numbers for the RIGHT plane */
    Plane right (clip[ 3] - clip[ 0], clip[ 7] - clip[ 4], clip[11] - clip[ 8], clip[15] - clip[12]);
    frustum.push_back(right);
    
    /* Extract the numbers for the LEFT plane */
    Plane left (clip[ 3] + clip[ 0], clip[ 7] + clip[ 4], clip[11] + clip[ 8], clip[15] + clip[12]);
    frustum.push_back(left);
    
    /* Extract the numbers for the BOTTOM plane */
    Plane bottom (clip[ 3] + clip[ 1], clip[ 7] + clip[ 5], clip[11] + clip[ 9], clip[15] + clip[13]);
    frustum.push_back(bottom);
    
    /* Extract the numbers for the TOP plane */
    Plane top (clip[ 3] - clip[ 1], clip[ 7] - clip[ 5], clip[11] - clip[ 9], clip[15] - clip[13]);
    frustum.push_back(top);
    
    /* Extract the numbers for the FAR plane */
    Plane farp (clip[ 3] - clip[ 2], clip[ 7] - clip[ 6], clip[11] - clip[10], clip[15] - clip[14]);
    frustum.push_back(farp);
    
    /* Extract the numbers for the NEAR plane */
    Plane nearp (clip[ 3] + clip[ 2], clip[ 7] + clip[ 6], clip[11] + clip[10], clip[15] + clip[14]);
    frustum.push_back(nearp);
    
    return frustum;
}

Ray Math::RayOfIntersection(const Plane& a, const Plane& b)
{
	Vector3 t = glm::cross(a.GetNormal(), b.GetNormal());
	if(t.length() < EPS)
		return Ray();
    
	t = glm::cross(t, a.GetNormal());
	Ray r(a.GetPosition(), t);
    
	Vector3 point = PointOfIntersection(r, b);
	Vector3 dir = glm::cross(a.GetNormal(), b.GetNormal());
	return Ray(point, dir);
}

Vector3 Math::PointOfIntersection(const Ray& ray, const Plane& plane)
{
	float dot2 = glm::dot(ray.GetDirection(), plane.GetNormal());
	if (fabs(dot2) < EPS)
		return Vector3();
    
	Vector3 x = ray.GetOrigin() - plane.GetPosition();
	float dot1 = glm::dot(x, plane.GetNormal());
	float t = -(dot1 / dot2);
    
	return ray.GetPosition(t);
}

Vector3 Math::PointOfIntersection(const Ray &ray, const Triangle &triangle)
{
	if (Intersect(ray, triangle))
	{
		return PointOfIntersection(ray, triangle.GetPlane());
	}
	return Vector3();
}

Vector3 Math::PointOfIntersection(const Line& line, const Plane& plane)
{
    return PointOfIntersection(line.GetRay(), plane);
}

Vector3 Math::PointOfIntersection(const Line &line, const Triangle &triangle)
{
	if (Intersect(line, triangle))
	{
		return PointOfIntersection(line, triangle.GetPlane());
	}
	return Vector3();
}

Vector3 Math::PointOfIntersection(const Plane& a, const Plane& b, const Plane& c)
{
	// get the intersection of a and b as a ray
	Ray ab = RayOfIntersection(a, b);
    
	return PointOfIntersection(ab, c);
}

Vector3 Math::PointOfIntersection(const Ray &ray, const Sphere &sphere)
{
	Vector3 closest = ray.Project(sphere.GetCenter());
	float a = glm::length(closest - sphere.GetCenter());
	float d = ray.GetDistance(closest);
	float sq = sphere.GetRadius() * sphere.GetRadius() - a*a;
    
	if (sq < 0)
        return Vector3();
    
	float x = sqrtf(sq);
	float dist = d - x;
	
    if (dist < 0)
        dist = d + x;
    
	return ray.GetPosition(dist);
}

bool Math::IsZero (float x) {
    return fabs(x) < EPS;
}

float Math::DistanceFromPlane(const Vector3 &point, const Plane &plane) {
    return glm::dot (point - plane.GetPosition(), plane.GetNormal());
}

Vector3 Math::Project(const Vector3 &point, const Ray &ray)
{
	Vector3 d = ray.GetOrigin() - point;
	float dot = glm::dot(d, ray.GetDirection());
	d -= dot * ray.GetDirection();
	Vector3 ret = point + d;
	return ret;
}

bool Math::Intersect(const Ray &ray, const Sphere &sphere) {

    Vector3 closestToSphere = ray.Project (sphere.GetCenter());
    Vector3 cc = sphere.GetCenter() - closestToSphere;
    
    float radiusSquared = sphere.GetRadius() * sphere.GetRadius();
    
    if (glm::length2(cc) > radiusSquared)
        return false;
    return true;
}

bool Math::Intersect(const Sphere &sphere1, const Sphere &sphere2) {
	float radsq = sphere1.GetRadius() + sphere2.GetRadius();
	radsq = radsq * radsq;
	return glm::length2(sphere1.GetCenter() - sphere2.GetCenter()) <= radsq;
}

bool Math::Intersect(const Ray &ray, const Plane &plane) {
	float dp = glm::dot(ray.GetDirection(), plane.GetNormal());
	if (IsZero(dp)) return false;
	return true;
}

bool Math::Intersect(const Ray &ray, const Triangle &triangle) {
	Plane plane = triangle.GetPlane();
	if (!Intersect(ray, plane)) return false;
	
	// ok, it intersects the plane, but is the 
	// intersection inside the triangle?
	Vector3 pt = PointOfIntersection(ray, plane);
	for (int i=0; i<3; i++) {
		if (UnderPlane(pt, triangle.GetEdgePlane(i))) return false;
	}
	return true;
}

bool Math::Intersect(const Ray &ray, const Quad &quad) {
	if (Intersect(ray, quad.GetTriangle(0))) return true;
	return Intersect(ray, quad.GetTriangle(1));
}

bool Math::Intersect(const Line& line, const Triangle& triangle) {
    Plane plane = triangle.GetPlane();
	if (!(UnderPlane(line.GetVertex(0), plane) ^ UnderPlane(line.GetVertex(1), plane))) {
		// both of the line's vertices are in the same side of the
		// plane - no intersection
		return false;
	}
    
	// get instersection with plane
	Vector3 isc = PointOfIntersection(line.GetRay(), plane);
    
	// is the intersection inside the triangle?
	for (unsigned int i=0; i<3; i++) {
		if (UnderPlane(isc, triangle.GetEdgePlane(i))) {
			return false;
		}
	}
    
	return true;
}

bool Math::Intersect(const Line &line, const Sphere& sphere) {
	// if the center of the sphere is outside the capsule's cylinder
	// there is no intersection
	Ray lineRay = line.GetRay();
	Vector3 projectedSphereCenter = Project(sphere.GetCenter(), lineRay);
	float radiusSq = sphere.GetRadius() * sphere.GetRadius();
	float distSq = glm::length2(projectedSphereCenter - sphere.GetCenter());
	if (distSq > radiusSq) return false;
    
	float paramSq = lineRay.GetDistance(projectedSphereCenter);
	paramSq *= paramSq;
	if (paramSq >= 0 || paramSq <= line.GetLength2()) return true;
    
	// see if the center is in the half spheres of the capsule
	distSq = glm::length2(sphere.GetCenter() - line.GetVertex(0));
	if (distSq <= radiusSq) return true;
    
	distSq = glm::length2(sphere.GetCenter() - line.GetVertex(1));
    if (distSq <= radiusSq) return true;
    
	return false;
}

bool Math::Intersect(const Triangle& a, const Triangle& b) {
	for (unsigned int i=0; i<3; i++) {
		if (Intersect(a.GetLine(i), b)) {
			return true;
		}
	}
    
	for (unsigned int i=0; i<3; i++) {
		if (Intersect(b.GetLine(i), a)) {
			return true;
		}
	}
    
	return false;
}

bool Math::Intersect(const Line& line, const Quad& quad) {
	Plane plane = quad.GetPlane();
	if (!(UnderPlane(line.GetVertex(0), plane) ^ UnderPlane(line.GetVertex(1), plane))) {
		// both of the line's vertices are in the same side of the
		// plane - no intersection
		return false;
	}
    
	// get instersection with plane
	Vector3 isc = PointOfIntersection(line.GetRay(), plane);
    
	// is the intersection inside the quad?
	for (unsigned int i=0; i<4; i++) {
		if (UnderPlane(isc, quad.GetEdgePlane(i))) {
			return false;
		}
	}
    
	return true;
}

bool Math::Intersect(const Triangle& triangle, const Quad& quad) {
	for (int i=0; i<2; i++) {
		if (Intersect(triangle, quad.GetTriangle(i))) {
			return true;
		}
	}
    
	return false;
}

unsigned int Math::fourVertsPlane(Plane plane, Vector3 vertices[]) {
	unsigned int under = 0;
	for (unsigned int i=0; i<4; i++) {
		if (UnderPlane(vertices[i], plane)) {
			under |= 1 << i;
		}
	}
    
	return under;
}

unsigned int Math::fourVertsSameSide(Plane plane, Vector3 vertices[]) {
	unsigned int under = fourVertsPlane(plane, vertices);
	if (!under || under == 15) return true;
	return false;
}

unsigned int Math::fourVertsUnder(Plane plane, Vector3 vertices[]) {
	unsigned int under = fourVertsPlane(plane, vertices);
	if (under == 15) return true;
	return false;
}

bool Math::Intersect(const Quad& a, const Quad& b) {
	// collect vertices and planes
	static Plane plane1, plane2;
	static Vector3 v1[4], v2[4];
	plane1 = a.GetPlane();
    plane2 = b.GetPlane();
    
	for (unsigned int i=0; i<4; i++) {
		v1[i] = a.GetVertex(i);
		v2[i] = b.GetVertex(i);
	}
    
	if (fourVertsSameSide(plane1, v2)) return false;
	if (fourVertsSameSide(plane2, v1)) return false;
    
	for (unsigned int i=0; i<4; i++) {
		if (fourVertsUnder(a.GetEdgePlane(i), v2)) return false;
		if (fourVertsUnder(b.GetEdgePlane(i), v1)) return false;
	}
    
	for (unsigned int i=0; i<4; i++) {
		if (Intersect(a.GetLine(i), b)) return true;
		if (Intersect(b.GetLine(i), a)) return true;
	}
    
	return false;
}

bool Math::Intersect(const Line& line, const Plane& p) {
	bool a = UnderPlane(line.GetVertex(0), p);
	bool b = UnderPlane(line.GetVertex(1), p);
    
	if (a ^ b) {
		return true;
	}
    
	return false;
}

bool Math::Intersect(const Sphere& s, const Plane& p) {
	float d = DistanceFromPlane(s.GetCenter(), p);
	if (d <= s.GetRadius()) return true;
	return false;
}


bool Math::Intersect(const Sphere &s, const Triangle &t) {
	if (!Intersect(s, t.GetPlane())) return false;
	if (UnderPlane(s, t.GetEdgePlane(0))) return false;
	if (UnderPlane(s, t.GetEdgePlane(1))) return false;
	if (UnderPlane(s, t.GetEdgePlane(2))) return false;
    
	if (Intersect(t.GetLine(0), s)) return true;
	if (Intersect(t.GetLine(1), s)) return true;
	if (Intersect(t.GetLine(2), s)) return true;
    
	// is the sphere totally inside the triangle?
	if (OverPlane(s, t.GetEdgePlane(0)) &&
		OverPlane(s, t.GetEdgePlane(1)) &&
		OverPlane(s, t.GetEdgePlane(2))) return true;
    
	return false;
}

bool Math::Intersect(const Ray &ray, const Box &box) {
	for (int i=0; i<6; i++) {
		if (Intersect(ray, box.GetQuad(i))) return true;
	}
	return false;
}

bool Math::Intersect(const Line& line, const Box &box) {
    for (int i=0; i<6; i++) {
		if (Intersect(line, box.GetQuad(i))) return true;
	}
	return false;
}

bool Math::Intersect(const Box &box1, const Box &box2) {
	unsigned char outside[8];
	memset(outside, 0, 8 * sizeof(unsigned char));
	
	// STEP 1: collect info about which vertex is in/outside which frustum plane
	// if any vertex is inside all frustum planes, then we return success
	for (unsigned int boxVertex=0; boxVertex<8; boxVertex++) {
		for (unsigned int plane=0; plane<6; plane++) {
			if (UnderPlane(box2.GetVertex(boxVertex), box1.GetPlane(plane))) {
				outside[boxVertex] |= (1 << plane);
			}
		}
		if (!outside[boxVertex]) {
			// this vertex is inside the frustum
			return true;
		}
	}
    
	// STEP 2: if all vertices are outside *any* plane then return failure
	unsigned char allVertices = outside[0];
    
	for (unsigned int i=1; i<8; i++) {
		allVertices &= outside[i];
	}
    
	if (allVertices) {
		// all verts are outside of at least one plane
		return false;
	}
    
	// STEP 3: if any quad of the frustum intersects any quad of the box, return success
	for (unsigned int quad1=0; quad1<6; quad1++) {
		for (unsigned int quad2=0; quad2<6; quad2++) {
			if (Intersect(box1.GetQuad(quad1), box2.GetQuad(quad2))) {
				return true;
			}
		}
	}
    
	// STEP 4: there are 2 cases left: 
	// 1 - the box is completely outside the frustum
	// 2 - the box contains the frustum
	// if any point of the frustum is outside any plane 
	// of the box then return failure
	for (unsigned int vertex=0; vertex<8; vertex++) {
		for (unsigned int plane=0; plane<6; plane++) {
			if (UnderPlane(box1.GetVertex(vertex), box2.GetPlane(plane))) {
				return false;
			}
		}
	}
	// yup! the bounding box *contains* the frustum :)
	return true;
}

bool Math::Intersect(const Sphere &sphere, const Box &box) {
	for (unsigned int i=0; i<6; i++) {
		if (UnderPlane(sphere, box.GetPlane(i)))
			return false;
	}
    
	return true;
}

bool Math::Intersect(const Vector3 &point, const Box &box) {
	for (unsigned int i=0; i<6; i++) {
		if (UnderPlane(point, box.GetPlane(i)))
			return false;
	}
    
	return true;
}

bool Math::Intersect(const Quad &quad, const Box &box) {
	for (unsigned int i=0; i<4; i++) {
		if (Intersect(quad.GetVertex(i), box))
			return true;
	}
    
	for (unsigned int i=0; i<6; i++) {
		if (Intersect(quad, box.GetQuad(i)))
			return true;
	}
    
	return false;
}

bool Math::Intersect(const Triangle &triangle, const Box &box) {
	for (unsigned int i=0; i<6; i++) {
		if (UnderPlane(triangle, box.GetPlane(i)))
			return false;
	}
    
	return true;
}

bool Math::UnderPlane(const Vector3& point, const Plane& plane) {
	return (glm::dot (point - plane.GetPosition(), plane.GetNormal()) < 0.0f);
}

bool Math::UnderPlane(const Triangle& triangle, const Plane& plane){
	for (unsigned int i=0; i<3; i++){
		if (!UnderPlane(triangle.GetVertex(i), plane))
			return false;
	}
	return true;
}

bool Math::UnderPlane(const Quad& quad, const Plane& plane) {
	for (unsigned int i=0; i<4; i++) {
		if (!UnderPlane(quad.GetVertex(i), plane))
			return false;
	}
	return true;
}

bool Math::UnderPlane(const Box& box, const Plane& plane) {
	for (unsigned int i=0; i<8; i++) {
		if (!UnderPlane(box.GetVertex(i), plane))
			return false;
	}
	return true;
}

bool Math::UnderPlane(const Sphere& sphere, const Plane& plane) {
	float dist = DistanceFromPlane(sphere.GetCenter(), plane);

	return (dist + sphere.GetRadius() < 0);
}

bool Math::OverPlane(const Vector3& point, const Plane& plane) {
	return (glm::dot (point - plane.GetPosition(), plane.GetNormal()) >= 0.0f);
}

bool Math::OverPlane(const Triangle& triangle, const Plane& plane){
	for (unsigned int i=0; i<3; i++){
		if (!OverPlane(triangle.GetVertex(i), plane))
			return false;
	}
	return true;
}

bool Math::OverPlane(const Quad& quad, const Plane& plane) {
	for (unsigned int i=0; i<4; i++) {
		if (!OverPlane(quad.GetVertex(i), plane))
			return false;
	}
	return true;
}

bool Math::OverPlane(const Box& box, const Plane& plane) {
	for (unsigned int i=0; i<8; i++) {
		if (!OverPlane(box.GetVertex(i), plane))
			return false;
	}
	return true;
}

bool Math::OverPlane(const Sphere &sphere, const Plane &plane) {
	float dist = DistanceFromPlane(sphere.GetCenter(), plane);
    
	return (dist - sphere.GetRadius() > 0);
}

float Math::Clamp(float value, float minimum, float maximum) {
    if (value <= minimum)
        value = minimum;
    else if (value >= maximum)
        value = maximum;
    
    return value;
}

int Math::Clamp(int value, int minimum, int maximum) {
    if (value <= minimum)
        value = minimum;
    else if (value >= maximum)
        value = maximum;
    
    return value;    
}