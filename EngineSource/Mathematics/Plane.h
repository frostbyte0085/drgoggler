#ifndef PLANE_H_DEF
#define PLANE_H_DEF

#include <Platform.h>

enum FrustumPlane
{
	FP_RIGHT,
	FP_LEFT,
	FP_BOTTOM,
	FP_TOP,
	FP_FAR,
	FP_NEAR
};

enum ClassifyPoint {
    CP_FRONT = 1,
    CP_BACK = 2,
    CP_ONPLANE = 3,
    CP_SPLIT = 4
};

class PlaneEquation {
public:
    
    PlaneEquation() {a=b=c=d=0;}
    PlaneEquation(float a, float b, float c, float d) {
        this->a = a;
        this->b = b;
        this->c = c;
        this->d = d;
    }
    
    float a,b,c,d;
};

class Plane {
public:
    Plane ();
    Plane (float a, float b, float c, float d);
    Plane (const PlaneEquation& equation);
    Plane (const Vector4 &p);
    Plane (const Vector3 &normal, const Vector3 &p);
    ~Plane();
    
    void SetPosition (const Vector3 &p) { this->position = position; recalcEquation();}
    const Vector3& GetPosition() const { return position; }
    
    void SetNormal (const Vector3 &normal) { 
        this->normal = normal;
        this->normalLength = glm::length(normal);
        this->normal = glm::normalize(this->normal);
        
        recalcEquation();
    }
    
    const Vector3& GetNormal() const { return normal; }
    
    PlaneEquation GetEquation() const { return equation; }
    
    unsigned int ClassifyPoint (const Vector3 &point);
    
private:
    void recalcEquation();
    
    Vector3 position;
    Vector3 normal;
    float normalLength;
    
    PlaneEquation equation;
};

#endif
