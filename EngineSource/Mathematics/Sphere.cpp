#include "Sphere.h"
#include "Math.h"

Sphere::Sphere() {
    center = Vector3(0,0,0);
    radius = 0.0f;
}

Sphere::Sphere (const Vector3 &center, float radius) {
    this->center = center;
    this->radius = radius;
}

Sphere::Sphere (const Sphere &other) {
    center = other.center;
    radius = other.radius;
}

Sphere::Sphere (const Vector<Vector3> &vertices) {
    Vector3 center(0,0,0);
    
    for (int i=0; i<(int)vertices.size(); i++) {
        center += vertices[i];
    }
    center /= vertices.size();
    
    float radius=0.0f;
    for (int i=0; i<(int)vertices.size(); i++) {
        Vector3 v = vertices[i] - center;
        float len2 = glm::length2(v);
        if (len2 > radius)
            radius = len2;
    }
    radius = sqrtf(radius);
    
    SetRadius(radius);
    SetCenter(center);
}

Sphere::~Sphere() {
    
}

Vector3 Sphere::GetTop() const {
    return center + Vector3(0,radius,0);
}

Vector3 Sphere::GetBottom() const {
    return center - Vector3(0,radius,0);
}

void Sphere::Transform(Matrix44 &m) {
    SetCenter ( glm::transformVector3ByMatrix(center, m) );
    Vector3 scaling;
    Math::ExtractScaling(m, scaling);
    float maxScaling = std::max(scaling.x, std::max(scaling.y, scaling.z));
    SetRadius(GetRadius() * maxScaling);
}

bool Sphere::InsideFrustum(const Vector<Plane> &frustum) {
    for(int i=0; i<(int)frustum.size(); i++) {
        const Plane &p = frustum[i];
		
		PlaneEquation eq = p.GetEquation();
		if (eq.a * center.x + eq.b * center.y + eq.c * center.z + eq.d <= -radius)
            return false;
    }
    return true;
}