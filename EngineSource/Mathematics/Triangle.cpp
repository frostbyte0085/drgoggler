#include "Triangle.h"
#include "Math.h"

Triangle::Triangle() {
    for (int i=0; i<3; i++) {
        vertices[i] = Vector3(0,0,0);
    }
}

Triangle::Triangle(const Vector3 &p1, const Vector3 &p2, const Vector3 &p3) {
    vertices[0] = p1;
    vertices[1] = p2;
    vertices[2] = p3;
}

Triangle::~Triangle() {
    
}

Vector3 Triangle::GetNormal() const {
	return glm::normalize(glm::cross((vertices[1] - vertices[0]), (vertices[2] - vertices[0])));
}

Vector3 Triangle::GetVertex(unsigned int index) const {
	if (index < 3) {
		return vertices[index];
	}
	return Vector3();
}

void Triangle::SetVertex(unsigned int index, const Vector3& v) {
	if (index < 3) {
		vertices[index] = v;
	}
}

Plane Triangle::GetPlane() const {
	return Plane(GetNormal(), vertices[0]);
}

Line Triangle::GetLine(unsigned int index) const {
	if (index < 3) {
		return Line(vertices[index], vertices[(index + 1) % 3]);
	}
	return Line();
}

Vector3 Triangle::GetCenter() const
{
	return (vertices[0] + vertices[1] + vertices[2]) / 3.0f;
}

float Triangle::GetArea()
{
	float a = GetLine(0).GetLength();
	float b = GetLine(0).GetLength();
	float c = GetLine(0).GetLength();
	float semi = (a + b + c) * 0.5f;
	return sqrtf((semi - a) * (semi - b) * (semi - c));
}

Line Triangle::GetSmallLine() const {
	Line A, B, C;
	A = GetLine(0);
	B = GetLine(1);
	C = GetLine(2);
    
	float al = A.GetLength();
	float bl = B.GetLength();
	float cl = C.GetLength();
    
	if (al < bl && al < cl ) return A;
	if (bl < cl) return B;
	return C;
}

Line Triangle::GetMediumLine() const {
	Line A, B, C;
	A = GetLine(0);
	B = GetLine(1);
	C = GetLine(2);
    
	float al = A.GetLength();
	float bl = B.GetLength();
	float cl = C.GetLength();
    
	if (al < bl && al > cl ) return A;
	if (al > bl && al < cl ) return A;
	
	if (bl < al && bl > cl ) return B;
	if (bl > al && bl < cl ) return B;
	
	return C;
}

Line Triangle::GetLargeLine() const {
	Line A, B, C;
	A = GetLine(0);
	B = GetLine(1);
	C = GetLine(2);
    
	float al = A.GetLength();
	float bl = B.GetLength();
	float cl = C.GetLength();
    
	if (al > bl && al > cl ) return A;
	if (bl > cl) return B;
	return C;
}

Plane Triangle::GetEdgePlane(unsigned int i) const
{
	if (i < 3)
	{
		Line line = GetLine(i);
		Vector3 c = line.GetVertex(0) + GetNormal();
		Triangle tri = Triangle(line.GetVertex(0), c, line.GetVertex(1));
		return tri.GetPlane();
	}
	return Plane();
}