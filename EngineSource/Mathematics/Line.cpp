#include "Line.h"
#include "Math.h"

Line::Line() {
	vertices[0] = vertices[1] = Vector3(0, 0, 0);
}

Line::Line(const Vector3& a, const Vector3& b) {
	vertices[0] = a;
	vertices[1] = b;
}

Line::Line(const Ray &ray, float length) {
    vertices[0] = ray.GetOrigin();
    vertices[1] = vertices[0] + ray.GetDirection() * length;
}

void Line::SetVertex(unsigned int index, const Vector3& v) {
	if (index < 2) {
		vertices[index] = v;
	}
}

Ray Line::GetRay() const {
	return Ray(vertices[0], glm::normalize(vertices[1] - vertices[0]));
}

float Line::GetLength() const {
	return glm::length(vertices[1] - vertices[0]);
}

float Line::GetLength2() const {
	return glm::length2(vertices[1] - vertices[0]);
}

bool Line::Inside(const Vector3 &v) const {
	Ray r = GetRay();
	float d = r.GetDistance(v);
    
	if (d < 0) return false;
	if (d > GetLength()) return false;
	
    return true;
}

Vector3 Line::GetMidpoint() const {
	return 0.5f * (vertices[0] + vertices[1]);
}

Line Line::GetReverse() const {
	return Line(vertices[1], vertices[0]);
}