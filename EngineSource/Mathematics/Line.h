#ifndef LINE_H_DEF
#define LINE_H_DEF

#include "Ray.h"

class Line
{
public:
	Line();
	Line(const Vector3& a, const Vector3& b);
    Line(const Ray &ray, float length);
    ~Line() {}
    
	const Vector3& GetVertex(unsigned int index) const {
		return vertices[index];
	}
    
	void SetVertex(unsigned int index, const Vector3& v);
    
	Ray GetRay() const;
	float GetLength() const;
	float GetLength2() const;
    
	bool Inside(const Vector3 &v) const;
    
	Vector3 GetMidpoint() const;
    
	Line GetReverse() const;
    
private:
    Vector3 vertices[2];
};

#endif
