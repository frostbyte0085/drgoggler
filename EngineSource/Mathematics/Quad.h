#ifndef QUAD_H_DEF
#define QUAD_H_DEF

#include <Platform.h>
#include "Plane.h"
#include "Triangle.h"
#include "Line.h"

class Quad {
public:
    Quad();
    Quad(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& d);
    Quad(const Vector4& a, const Vector4& b, const Vector4& c, const Vector4& d);
    ~Quad();
    
    Vector3 GetVertex(unsigned int index) const;
	void SetVertex(unsigned int index, const Vector3& v);
    
    Triangle GetTriangle(unsigned int index) const;
	Vector3 GetNormal() const;
    
	Plane GetPlane() const;
    Plane GetEdgePlane(unsigned int i) const;
    
    Line GetLine(unsigned int index) const {
		return Line(vertices[index], vertices[(index + 1) % 4]);
	}
    
    void Flip();
    
    
private:
    Vector3 vertices[4];
};

#endif
