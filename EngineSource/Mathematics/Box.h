#ifndef BOX_H_DEF
#define BOX_H_DEF

#include "Plane.h"
#include "Quad.h"

class AlignedBox;

class Box
{
public:    
	Box();
    Box(const Box &other);
	Box(const Plane& front, const Plane& back,
		const Plane& left, const Plane& right,
		const Plane& top, const Plane& bottom);
	Box(const Quad& front, const Quad& back);
	
	// Axis aligned box
	Box(const Vector3 &from, const Vector3 &to);
    
	// TODO: move this to AlignedBox and let the copy constructor do its job
	// creates a bounding box for a set of vertices
	Box(const Vector<Vector3> &vertices);
    
    
	Box(const AlignedBox &ab);
    
    void CalculatePlanes();
	void CalculateVertices();

    bool InsideFrustum(const Box &frustum, Matrix44 &transform);
    bool InsideFrustum2(const Vector<Plane> &frustum, Matrix44 &transform);
    
	Quad GetQuad(unsigned int i) const;
    
	const Plane& GetPlane(unsigned int index) const {return planes[index];}
    
	void SetPlane(unsigned int index, const Plane& p);
    
	const Vector3& GetVertex(unsigned int index) const {return vertices[index];}
    
	void SetVertex(unsigned int index, const Vector3& v);
    
	unsigned int ClassifyPlane (Plane &p) const;
    
	Vector3 GetViewer() const;
    
	Vector3 GetCenter() const;
    
	Vector3 GetDiag() const {return vertices[7] - vertices[1];}
    
    void Transform (Matrix44 &m);
    
private:
    Plane planes[6];
	Vector3 vertices[8];
};

#endif
