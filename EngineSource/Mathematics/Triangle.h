#ifndef TRIANGLE_H_DEF
#define TRIANGLE_H_DEF

#include "Plane.h"
#include "Line.h"

class Triangle {
public:
    Triangle();
    Triangle (const Vector3 &p1, const Vector3 &p2, const Vector3 &p3);
    ~Triangle();
    
    Vector3 GetNormal() const;
    Vector3 GetVertex(unsigned int index) const;
    void SetVertex (unsigned int index, const Vector3 &v);

    Plane GetPlane() const;
    // returns a plane that intersects the edge given
	// the new plane has a right angle with the triangle's plane
	// The new plane's normal points inside the triangle
    Plane GetEdgePlane (unsigned int i) const;
  
    Line GetLine(unsigned int index) const;
    
	Line GetSmallLine() const;
	Line GetMediumLine() const;
	Line GetLargeLine() const;
    
    Vector3 GetCenter() const;
    float GetArea();
    
    typedef Vector<Triangle> TriangleList;
    
private:    
    Vector3 vertices[3];

    
};

#endif
