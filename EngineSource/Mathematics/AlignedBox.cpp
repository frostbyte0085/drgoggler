#include "AlignedBox.h"

void AlignedBox::Update() {
	box = Box(from, to);
}

AlignedBox::AlignedBox() {
	from = to = Vector3(0, 0, 0);
}

AlignedBox::AlignedBox(const Box &box) {
	Vector3 minv, maxv;
	minv = maxv = box.GetVertex(0);
    
	for (unsigned int i=1; i<8; i++) {
		Vector3 v = box.GetVertex(i);
		if (v.x < minv.x) minv.x = v.x;
		if (v.y < minv.y) minv.y = v.y;
		if (v.z < minv.z) minv.z = v.z;
        
		if (v.x > maxv.x) maxv.x = v.x;
		if (v.y > maxv.y) maxv.y = v.y;
		if (v.z > maxv.z) maxv.z = v.z;
	}
    
	*this = AlignedBox(minv, maxv);
}

AlignedBox::AlignedBox(const Vector3 &from, const Vector3 &to) {
	this->from = from;
	this->to = to;
	Update();
}

Vector3 AlignedBox::GetFrom() const {
	return from;
}

Vector3 AlignedBox::GetTo() const {
	return to;
}

AlignedBox AlignedBox::GetSubBox(int sb) const {
	Vector3 middle = from + 0.5f * (to - from);
	
    switch (sb) {
		case 0:
			return AlignedBox(
                              Vector3(from.x,		middle.y,	from.z),
                              Vector3(middle.x,	to.y,		middle.z));
		case 1:
			return AlignedBox(
                              Vector3(middle.x,	middle.y,	from.z),
                              Vector3(to.x,		to.y,		middle.z));
		case 2:
			return AlignedBox(
                              Vector3(from.x,		from.y,		from.z),
                              Vector3(middle.x,	middle.y,	middle.z));
		case 3:
			return AlignedBox(
                              Vector3(middle.x,	from.y,		from.z),
                              Vector3(to.x,		middle.y,	middle.z));
            
		case 4:
			return AlignedBox(
                              Vector3(from.x,		middle.y,	middle.z),
                              Vector3(middle.x,	to.y,		to.z));
		case 5:
			return AlignedBox(
                              Vector3(middle.x,	middle.y,	middle.z),
                              Vector3(to.x,		to.y,		to.z));
		case 6:
			return AlignedBox(
                              Vector3(from.x,		from.y,		middle.z),
                              Vector3(middle.x,	middle.y,	to.z));
		case 7:
			return AlignedBox(
                              Vector3(middle.x,	from.y,		middle.z),
                              Vector3(to.x,		middle.y,	to.z));
            
		default:
			break;
	}
	return AlignedBox();
}