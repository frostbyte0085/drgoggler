#ifndef RAY_H_DEF
#define RAY_H_DEF

#include <Platform.h>

class Ray {
public:
    Ray (const Vector3 &origin, const Vector3 &direction);
    Ray();
    
    const Vector3& GetOrigin() const { return origin; }
    void SetOrigin(const Vector3 &origin) { this->origin = origin; }
    const Vector3& GetDirection() const { return direction; }
    void SetDirection(const Vector3 &direction) { this->direction = glm::normalize(direction); }
    
    Vector3 GetPosition (float distance) const;
    float GetDistance(const Vector3 &position) const;
    Vector3 Project(const Vector3 &v) const;
    
private:
    Vector3 origin;
    Vector3 direction;
};

#endif
