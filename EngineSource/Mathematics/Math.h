#ifndef MATH_H_DEF
#define MATH_H_DEF


#include <Platform.h>
#include "GlmExtensions.h"

#include "Plane.h"

#if PLATFORM == PLATFORM_WINDOWS
#ifndef roundf
#define roundf(x) (int) (x + 0.5f)
#endif
#endif

#   define EPS 0.00001f
#   define PI 3.14159265358979323846
#   define PI_OVER_180	 0.017453292519943295769236907684886
#   define PI_OVER_360	 0.0087266462599716478846184538424431
#   define DEG2RAD(x) ((x) * ((PI) / (180.0f)))
#   define RAD2DEG(x) ((x) * ((180.0f) / (PI)))

class Box;
class Triangle;
class Line;
class Sphere;
class Quad;
class Ray;


// some important mathematic methods - populate this with more.
class Math {
public:
	static int Random(int from, int to);
	static float RandomF(float from, float to);
	static double RandomD(double low, double high);

    static float Clamp(float value, float minimum, float maximum);
    static int Clamp(int value, int minimum, int maximum);
    
    static bool IsZero(float x);
    static void ComposeMatrix (Matrix44 &output, Vector3 translation, Quaternion rotation, Vector3 scaling);
    static void DecomposeMatrix (Matrix44 &input, Vector3 &translation, Quaternion &rotation, Vector3 &scaling);
    
    static void ExtractTranslation (Matrix44 &mat, Vector3 &extracted);
    static void ExtractScaling (Matrix44 &mat, Vector3 &extracted);
    static void ExtractRotation (Matrix44 &mat, Quaternion &extracted);

    static Vector3 TransformVectorByQuaternion (const Quaternion &q, const Vector3 &v);
    static Quaternion ComputeQuaternionW (const Quaternion &q);
    
    
    static float DistanceFromPlane (const Vector3 &point, const Plane &plane);
    static Vector3 ProjectToPlane (const Vector3 &point, const Plane &plane);
    static Vector3 Project (const Vector3 &point, const Ray &ray);
    
    // frustum
    static Vector<Plane> CalculateViewFrustum (const Matrix44 &view, const Matrix44 &projection);
    
    // intersections
    static bool Intersect(const Ray &ray, const Plane &plane);
    static bool Intersect(const Ray &ray, const Triangle &triangle);
    static bool Intersect(const Ray &ray, const Quad &quad);
    static bool Intersect(const Line& line, const Triangle& triangle);
    static bool Intersect(const Triangle& a, const Triangle& b);
    static bool Intersect(const Line& line, const Quad& quad);
    static bool Intersect(const Line& line, const Sphere& sphere);
    static bool Intersect(const Triangle& triangle, const Quad& quad);
    static bool Intersect(const Quad& a, const Quad& b);
    static bool Intersect(const Line& line, const Plane& p);
    static bool Intersect(const Sphere& s, const Plane& p);
    static bool Intersect(const Sphere &s, const Triangle &t);
    static bool Intersect(const Line &line, const Box &box);
    static bool Intersect(const Ray &ray, const Box &box);
    static bool Intersect(const Ray &ray, const Sphere &sphere);
    static bool Intersect(const Box &box1, const Box &box2);
    static bool Intersect(const Sphere &sphere, const Box &box);
    static bool Intersect(const Vector3 &point, const Box &box);
    static bool Intersect(const Quad &quad, const Box &box);
    static bool Intersect(const Triangle &triangle, const Box &box);
    static bool Intersect(const Sphere &a, const Sphere &b);
    
    static Ray RayOfIntersection(const Plane& a, const Plane& b);
    
    static Vector3 PointOfIntersection(const Ray& ray, const Plane& plane);
    static Vector3 PointOfIntersection(const Ray &ray, const Triangle &triangle);
    static Vector3 PointOfIntersection(const Plane& a, const Plane& b, const Plane& c);
    static Vector3 PointOfIntersection(const Ray &ray, const Sphere &sphere);
    static Vector3 PointOfIntersection(const Line &line, const Triangle &triangle);
    static Vector3 PointOfIntersection(const Line &line, const Plane &plane);
    
    static bool UnderPlane(const Vector3& point, const Plane& plane);
    static bool UnderPlane(const Triangle& triangle, const Plane& plane);
    static bool UnderPlane(const Quad& quad, const Plane& plane);
    static bool UnderPlane(const Box& box, const Plane& plane);
    static bool UnderPlane(const Sphere& sphere, const Plane& plane);
    
    static bool OverPlane(const Vector3& point, const Plane& plane);
    static bool OverPlane(const Triangle& triangle, const Plane& plane);
    static bool OverPlane(const Quad& quad, const Plane& plane);
    static bool OverPlane(const Box& box, const Plane& plane);
    static bool OverPlane(const Sphere &sphere, const Plane &plane);
    
private:
    static unsigned int fourVertsUnder(Plane plane, Vector3 vertices[]);
    static unsigned int fourVertsSameSide(Plane plane, Vector3 vertices[]);
    static unsigned int fourVertsPlane(Plane plane, Vector3 vertices[]);
};

#endif
