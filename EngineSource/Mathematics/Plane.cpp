#include "Plane.h"

Plane::Plane() {
    position = Vector3(0,0,0);
    normal = Vector3(0,1,0);
}

Plane::Plane(float a, float b, float c, float d) {

    normal = Vector3(a,b,c);
    normalLength = glm::length (normal);
    normal = glm::normalize(normal);
    
    a /= normalLength;
    b /= normalLength;
    c /= normalLength;
    d /= normalLength;
    
    position = d * normalLength * normal;
    
    equation = PlaneEquation (a,b,c,d);
}

Plane::Plane(const Vector4 &p) {
    *this = Plane (p.x, p.y, p.z, p.w);
}

Plane::Plane (const PlaneEquation &equation) {
    *this = Plane(equation.a, equation.b, equation.c, equation.d);
}

void Plane::recalcEquation() {
    normal = glm::normalize (normal);
    
    equation.a = normal.x * normalLength;
    equation.b = normal.y * normalLength;
    equation.c = normal.z * normalLength;
    
    equation.d = glm::dot (normal, position) / normalLength;
}

Plane::Plane (const Vector3 &normal, const Vector3 &p) {
    this->normalLength = glm::length(normal);
    this->normal = glm::normalize( normal );
    this->position = p;
    
    recalcEquation();
}

Plane::~Plane() {
    
}

unsigned int Plane::ClassifyPoint(const Vector3 &point) {
    Vector3 direction = position - point;
    float result = glm::dot(normal, direction);
    
    if (result < -0.01f)
        return CP_FRONT;
    
    if (result > 0.01f)
        return CP_BACK;
    
    return CP_ONPLANE;
}