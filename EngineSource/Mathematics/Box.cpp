#include "Box.h"
#include "Math.h"
#include "AlignedBox.h"
#include <Utilities/StringOperations.h>
#include "Plane.h"

void Box::CalculatePlanes() {
	for (unsigned int i=0; i<6; i++) {
		Quad q = GetQuad(i);
		planes[i] = Plane(q.GetNormal(), q.GetVertex(0));
	}
}

void Box::CalculateVertices() {
	vertices[0] = Math::PointOfIntersection(planes[FP_FAR], planes[FP_TOP], planes[FP_LEFT]);
	vertices[1] = Math::PointOfIntersection(planes[FP_FAR], planes[FP_BOTTOM], planes[FP_LEFT]);
	vertices[2] = Math::PointOfIntersection(planes[FP_FAR], planes[FP_BOTTOM], planes[FP_RIGHT]);
	vertices[3] = Math::PointOfIntersection(planes[FP_FAR], planes[FP_TOP], planes[FP_RIGHT]);
    
	vertices[4] = Math::PointOfIntersection(planes[FP_NEAR], planes[FP_TOP], planes[FP_LEFT]);
	vertices[5] = Math::PointOfIntersection(planes[FP_NEAR], planes[FP_BOTTOM], planes[FP_LEFT]);
	vertices[6] = Math::PointOfIntersection(planes[FP_NEAR], planes[FP_BOTTOM], planes[FP_RIGHT]);
	vertices[7] = Math::PointOfIntersection(planes[FP_NEAR], planes[FP_TOP], planes[FP_RIGHT]);
}

Box::Box(const Box &other) {
    for (int i=0; i<8; i++) {
        vertices[i] = other.vertices[i];
    }
    
    for (int i=0; i<6; i++) {
        planes[i] = other.planes[i];
    }
}

Box::Box()
{
	for (unsigned int i=0; i<6; i++) {
		planes[i] = Plane();
	}
    
	for (unsigned int i=0; i<8; i++) {
		vertices[i] = Vector3(0, 0, 0);
	}
}

Box::Box(const Plane& front, const Plane& back,
         const Plane& left, const Plane& right,
         const Plane& top, const Plane& bottom) {
	planes[0] = front;
	planes[1] = back;
	planes[2] = left;
	planes[3] = right;
	planes[4] = top;
	planes[5] = bottom;
    
	CalculateVertices();
}

Box::Box(const Quad& front, const Quad& back) {
	vertices[0] = front.GetVertex(0);
	vertices[1] = front.GetVertex(1);
	vertices[2] = front.GetVertex(2);
	vertices[3] = front.GetVertex(3);
    
	vertices[4] = back.GetVertex(0);
	vertices[5] = back.GetVertex(1);
	vertices[6] = back.GetVertex(2);
	vertices[7] = back.GetVertex(3);
    
	CalculatePlanes();
}

// Axis aligned box
Box::Box(const Vector3 &from, const Vector3 &to) {
	Vector3 size = to - from;
	Quad front(
               from + Vector3(0, size.y, 0),
               from + Vector3(0, 0, 0),
               from + Vector3(size.x, 0, 0),
               from + Vector3(size.x, size.y, 0));
	Quad back(
              front.GetVertex(0) + Vector3(0, 0, size.z),
              front.GetVertex(1) + Vector3(0, 0, size.z),
              front.GetVertex(2) + Vector3(0, 0, size.z),
              front.GetVertex(3) + Vector3(0, 0, size.z));
    
	*this = Box(front, back);
}

Box::Box(const Vector<Vector3> &vertices) {
	if (vertices.size()==0) {
		*this = Box(Vector3(0, 0, 0), Vector3(0, 0, 0));
		return;
	}
	
	Vector3 from, to;
	from = to = vertices[0];
	for (unsigned int i=1; i<vertices.size(); i++) {
		if (vertices[i].x < from.x) from.x = vertices[i].x;
		if (vertices[i].y < from.y) from.y = vertices[i].y;
		if (vertices[i].z < from.z) from.z = vertices[i].z;
        
		if (vertices[i].x > to.x) to.x = vertices[i].x;
		if (vertices[i].y > to.y) to.y = vertices[i].y;
		if (vertices[i].z > to.z) to.z = vertices[i].z;
	}
	
	*this = Box(from, to);
}

Box::Box(const AlignedBox &ab) {
	*this = ab.GetBox();
}

Quad Box::GetQuad(unsigned int i) const {
	switch (i) {
		case FP_FAR:
			return Quad(vertices[3], vertices[0], vertices[1], vertices[2]);
		case FP_NEAR:
			return Quad(vertices[4], vertices[7], vertices[6], vertices[5]);
		case FP_LEFT:
			return Quad(vertices[0], vertices[4], vertices[5], vertices[1]);
		case FP_RIGHT:
			return Quad(vertices[3], vertices[2], vertices[6], vertices[7]);
		case FP_TOP:
			return Quad(vertices[0], vertices[3], vertices[7], vertices[4]);
		case FP_BOTTOM:
			return Quad(vertices[2], vertices[1], vertices[5], vertices[6]);
	}
	return Quad();
}

void Box::SetPlane(unsigned int index, const Plane& p) {
	if (index < 6) {
		planes[index] = p;
		CalculateVertices();
	}
}

void Box::SetVertex(unsigned int index, const Vector3& v) {
	if (index < 8) {
		vertices[index] = v;
		CalculatePlanes();
	}
}

unsigned int Box::ClassifyPlane (Plane &p) const {
	unsigned int front=0, back=0;
    
	for (int i = 0; i < 8; i ++) {
		unsigned int cp = p.ClassifyPoint (vertices[i]);
		switch (cp) {
            case CP_FRONT:
                front++;
                break;
            case CP_BACK:
                back++;
                break;
            case CP_ONPLANE:
                front++;
                back++;
                break;
		}
	}
    
	if ((front == 8) && (back == 8)) {
		return CP_ONPLANE;
	}
	if (front == 8) {
		return CP_FRONT;
	}
	if (back == 8) {
		return CP_BACK;
	}
	return CP_SPLIT;
}

Vector3 Box::GetViewer() const {
	Ray r = Math::RayOfIntersection(GetPlane(FP_TOP), GetPlane(FP_BOTTOM));
	return Math::PointOfIntersection(r, GetPlane(FP_RIGHT));
}

Vector3 Box::GetCenter() const {
	Vector3 sum(0, 0, 0);
	for (unsigned int i=0; i<8; i++) {
		sum += vertices[i];
	}
    sum.x /= 8;
    sum.y /= 8;
    sum.z /= 8;
    
	return sum;
}

bool Box::InsideFrustum (const Box &frustum, Matrix44 &transform) {
    
    Box transformed = *this;
    
    transformed.Transform ( transform );
    
    return Math::Intersect (frustum, transformed);
}

bool Box::InsideFrustum2 (const Vector<Plane> &frustum, Matrix44 &transform) {
    
    Box transformed = *this;
    
    transformed.Transform ( transform );
    
    Vector<Plane> planes = frustum;
    
    for (int i=0; i<(int)planes.size(); i++) {
        Plane &p = planes[i];
        
        for (int j=0; j<8; j++) {
            if (Math::DistanceFromPlane(transformed.GetVertex(j), p) < 0)
                return true;
        }
    }
    
    return false;
}

void Box::Transform(Matrix44 &m) {
    
    for (unsigned int i=0; i<8; i++) {
        vertices[i] = glm::transformVector3ByMatrix (vertices[i], m);
    }
    
    CalculatePlanes();
}