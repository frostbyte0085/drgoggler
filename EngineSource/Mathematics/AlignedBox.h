#ifndef ALIGNED_BOX_H_DEF
#define ALIGNED_BOX_H_DEF

#include "Box.h"

class AlignedBox {
public:
    
	AlignedBox();
	AlignedBox(const Box &box);
	AlignedBox(const Vector3 &from, const Vector3 &to);
    ~AlignedBox() {} 
    
	Vector3 GetFrom() const;
	Vector3 GetTo() const;
    
	Box GetBox() const {return box;};
	AlignedBox GetSubBox(int sb) const;
    
	void Update();
    
private:
	Box box;
    Vector3 from, to;
};

#endif
