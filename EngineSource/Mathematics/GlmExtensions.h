#ifndef GLM_EXTENSIONS_H_DEF
#define GLM_EXTENSIONS_H_DEF

#include <Platform.h>

namespace glm {
    //http://willperone.net/Code/quaternion.php
    static Quaternion slerp (const Quaternion &q1, const Quaternion &q2, float t) {
        Quaternion q3;
        float dot = glm::dot(q1, q2);
        
        /*	dot = cos(theta)
         if (dot < 0), q1 and q2 are more than 90 degrees apart,
         so we can invert one to reduce spinning	*/
        if (dot < 0)
        {
            dot = -dot;
            q3 = -q2;
        } else q3 = q2;
        
        if (dot < 1.0f)
        {
            float angle = acosf(dot);
            return (q1*sinf(angle*(1-t)) + q3*sinf(angle*t))/sinf(angle);
        } else // if the angle is small, use linear interpolation								
            return glm::normalize(q1*(1-t) + q2*t);
    }
    
    static Vector3 transformVector3ByMatrix (const Vector3 &v, const Matrix44 &m) {
        Vector3 ret;
        
        Vector4 v4 = Vector4(v.x, v.y, v.z, 1.0f);
        Vector4 vt4 = m * v4;
        ret = Vector3(vt4.x, vt4.y, vt4.z);
        
        return ret;
    }
    
    static Vector4 transformVector4ByMatrix (const Vector4 &v, const Matrix44 &m) {
        return m * v;
    }
}

#endif
