#ifndef MATHRECTANGLE_H_DEF
#define MATHRECTANGLE_H_DEF

#include <Platform.h>

#include "Math.h"

class MathRect {
public:
	MathRect() { X=Y=Width=Height=0.0f;}
    MathRect(float x, float y, float width, float height) { X = x; Y = y; Width = width; Height = height; }
    MathRect(const Vector2 &from, const Vector2 &to) { X = from.x; Y = from.y; Width = to.x; Height = to.y; }
    
    float X;
    float Y;
    float Width;
    float Height;
};


#endif
