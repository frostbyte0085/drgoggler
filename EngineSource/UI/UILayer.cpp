#include "UILayer.h"
#include "Widgets/UIButton.h"
#include "Widgets/UITexture.h"
#include "Widgets/UITextLabel.h"
#include "Widgets/UIWindow.h"

#include <Graphics/Material/Material.h>
#include <Graphics/Texture/Texture.h>
#include <Resources/MaterialResource.h>
#include <Resources/ResourceManager.h>
#include <Graphics/Shader/Shader.h>
#include <Resources/ShaderResource.h>
#include <Graphics/Font/Font.h>

#include <Graphics/GraphicsContext/GraphicsContext.h>

#include <Input/Input.h>

#include <Audio/AudioSource/AudioSource.h>

#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>

/*
 Parse the Zwoptex texture atlas .xml and store the different elements in the TextureAtlasMapping structure.
 
 By default, the virtual resolution is set to 1920x1080.
*/
UILayer::UILayer(const String &atlas) {
    
    clickSource = nullptr;
    
    String atlasFullPath = Filesystem::GetFullPathForFile(atlas);
    
    String fullPath = atlasFullPath;
    if (Filesystem::FileExists(atlas)) {
        
        String propertyValue;
        
        XMLNode mainNode = XMLNode::openFileHelper(fullPath.c_str(), "TextureAtlas");
        
        if (!mainNode.isEmpty()) {
            propertyValue = mainNode.getAttribute("materialPath");
            
            atlasMaterial = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor(propertyValue, false))->MaterialObject;
            
            // go through all the SubTexture nodes
            for (int i=0; i<mainNode.nChildNode("SubTexture"); i++) {
                XMLNode node = mainNode.getChildNode("SubTexture", i);
                if (!node.isEmpty()){
                    TextureAtlasMapping nodeMapping;
                    
                    String nodeName = node.getAttribute("name");
                    nodeName = nodeName.substr(0, nodeName.find_last_of("."));
                    
                    nodeMapping.x = StringOperations::FromString<int>(node.getAttribute("x"));
                    nodeMapping.y = StringOperations::FromString<int>(node.getAttribute("y"));
                    nodeMapping.width = StringOperations::FromString<int>(node.getAttribute("width"));
                    nodeMapping.height = StringOperations::FromString<int>(node.getAttribute("height"));
                    
                    atlasMappings[nodeName] = nodeMapping;
                }
            }
        }
        
    }
    
    //  create the material that will be used for rendering the textured quads. This will be copied around.
    String materialName = "TextureQuadMaterial";
    
    texturedQuadMaterial = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor(materialName, true))->MaterialObject;
    
    texturedQuadMaterial->SetShader (ResourceManager::Create<SHADER>(new ShaderResourceDescriptor("Data/Shaders/UI.shader"))->ShaderObject);
    texturedQuadMaterial->SetBlending(true);
    texturedQuadMaterial->SetBlendingFunction(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    texturedQuadMaterial->SetDiffuseMapUniform ("uiTexture");

    // create the material that will be used for rendering the text labels. This will be copied around.
    materialName = "TextLabelMaterial";
    
    textLabelMaterial = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor(materialName, true))->MaterialObject;
    
    textLabelMaterial->SetShader (ResourceManager::Create<SHADER>(new ShaderResourceDescriptor("Data/Shaders/UI.shader"))->ShaderObject);
    textLabelMaterial->SetBlending(true);
    textLabelMaterial->SetBlendingFunction(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    textLabelMaterial->SetDiffuseMapUniform ("uiTexture");
    
    
    SetVirtualResolution(1920, 1080);
}

UILayer::~UILayer() {
    
    atlasMappings.clear();
}

void UILayer::SetVirtualResolution (int width, int height) {
    vrWidth = width;
    vrHeight = height;
    
    float vTop = 0.0f;
	float vLeft = 0.0f;
	float vWidth = (float)vrWidth;
	float vHeight = (float)vrHeight;
    
    orthoProjection[0] = vLeft;
    orthoProjection[1] = vTop;
    orthoProjection[2] = vWidth;
    orthoProjection[3] = vHeight;
}

/*
 The Button element is comprised by the UIButton and the button-centered UITextLabel.
*/
bool UILayer::Button (Font *font, const Vector2 &from, const Vector2 &to, const String &text) {
    assert (font);
    
    UIButton *uiButton = new UIButton(this, atlasMaterial, from, to);
    
    bool clicked = widgetClicked(uiButton);
    bool hover = widgetHovered(uiButton);
    bool pressed = widgetPressed(uiButton);
    
    // set the state accordingly so the new button is rendered with the correct subtexture from the texture atlas.
    if (hover)
        uiButton->SetState(WS_HOVERED);
    
    if (pressed || clicked)
        uiButton->SetState(WS_PRESSED);
    
    uiButton->MakeWidget();
    
    widgets.push_back(uiButton);
    
    // add the button text
    float textWidth = font->GetTextWidth (text);
    float textHeight = (float)font->GetLineHeight();
    float buttonWidth = fabs(to.x - from.x);
    float buttonHeight = fabs(to.y - from.y);
    
    float halfTextWidth = ceilf(textWidth/2);
    
    Vector2 textPos (from.x + buttonWidth/2 - halfTextWidth, from.y + buttonHeight/2 - textHeight/2);
    TextLabel(font, textPos, text);
    
    return clicked;
}

void UILayer::Window (const Vector2 &from, const Vector2 &to, const Vector4 &color) {
    UIWindow *uiWindow = new UIWindow (this, atlasMaterial, from, to, color);
    
    uiWindow->SetState(WS_NORMAL);
    
    uiWindow->MakeWidget();
    widgets.push_back(uiWindow);
}

void UILayer::TexturedQuad (Texture *texture, const Vector2 &position, const Vector2 &scaling, const Vector4 &color) {
    assert (texture);
    
    Vector2 quadSize (texture->GetWidth() * scaling.x, texture->GetHeight() * scaling.y);
    
    UITexture *uiTexture = new UITexture(this, texturedQuadMaterial, texture, position, position + quadSize, color);
    
    uiTexture->MakeWidget();
    
    widgets.push_back (uiTexture);
}

void UILayer::TextLabel(Font *font, const Vector2 &pos, const String &text, const Vector4 &color) {
    assert (font);
    
    Vector2 newPos = Vector2 ((int)pos.x, (int)pos.y);
    
    UITextLabel *uiLabel = new UITextLabel(this, textLabelMaterial, font, newPos, text, color);
    
    uiLabel->MakeWidget();
    
    widgets.push_back (uiLabel);
}

void UILayer::Clear() {
    for (int i=0; i<(int)widgets.size(); i++) {
        UIWidget *widget = widgets[i];
        if (widget)
            delete widget;
    }
    widgets.clear();
}

/*
 This method is used by the widgetClicked to transform the mouse coordinates from actual resolution space
 to virtual resolution space.
*/
Vector2 UILayer::transformMouseCoordinate (const Vector2 &coord) {
    // transform the mouse coords to virtual screen mouse coords.
    Vector2 virtualRes (GetVirtualWidth(), GetVirtualHeight());
    Vector2 virtualPos;
    
    Vector2 actualRes (GraphicsContext::GetCurrentDisplayMode().Width, GraphicsContext::GetCurrentDisplayMode().Height);
    Vector2 actualPos = coord;
    
    float widthDif = actualRes.x / virtualRes.x;
    float heightDif = actualRes.y / virtualRes.y;
    
    virtualPos = Vector2 (actualPos.x / widthDif, actualPos.y / heightDif);
    
    return virtualPos;
}

/*
 Check if the UIWidget is clicked
*/
bool UILayer::widgetClicked (UIWidget *widget) {
    assert (widget);
    Vector2 virtualMousePos = transformMouseCoordinate(Input::GetMousePosition());
    
    bool clicked = Input::IsClicked("MenuClick") && widget->CheckInside(virtualMousePos);
    
    if (clicked && clickSource)
        clickSource->Play();
    
    return clicked;
}

/*
 Check if the UIWidget is pressed
*/
bool UILayer::widgetPressed (UIWidget *widget) {
    assert (widget);
    Vector2 virtualMousePos = transformMouseCoordinate(Input::GetMousePosition());
    
    
    bool pressed = (Input::GetValue("MenuClick") > 0) && widget->CheckInside(virtualMousePos);
    
    return pressed;
}

/*
 Check if the UIWidget is hovered by the mouse
*/
bool UILayer::widgetHovered(UIWidget *widget) {
    assert (widget);
    Vector2 virtualMousePos = transformMouseCoordinate(Input::GetMousePosition());
    
    bool hovered = widget->CheckInside(virtualMousePos);
    
    return hovered;
}

/*
 Return a TextureAtlasMapping correspoding to the subtexture name.
*/
TextureAtlasMapping UILayer::GetAtlasMapping(const String &name) {
    HashMap<String, TextureAtlasMapping>::const_iterator it = atlasMappings.find(name);
    if (it != atlasMappings.end())
        return it->second;
    return TextureAtlasMapping();
}