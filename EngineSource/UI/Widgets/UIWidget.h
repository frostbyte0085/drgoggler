/*
 Author: 
 Pantelis Lekakis

 Description:
 The UIWidget class is the base for all UI elements, containing a list of vertices, texture coordinates and indices
 that each widget can build to their needs.
 Some UIWIdgets (like buttons for example) can have states, a state can either be normal, pressed or hovered. Each state
 corresponds to a different subtexture in the texture atlas.
 
*/

#ifndef UI_WIDGET_H_DEF
#define UI_WIDGET_H_DEF

#include <Platform.h>
#include <Mathematics/Math.h>

class Material;

enum WidgetType {
    WT_BUTTON,
    WT_TEXTURE,
    WT_TEXTLABEL,
    WT_WINDOW
};

enum WidgetState {
    WS_NORMAL,
    WS_PRESSED,
    WS_HOVERED
};

class UILayer;

class UIWidget {
    friend class UIRenderer;
    
public:
    UIWidget();
    virtual ~UIWidget();
    
    virtual void MakeWidget ();
    
    virtual void MakeVertices()=0;
    virtual void MakeIndices()=0;
    virtual void MakeTextureCoords()=0;
    
    virtual WidgetType GetWidgetType()const =0;
    bool CheckInside (const Vector2 &point);

    void SetState (WidgetState state) { this->state = state; }
    
protected:
    WidgetState state;
    UILayer *ui;
    Vector<Vector2> vertices;
    Vector<unsigned short> indices;
    Vector<Vector2> textureCoordinates;
    Vector4 color;
    
    Vector2 from, to;
    Material *material;
private:
    
};

#endif
