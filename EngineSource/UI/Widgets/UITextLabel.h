/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The UITextLabel uses a Font class instance to draw single line, bitmapped text.
 */
#ifndef UI_TEXT_LABEL_H_DEF
#define UI_TEXT_LABEL_H_DEF

#include "UIWidget.h"

class Font;
class Material;
class UILayer;

class UITextLabel : public UIWidget {
public:
    UITextLabel(UILayer *ui, Material *material, Font *font, const Vector2 &pos, const String &text, const Vector4 &color);
    virtual ~UITextLabel();
    
    WidgetType GetWidgetType() const;

    void MakeIndices();
    void MakeVertices();
    void MakeTextureCoords();
    void MakeWidget();
private:
    Font *font;
    String text;
};

#endif
