#include "UITexture.h"
#include <Graphics/Material/Material.h>
#include "../UILayer.h"

UITexture::UITexture (UILayer *ui, Material* material, Texture *texture, const Vector2 &from, const Vector2 &to, const Vector4 &color) {
    assert (material);
    assert (texture);
    
    this->color = color;
    this->ui = ui;
    // copy the material to allow for many textured quads with different textures.
    // this functionality should be in the ResourceManager, note this for the next semester's engine.
    this->material = new Material(material);
    this->material->SetDiffuseMap(texture);
    
    this->from = from;
    this->to = to;
    
    MakeWidget();
}

UITexture::~UITexture() {
    if (material)
        delete material;
}

WidgetType UITexture::GetWidgetType() const {
    return WT_TEXTURE;
}

void UITexture::MakeTextureCoords() {
    // make tex coords form the atlas map here.
    textureCoordinates.push_back ( Vector2(0,0) );
    textureCoordinates.push_back ( Vector2(1,0) );
    textureCoordinates.push_back ( Vector2(1,1) );
    textureCoordinates.push_back ( Vector2(0,1) );
}

void UITexture::MakeVertices() {
    vertices.push_back ( Vector2 (from.x, from.y) );
    vertices.push_back ( Vector2 (to.x, from.y) );
    vertices.push_back ( Vector2 (to.x, to.y) );
    vertices.push_back ( Vector2 (from.x, to.y) );
}

void UITexture::MakeIndices() {
    indices.push_back (0);
    indices.push_back (1);
    indices.push_back (2);
    indices.push_back (2);
    indices.push_back (3);
    indices.push_back (0);
}