/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The UIWindow is similar to a UIButton, only that it's not clickable.
 */
#ifndef UI_WINDOW_H_DEF
#define UI_WINDOW_H_DEF

#include "UIWidget.h"

class Material;
class Font;
class UILayer;

class UIWindow : public UIWidget {
public:
    UIWindow(UILayer *ui, Material* material, const Vector2 &from, const Vector2 &to, const Vector4 &color);
    virtual ~UIWindow();
    
    WidgetType GetWidgetType() const;
    
    void MakeWidget ();
    
    void MakeIndices();
    void MakeVertices();
    void MakeTextureCoords();
    
private:
};

#endif
