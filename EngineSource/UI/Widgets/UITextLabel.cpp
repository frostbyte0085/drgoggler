#include "UITextLabel.h"
#include <Graphics/Font/Font.h>
#include <Graphics/Material/Material.h>
#include <Graphics/Texture/Texture.h>
#include "../UILayer.h"

UITextLabel::UITextLabel (UILayer *ui, Material *material, Font* font, const Vector2 &pos, const String &text, const Vector4 &color) {
    assert (font);
    
    this->color = color;
    this->ui = ui;
    this->font = font;
    this->text = text;
    
    this->material = new Material(material);

    float textWidth = font->GetTextWidth (text);
    float textHeight = (float)font->GetLineHeight();
    
    this->from = pos;
    this->to = Vector2(this->from.x + textWidth, this->from.y + textHeight);
}

UITextLabel::~UITextLabel() {
    if (material)
        delete material;
}

WidgetType UITextLabel::GetWidgetType() const {
    return WT_TEXTLABEL;
}

/*
 Generate all the vertex, texture coordinate and index data for this TextLabel element.
*/
void UITextLabel::MakeWidget() {
    UIWidget::MakeWidget();
    
    float posx = (float)((int)from.x);
    
    bool first = true;
    
    String::const_iterator it;
    int index=0;
    
    for (it=text.begin(); it!=text.end(); it++, index++) {
        int kern = 0;
        
        if (!first)
            kern = font->GetKerning((int)(*(it-1)), (int)(*it));
        
        first = false;
        
        posx += kern;
        Font::CharData charData = font->GetChar((int)(*it));
        if (charData.valid) {
            
            // we only support one page per font, but this is never a problem really.
            Texture *page = font->GetPage(charData.page);
            material->SetDiffuseMap(page);
            
            float pageWidth = (float)page->GetWidth();
            float pageHeight = (float)page->GetHeight();
            
            float cdX = (float)charData.x;
            float cdY = (float)charData.y;
            float cdWidth = (float)charData.width;
            float cdHeight = (float)charData.height;
            float cdXOffset = (float)charData.xOffset;
            float cdYOffset = (float)charData.yOffset;
            
            float frameX = (float)(from.x - cdX + cdXOffset);
            float frameY = (float)(from.y - cdY + cdYOffset);
            float windowX = cdX;
            float windowY = cdY;
            float windowWidth = cdWidth;
            float windowHeight = cdHeight;
            
            textureCoordinates.push_back(Vector2((frameX * pageWidth + windowX)/pageWidth, (frameY * pageHeight + windowY)/pageHeight) );
            textureCoordinates.push_back(Vector2((frameX * pageWidth + windowX + windowWidth)/pageWidth, (frameY * pageHeight + windowY)/pageHeight) );
            textureCoordinates.push_back(Vector2((frameX * pageWidth + windowX + windowWidth)/pageWidth, (frameY * pageHeight + windowY + windowHeight)/pageHeight));
            textureCoordinates.push_back(Vector2((frameX * pageWidth + windowX)/pageWidth, (frameY * pageHeight + windowY + windowHeight)/pageHeight) );
            
            float finalPosX = posx - cdX + cdXOffset + windowX;
            float finalPosY = from.y - cdY + cdYOffset + windowY;

            
            vertices.push_back(Vector2(finalPosX, finalPosY) );
            vertices.push_back(Vector2(finalPosX + windowWidth, finalPosY) );
            vertices.push_back(Vector2(finalPosX + windowWidth, finalPosY + windowHeight) );
            vertices.push_back(Vector2(finalPosX, finalPosY + windowHeight) );
            
            
            indices.push_back ((index*4)+0);
            indices.push_back ((index*4)+1);
            indices.push_back ((index*4)+2);
            indices.push_back ((index*4)+2);
            indices.push_back ((index*4)+3);
            indices.push_back ((index*4)+0);
            
            posx += charData.xAdvance;
        }
        else {
            posx += 10;
        }
    }
    
}

void UITextLabel::MakeTextureCoords() {

}

void UITextLabel::MakeVertices() {

}

void UITextLabel::MakeIndices() {

}