/*
 Author: 
 Pantelis Lekakis
 
 Description:
 UITexture can be used to display a textured quad on the screen.
 */
#ifndef UI_TEXTURE_H_DEF
#define UI_TEXTURE_H_DEF

#include "UIWidget.h"

class Texture;
class UILayer;

class UITexture : public UIWidget {
public:
    UITexture(UILayer *ui, Material* material, Texture *texture, const Vector2 &from, const Vector2 &to, const Vector4 &color);
    virtual ~UITexture();
    
    WidgetType GetWidgetType() const;

    void MakeIndices();
    void MakeVertices();
    void MakeTextureCoords();

};

#endif
