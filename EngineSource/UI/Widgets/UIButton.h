/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The UIButton represents a scaleable clickable button element.
 Text must be added separately, see UILayer::Button for more information
 */
#ifndef UI_BUTTON_H_DEF
#define UI_BUTTON_H_DEF

#include "UIWidget.h"

class Material;
class Font;
class UILayer;

class UIButton : public UIWidget {
public:
    UIButton(UILayer *ui, Material* material, const Vector2 &from, const Vector2 &to);
    virtual ~UIButton();
    
    WidgetType GetWidgetType() const;
    
    void MakeWidget ();
    
    void MakeIndices();
    void MakeVertices();
    void MakeTextureCoords();
    
private:
};

#endif
