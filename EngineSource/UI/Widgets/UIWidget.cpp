#include "UIWidget.h"

UIWidget::UIWidget() {
    material = nullptr;
    ui = nullptr;
    state = WS_NORMAL;
    color = Vector4(1,1,1,1);
}

UIWidget::~UIWidget() {
    vertices.clear();
    textureCoordinates.clear();
    indices.clear();
}

/*
 Check if the specified point is inside the widget drawable area
*/
bool UIWidget::CheckInside(const Vector2 &point) {
    
    if (point.x >= from.x && point.x <= to.x) {
        if (point.y >= from.y && point.y <= to.y){
            return true;
        }
    }
    return false;
}

void UIWidget::MakeWidget () {
    
    MakeVertices();
    MakeIndices();
    MakeTextureCoords();
}