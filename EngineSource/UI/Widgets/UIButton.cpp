#include "UIButton.h"
#include <Graphics/Material/Material.h>
#include "../UILayer.h"
#include <Graphics/Texture/Texture.h>
#include "../UILayer.h"
#include <Graphics/Font/Font.h>

UIButton::UIButton(UILayer *ui, Material *material, const Vector2 &from, const Vector2 &to) {
    assert (material);
    
    this->ui = ui;
    this->material = material;
    
    this->from = from;
    this->to = to;
}

UIButton::~UIButton() {
    
}

WidgetType UIButton::GetWidgetType() const {
    return WT_BUTTON;
}

/*
 Create the button out of 9 different subtextures to generate a scaleable widget mesh.
*/
void UIButton::MakeWidget() {
    UIWidget::MakeWidget();
    
    String stateTextureName = "ButtonNormal";
    if (state == WS_PRESSED)
        stateTextureName = "ButtonPressed";
    else if (state == WS_HOVERED)
        stateTextureName = "ButtonHovered";
    
    TextureAtlasMapping mapping = ui->GetAtlasMapping (stateTextureName);
    Texture *atlas = material->GetDiffuseMap()->second;
    
    float xMarginPixels = 15.0f;
    float yMarginPixels = 15.0f;
    
    float xMargin = xMarginPixels / (float)atlas->GetWidth();
    float yMargin = yMarginPixels / (float)atlas->GetHeight();
    
    float xx = mapping.x / (float)atlas->GetWidth();
    float yy = mapping.y / (float)atlas->GetHeight();
    float wwidth = ( mapping.x + mapping.width) / (float)atlas->GetWidth();
    float hheight = ( mapping.y + mapping.height) / (float)atlas->GetHeight();
    
    int index=0;
    // make top-left corner
    textureCoordinates.push_back ( Vector2(xx,yy) );
    textureCoordinates.push_back ( Vector2(xx+xMargin,yy) );
    textureCoordinates.push_back ( Vector2(xx+xMargin,yy+yMargin) );
    textureCoordinates.push_back ( Vector2(xx,yy+yMargin) );
    
    vertices.push_back ( Vector2 (from.x, from.y) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, from.y) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, from.y+yMarginPixels) );
    vertices.push_back ( Vector2 (from.x, from.y+yMarginPixels) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;

    // make bottom-left corner
    textureCoordinates.push_back ( Vector2(xx,hheight-yMargin) );
    textureCoordinates.push_back ( Vector2(xx+xMargin,hheight-yMargin) );
    textureCoordinates.push_back ( Vector2(xx+xMargin,hheight) );
    textureCoordinates.push_back ( Vector2(xx,hheight) );
    
    vertices.push_back ( Vector2 (from.x, to.y-yMarginPixels) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, to.y-yMarginPixels) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, to.y) );
    vertices.push_back ( Vector2 (from.x, to.y) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;
    
    // make left side
    textureCoordinates.push_back ( Vector2(xx,yy+yMargin) );
    textureCoordinates.push_back ( Vector2(xx+xMargin,yy+yMargin) );
    textureCoordinates.push_back ( Vector2(xx+xMargin,hheight-yMargin) );
    textureCoordinates.push_back ( Vector2(xx,hheight-yMargin) );
    
    vertices.push_back ( Vector2 (from.x, from.y+yMarginPixels) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, from.y+yMarginPixels) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, to.y-yMarginPixels) );
    vertices.push_back ( Vector2 (from.x, to.y-yMarginPixels) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;
    // make top side
    textureCoordinates.push_back ( Vector2(xx + xMargin,yy) );
    textureCoordinates.push_back ( Vector2(wwidth - xMargin,yy) );
    textureCoordinates.push_back ( Vector2(wwidth - xMargin,yy + yMargin) );
    textureCoordinates.push_back ( Vector2(xx + xMargin,yy + yMargin) );
    
    vertices.push_back ( Vector2 (from.x + xMarginPixels, from.y) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, from.y) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, from.y + yMarginPixels) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, from.y + yMarginPixels) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;
    
    // make center region
    textureCoordinates.push_back ( Vector2(xx + xMargin,yy + yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth - xMargin,yy + yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth - xMargin,hheight - yMargin) );
    textureCoordinates.push_back ( Vector2(xx + xMargin,hheight - yMargin) );
    
    vertices.push_back ( Vector2 (from.x + xMarginPixels, from.y + yMarginPixels) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, from.y + yMarginPixels) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, to.y - yMarginPixels) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, to.y - yMarginPixels) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;
    
    // make top-right corner
    textureCoordinates.push_back ( Vector2(wwidth-xMargin,yy) );
    textureCoordinates.push_back ( Vector2(wwidth,yy) );
    textureCoordinates.push_back ( Vector2(wwidth,yy+yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth-xMargin,yy+yMargin) );
    
    vertices.push_back ( Vector2 (to.x-xMarginPixels, from.y) );
    vertices.push_back ( Vector2 (to.x, from.y) );
    vertices.push_back ( Vector2 (to.x, from.y+yMarginPixels) );
    vertices.push_back ( Vector2 (to.x-xMarginPixels, from.y+yMarginPixels) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;
    
    // make right side
    textureCoordinates.push_back ( Vector2(wwidth-xMargin,yy+yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth,yy+yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth,hheight-yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth-xMargin,hheight-yMargin) );
    
    vertices.push_back ( Vector2 (to.x - xMarginPixels, from.y+yMarginPixels) );
    vertices.push_back ( Vector2 (to.x, from.y+yMarginPixels) );
    vertices.push_back ( Vector2 (to.x, to.y-yMarginPixels) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, to.y-yMarginPixels) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    
    index++;
    
    // make bottom-right corner
    textureCoordinates.push_back ( Vector2(wwidth-xMargin,hheight-yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth,hheight-yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth,hheight) );
    textureCoordinates.push_back ( Vector2(wwidth-xMargin,hheight) );
    
    vertices.push_back ( Vector2 (to.x-xMarginPixels, to.y-yMarginPixels) );
    vertices.push_back ( Vector2 (to.x, to.y-yMarginPixels) );
    vertices.push_back ( Vector2 (to.x, to.y) );
    vertices.push_back ( Vector2 (to.x-xMarginPixels, to.y) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
    index++;
    
    // make bottom side
    textureCoordinates.push_back ( Vector2(xx + xMargin,hheight - yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth - xMargin,hheight - yMargin) );
    textureCoordinates.push_back ( Vector2(wwidth - xMargin,hheight) );
    textureCoordinates.push_back ( Vector2(xx + xMargin,hheight) );
    
    vertices.push_back ( Vector2 (from.x + xMarginPixels, to.y - yMarginPixels) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, to.y - yMarginPixels) );
    vertices.push_back ( Vector2 (to.x - xMarginPixels, to.y) );
    vertices.push_back ( Vector2 (from.x + xMarginPixels, to.y) );
    
    indices.push_back ((index*4) + 0);
    indices.push_back ((index*4) + 1);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 2);
    indices.push_back ((index*4) + 3);
    indices.push_back ((index*4) + 0);
}

void UIButton::MakeTextureCoords() {

}

void UIButton::MakeVertices() {

}

void UIButton::MakeIndices() {

}