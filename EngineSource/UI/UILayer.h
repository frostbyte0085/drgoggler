/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The UILayer is the center where all UI issuing happens. Internaly it uses a virtual resolution to achieve
 resolution independency, and all widgets and input checks are normalised to this resolution.
 The texture atlas is generated using the tool Zwoptex for Mac OSX, creating the .png file and the texture coordinate
 .xml file, which are then parsed by the UILayer class.
 
 As an extra, clickable elements can have an AudioSource attached to them for simple "click" sound playback.
 
 Note that a game can only have only one UILayer per scene.
 */
#ifndef UI_LAYER_H_DEF
#define UI_LAYER_H_DEF

#include <Platform.h>

class UIWidget;
class Material;
class Texture;
class Font;

class AudioSource;

struct TextureAtlasMapping {
    int x;
    int y;
    int width;
    int height;
};

class UILayer {
    friend class UIRenderer;
    
public:
    UILayer(const String &atlas);
    virtual ~UILayer();
    
    void SetClickAudioSource (AudioSource *clickSource) { this->clickSource = clickSource; }
    
    bool Button(Font *font, const Vector2 &from, const Vector2 &to, const String &text);
    void TexturedQuad(Texture *texture, const Vector2 &position, const Vector2 &scaling=Vector2(1,1), const Vector4 &color=Vector4(1,1,1,1));
    void TextLabel (Font *font, const Vector2 &pos, const String &text, const Vector4 &color=Vector4(1,1,1,1));
    void Window (const Vector2 &from, const Vector2 &to, const Vector4 &color=Vector4(1,1,1,1));
    
    void SetVirtualResolution (int width, int height);
    int GetVirtualWidth() const { return vrWidth; }
    int GetVirtualHeight() const { return vrHeight; }
    
    TextureAtlasMapping GetAtlasMapping(const String &name);
    
    void Clear();
protected:
    
private:
    GLfloat orthoProjection[4];
    
    AudioSource *clickSource;
    
    int vrWidth;
    int vrHeight;
    
    Vector2 transformMouseCoordinate(const Vector2 &coord);
    
    bool widgetClicked(UIWidget *widget);
    bool widgetPressed(UIWidget *widget);
    bool widgetHovered(UIWidget *widget);
    
    Vector<UIWidget*> widgets;
    
    HashMap<String, TextureAtlasMapping> atlasMappings;
    Material *atlasMaterial;
    Material *texturedQuadMaterial;
    Material *textLabelMaterial;
};

#endif
