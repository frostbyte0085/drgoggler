#include "Filesystem.h"
#include <Utilities/StringOperations.h>

#if PLATFORM == PLATFORM_MACOSX
#   include <MacOSX/BundleSupport.h>
#endif

String Filesystem::personalDirectory;
String Filesystem::applicationName;

void Filesystem::Destroy() {
    PHYSFS_deinit();
    Log ("Filesystem::Destroy: Success");
}

String Filesystem::GetApplicationName() {
    return applicationName;
}

String Filesystem::GetPersonalDirectory() {
	return personalDirectory;
}

String Filesystem::GetApplicationDirectory() {
	return PHYSFS_getBaseDir();
}

/*
 Get the resources directory.
 1) Windows:
    the current directoryr + "Data" prefix.
 2) OSX:
    the bundle contents/resources/data directory.
*/
String Filesystem::GetResourceDirectory() {
#if PLATFORM == PLATFORM_MACOSX
	String resourceDirectory = Bundle_GetContentsDirectory();
    String ret = resourceDirectory + "/Contents/Resources/Data/";
    return ret;
#else
	TCHAR szDirectory[MAX_PATH] = "";
	if(!::GetCurrentDirectory(sizeof(szDirectory) - 1, szDirectory)) {
		Log("Failed to get the working directory\n");
	}
	String resourceDirectory = szDirectory;

    resourceDirectory = resourceDirectory.substr(0, resourceDirectory.rfind (PHYSFS_getDirSeparator()));
    return resourceDirectory + PHYSFS_getDirSeparator() + "Data" + PHYSFS_getDirSeparator();
#endif
}

bool Filesystem::CreateDirectory(const String &path) {
	return (PHYSFS_mkdir (path.c_str()) != 0);
}

bool Filesystem::DeleteDirectory(const String &path) {
	if (PHYSFS_isDirectory (path.c_str()) != 0)
		return (bool)PHYSFS_delete (path.c_str());
	return false;
}

bool Filesystem::DeleteFile (const String &path) {
	if (PHYSFS_isDirectory (path.c_str()) == 0)
		return (bool)PHYSFS_delete (path.c_str());
	return false;
}

bool Filesystem::FileExists (const String &path) {
	if (PHYSFS_isDirectory(path.c_str()) == 0)
		return (PHYSFS_exists (path.c_str()) != 0);
	return false;
}

bool Filesystem::DirectoryExists(const String &path)
{
	if (PHYSFS_isDirectory(path.c_str()) != 0)
		return (PHYSFS_exists (path.c_str()) != 0);
	return false;
}

String Filesystem::GetFileExtension(const String &filename) {
    size_t dotPosition = filename.find_last_of(".");
    if ( dotPosition != String::npos)
        return filename.substr (dotPosition + 1);
    return "";
}

String Filesystem::GetFullPathForFile(const String &path) {
	String dirSeparator = PHYSFS_getDirSeparator();
    String resourceDirectoryWithoutData = GetResourceDirectory();
    size_t idx = resourceDirectoryWithoutData.find_last_of(dirSeparator);

    resourceDirectoryWithoutData = resourceDirectoryWithoutData.substr(0, idx-5);
    String ret = resourceDirectoryWithoutData + dirSeparator + path;

	// replace / with platform specific separators as we are using system paths here
	char platformDirSeparator = '/';
	if (dirSeparator == "\\")
		platformDirSeparator = '\\';

	std::replace (ret.begin(), ret.end(), '/', platformDirSeparator);
	return ret;
}

String Filesystem::GetFullPathForDirectory(const String &path) {
	String dirSeparator = PHYSFS_getDirSeparator();
    String resourceDirectoryWithoutData = GetResourceDirectory();
    size_t idx = resourceDirectoryWithoutData.find_last_not_of("Data/");
    
    resourceDirectoryWithoutData = resourceDirectoryWithoutData.substr(0, idx);
    
	String ret = resourceDirectoryWithoutData + dirSeparator;
	// replace / with platform specific separators as we are using system paths here
	char platformDirSeparator = '/';
	if (dirSeparator == "\\")
		platformDirSeparator = '\\';

	std::replace (ret.begin(), ret.end(), '/', platformDirSeparator);
	return ret;

}

void Filesystem::Initialize() {
#if PLATFORM == PLATFORM_MACOSX
    applicationName = Bundle_GetApplicationName();
#elif PLATFORM == PLATFORM_WINDOWS
    
#endif
    
	String userDir = PHYSFS_getUserDir();
	String dirSeparator = PHYSFS_getDirSeparator();
    
	PHYSFS_setWriteDir (userDir.c_str());
	Mount (PHYSFS_getWriteDir(), "", 0);
    
	String save1 = "";
#if PLATFORM == PLATFORM_WINDOWS || PLATFORM == PLATFORM_MACOSX
	String save2 = "UEA";
    
#   if PLATFORM == PLATFORM_MACOSX
        save1 = "Library/Application Support";
#   endif
#endif
    
	// find the correct save folder:
#if PLATFORM == PLATFORM_WINDOWS
    
    LPITEMIDLIST pidl;
    
    // Get a pointer to an item ID list that represents the path of a special folder
    HRESULT hr = SHGetSpecialFolderLocation(NULL, CSIDL_PERSONAL, &pidl);
    
    // Convert the item ID list's binary representation into a file system path
    wchar_t szPath[_MAX_PATH];
    SHGetPathFromIDListW(pidl, szPath);
    
    // Allocate a pointer to an IMalloc interface
    LPMALLOC pMalloc;
    
    // Get the address of our task allocator's IMalloc interface
    hr = SHGetMalloc(&pMalloc);
    
    // Free the item ID list allocated by SHGetSpecialFolderLocation
    pMalloc->Free(pidl);
    // Free our task allocator
    pMalloc->Release();
    // normalize the path
    
    // convert to utf-8 for PHYSFS
    char utf8_path[_MAX_PATH * 2];
    PHYSFS_utf8FromUcs2((PHYSFS_uint16*)&szPath, utf8_path, _MAX_PATH * 2);
    
    save1 = utf8_path;
    save1 = save1.substr(save1.find_last_of('\\') + 1);
#endif
    
    CreateDirectory (save1 + "/" + save2 + "/" + applicationName); // create our directory
    
	PHYSFS_removeFromSearchPath (PHYSFS_getWriteDir());
    
    personalDirectory = userDir + save1 + dirSeparator + save2 + dirSeparator + applicationName;
    
    String personal = GetPersonalDirectory();
	String base = GetApplicationDirectory();
	String resource = GetResourceDirectory();
    
	PHYSFS_setWriteDir (personal.c_str());
	Mount (personal, "Personal/", 0);
	Mount (resource, "Data/", 1);
    
    Log ("Filesystem::Initialize: Success");
    Log ("  resource path: " + resource);
    Log ("  user path: " + personal);
}

String Filesystem::GetDirectoryForFile(const std::string &path) {    
    size_t index = path.find_last_of('/');
    return path.substr(0, index) + '/';
}

/*
 Enumerate the directories in the specified path, returning a vector of their names.
*/
bool Filesystem::EnumerateDirectories(const String &path, Vector<String> &outDirectoryNames) {
	outDirectoryNames.clear();
    
	bool found = false;
	char **rc = PHYSFS_enumerateFiles(path.c_str());
	char **i;
	String fullPath = "";
    
	for (i = rc; *i != nullptr; i++)
	{
		fullPath = path + *i;
		if (PHYSFS_isDirectory(fullPath.c_str()) != 0)
		{
			found = true;
			outDirectoryNames.push_back (*i);
		}
	}
	PHYSFS_freeList(rc);
	return found;
}

/*
 Enumerate the files in the specified path, returning a vector of their names. Specifying "" for withExtension will
 return all files.
*/
bool Filesystem::EnumerateFiles(const String &path, const String &withExtension, Vector<String> &outFileNames) {
	outFileNames.clear();
    
	bool found = false;
	char **rc = PHYSFS_enumerateFiles(path.c_str());
	char **i;
	String fullPath = "";
    
	for (i = rc; *i != nullptr; i++)
	{
		fullPath = path + *i;
		if (PHYSFS_isDirectory(fullPath.c_str()) == 0)
		{
            if (withExtension != "") {
                String extension = GetFileExtension(fullPath);
                if (extension != withExtension)
                    continue;
            }
            
			found = true;
			outFileNames.push_back (*i);
		}
	}
	PHYSFS_freeList(rc);
	return found;
}

bool Filesystem::Mount(const String &path, const String &mountPoint, int appendToPath) {
	return (PHYSFS_mount (path.c_str(), mountPoint.c_str(), appendToPath) != 0);
}

String Filesystem::GetMountPoint(const String &path) {
	return PHYSFS_getMountPoint (path.c_str());
}
