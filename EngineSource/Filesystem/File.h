/*
 Author: 
 Pantelis Lekakis
 
 Description:
 Open a File in the PHYSFS filesystem, supports write, read and appending to file.
 
 */
#ifndef FILE_H_DEF
#define FILE_H_DEF

#include <Platform.h>

class File
{
public:
	enum FileAccess
	{
		WriteFile,
		ReadFile,
		AppendFile
	};

	File (const String &path, const FileAccess fileAccess);
	File();
	~File();

	bool Open(const String &path, const FileAccess fileAccess);
	bool Flush() const;
	bool SetBuffer (PHYSFS_uint64 buffer_size) const;
	PHYSFS_sint64 Length() const;
	int Seek(PHYSFS_uint64 position) const;
	long Tell() const;
	bool Eof() const;
	PHYSFS_sint64 Write (const void *buffer, PHYSFS_uint32 objectSize, PHYSFS_uint32 objectCount) const;
	PHYSFS_sint64 Read (void *buffer, PHYSFS_uint32 objectSize, PHYSFS_uint32 objectCount) const;
	PHYSFS_sint64 ReadToEOF (void *buffer) const;
	void Close() const;
	bool IsOpen() const;

private:
	bool opened;
	PHYSFS_File *fileHandle;
};

#endif
