/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The FileSystem uses the library PHYSFS to create a virtual file system
 
 Moreover, getting the user directory on specific platforms is available to store game data.
 */
#ifndef FILESYSTEM_H_DEF
#define FILESYSTEM_H_DEF

#include <Platform.h>

class Filesystem {
public:
    static void Initialize();
    static void Destroy();
    
    static String GetFileExtension(const String &filename);
    static String GetApplicationName();
    static String GetPersonalDirectory();
    static String GetApplicationDirectory();
    static String GetResourceDirectory();
    static String GetDirectoryForFile (const String &path);
    static String GetFullPathForFile (const String& path);
    static String GetFullPathForDirectory (const String& path);
    static bool CreateDirectory(const String &path);
    static bool DeleteDirectory(const String &path);
    static bool DeleteFile (const String &path);
    static bool FileExists (const String &path);
    static bool DirectoryExists (const String &path);
    static bool EnumerateDirectories (const String &path, Vector<String>& outDirectoryNames);
    static bool EnumerateFiles (const String &path, const String &withExtension, Vector<String>& outFileNames);
    
    static bool Mount (const String &path, const String &mountPoint, int appendToPath);
    static String GetMountPoint (const String &path);
    
private:
    static String applicationName;
    static String personalDirectory;
};

#endif
