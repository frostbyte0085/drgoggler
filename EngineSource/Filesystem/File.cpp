#include "File.h"
#include <Utilities/Exception.h>

File::File(const String &path, const FileAccess fileAccess) : opened(false), fileHandle(nullptr) {
	Open (path, fileAccess);
}

File::File() : opened(false), fileHandle(nullptr) {

}

File::~File() {
	Close();
}

bool File::Open(const String &path, const FileAccess fileAccess) {
	opened = true;
	switch (fileAccess)
	{
	case File::WriteFile:
		if ( (fileHandle = PHYSFS_openWrite(path.c_str())) == nullptr)
			opened = false;
		break;
	case File::ReadFile:
		if ( (fileHandle = PHYSFS_openRead(path.c_str())) == nullptr)
			opened = false;
		break;
	case File::AppendFile:
		if ( (fileHandle = PHYSFS_openAppend(path.c_str())) == nullptr)
			opened = false;
		break;
	default:
		opened = false;
		break;
	}
	return opened;
}

void File::Close() const {
	PHYSFS_close (fileHandle);
}

bool File::Flush() const {
	return (PHYSFS_flush (fileHandle) != 0);
}

bool File::IsOpen() const {
	return opened;
}

PHYSFS_sint64 File::Length() const {
	return PHYSFS_fileLength (fileHandle);
}

int File::Seek(PHYSFS_uint64 position) const {
	return PHYSFS_seek (fileHandle, position);
}

long File::Tell() const {
	return (long)PHYSFS_tell (fileHandle);
}

bool File::Eof() const {
	return (PHYSFS_eof(fileHandle) != 0);
}

bool File::SetBuffer (PHYSFS_uint64 bufferSize) const {
	return (PHYSFS_setBuffer (fileHandle, bufferSize) != 0);
}

PHYSFS_sint64 File::Write (const void *buffer, PHYSFS_uint32 objectSize, PHYSFS_uint32 objectCount) const {
	return PHYSFS_write (fileHandle, buffer, objectSize, objectCount);
}

PHYSFS_sint64 File::Read (void *buffer, PHYSFS_uint32 objectSize, PHYSFS_uint32 objectCount) const {
	return PHYSFS_read (fileHandle, buffer, objectSize, objectCount);
}

PHYSFS_sint64 File::ReadToEOF(void *buffer) const {
	return (PHYSFS_sint64)Read (buffer, 1, Length());
}