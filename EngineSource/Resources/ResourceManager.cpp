#include "ResourceManager.h"

HashMap<String, Resource*> ResourceManager::resources;
bool ResourceManager::isDestroying = false;
bool ResourceManager::CacheResources = false;
Set<Resource*> ResourceManager::cache;

void ResourceManager::Initialize() {
    Log ("ResourceManager::Initialize: Success");    
}

void ResourceManager::Destroy() {
    isDestroying = true;
    // destroy resources here
    HashMap<String, Resource*>::iterator it;
    for (it=resources.begin(); it!=resources.end(); it++) {
        Resource *resource = it->second;
        if (resource) {
            delete resource;
            resource = nullptr;
        }
    }
    
    cache.clear();
    resources.clear();
    Log ("ResourceManager::Destroy: Success");
}

void ResourceManager::ClearCache() {
    if (isDestroying)
        return;
    
    Set<Resource*>::iterator it;
    for (it=cache.begin(); it!=cache.end(); it++) {
        Resource *resource = *it;
        Remove (resource, true, false);
    }
    CacheResources = false;
    cache.clear();
}

void ResourceManager::Remove(Resource *resource, bool force, bool eraseFromCache) {
    // if the resource manager has taken ownership of all resources, don't try to remove this manually
    // the resource manager destroy method will.
    if (!resource || isDestroying)
        return;
    
    int ref = resource->RemoveReference();
    if (ref > 0 && !force)
        return;
    
    HashMap<String, Resource*>::iterator it;
    for (it=resources.begin(); it!=resources.end(); it++) {
        Resource *res = it->second;
        
        if (res == resource) {
            if (eraseFromCache)
                cache.erase (resource);
            
            delete res;
            res = nullptr;
            break;
        }
    }
	if (it != resources.end())
		resources.erase(it);
    
}

// calls every resource's invalidate method
// this must be called before display mode switching
void ResourceManager::InvalidateVideoBasedResources() {
    HashMap<String, Resource*>::iterator it;
    for (it=resources.begin(); it!=resources.end(); it++) {
        Resource *resource = it->second;
        if (resource && resource->IsVideoBased())
            resource->InvalidateVideoBasedData();
    }
    Log ("ResourceManager::InvalidateVideoBasedResources: Success");
}

// calls every resource's restore method
// this must be called after display mode switching
void ResourceManager::RestoreVideoBasedResources() {
    HashMap<String, Resource*>::iterator it;
    for (it=resources.begin(); it!=resources.end(); it++) {
        Resource *resource = it->second;
        if (resource && resource->IsVideoBased())
            resource->RestoreVideoBasedData();
    }
    Log ("ResourceManager::RestoreVideoBasedResources: Success");
}