/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The MaterialResource loads a material description .xml file from a given path and creates a Material class instance.
 
 A custom empty material can also be created.
 
 */
#ifndef MATERIAL_RESOURCE_H_DEF
#define MATERIAL_RESOURCE_H_DEF

#include "Resource.h"

#define MATERIAL MaterialResource, MaterialResourceDescriptor

class Material;

// the descriptor
class MaterialResourceDescriptor: public ResourceDescriptor {
public:
    MaterialResourceDescriptor(const String &filename, bool customMaterial);

    String GetAssociatedResourceType();
    
    bool IsCustomMaterial;
};

// the actual resource
class MaterialResource : public Resource {
public:
    static MaterialResource* LoadResource (MaterialResourceDescriptor *descriptor);
    
    void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
    
    Material *MaterialObject;
    
private:
    MaterialResource();
    virtual ~MaterialResource();
    
    void createResource();
    void createResourceCustom();
    void createResourceFromFile();
    
    MaterialResourceDescriptor *descriptor;
};

#endif
