/*
 Author: 
 Manuel Miguel Feria Gonzalez
 
 Description:
 AudioResource, loads an FMOD supported audio clip from a given path and creates the AudioClip class.
 
 */
#ifndef AUDIO_RESOURCE_H_DEF
#define AUDIO_RESOURCE_H_DEF

#include "Resource.h"

#define AUDIO AudioResource, AudioResourceDescriptor

class AudioClip;

// the descriptor
class AudioResourceDescriptor: public ResourceDescriptor {
public:
    AudioResourceDescriptor(const String &filename, bool sound3d=true, bool streamed=false);
    
    String GetAssociatedResourceType();

    bool Sound3D;
    bool Streamed;
};

// the actual resource
class AudioResource : public Resource {
public:
    static AudioResource* LoadResource (AudioResourceDescriptor *descriptor);
    
    AudioClip *AudioObject;

	void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
private:
    AudioResource();
    virtual ~AudioResource();
    
    void createResource();
    
    AudioResourceDescriptor *descriptor;
};

#endif
