#include "Resource.h"
#include "ResourceManager.h"

// the descriptor
ResourceDescriptor::ResourceDescriptor() {
    filename = "";
}

// the base resource class
Resource::Resource() {
    
    referenceCount = 0;
    referenceCounted = true;
    videoBased = false;
    ownedResources.clear();
}

Resource::~Resource() {
    
    for (int i=0; i<(int)ownedResources.size(); i++) {
        if (ownedResources[i]) 
            ResourceManager::Remove (ownedResources[i]);
    }
    ownedResources.clear();
}

int Resource::AddReference() {
    return ++referenceCount;
}

int Resource::RemoveReference() {
    if (referenceCount > 0)
        referenceCount--;
    return referenceCount;
}