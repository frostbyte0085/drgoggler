/*
 Author: 
 Pantelis Lekakis
 
 Description:
 SharedResource, loads a .shader file from a given path and creates the Shader class.
 
 */
#ifndef SHADER_RESOURCE_H_DEF
#define SHADER_RESOURCE_H_DEF

#include "Resource.h"

#define SHADER ShaderResource, ShaderResourceDescriptor

class Shader;

// the descriptor
class ShaderResourceDescriptor: public ResourceDescriptor {
public:
    ShaderResourceDescriptor(const String &filename);
    
    String GetAssociatedResourceType();
};

// the actual resource
class ShaderResource : public Resource {
public:
    static ShaderResource* LoadResource (ShaderResourceDescriptor *descriptor);
    
    void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
    
    Shader *ShaderObject;
    
private:
    ShaderResource();
    virtual ~ShaderResource();

    void createResource();

    ShaderResourceDescriptor *descriptor;
};

#endif
