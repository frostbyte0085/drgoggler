/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The MeshResource will load an md5mesh from a given path and also enumerate the md5anim files associated with it.
 Created are the Mesh and MeshAnimation classes.
 
 Creating a custom empty mesh is also possible.
 
 */
#ifndef MESH_RESOURCE_H_DEF
#define MESH_RESOURCE_H_DEF

#include "Resource.h"

#define MESH MeshResource, MeshResourceDescriptor

class Mesh;

// the descriptor
class MeshResourceDescriptor: public ResourceDescriptor {
public:
    MeshResourceDescriptor(const String &filename, bool customMesh=false);
    
    String GetAssociatedResourceType();
    
    bool IsCustomMesh;
};

// the actual resource
class MeshResource : public Resource {
public:
    static MeshResource* LoadResource (MeshResourceDescriptor *descriptor);
    
    void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
    
    Mesh *MeshObject;
    
private:
    MeshResource();
    virtual ~MeshResource();
    
    void parseAnimations();
    void parseSingleAnimation(const String &filename, const String &name);
    Matrix44 axisChangeFromCmdLine(String cmdLine);
                                   
    void createResourceFromFile();
    void createResourceCustom();
    void createResource();
    
    MeshResourceDescriptor *descriptor;
};

#endif
