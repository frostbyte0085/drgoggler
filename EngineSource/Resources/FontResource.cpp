#include "FontResource.h"

#include <Filesystem/File.h>
#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>
#include <Utilities/Exception.h>

#include <Graphics/Font/Font.h>
#include <Graphics/Texture/Texture.h>
#include "TextureResource.h"

#include <Utilities/KeyValueList.h>
#include "ResourceManager.h"

// the descriptor
FontResourceDescriptor::FontResourceDescriptor (const String &filename) {
    this->filename = filename;
}

String FontResourceDescriptor::GetAssociatedResourceType() {
    return "Font";
}

// the actual resource
FontResource::FontResource() {
    FontObject = new Font();
}

FontResource::~FontResource() {
    if (FontObject) {
        delete FontObject;
    }
    FontObject = nullptr;
    Log ("FontResource::~FontResource: Success (" + descriptor->GetFilename() + ")");
}

void FontResource::createResource() {

    assert (FontObject);
    
    String fontFilename = descriptor->GetFilename();
	String fontFullFilename = Filesystem::GetFullPathForFile(fontFilename);
    String directoryPath = Filesystem::GetDirectoryForFile(fontFilename);
    
    // load the file
    std::ifstream fr;
    fr.open (fontFullFilename.c_str());
    KeyValueList values("");
    
    String s;
    
    std::getline(fr, s);  // read the first line
    std::getline(fr, s);  // in the second line we are interested in the
                         // "pages" field, which is the page count for
                         // the font, and in the "lineHeight" field
                         // which is the maximum character height
    
    values.ProcessLine(s);
    int pageCount = values.GetIntValue("pages");
    assert (pageCount==1); // only supporting fonts with 1 page
    
    FontObject->SetLineHeight( values.GetIntValue("lineHeight") );
    
    // load all pages
    for (int i=0; i<pageCount; i++) {
        std::getline (fr, s);
        values.ProcessLine(s);
        String textureFilename = directoryPath;
        textureFilename += values.GetValueNoQuotes("file");
        
        Texture *tex = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor(textureFilename))->TextureObject;
        assert (tex);
        
        FontObject->pages.push_back(tex);
    }
    
    // read chars count line
    std::getline(fr, s);
    values.ProcessLine(s);
    int charCount = values.GetIntValue("count");
    
    for (int i=0; i<charCount; i++) {
        std::getline(fr, s);
        FontObject->chars.push_back(FontObject->charLineToCharData(s));
    }
    
    // read kernings count line
    std::getline (fr, s);
    if (s == "")
        return;
    
    values.ProcessLine(s);
    int kerningCount = values.GetIntValue("count");
    
    for (int i=0; i<kerningCount; i++) {
        std::getline (fr, s);
        Font::KerningData kdat = FontObject->kerningLineToKerningData(s);
        FontObject->kernings[kdat.first][kdat.second] = kdat.displace;
    }
    
    if (!FontObject->createFont()) {
        Log ("FontResource::createResource: Failed (" + fontFilename + ": cannot create font)");
        throw new Exception ("Font '" + fontFilename + "' could not be created.");
    }
    
    Log ("FontResource::createResource: Success (" + fontFilename + ")");
}

FontResource* FontResource::LoadResource (FontResourceDescriptor *descriptor) {
    
    FontResource *returnedResource = new FontResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void FontResource::InvalidateVideoBasedData() {
    if (!FontObject)
        return;
    
    FontObject->destroyVideoBasedData();
}

void FontResource::RestoreVideoBasedData() {
    if (!FontObject)
        return;
    
    FontObject->restoreVideoBasedData();
}