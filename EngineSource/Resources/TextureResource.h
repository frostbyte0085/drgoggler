/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The TextureResource uses SOIL library to load the image data from a given path and creates a Texture class to generate the opengl texture.
 
 A custom empty texture can also be created, but no functionality to generate the texture is directly exposed.
 */

#ifndef TEXTURE_RESOURCE_H_DEF
#define TEXTURE_RESOURCE_H_DEF

#include "Resource.h"

#define TEXTURE TextureResource, TextureResourceDescriptor

#include <Graphics/Texture/Texture.h>

// the descriptor
class TextureResourceDescriptor: public ResourceDescriptor {
public:
    TextureResourceDescriptor(const String &filename, bool mipmaps=true, bool cubemap=false, TextureAddressMode addressMode = TAM_REPEAT, bool customTexture=false);
    
    String GetAssociatedResourceType();
    
    bool Mipmaps;
    bool Cubemap;
    bool IsCustomTexture;
    TextureAddressMode AddressMode;
};

// the actual resource
class TextureResource : public Resource {
public:
    static TextureResource* LoadResource (TextureResourceDescriptor *descriptor);
    
    void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
    
    Texture *TextureObject;
private:
    TextureResource();
    virtual ~TextureResource();
    
    void createResource();
    void createResourceCustom();
    void createResourceFromFile();
    
    TextureResourceDescriptor *descriptor;
};

#endif
