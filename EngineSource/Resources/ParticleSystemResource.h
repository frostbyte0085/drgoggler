/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The ParticleSystemResource loads an .xml from a given path and creates a ParticleSystem class instance.
 
 A custom empty particle system can also be created.
 
 */
#ifndef PARTICLE_SYSTEM_RESOURCE_H_DEF
#define PARTICLE_SYSTEM_RESOURCE_H_DEF

#include "Resource.h"

#define PARTICLE ParticleSystemResource, ParticleSystemResourceDescriptor

class ParticleSystem;

// the descriptor
class ParticleSystemResourceDescriptor: public ResourceDescriptor {
public:
    ParticleSystemResourceDescriptor(const String &filename, bool customParticleSystem);
    
    String GetAssociatedResourceType();
    
    bool IsCustomParticleSystem;
};

// the actual resource
class ParticleSystemResource : public Resource {
public:
    static ParticleSystemResource* LoadResource (ParticleSystemResourceDescriptor *descriptor);
    
    void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
    
    ParticleSystem *ParticleObject;
private:
    ParticleSystemResource();
    virtual ~ParticleSystemResource();
    
    void createResource();
    void createResourceCustom();
    void createResourceFromFile();
    
    ParticleSystemResourceDescriptor *descriptor;
};

#endif
