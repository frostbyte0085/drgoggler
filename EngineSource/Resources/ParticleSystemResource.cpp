#include "ParticleSystemResource.h"

#include <Filesystem/File.h>
#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>
#include <Utilities/Exception.h>

#include <Graphics/Material/Material.h>
#include <Graphics/Texture/Texture.h>
#include <Graphics/Particles/ParticleSystem.h>

#include "MaterialResource.h"
#include "ResourceManager.h"

// the descriptor
ParticleSystemResourceDescriptor::ParticleSystemResourceDescriptor (const String &filename, bool customParticleSystem) {
    this->filename = filename;
    this->IsCustomParticleSystem = customParticleSystem;
}

String ParticleSystemResourceDescriptor::GetAssociatedResourceType() {
    return "ParticleSystem";
}

// the actual resource
ParticleSystemResource::ParticleSystemResource() {
    ParticleObject = new ParticleSystem();
    videoBased = true;
    referenceCounted = false;
}

ParticleSystemResource::~ParticleSystemResource() {
    if (ParticleObject) {
        delete ParticleObject;
    }
    ParticleObject = nullptr;
    Log ("ParticleSystemResource::~ParticleSystemResource: Success (" + descriptor->GetFilename() + ")");
}

void ParticleSystemResource::createResource() {
    assert (ParticleObject);
    
    if (descriptor->IsCustomParticleSystem)
        createResourceCustom();
    else
        createResourceFromFile();
}

void ParticleSystemResource::createResourceCustom() {
    
}

void ParticleSystemResource::createResourceFromFile() {
    assert (ParticleObject);
    
    String psFilename = descriptor->GetFilename();
	String psFullFilename = Filesystem::GetFullPathForFile(psFilename);
    String directoryPath = Filesystem::GetDirectoryForFile(psFilename);
    
    // load the file
    std::ifstream fr;
    fr.open (psFullFilename.c_str());

    if (Filesystem::FileExists(psFilename)) {
        
        XMLNode mainNode = XMLNode::openFileHelper(psFullFilename.c_str(), "ParticleSystem");
        XMLNode emitterNode = mainNode.getChildNode("Emitter");
        XMLNode animatorNode = mainNode.getChildNode("Animator");
        
        // material properties
        String propertyStringValue = mainNode.getAttribute("material");
        ParticleObject->SetMaterial(ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor(directoryPath + propertyStringValue, false))->MaterialObject);
        
        // Emitter parameters
        // interval
        XMLNode intervalNode = emitterNode.getChildNode("Interval");
        if (!intervalNode.isEmpty()) {
            propertyStringValue = intervalNode.getText();
            ParticleObject->Emitter.Interval = StringOperations::FromString<float>(propertyStringValue);
        }
        // particles per emission
        XMLNode pppNode = emitterNode.getChildNode("ParticlesPerEmission");
        if (!pppNode.isEmpty()) {
            propertyStringValue = pppNode.getText();
            ParticleObject->Emitter.ParticlesPerEmission = StringOperations::FromString<int>(propertyStringValue);
        }
        // minimum size
        XMLNode minSizeNode = emitterNode.getChildNode("MinSize");
        if (!minSizeNode.isEmpty()) {
            propertyStringValue = minSizeNode.getText();
            ParticleObject->Emitter.MinSize = StringOperations::FromString<float>(propertyStringValue);
        }
        // maximum size
        XMLNode maxSizeNode = emitterNode.getChildNode("MaxSize");
        if (!maxSizeNode.isEmpty()) {
            propertyStringValue = maxSizeNode.getText();
            ParticleObject->Emitter.MaxSize = StringOperations::FromString<float>(propertyStringValue);
        }
        // minimum energy
        XMLNode minEnergyNode = emitterNode.getChildNode("MinEnergy");
        if (!minEnergyNode.isEmpty()) {
            propertyStringValue = minEnergyNode.getText();
            ParticleObject->Emitter.MinEnergy = StringOperations::FromString<float>(propertyStringValue);
        }
        // maximum energy
        XMLNode maxEnergyNode = emitterNode.getChildNode("MaxEnergy");
        if (!maxEnergyNode.isEmpty()) {
            propertyStringValue = maxEnergyNode.getText();
            ParticleObject->Emitter.MaxEnergy = StringOperations::FromString<float>(propertyStringValue);
        }
        // minimum emission
        XMLNode minEmissionNode = emitterNode.getChildNode("MinEmission");
        if (!minEmissionNode.isEmpty()) {
            propertyStringValue = minEmissionNode.getText();
            ParticleObject->Emitter.MinEmission = StringOperations::FromString<int>(propertyStringValue);
        }
        // maximum emission
        XMLNode maxEmissionNode = emitterNode.getChildNode("MaxEmission");
        if (!maxEmissionNode.isEmpty()) {
            propertyStringValue = maxEmissionNode.getText();
            ParticleObject->Emitter.MaxEmission = StringOperations::FromString<int>(propertyStringValue);
        }
        // emit once
        XMLNode emitOnceNode = emitterNode.getChildNode("EmitOnce");
        if (!emitOnceNode.isEmpty()) {
            propertyStringValue = emitOnceNode.getText();
            ParticleObject->Emitter.EmitOnce = (bool)StringOperations::FromString<int>(propertyStringValue);
        }
        // velocity
        XMLNode velocityNode = emitterNode.getChildNode("Velocity");
        if (!velocityNode.isEmpty()) {
            propertyStringValue = velocityNode.getText();
            Vector<String> velocityComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(velocityComponents[0]);
            float y = StringOperations::FromString<float>(velocityComponents[1]);
            float z = StringOperations::FromString<float>(velocityComponents[2]);
            
            ParticleObject->Emitter.Velocity = Vector3(x,y,z);
        }
        // random velocity
        XMLNode randomVelocityNode = emitterNode.getChildNode("RandomVelocity");
        if (!randomVelocityNode.isEmpty()) {
            propertyStringValue = randomVelocityNode.getText();
            Vector<String> randomVelocityComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(randomVelocityComponents[0]);
            float y = StringOperations::FromString<float>(randomVelocityComponents[1]);
            float z = StringOperations::FromString<float>(randomVelocityComponents[2]);
            
            ParticleObject->Emitter.RandomVelocity = Vector3(x,y,z);
        }
        // gravity
        XMLNode gravityNode = emitterNode.getChildNode("Gravity");
        if (!gravityNode.isEmpty()) {
            propertyStringValue = gravityNode.getText();
            Vector<String> gravityComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(gravityComponents[0]);
            float y = StringOperations::FromString<float>(gravityComponents[1]);
            float z = StringOperations::FromString<float>(gravityComponents[2]);
            
            ParticleObject->Emitter.Gravity = Vector3(x,y,z);
        }
        
        // Animator
        // color1
        XMLNode color1Node = animatorNode.getChildNode("Color1");
        if (!color1Node.isEmpty()) {
            propertyStringValue = color1Node.getText();
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            ParticleObject->Animator.Color1 = Vector4(r,g,b,a);
        }
        // color2
        XMLNode color2Node = animatorNode.getChildNode("Color2");
        if (!color2Node.isEmpty()) {
            propertyStringValue = color2Node.getText();
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            ParticleObject->Animator.Color2 = Vector4(r,g,b,a);
        }
        // color3
        XMLNode color3Node = animatorNode.getChildNode("Color3");
        if (!color3Node.isEmpty()) {
            propertyStringValue = color3Node.getText();
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            ParticleObject->Animator.Color3 = Vector4(r,g,b,a);
        }
        // color4
        XMLNode color4Node = animatorNode.getChildNode("Color4");
        if (!color4Node.isEmpty()) {
            propertyStringValue = color4Node.getText();
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            ParticleObject->Animator.Color4 = Vector4(r,g,b,a);
        }
        // color5
        XMLNode color5Node = animatorNode.getChildNode("Color5");
        if (!color5Node.isEmpty()) {
            propertyStringValue = color5Node.getText();
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            ParticleObject->Animator.Color5 = Vector4(r,g,b,a);
        }
        // color6
        XMLNode color6Node = animatorNode.getChildNode("Color6");
        if (!color6Node.isEmpty()) {
            propertyStringValue = color6Node.getText();
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            ParticleObject->Animator.Color6 = Vector4(r,g,b,a);
        }
        // size grow
        XMLNode sizeGrowNode = animatorNode.getChildNode("SizeGrow");
        if (!sizeGrowNode.isEmpty()) {
            propertyStringValue = sizeGrowNode.getText();
            ParticleObject->Animator.SizeGrow = StringOperations::FromString<float>(propertyStringValue);
        }
        // minimum size
        minSizeNode = animatorNode.getChildNode("MinimumSize");
        if (!minSizeNode.isEmpty()) {
            propertyStringValue = minSizeNode.getText();
            ParticleObject->Animator.MinimumSize = StringOperations::FromString<float>(propertyStringValue);
        }
        // maximum size
        maxSizeNode = animatorNode.getChildNode("MaximumSize");
        if (!maxSizeNode.isEmpty()) {
            propertyStringValue = maxSizeNode.getText();
            ParticleObject->Animator.MaximumSize = StringOperations::FromString<float>(propertyStringValue);
        }
        
        if (!ParticleObject->createParticleSystem()) {
            Log ("ParticleSystemResource::createResource: Failed (" + psFilename + ": cannot create particle system)");
            throw new Exception ("Font '" + psFilename + "' could not be created.");
        }
        Log ("ParticleSystemResource::createResource: Success (" + psFilename + ")");
        return;
    }
    
    Log ("ParticleSystemResource::createResource: Failed (" + psFilename + ": file not found)");
}

ParticleSystemResource* ParticleSystemResource::LoadResource (ParticleSystemResourceDescriptor *descriptor) {
    
    ParticleSystemResource *returnedResource = new ParticleSystemResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void ParticleSystemResource::InvalidateVideoBasedData() {
    if (!ParticleObject)
        return;
    
    ParticleObject->destroyVideoBasedData();
}

void ParticleSystemResource::RestoreVideoBasedData() {
    if (!ParticleObject)
        return;
    
    ParticleObject->restoreVideoBasedData();
}