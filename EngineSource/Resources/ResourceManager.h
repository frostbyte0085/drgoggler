/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The ResourceManager is the center of loading assets in the engine in a safe and managed way.
 Typical usage is:
    ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor ("Data/Textures/diffuse.png", true, false));
    
 the above will load a texture from file with mipmapping.
 
 Reference counting works as in Microsoft COM model; Remove decreases the resource's reference count, Create increases it. If the reference count is 0, or if the resource is not reference counted, the resource is removed and its data are freed.
 
 
 Resource Caching enables recently loaded resources (for example game level's) to be removed with one call to ClearCache. Tis required for CacheResources to be set to true before loading any.
 
 
 Resource invalidation and restoring happens when the display mode is changed and only applies to video based resources
 such as Textures, Meshes and so on.
 */
#ifndef RESOURCE_MANAGER_H_DEF
#define RESOURCE_MANAGER_H_DEF

#include <Platform.h>
#include "Resource.h"

// Based on http://immersedcode.org/2011/4/8/stacked-resource-manager/
//
class ResourceManager {
    friend class Resource;
public:
    static void Initialize();
    static void Destroy();

    static bool CacheResources;
    
    static void ClearCache();
    
    template<class T, class D> static T* Create (D *descriptor) {
        String filename = descriptor->GetFilename();
        // check if the resource already exists. if it does, increase the reference count and return it.
        HashMap<String, Resource*>::const_iterator it = resources.find(filename);
        if (it != resources.end()) {
            T *existingResource = dynamic_cast<T*> (it->second);
            if (existingResource->IsReferenceCounted()) {
            
                existingResource->AddReference();
            
                assert(existingResource);
                return existingResource;
            }
        }
        // the resource does not exist yet, so let's create it and return a pointer to it.
        T *resourceLoading = T::LoadResource (descriptor);
        resourceLoading->AddReference();
        resources[filename] = resourceLoading;
        
        if (CacheResources)
            cache.insert (resourceLoading);
        
        return resourceLoading;
    }
    
    template<class T> static T* Get (const String &filename) {
        HashMap<String, Resource*>::const_iterator it = resources.find(filename);
        if (it != resources.end())
            return dynamic_cast<T*>(it->second);
        
        return nullptr;
    }
    
    static void Remove (Resource *resource, bool force=false, bool eraseFromCache=true);
    
    static void InvalidateVideoBasedResources();
    static void RestoreVideoBasedResources();
    
    static unsigned int GetResourceCount() { return (unsigned int)resources.size(); }
    
private:
    static Set<Resource*> cache;
    
    static HashMap<String, Resource*> resources;
    static bool isDestroying;
};

#endif
