#include "ShaderResource.h"
#include <Filesystem/File.h>
#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>
#include <Utilities/Exception.h>

#include <Graphics/Shader/Shader.h>

// the descriptor
ShaderResourceDescriptor::ShaderResourceDescriptor (const String &filename) {
    this->filename = filename;
}

String ShaderResourceDescriptor::GetAssociatedResourceType() {
    return "Shader";
}

// the actual resource
ShaderResource::ShaderResource() {
    ShaderObject = new Shader();
    videoBased = true;
}

ShaderResource::~ShaderResource() {
    if (ShaderObject) {
        delete ShaderObject;
    }
    ShaderObject = nullptr;
    Log ("ShaderResource::~ShaderResource: Success (" + descriptor->GetFilename() + ")");
}

void ShaderResource::createResource() {
    assert (ShaderObject);
    
    // read the shader text file and retrieve the vp and fp filenames
    String shaderFilename = descriptor->GetFilename();
    String shaderDirectory = Filesystem::GetDirectoryForFile(shaderFilename);
    String shaderFullFilename = Filesystem::GetFullPathForFile(shaderFilename);
    String shaderFullDirectory = Filesystem::GetFullPathForDirectory(shaderDirectory);
    
    File *f = new File(shaderFilename, File::ReadFile);
    
    if (f && f->IsOpen()) {
        char buffer[8000];
        memset (&buffer, 0, sizeof(buffer));
        f->ReadToEOF(&buffer);
        String shaderContents = buffer;
        // split the two filenames in the .shader file
        Vector<String> shaderFilenames = StringOperations::SplitString(shaderContents, '\n');
        String vpFilename = shaderDirectory + shaderFilenames[0];
        String fpFilename = shaderDirectory + shaderFilenames[1];
        
        
        f->Close();
        delete f;
        f = nullptr;
        
        // create the shader instances
        // vertex program
        File *vpShaderFile = new File(vpFilename, File::ReadFile);
        if (vpShaderFile && vpShaderFile->IsOpen()) {
            memset (&buffer, 0, sizeof(buffer));
            vpShaderFile->ReadToEOF(&buffer);
            
            vpShaderFile->Close();
            delete vpShaderFile;
            vpShaderFile = nullptr;
            
            if (!ShaderObject->createVertexShader (buffer)) {
                Log ("ShaderResource::createResource: Failed (" + shaderFilename + ": vertex validation failed)");
                throw new Exception ("Validation of shader '" + vpFilename + "' has failed!");
            }
        }
        
        // fragment program
        File *fpShaderFile = new File(fpFilename, File::ReadFile);
        if (fpShaderFile && fpShaderFile->IsOpen()) {
            memset (&buffer, 0, sizeof(buffer));
            fpShaderFile->ReadToEOF(&buffer);
            
            fpShaderFile->Close();
            delete fpShaderFile;
            fpShaderFile = nullptr;
            
            if (!ShaderObject->createFragmentShader (buffer)) {
                Log ("ShaderResource::createResource: Failed (" + shaderFilename + ": fragment validation failed)");
                throw new Exception ("Validation of shader '" + fpFilename + "' has failed!"); 
            }
        }
        
        // link the shaders and create the program
        if (!ShaderObject->link()) {
            Log ("ShaderResource::createResource: Failed (" + shaderFilename + ": link failed)");
            throw new Exception ("Validation of program '" + shaderFilename + "' has failed!"); 
        }
        Log ("ShaderResource::createResource: Success (" + shaderFilename + ")");
        return;
    }
    Log ("ShaderResource::createResource: Failed (" + shaderFilename + ": file not found)");
}

ShaderResource* ShaderResource::LoadResource (ShaderResourceDescriptor *descriptor) {
    
    ShaderResource *returnedResource = new ShaderResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void ShaderResource::InvalidateVideoBasedData() {
    if (!ShaderObject)
        return;

    ShaderObject->destroyVideoBasedData();
}

void ShaderResource::RestoreVideoBasedData() {
    if (!ShaderObject)
        return;
    
    ShaderObject->restoreVideoBasedData();
}