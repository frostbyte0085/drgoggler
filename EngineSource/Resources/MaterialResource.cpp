#include "MaterialResource.h"

#include <Filesystem/File.h>
#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>
#include <Utilities/Exception.h>

#include "ShaderResource.h"
#include "TextureResource.h"

#include <Graphics/Material/Material.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>

#include "ResourceManager.h"

// the descriptor
MaterialResourceDescriptor::MaterialResourceDescriptor (const String &filename, bool customMaterial) {
    this->filename = filename;
    this->IsCustomMaterial = customMaterial;
}

String MaterialResourceDescriptor::GetAssociatedResourceType() {
    return "Material";
}

// the actual resource
MaterialResource::MaterialResource() {
    MaterialObject = new Material();
}

MaterialResource::~MaterialResource() {
    if (MaterialObject) {
        delete MaterialObject;
    }
    MaterialObject = nullptr;
    Log ("MaterialResource::~MaterialResource: Success (" + descriptor->GetFilename() + ")");
}

// helper for blendfunc
unsigned int StringToBlendFunc (const String &func) {
    if (func == "zero")
        return GL_ZERO;
    else if (func == "one")
        return GL_ONE;
    else if (func == "srccolor")
        return GL_SRC_COLOR;
    else if (func == "invsrccolor")
        return GL_ONE_MINUS_SRC_COLOR;
    else if (func == "dstcolor")
        return GL_DST_COLOR;
    else if (func == "invdstcolor")
        return GL_ONE_MINUS_DST_COLOR;
    else if (func == "srcalpha")
        return GL_SRC_ALPHA;
    else if (func == "invsrcalpha")
        return GL_ONE_MINUS_SRC_ALPHA;
    else if (func == "dstalpha")
        return GL_DST_ALPHA;
    else if (func == "invdstalpha")
        return GL_ONE_MINUS_DST_ALPHA;
    else if (func == "constantcolor")
        return GL_CONSTANT_COLOR;
    else if (func == "invconstantcolor")
        return GL_ONE_MINUS_CONSTANT_COLOR;
    else if (func == "constantalpha")
        return GL_CONSTANT_ALPHA;
    else if (func == "invconstantalpha")
        return GL_ONE_MINUS_CONSTANT_ALPHA;
    else if (func == "srcalphasat")
        return GL_SRC_ALPHA_SATURATE;
    
    return GL_INVALID_VALUE;
}

void MaterialResource::createResource() {
    assert (MaterialObject);
    
    if (descriptor->IsCustomMaterial)
        createResourceCustom();
    else
        createResourceFromFile();
}

/*
 Parse the xml file and add the elements to the created Material instance.
*/
void MaterialResource::createResourceFromFile() {
    
    String materialFilename = descriptor->GetFilename();
    String materialFullPath = Filesystem::GetFullPathForFile(materialFilename);
    
    String fullPath = materialFullPath;
    if (Filesystem::FileExists(materialFilename)) {
        
        XMLNode mainNode = XMLNode::openFileHelper(fullPath.c_str(), "material");
        XMLNode shaderNode = mainNode.getChildNode("shader");
        XMLNode diffuseNode = mainNode.getChildNode("diffuse");
        
        // material properties
        String propertyStringValue;

        TextureAddressMode addressMode = TAM_REPEAT;
        // tetxure address mode
        XMLNode textureAddressNode = mainNode.getChildNode("tex-address");
        if (!textureAddressNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(textureAddressNode.getText());
            if (propertyStringValue == "clamp")
                MaterialObject->addressMode = TAM_CLAMP;
        }
        
        // depth writing
        XMLNode depthWriteNode = mainNode.getChildNode("depth-write");
        if (!depthWriteNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(depthWriteNode.getText());
            MaterialObject->depthWrite = (bool)StringOperations::FromString<int>(propertyStringValue);
        }
        
        // lighting
        XMLNode lightingNode = mainNode.getChildNode("lighting");
        if (!lightingNode.isEmpty()) {
            propertyStringValue = lightingNode.getText();
            MaterialObject->lighting = (bool)StringOperations::FromString<int>(propertyStringValue);
        }
        
        // cubemap
        XMLNode cubemapNode = mainNode.getChildNode("cubemap");
        if (!cubemapNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(cubemapNode.getText());
            MaterialObject->cubemap = (bool)StringOperations::FromString<int>(propertyStringValue);
        }
        
        // blending
        XMLNode blendingNode = mainNode.getChildNode("blending");
        if (!blendingNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(blendingNode.getText());
            MaterialObject->blending = (bool)StringOperations::FromString<int>(propertyStringValue);
        }
        
        // blend function
        XMLNode blendFuncNode = mainNode.getChildNode("blend-func");
        if (!blendFuncNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(blendFuncNode.getText());
            Vector<String> blendFuncs = StringOperations::SplitString(propertyStringValue, ',');
            
            
            MaterialObject->blendSrc = StringToBlendFunc(blendFuncs[0]);
            MaterialObject->blendDest = StringToBlendFunc(blendFuncs[1]);
        }
        
        // ambient color
        XMLNode ambientColorNode = mainNode.getChildNode("ambient-color");
        if (!ambientColorNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(ambientColorNode.getText());
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            MaterialObject->SetAmbientColor(Vector4(r,g,b,a));
        }

        // diffuse color
        XMLNode diffuseColorNode = mainNode.getChildNode("diffuse-color");
        if (!diffuseColorNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(diffuseColorNode.getText());
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            MaterialObject->SetDiffuseColor(Vector4(r,g,b,a));
        }
        
        // specular color
        XMLNode specularColorNode = mainNode.getChildNode("specular-color");
        if (!specularColorNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(specularColorNode.getText());
            Vector<String> colorComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float r = StringOperations::FromString<float>(colorComponents[0]);
            float g = StringOperations::FromString<float>(colorComponents[1]);
            float b = StringOperations::FromString<float>(colorComponents[2]);
            float a = StringOperations::FromString<float>(colorComponents[3]);
            
            MaterialObject->SetSpecularColor(Vector4(r,g,b,a));
        }
        
        // shininess
        XMLNode shininessNode = mainNode.getChildNode("shininess");
        if (!shininessNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(shininessNode.getText());
            
            MaterialObject->SetShininess (StringOperations::FromString<float>(propertyStringValue));
        }
        
        // diffuse map repeat
        XMLNode diffuseRepeatNode = mainNode.getChildNode("diffuse-repeat");
        if (!diffuseRepeatNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(diffuseRepeatNode.getText());
            Vector<String> repeatComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(repeatComponents[0]);
            float y = StringOperations::FromString<float>(repeatComponents[1]);
            
            MaterialObject->SetDiffuseMapRepeat(Vector2(x,y));
        }
        
        // normal map repeat
        XMLNode normalRepeatNode = mainNode.getChildNode("normal-repeat");
        if (!normalRepeatNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(normalRepeatNode.getText());
            Vector<String> repeatComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(repeatComponents[0]);
            float y = StringOperations::FromString<float>(repeatComponents[1]);
            
            MaterialObject->SetNormalMapRepeat(Vector2(x,y));
        }
        
        // specular map repeat
        XMLNode specularRepeatNode = mainNode.getChildNode("specular-repeat");
        if (!specularRepeatNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(specularRepeatNode.getText());
            Vector<String> repeatComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(repeatComponents[0]);
            float y = StringOperations::FromString<float>(repeatComponents[1]);
            
            MaterialObject->SetSpecularMapRepeat(Vector2(x,y));
        }
        
        // diffuse map scroll
        XMLNode diffuseScrollNode = mainNode.getChildNode("diffuse-scroll");
        if (!diffuseScrollNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(diffuseScrollNode.getText());
            Vector<String> scrollComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(scrollComponents[0]);
            float y = StringOperations::FromString<float>(scrollComponents[1]);
            
            MaterialObject->SetDiffuseMapScroll(Vector2(x,y));
        }

        // normal map scroll
        XMLNode normalScrollNode = mainNode.getChildNode("normal-scroll");
        if (!normalScrollNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(normalScrollNode.getText());
            Vector<String> scrollComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(scrollComponents[0]);
            float y = StringOperations::FromString<float>(scrollComponents[1]);
            
            MaterialObject->SetNormalMapScroll(Vector2(x,y));
        }
        
        // specular map scroll
        XMLNode specularScrollNode = mainNode.getChildNode("specular-scroll");
        if (!specularScrollNode.isEmpty()) {
            propertyStringValue = StringOperations::ToLower(specularScrollNode.getText());
            Vector<String> scrollComponents = StringOperations::SplitString(propertyStringValue, ',');
            
            float x = StringOperations::FromString<float>(scrollComponents[0]);
            float y = StringOperations::FromString<float>(scrollComponents[1]);
            
            MaterialObject->SetSpecularMapScroll(Vector2(x,y));
        }
        
        // create the shader
        String shaderFilename = shaderNode.getText();
        ShaderResource *shaderResource = ResourceManager::Create<SHADER>(new ShaderResourceDescriptor(shaderFilename));
        MaterialObject->shader = shaderResource->ShaderObject;
        ownedResources.push_back(shaderResource);
    
        // create the diffuse texture
        String diffuseFilename = diffuseNode.getText();
        String diffuseUniform = diffuseNode.getAttribute("uniform");
        
        TextureResource *diffuseResource = ResourceManager::Create<TEXTURE>(new TextureResourceDescriptor(diffuseFilename,
                                                                                                          true, 
                                                                                                          MaterialObject->cubemap,
                                                                                                          addressMode));
        ownedResources.push_back(diffuseResource);
        
        MaterialObject->diffuseMap = std::make_pair (diffuseUniform, diffuseResource->TextureObject);
        
        
        Log ("MaterialResource::createResource: Success (" + materialFilename + ")");
        return;
    }
    Log ("MaterialResource::createResource: Failed (" + materialFilename + ": file not found)");
}

void MaterialResource::createResourceCustom() {
    
}

MaterialResource* MaterialResource::LoadResource (MaterialResourceDescriptor *descriptor) {
    
    MaterialResource *returnedResource = new MaterialResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void MaterialResource::InvalidateVideoBasedData() {

}

void MaterialResource::RestoreVideoBasedData() {

}