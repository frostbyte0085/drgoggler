/*
 Author: 
 Pantelis Lekakis
 
 Description:
 A Resource and its ResourceDescriptor.
 
 Resources in the engine can be either reference counted or copied, and also videobased or not video based.
 Video based resources are the ones that will be recreated on a display mode change.
 
 */
#ifndef RESOURCE_H_DEF
#define RESOURCE_H_DEF

#include <Platform.h>

// the descriptor
class ResourceDescriptor {
public:
    ResourceDescriptor();
    
    virtual String GetAssociatedResourceType()=0;
    String GetFilename() const { return filename; }
protected:
    
    String filename;
};

// the base resource class
class Resource {
    friend class ResourceManager;
public:
    int AddReference();
    int RemoveReference();
    int GetReferenceCount() const { return referenceCount; }
    bool IsVideoBased() const { return videoBased; }
    bool IsReferenceCounted() const { return referenceCounted; }
    virtual void InvalidateVideoBasedData()=0;
    virtual void RestoreVideoBasedData()=0;
    
protected:
    virtual void createResource()=0;

    Resource();
    virtual ~Resource();
    
    bool videoBased;
    bool referenceCounted;
    Vector<Resource*> ownedResources;
    
private:
    int referenceCount;
};

#endif
