#include "TextureResource.h"

#include <Filesystem/File.h>
#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>
#include <Utilities/Exception.h>

#include <Graphics/Texture/Texture.h>

// the descriptor
TextureResourceDescriptor::TextureResourceDescriptor (const String &filename, bool mipmaps, bool cubemap, TextureAddressMode addressMode, bool customTexture) {
    this->filename = filename;
    this->Mipmaps = mipmaps;
    this->Cubemap = cubemap;
    this->IsCustomTexture = customTexture;
    this->AddressMode = addressMode;
}

String TextureResourceDescriptor::GetAssociatedResourceType() {
    return "Texture";
}

// the actual resource
TextureResource::TextureResource() {
    TextureObject = new Texture();
    videoBased = true;
}

TextureResource::~TextureResource() {
    if (TextureObject) {
        delete TextureObject;
    }
    TextureObject = nullptr;
    Log ("TextureResource::~TextureResource: Success (" + descriptor->GetFilename() + ")");
}

void TextureResource::createResource() {
    assert (TextureObject);
    
    if (descriptor->IsCustomTexture)
        createResourceCustom();
    else
        createResourceFromFile();
}

void TextureResource::createResourceCustom() {
    TextureObject->emptyTexture = true;
    
    TextureObject->createEmptyTexture();
}

/*
 createResourceFromFile keeps the system memory copies which are required to restore the texture
 after a display mode change. In case of a cubemap, there are 6 system copies.
*/
void TextureResource::createResourceFromFile() {
    assert (TextureObject);
    
    String textureFilename = descriptor->GetFilename();
	String textureFullFilename = Filesystem::GetFullPathForFile(textureFilename);

    if (descriptor->Cubemap) {
        String cubemapFaces[6] = { "_posx.png", "_negx.png", "_posy.png", "_negy.png", "_posz.png", "_negz.png"};
        
        // have some space for 6 system copies
        TextureObject->systemMemoryCopies.resize(6);
        for (int i=0; i<6; i++) {
            String currentCubemapFilename = textureFullFilename + cubemapFaces[i];

            
            TextureObject->systemMemoryCopies[i] = SOIL_load_image(currentCubemapFilename.c_str(), &TextureObject->width, &TextureObject->height, &TextureObject->channels, 0);
            if (!TextureObject->systemMemoryCopies[i]) {
                Log ("TextureResource::createResource: Failed (" + currentCubemapFilename + ": cannot load image data)");
                return;
            }
            
        }
    }
    else {
        // have space for our system copy
        TextureObject->systemMemoryCopies.resize(1);
        TextureObject->systemMemoryCopies[0] = SOIL_load_image(textureFullFilename.c_str(), &TextureObject->width, &TextureObject->height, &TextureObject->channels, 0);
        
        if (!TextureObject->systemMemoryCopies[0]) {
            Log ("TextureResource::createResource: Failed (" + textureFullFilename + ": cannot load image data)");
            return;
        }
 
    }
    
    if (!TextureObject->createTexture(descriptor->Mipmaps, descriptor->Cubemap, descriptor->AddressMode)) {
        Log ("TextureResource::createResource: Failed (" + textureFilename + ": cannot create texture)");
        throw new Exception ("Texture '" + textureFilename + "' could not be created.");
    }
    
    Log ("TextureResource::createResource: Success (" + textureFilename + ")");
}

TextureResource* TextureResource::LoadResource (TextureResourceDescriptor *descriptor) {
    
    TextureResource *returnedResource = new TextureResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void TextureResource::InvalidateVideoBasedData() {
    if (!TextureObject)
        return;
    
    TextureObject->destroyVideoBasedData();
}

void TextureResource::RestoreVideoBasedData() {
    if (!TextureObject)
        return;
    
    TextureObject->restoreVideoBasedData();
}