#include "AudioResource.h"

#include <Filesystem/File.h>
#include <Filesystem/Filesystem.h>
#include <Utilities/StringOperations.h>
#include <Utilities/Exception.h>

#include <Audio/AudioClip/AudioClip.h>

// the descriptor
AudioResourceDescriptor::AudioResourceDescriptor (const String &filename, bool sound3d, bool streamed) {
    this->filename = filename;
    this->Sound3D = sound3d;
    this->Streamed = streamed;
}

String AudioResourceDescriptor::GetAssociatedResourceType() {
    return "Audio";
}

// the actual resource
AudioResource::AudioResource() {
    AudioObject = new AudioClip();
}

AudioResource::~AudioResource() {
    if (AudioObject) {
        delete AudioObject;
    }
    AudioObject = nullptr;
    Log ("AudioResource::~AudioResource: Success (" + descriptor->GetFilename() + ")");
}

void AudioResource::createResource() {

    assert (AudioObject);
    
    String audioClipFilename = descriptor->GetFilename();
	String audioClipFullFilename = Filesystem::GetFullPathForFile(audioClipFilename);

	if (!AudioObject->createAudioClip(audioClipFullFilename, descriptor->Sound3D, descriptor->Streamed)) {
        Log ("AudioResource::createResource: Failed (" + audioClipFilename + ": cannot create audioClip)");
        throw new Exception ("Audioclip '" + audioClipFilename + "' could not be created.");
    }
    Log ("AudioResource::createResource: Success (" + audioClipFilename + ")");
}

AudioResource* AudioResource::LoadResource (AudioResourceDescriptor *descriptor) {
    
    AudioResource *returnedResource = new AudioResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void AudioResource::InvalidateVideoBasedData() {
    if (!AudioObject)
        return;
}

void AudioResource::RestoreVideoBasedData() {
    if (!AudioObject)
        return;
}