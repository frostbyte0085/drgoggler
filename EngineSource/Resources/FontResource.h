/*
 Author: 
 Pantelis Lekakis
 
 Description:
 The FontResource loads a Bitmap Font xml file created using Angelcode's tool and creates the Font class instance.
 
 */
#ifndef FONT_RESOURCE_H_DEF
#define FONT_RESOURCE_H_DEF

#include "Resource.h"

#define FONT FontResource, FontResourceDescriptor

class Font;

// the descriptor
class FontResourceDescriptor: public ResourceDescriptor {
public:
    FontResourceDescriptor(const String &filename);
    
    String GetAssociatedResourceType();
};

// the actual resource
class FontResource : public Resource {
public:
    static FontResource* LoadResource (FontResourceDescriptor *descriptor);
    
    void InvalidateVideoBasedData();
    void RestoreVideoBasedData();
    
    Font *FontObject;
private:
    FontResource();
    virtual ~FontResource();
    
    void createResource();
    
    FontResourceDescriptor *descriptor;
};

#endif
