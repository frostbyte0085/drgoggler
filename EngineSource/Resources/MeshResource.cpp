#include "MeshResource.h"

#include "Filesystem/File.h"
#include "Filesystem/Filesystem.h"
#include "Utilities/StringOperations.h"
#include "Utilities/Exception.h"

#include "Graphics/Mesh/Mesh.h"
#include <Graphics/Mesh/Animation.h>
#include <Utilities/StringOperations.h>

#include "MaterialResource.h"
#include <Graphics/Material/Material.h>
#include <Graphics/Shader/Shader.h>
#include <Graphics/Texture/Texture.h>
#include "ResourceManager.h"

// the descriptor
MeshResourceDescriptor::MeshResourceDescriptor (const String &filename, bool customMesh) {
    this->filename = filename;
    this->IsCustomMesh = customMesh;
}

String MeshResourceDescriptor::GetAssociatedResourceType() {
    return "Mesh";
}

// the actual resource
MeshResource::MeshResource() {
    MeshObject = new Mesh();
    videoBased = true;
    referenceCounted = false;
}

MeshResource::~MeshResource() {
    if (MeshObject) {
        delete MeshObject;
    }
    MeshObject = nullptr;
    Log ("MeshResource::MeshResource: Success (" + descriptor->GetFilename() + ")");
}

/*
 Useful method to convert the 3dsmax Z up axis system to the opengl Y up axis system.
 Uses the md5mesh cmd line to see in which axis the model should be rotated and by how many degrees.
*/
Matrix44 MeshResource::axisChangeFromCmdLine(String cmdLine) {
    Matrix44 matrix = Matrix44(1.0f);
    
    
    cmdLine = StringOperations::Trim(cmdLine);
    cmdLine = StringOperations::RemoveQuotes(cmdLine);
    
    if (cmdLine != "") {
        Vector<String> rotationAxisStrings;
        // axis conversion
        // check on which axis we need to rotate the model.
        Quaternion finalRotationAxis;
        
        rotationAxisStrings = StringOperations::SplitString(cmdLine, ',');
        for (int i=0; i<(int)rotationAxisStrings.size(); i++) {
            Vector<String> axisAngle = StringOperations::SplitString(rotationAxisStrings[i], ':');
            Quaternion rotation;
            String axis = StringOperations::ToLower(axisAngle[0]);
            int angle = StringOperations::FromString<int>(axisAngle[1]);
            if (axis == "x")
                rotation = glm::rotate(Quaternion(), (float)angle, Vector3(1,0,0));
            else if (axis == "y")
                rotation = glm::rotate(Quaternion(), (float)angle, Vector3(0,1,0));
            else if (axis == "z")
                rotation = glm::rotate(Quaternion(), (float)angle, Vector3(0,0,1));
            
            finalRotationAxis = rotation * finalRotationAxis;
        }
        
        matrix = glm::mat4_cast(finalRotationAxis);
    }

    
    return matrix;
}

void MeshResource::createResourceFromFile() {
    String meshFilename = descriptor->GetFilename();
    String fullPath = Filesystem::GetFullPathForFile(meshFilename);
    String directoryPath = Filesystem::GetDirectoryForFile(meshFilename);
    
    std::ifstream f (fullPath.c_str());
    if (f.is_open()) {
        String param;
        String junk;
        String cmdLine;
        
        long pos = (long)f.tellg();
        f.seekg(0, std::ios::end);
        long length = (long)f.tellg();
        assert (length > 0);
        f.seekg(pos);
        
        f >> param;
        
        int jointCount;
        int submeshCount;
        
        Matrix44 axisConversionMatrix = Matrix44(1.0f);
        
        while (!f.eof()) {
            
            if (param == "MD5Version") {
                f >> MeshObject->md5Version;
            }
            else if (param == "commandline") {
                f >> cmdLine;
                axisConversionMatrix = axisChangeFromCmdLine(cmdLine);
            }
            else if (param == "numJoints") {
                f >> jointCount;
                MeshObject->joints.reserve(jointCount);
                MeshObject->animatedBones.assign (jointCount, Matrix44(1.0f));
                
            }
            else if (param == "numMeshes") {
                f >> submeshCount;
                MeshObject->submeshes.reserve(submeshCount);
            }
            else if (param == "joints") {
                f >> junk; // { character
                
                for (int i=0; i<jointCount; i++) {
                    Mesh::Joint joint;
                    
                    f >> joint.name >> joint.parentID >> junk >>
                    joint.position.x >> joint.position.y >> joint.position.z >> junk >> junk >>
                    joint.orientation.x >> joint.orientation.y >> joint.orientation.z >> junk;
                    
                    joint.name = StringOperations::RemoveQuotes(joint.name);
                    joint.orientation = Math::ComputeQuaternionW (joint.orientation);
                    
                    MeshObject->joints.push_back (joint);
                    
                    f.ignore (length, '\n');
                }
                MeshObject->buildBindPose(axisConversionMatrix);
                f >> junk;
            }
            else if (param == "mesh") {
                Mesh::Submesh *submesh = new Mesh::Submesh();
                int vertexCount, triangleCount, weightCount;
                f >> junk; // { character
                f >> param;
                
                while (param != "}") {
                    if (param == "shader") {
                        String materialFile;
                        f >> materialFile;
                        materialFile = StringOperations::RemoveQuotes(materialFile);
                        materialFile = StringOperations::ToLower(materialFile);
                        
                        if (Filesystem::GetFileExtension(materialFile) == "mat") {
                            String materialFilename = directoryPath + materialFile;
                            
                            // create the material
                            MaterialResource *material = ResourceManager::Create<MATERIAL>(new MaterialResourceDescriptor(materialFilename, false));
                            
                            ownedResources.push_back(material);
                            
                            submesh->material = material->MaterialObject;
                        }
                        f.ignore (length, '\n');
                    }
                    else if (param == "numverts") {
                        f >> vertexCount;
                        f.ignore (length, '\n');
                        
                        for (int i=0; i<vertexCount; i++) {
                            Mesh::MeshVertex vertex;
                            
                            f >> junk >> junk >> junk >> vertex.textureCoordinate0[0] >> vertex.textureCoordinate0[1] >> junk
                            >> vertex.startWeight >> vertex.weightCount;
                            
                            f.ignore (length, '\n');
                            
                            submesh->vertices.push_back (vertex);
                            submesh->texCoordBuffer.push_back (vertex.textureCoordinate0);
                        }
                    }
                    else if (param == "numtris") {
                        f >> triangleCount;
                        f.ignore (length, '\n');
                        
                        for (int i=0; i<triangleCount; i++) {
                            Mesh::Triangle triangle;
                            f >> junk >> junk >> triangle.indices[0] >> triangle.indices[1] >> triangle.indices[2];
                            
                            f.ignore (length, '\n');
                            
                            submesh->triangles.push_back(triangle);
                            submesh->indexBuffer.push_back((GLuint)triangle.indices[0]);
                            submesh->indexBuffer.push_back((GLuint)triangle.indices[1]);
                            submesh->indexBuffer.push_back((GLuint)triangle.indices[2]);
                        }
                    }
                    else if (param == "numweights") {
                        f >> weightCount;
                        f.ignore (length, '\n');
                        
                        for (int i=0; i<weightCount; i++) {
                            Mesh::Weight weight;
                            f >> junk >> junk >> weight.jointID >> weight.bias >> junk >> weight.position.x >>
                            weight.position.y >> weight.position.z >> junk;
                            
                            f.ignore(length, '\n');
                            submesh->weights.push_back(weight);
                        }
                    }
                    else {
                        f.ignore(length, '\n');
                    }
                    f >> param;
                }
                
                MeshObject->submeshes.push_back(submesh);
            }
            
            f >> param;
        }
        
        f.close();
        
        MeshObject->Build();

        
        // see if we have animations
        parseAnimations();
        
        Log ("MeshResource::createResource: Success (" + meshFilename + ")");
        
        return;
    }
    Log ("MeshResource::createResource: Failed (" + meshFilename + ": file not found)");
}

void MeshResource::createResourceCustom() {
    
}

void MeshResource::createResource() {
    assert (MeshObject);
    
    if (descriptor->IsCustomMesh)
        createResourceCustom();
    else
        createResourceFromFile();
}

void MeshResource::parseAnimations() {
    // enumerate the md5anim files in the mesh's directory and load them
    Vector<String> animationFilenames;
    String meshDirectory = Filesystem::GetDirectoryForFile(descriptor->GetFilename());
    Filesystem::EnumerateFiles(meshDirectory, "md5anim", animationFilenames);

    for (int i=0; i<(int)animationFilenames.size(); i++) {
        String name = animationFilenames[i].substr(0, animationFilenames[i].find_first_of("."));
        String animationFilename = meshDirectory + animationFilenames[i];
        String fullAnimationPath = Filesystem::GetFullPathForFile(animationFilename);
        
        parseSingleAnimation (fullAnimationPath, name);
    }
}

void MeshResource::parseSingleAnimation (const String &filename, const String &name) {
    String param;
    String junk;   // Read junk from the file
    String cmdLine;
    
    std::ifstream file (filename.c_str());
    if (!file.is_open()) {
        
    }
    long pos = (long)file.tellg();
    file.seekg(0, std::ios::end);
    long length = (long)file.tellg();
    assert (length > 0);
    file.seekg(pos);
    
    MeshAnimation *anim = new MeshAnimation();
    
    file >> param;
    
    int frameCount=0;
    int jointCount=0;
    
    Matrix44 axisMatrixConversion = Matrix44(1.0f);
    
    while( !file.eof() )
    {
        if ( param == "MD5Version" )
        {
            file >> anim->md5Version;
            assert( anim->md5Version == 10 );
        }
        else if ( param == "commandline" )
        {
            file >> cmdLine;
            axisMatrixConversion = axisChangeFromCmdLine(cmdLine);
        }
        else if ( param == "numFrames" )
        {
            file >> frameCount;
            file.ignore(length, '\n');
        }
        else if ( param == "numJoints" )
        {
            file >> jointCount;
            file.ignore(length, '\n');
        }
        else if ( param == "frameRate" )
        {
            file >> anim->frameRate;
            file.ignore(length, '\n');
        }
        else if ( param == "numAnimatedComponents" )
        {
            file >> anim->animatedComponentCount;
            file.ignore(length, '\n');
        }
        else if ( param == "hierarchy" )
        {
            file >> junk; // read in the '{' character
            for ( int i = 0; i < jointCount; i++ )
            {
                MeshAnimation::JointInfo joint;
                file >> joint.name >> joint.parentID >> joint.flags >> joint.startIndex;
                joint.name = StringOperations::RemoveQuotes( joint.name );
                
                anim->jointInfos.push_back(joint);
                
                file.ignore( length, '\n' );
            }
            file >> junk; // read in the '}' character
        }
        else if ( param == "bounds" )
        {
            file >> junk; // read in the '{' character
            file.ignore( length, '\n' );
            for ( int i = 0; i < frameCount; i++ ) 
            {
                MeshAnimation::Bound bound;
                file >> junk; // read in the '(' character
                file >> bound.min.x >> bound.min.y >> bound.min.z;
                file >> junk >> junk; // read in the ')' and '(' characters.
                file >> bound.max.x >> bound.max.y >> bound.max.z;
                
                anim->bounds.push_back(bound);
                
                file.ignore( length, '\n' );
            }
            
            file >> junk; // read in the '}' character
            file.ignore( length, '\n' );
        }
        else if ( param == "baseframe" )
        {
            file >> junk; // read in the '{' character
            file.ignore( length, '\n' );
            
            
            for ( int i = 0; i < jointCount; i++ )
            {
                MeshAnimation::BaseFrame baseFrame;
                file >> junk;
                file >> baseFrame.position.x >> baseFrame.position.y >> baseFrame.position.z;
                file >> junk >> junk;
                file >> baseFrame.orientation.x >> baseFrame.orientation.y >> baseFrame.orientation.z;
                file.ignore( length, '\n' );
                
                anim->baseFrames.push_back(baseFrame);
            }
            
            file >> junk; // read in the '}' character
            file.ignore( length, '\n' );
        }
        else if ( param == "frame" )
        {
            MeshAnimation::FrameData frame;
            file >> frame.frameID >> junk; // Read in the '{' character
            file.ignore(length, '\n' );
            
            for ( int i = 0; i < anim->animatedComponentCount; i++ )
            {
                float frameData;
                file >> frameData;
                
                frame.frameData.push_back(frameData);
            }
            
            anim->frames.push_back(frame);
            
            // Build a skeleton for this frame
            anim->buildFrameSkeleton(axisMatrixConversion, anim->skeletons, anim->jointInfos, anim->baseFrames, frame );
            
            file >> junk; // Read in the '}' character
            file.ignore(length, '\n' );        
        }
        
        file >> param;
    }
    
    // Make sure there are enough joints for the animated skeleton.
    anim->animatedSkeleton.joints.assign(jointCount, MeshAnimation::SkeletonJoint() );
    anim->animatedSkeleton.boneMatrices.assign( jointCount, Matrix44(1.0) );
    
    anim->frameDuration = 1.0f / (float)anim->frameRate;
    anim->animationDuration = ( anim->frameDuration * (float)frameCount );
    anim->animationTime = 0.0f;
    
    if (MeshObject->isAnimationCompatible(*anim)) {
        MeshObject->animations[name] = anim;
    }
    
}

MeshResource* MeshResource::LoadResource (MeshResourceDescriptor *descriptor) {
    
    MeshResource *returnedResource = new MeshResource();
    returnedResource->descriptor = descriptor;
    returnedResource->createResource();
    
    return returnedResource;
}

void MeshResource::InvalidateVideoBasedData() {
    if (!MeshObject)
        return;
    
    MeshObject->destroyVideoBasedData();
}

void MeshResource::RestoreVideoBasedData() {
    if (!MeshObject)
        return;
    
    MeshObject->restoreVideoBasedData();
}