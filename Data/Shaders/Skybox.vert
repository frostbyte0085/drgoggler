#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

void main()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[0].xyz = normalize(gl_Vertex.xyz);

    mat4 modelOrientation = mat4(mat3(modelMatrix));
    mat4 viewOrientation = mat4(mat3(viewMatrix));
    mat4 mvp = projectionMatrix * viewOrientation * modelOrientation;

    gl_Position = mvp * gl_Vertex;
}
