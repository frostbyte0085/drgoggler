#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

uniform mat4 boneMatrix[12];

uniform float currentTime;

varying float time;

varying vec4 lightDiffuse, lightAmbient;
varying vec3 lightDir, normal, halfVector;

void doLight(mat4 pmv) {
    mat3 normalMatrix = mat3(modelMatrix * viewMatrix);
    normalMatrix = -transpose(normalMatrix);
    normal = normalMatrix * gl_Normal.xyz;
    
    lightDir = normalize (gl_LightSource[0].position.xyz);
    
    halfVector = normalize(gl_LightSource[0].halfVector.xyz);
    
    lightDiffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
    lightAmbient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
    lightAmbient += gl_LightModel.ambient * gl_FrontMaterial.ambient;
}

void main()
{
    time = currentTime;
    
    gl_TexCoord[0] = gl_MultiTexCoord0;
    
    mat4 pmv = projectionMatrix * viewMatrix * modelMatrix;
    
    vec4 weight0 = gl_MultiTexCoord1;
    vec4 matrixIndex0 = gl_MultiTexCoord2;
    
    vec4 position = vec4(0,0,0,0);
    
    for (int i=0; i<4; i++) {
        position += boneMatrix[int(matrixIndex0[i])] * gl_Vertex * weight0[i];
    }
    
    gl_Position = pmv * position;
    
    doLight(pmv);
}