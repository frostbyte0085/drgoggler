#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

void main()
{
    mat4 pmv = projectionMatrix * viewMatrix * modelMatrix;
    gl_TexCoord[0] = gl_MultiTexCoord0;
	
    gl_Position = pmv * gl_Vertex;
}
