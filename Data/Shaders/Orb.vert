#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

uniform float currentTime;
varying float time;

void main()
{
    gl_TexCoord[0] = gl_MultiTexCoord0;
    
    time = currentTime;

    mat4 pmv = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = pmv * gl_Vertex;
}
