#version 120

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

uniform float currentTime;

varying float varCurrentTime;

void main()
{
    mat4 pmv = projectionMatrix * viewMatrix * modelMatrix;

    varCurrentTime = currentTime;
    float timeModifier = varCurrentTime * 0.0035;
    
    vec2 plane1Repeat = vec2(5,5);
    vec2 plane2Repeat = vec2(4,4);
    vec2 plane3Repeat = vec2(2,2);
    vec2 plane4Repeat = vec2(2,2);
    
    vec2 plane1Scroll = vec2(1,0);
    vec2 plane2Scroll = vec2(-1,0);
    vec2 plane3Scroll = vec2(0,1);
    vec2 plane4Scroll = vec2(0,-1);

    
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[1] = gl_MultiTexCoord0;
    gl_TexCoord[2] = gl_MultiTexCoord0;
    gl_TexCoord[3] = gl_MultiTexCoord0;
    gl_TexCoord[4] = gl_MultiTexCoord0;
    gl_TexCoord[5] = gl_MultiTexCoord0;
    gl_TexCoord[6] = gl_MultiTexCoord0;
    gl_TexCoord[7] = gl_MultiTexCoord0;
    
    vec2 texcoord1 = vec2(gl_TexCoord[0].x * plane1Repeat.x + plane1Scroll.x*(timeModifier), gl_TexCoord[0].y * plane1Repeat.y + plane1Scroll.y*(timeModifier));
    gl_TexCoord[0] = vec4(texcoord1.xy, 0, 0);

    vec2 texcoord2 = vec2(gl_TexCoord[1].x * plane2Repeat.x + plane2Scroll.x*(timeModifier), gl_TexCoord[1].y * plane2Repeat.y + plane2Scroll.y*(timeModifier));
    gl_TexCoord[1] = vec4(texcoord2.xy, 0, 0);
    
    vec2 texcoord3 = vec2(gl_TexCoord[2].x * plane3Repeat.x + plane3Scroll.x*(timeModifier), gl_TexCoord[2].y * plane3Repeat.y + plane3Scroll.y*(timeModifier));
    gl_TexCoord[2] = vec4(texcoord3.xy, 0, 0);
    
    vec2 texcoord4 = vec2(gl_TexCoord[3].x * plane4Repeat.x + plane4Scroll.x*(timeModifier), gl_TexCoord[3].y * plane4Repeat.y + plane4Scroll.y*(timeModifier));
    gl_TexCoord[3] = vec4(texcoord4.xy, 0, 0);
    
    
    timeModifier *= 2.0;
    
    vec2 texcoord5 = vec2(gl_TexCoord[4].x * plane1Repeat.x + plane1Scroll.x*(timeModifier), gl_TexCoord[4].y * plane1Repeat.y - plane1Scroll.y*(timeModifier));
    gl_TexCoord[4] = vec4(texcoord5.xy, 0, 0);
    
    vec2 texcoord6 = vec2(gl_TexCoord[5].x * plane2Repeat.x - plane2Scroll.x*(timeModifier), gl_TexCoord[5].y * plane2Repeat.y + plane2Scroll.y*(timeModifier));
    gl_TexCoord[5] = vec4(texcoord6.xy, 0, 0);
    
    vec2 texcoord7 = vec2(gl_TexCoord[6].x * plane3Repeat.x + plane3Scroll.x*(timeModifier), gl_TexCoord[6].y * plane3Repeat.y - plane3Scroll.y*(timeModifier));
    gl_TexCoord[6] = vec4(texcoord7.xy, 0, 0);
    
    vec2 texcoord8 = vec2(gl_TexCoord[7].x * plane4Repeat.x - plane4Scroll.x*(timeModifier), gl_TexCoord[7].y * plane4Repeat.y + plane4Scroll.y*(timeModifier));
    gl_TexCoord[7] = vec4(texcoord8.xy, 0, 0);

    
    gl_Position = pmv * gl_Vertex;
}
