#version 120

uniform sampler2D diffuseTexture;

varying float time;

vec4 colorRed = vec4(1,0.1f,0,0);
vec4 colorBlue = vec4(0.2f,0.5f,0.9f,0);

varying vec4 lightDiffuse, lightAmbient;
varying vec3 normal, lightDir, halfVector;

vec4 doLight() {
    vec3 n, halfV;
    float NdotL, NdotHV;
    
    vec4 color = lightAmbient;
    
    n = normalize(normal);
    
    NdotL = max(dot(n, lightDir), 0.0);
    
    if (NdotL > 0.0f) {
        halfV = normalize(halfVector);
        
        NdotHV = max(dot(n, halfV), 0.0);
        
        color += gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV, gl_FrontMaterial.shininess);
        color += lightDiffuse * NdotL;
    }
    
    return color;
}

void main(void)
{
    vec4 litColor = doLight();
    
    vec4 color = texture2D (diffuseTexture, gl_TexCoord[0].xy);
    if (color.a < 0.1f) {
        color = mix (colorRed, colorBlue, sin(time*3));
    }
    
	gl_FragColor = color * litColor;
}