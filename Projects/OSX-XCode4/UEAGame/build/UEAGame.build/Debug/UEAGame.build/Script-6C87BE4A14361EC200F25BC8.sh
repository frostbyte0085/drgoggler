#!/bin/sh
cp -R -f "$TARGET_BUILD_DIR/$PRODUCT_NAME.app" ../../../Binaries/"$PRODUCT_NAME.app"
install_name_tool -change ./libfmodex.dylib @executable_path/libfmodex.dylib "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/MacOS/$PRODUCT_NAME"
install_name_tool -change ./libfmodexL.dylib @executable_path/libfmodexL.dylib "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/MacOS/$PRODUCT_NAME"
