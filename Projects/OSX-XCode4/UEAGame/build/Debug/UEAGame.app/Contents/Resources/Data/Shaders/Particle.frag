#version 120

uniform sampler2D diffuseTexture;

void main(void)
{
    vec4 color = texture2D (diffuseTexture, gl_TexCoord[0].xy) * gl_Color;
	gl_FragColor = color;
}