#version 120

uniform mat4 projectionMatrix;

void main()
{
    mat4 viewMatrix = mat4(1.0);
    mat4 modelMatrix = mat4(1.0);
    
    mat4 pmv = projectionMatrix * viewMatrix * modelMatrix;
    gl_TexCoord[0] = gl_MultiTexCoord0;
	
    gl_Position = pmv * gl_Vertex;
}
