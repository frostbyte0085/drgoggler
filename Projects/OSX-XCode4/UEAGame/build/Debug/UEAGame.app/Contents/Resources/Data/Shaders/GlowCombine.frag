#version 120

uniform sampler2D sceneTexture;
uniform sampler2D blurTexture;
uniform float rtWidth;
uniform float rtHeight;

vec4 borderTexture2D(sampler2D sampler, vec2 uv) {
	if (uv.x < 0 || uv.x > 1 || uv.y < 0 || uv.y > 1) return vec4(0, 0, 0, 0);
	return texture2D(sampler, uv);
}

void main(void)
{
    vec2 texelSize = vec2(1.0 / rtWidth, 1.0 / rtHeight);
    
    vec2 texCoord1 = gl_TexCoord[0].xy;
    vec2 texCoord2 = gl_TexCoord[0].xy;
    texCoord2.x -= texelSize.x;
    
    vec4 color = texture2D (sceneTexture, texCoord1)*0.1 + 20 * borderTexture2D(blurTexture, texCoord2);
	
    gl_FragColor = color;
}