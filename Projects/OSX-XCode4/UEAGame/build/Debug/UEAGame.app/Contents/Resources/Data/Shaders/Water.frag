#version 120

uniform sampler2D waterTexture;


varying float varCurrentTime;


vec4 toGreyscale (vec4 color) {    
    vec4 c = color;
    
    float grey = 0.21 * c.r + 0.71 * c.g + 0.07 * c.b;
    
    return vec4(grey, grey, grey, 1);
}

void main(void) {
    
    float timeModifier = varCurrentTime * 0.15f;
    
    vec4 color1 = texture2D (waterTexture, gl_TexCoord[0].xy);
    vec4 color2 = texture2D (waterTexture, gl_TexCoord[1].xy + color1.xy * 0.5 * timeModifier);
    vec4 color3 = texture2D (waterTexture, gl_TexCoord[2].xy + color2.xy * 0.2 * timeModifier - color1.xy * 0.1 * timeModifier);
    vec4 color4 = texture2D (waterTexture, gl_TexCoord[3].xy + color3.xy * 0.1 * timeModifier - color2.xy * 0.1 * timeModifier);
    
    color2 = toGreyscale(color2);
    color3 = toGreyscale(color3);
    color4 = toGreyscale(color4);
    
    
    vec4 color5 = texture2D (waterTexture, gl_TexCoord[4].xy);
    vec4 color6 = texture2D (waterTexture, gl_TexCoord[5].xy + color5.xy * 0.5 * timeModifier);
    vec4 color7 = texture2D (waterTexture, gl_TexCoord[6].xy + color6.xy * 0.2 * timeModifier - color5.xy * 0.1 * timeModifier);
    vec4 color8 = texture2D (waterTexture, gl_TexCoord[7].xy + color7.xy * 0.1 * timeModifier - color6.xy * 0.1 * timeModifier);
    
    
    color5 = toGreyscale(color5);
    color6 = toGreyscale(color6);
    color7 = toGreyscale(color7);
    //color8 = toGreyscale(color8);
    
    
    vec4 finalColor1 = color1 + color2 + color3 * color4;
    vec4 finalColor2 = color5 + color6 + color7 * color8;
        
    vec4 finalColor = finalColor1 + finalColor2;
    
    finalColor.r = pow (finalColor.r, 2);
    finalColor.g = pow (finalColor.g, 2);
    finalColor.b = pow (finalColor.b, 2);
    
    gl_FragColor = finalColor * 0.23;
}