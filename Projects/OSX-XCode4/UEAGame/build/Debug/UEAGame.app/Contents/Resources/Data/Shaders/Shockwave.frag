#version 120

uniform sampler2D diffuseTexture;

void main(void)
{   
    vec4 litColor = gl_FrontMaterial.diffuse;
    
    vec4 color = texture2D (diffuseTexture, gl_TexCoord[0].xy);
	gl_FragColor = color * litColor;
}