#version 120

uniform sampler2D uiTexture;
uniform vec4 uiColor;

void main(void)
{
    vec4 color = texture2D (uiTexture, gl_TexCoord[0].xy);
    
    gl_FragColor = color * uiColor;
}