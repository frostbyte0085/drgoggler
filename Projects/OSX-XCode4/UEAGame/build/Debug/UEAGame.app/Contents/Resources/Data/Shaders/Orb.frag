#version 120

uniform sampler2D diffuseTexture;

varying float time;

vec4 white = vec4(0, 0, 0.2, 1);

void main(void) {
     
    vec4 color = texture2D (diffuseTexture, gl_TexCoord[0].xy);
    color.r = color.r * 0.7f;
    color.b = color.b * 0.7f;
    color.g = color.g * 0.7f;
    
    gl_FragColor = mix(color, white, sin(time*17.5f));
}