#import <Cocoa/Cocoa.h>
#include <GameApplication.h>

int main(int argc, char *argv[])
{
    PHYSFS_init(argv[0]);
    
    GameApplication app;
    return app.Run();
}
