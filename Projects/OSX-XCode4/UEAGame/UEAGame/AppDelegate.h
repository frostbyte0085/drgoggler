//
//  AppDelegate.h
//  UEAGame
//
//  Created by Alkis Lekakis on 9/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    
    //NSWindow *_window;
}

@property (assign) IBOutlet NSWindow *window;

@end
